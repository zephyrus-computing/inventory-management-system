# Inventory Management System
IMS is a simple django web application for managing inventory for small and medium-sized businesses. It is built around the concept of Parts (i.e., components, elements, items, et cetera), having some sort of SKU or ID, and Storage (i.e., drawer, container, workspace, room, building, et cetera), a place of holding, to help keep track of the quantity of inventory. In addition, Assemblies are a completed summation of Parts, Kits are a combination of Parts independently tracked, and Alternate SKUs as an extension for tracking external entities identification of a Part.

![List view of Parts](docs/ims-part-list-view-3.2.PNG)

![Detailed view of a Part](docs/ims-part-detail-view-3.2.PNG)

![List view of Storage](docs/ims-storage-list-view-3.2.PNG)

![Detailed view of a Storage](docs/ims-storage-detail-view-3.2.PNG)

![List view of Hierarchy](docs/ims-hierarchy-list-view-3.2.PNG)

![Detailed view of a Hierarchy](docs/ims-hierarchy-detail-view-3.2.PNG)

![List view of Assembly](docs/ims-assembly-list-view-3.2.PNG)

![Detailed view of an Assembly](docs/ims-assembly-detail-view-3.2.PNG)

![List view of Kit](docs/ims-kit-list-view-3.2.PNG)

![Detailed view of a kit](docs/ims-kit-detail-view-3.2.PNG)

![List view of Alternate SKU](docs/ims-altsku-list-view-3.2.PNG)

![Search view](docs/ims-search-view-3.2.PNG)


## Installation
Quick installation instructions can be found in the [Quick Install](https://gitlab.com/zephyrus-computing/inventory-management-system/-/wikis/Quick-Install) Wiki section.

Detailed instructions can be found in the [Admin Guide](https://gitlab.com/zephyrus-computing/inventory-management-system/-/wikis/admin-guide) Wiki section.

The Debian package can be downloaded from the [Package Registry](https://gitlab.com/zephyrus-computing/inventory-management-system/-/packages).

## Roadmap
- Improvements to Email setup and configuration

- Security patching as necessary

- Most additional feature sets will be created as separate projects to be optional add-on packages

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

### Development Environment Setup
```
git clone https://gitlab.com/zephyrus-computing/inventory-management-system.git ims
cd ims/
./dev.sh setup
source venv/bin/activate
```
The dev.sh script with the setup option will configure a python virtual environment and install the packages specified in requirements.txt, create a dev db, and execute the superuser creation process.

When you have finished your work, you can run ```deactivate``` to close the environment.

If you need to reset your environment, run the following:
```
./dev.sh delete
./dev.sh setup
```

## License
[GPL3](http://choosealicense.com/licenses/gpl-3.0/) 
