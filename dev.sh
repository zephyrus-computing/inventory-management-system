#! /bin/bash
list=(setup delete help)
if [[ ! " ${list[*]} " =~ " ${1} " ]]; then
    echo "Invalid option."
    echo "$0 [${list[*]}] [-v|--verbose]"
    exit
fi
[[ $2 == "-v" || $2 == "--verbose" ]] && verbose=1
if [ $1 == "help" ]; then
    echo "$0 help	This helpful text"
    echo "$0 setup	Create the virtual environment for development"
    echo "$0 delete	Delete the created virtual environment"
    echo ""
fi
folder=$(dirname $0)
if [ $1 == "setup" ]; then
    [ $verbose ] && echo -e "\e[32mChecking for virtualenv\e[39m"
    if [[ $(which virtualenv) == "" ]]; then
        [ $verbose ] && echo -e "\e[32mChecking OS...\e[39m"
        release=$(lsb_release -i)
        if [[ "$release" =~ "Ubuntu" ]]; then
            [ $verbose ] && echo -e "\e[32mDetected Ubuntu\e[39m"
            sudo apt -y install virtualenv
        elif [[ "$release" =~ "Debian" ]]; then
            [ $verbose ] && echo -e "\e[32mDetected Debian\e[39m"
            su root -c apt -y install virtualenv
        else
            echo -e "\e[31mUnknown OS: $release\nPlease contact support for assistance.\e[39m"
            exit
        fi
    fi
    [ $verbose ] && echo -e "\e[32mCreating Virtual Environment\e[39m"
    virtualenv "$folder/venv"
    source "$folder/venv/bin/activate"
    [ $verbose ] && echo -e "\e[32mUpdating pip, setuptools, and wheel\e[39m"
    pip install -U pip setuptools wheel
    [ $verbose ] && echo -e "\e[32mInstall required packages\e[39m"
    pip install -r "$folder/requirements.txt"
    export DJANGO_DEVENV=True
    export DJANGO_DEBUG=True
    [ $verbose ] && echo -e "\e[32mIntializing Database\e[39m"
    python "$folder/ims/manage.py" migrate
    [ $verbose ] && echo -e "\e[32mCreating Superuser Account. Please follow the prompts.\e[39m"
    python "$folder/ims/manage.py" createsuperuser
    [ $verbose ] && echo -e "\e[32mGenerating local .env file\e[39m"
    python "$folder/ims/scripts/generate_env.py"
    deactivate
    [ $verbose ] && echo -e "\e[32mSetup complete. Run 'source venv/bin/activate' to begin working.\e[39m"
fi
if [ $1 == "delete" ]; then
    rm -rf "$folder/venv"
    [ $verbose ] && echo -e "\e[32mDeletion Complete...\e[39m"
fi
