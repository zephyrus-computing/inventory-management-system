#!/usr/bin/env python

from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType

readonlyPermissions = ['Can view alternate sku','Can view assembly','Can view part','Can view part alternate sku','Can view part storage','Can view storage','Can view kit','Can view kit part storage']
operatorPermissions = ['Can increment count part storage','Can decrement count part storage','Can increment count kit','Can decrement count kit']
managerPermissions = ['Can add alternate sku','Can change alternate sku','Can add assembly','Can change assembly','Can add part','Can change part','Can add part alternate sku','Can change part alternate sku','Can add part storage','Can change part storage','Can add storage','Can change storage','Can add kit','Can change kit','Can add kit part storage','Can change kit part storage']
adminPermissions = ['Can delete alternate sku','Can delete assembly','Can delete part','Can delete part alternate sku','Can delete part storage','Can delete storage','Can delete kit','Can delete kit part storage']
auditorPermissions = ['Can view log']
accountPermissions = Permission.objects.filter(content_type__in=ContentType.objects.filter(app_label='accounts'))
accountPermissions2 = Permission.objects.filter(content_type__in=ContentType.objects.filter(app_label='auth',model__in=['user','group']))

invReadonly = Group.objects.get_or_create(name='Inventory ReadOnly')[0]
invOperator = Group.objects.get_or_create(name='Inventory Operator')[0]
invManager = Group.objects.get_or_create(name='Inventory Manager')[0]
invAdmin = Group.objects.get_or_create(name='Inventory Admin')[0]
acctAdmin = Group.objects.get_or_create(name='Account Admin')[0]
logAuditor = Group.objects.get_or_create(name='Log Auditor')[0]

for i in range(0,len(readonlyPermissions)):
    p = Permission.objects.get(name=readonlyPermissions[i])
    invReadonly.permissions.add(p)
    invOperator.permissions.add(p)
    invManager.permissions.add(p)
    invAdmin.permissions.add(p)

for i in range(0, len(operatorPermissions)):
    p = Permission.objects.get(name=operatorPermissions[i])
    invOperator.permissions.add(p)
    invManager.permissions.add(p)
    invAdmin.permissions.add(p)

for i in range(0,len(managerPermissions)):
    p = Permission.objects.get(name=managerPermissions[i])
    invManager.permissions.add(p)
    invAdmin.permissions.add(p)

for i in range(0,len(adminPermissions)):
    p = Permission.objects.get(name=adminPermissions[i])
    invAdmin.permissions.add(p)

for perm in accountPermissions:
    acctAdmin.permissions.add(perm)

for perm in accountPermissions2:
    acctAdmin.permissions.add(perm)

for i in range(0,len(auditorPermissions)):
    p = Permission.objects.get(name=auditorPermissions[i])
    logAuditor.permissions.add(p)

invReadonly.save()
invManager.save()
invAdmin.save()
logAuditor.save()
