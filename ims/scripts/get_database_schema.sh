#! /bin/bash
set -e;
# Ensure that the user executing this script is 'imsuser'
if [ "$USER" != "imsuser" ]; then
    echo "This script needs to be run as imsuser!";
    exit;
fi
folder=$(dirname $0); # Should be /etc/zc/ims/scripts
parentfolder=$(dirname $folder); # Should be /etc/zc/ims
# Find the .env file
if [[ -f "$parentfolder/.env" ]]; then
    envfile="$parentfolder/.env";
else
    echo "ERROR: IMS .env File Not Found!";
    exit 1;
fi
EXISTING=$(grep -e 'IMS_DATABASE' $envfile);
SQLITE=$(echo "$EXISTING" | grep sqlite | wc -l);
MARIA=$(echo "$EXISTING" | grep mysql | wc -l);
POSTGRES=$(echo "$EXISTING" | grep postgresql | wc -l);
if [ "$SQLITE" -gt "0" ]; then
    sqlite3 /etc/zc/ims/db.sqlite3 ".schema %_%" > /var/log/zc/ims/db_schema.txt;
else
    for i in $(seq 1 10); do
        [[ $(echo "$EXISTING" | cut -d',' -f$i) == '' ]] && break;
        if [[ $(echo "$EXISTING" | cut -d',' -f$i | cut -d'=' -f1) == 'PASSWORD' ]]; then
            userpass=$(echo "$EXISTING" | cut -d',' -f$i | cut -d'=' -f2);
        fi
        if [[ $(echo "$EXISTING" | cut -d',' -f$i | cut -d'=' -f1) == 'USER' ]]; then
            username=$(echo "$EXISTING" | cut -d',' -f$i | cut -d'=' -f2);
        fi
        if [[ $(echo "$EXISTING" | cut -d',' -f$i | cut -d'=' -f1) == 'NAME' ]]; then
            name=$(echo "$EXISTING" | cut -d',' -f$i | cut -d'=' -f2);
        fi
    done;
    if [ "$POSTGRES" -gt "0" ]; then
        psql -d $name -c "select table_name,column_name,udt_name from information_schema.columns where table_catalog = '$name' order by table_name;" > /var/log/zc/ims/db_schema.txt;
    fi
    if [ "$MARIA" -gt "0" ]; then
        mysql -u $username -p $userpass -e "select table_name,column_name,data_type from information_schema.columns where table_schema = '$name' order by table_name;" > /var/log/zc/ims/db_schema.txt;
    fi
fi
