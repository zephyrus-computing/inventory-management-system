#! /bin/bash
# Ensure that the user executing this script is 'imsuser'
if [ "$USER" != "imsuser" ]; then
    echo "This script needs to be run as imsuser!";
    exit;
fi
folder=$(dirname $0); # Should be /etc/zc/ims/scripts
parentfolder=$(dirname $folder);
# Find the .env file
if [[ -f "$parentfolder/.env" ]]; then
    envfile="$parentfolder/.env";
else
    echo "ERROR: IMS .env File Not Found!";
    exit 1;
fi
if [[ ! $(grep -e "LOG_DIR" $envfile) == "" ]]; then
    logfile=$(echo $(grep -e "LOG_DIR" $envfile | cut -d'=' -f2 | sed -e "s/\/$//" -e "s/'//g")"/install.log");
else
    logfile="/var/log/zc/ims/install.log";
fi
if [[ ! -f "$folder/get_stale_relationships.py" ]]; then
    echo "ERROR: Missing files";
    exit 1;
fi
if [[ ! -d $(dirname $logfile) ]]; then
    [[ -d "$parentfolder/logs/" ]] && logfile="$parentfolder/logs/install.log" || logfile="$folder/install.log";
fi
echo "$(date)" >> $logfile;
echo "Searching the database for Stale Relationships..." >> $logfile;
su -c "source $parentfolder/venv/bin/activate; python $parentfolder/manage.py shell --command "$(cat scripts/get_stale_relationships.py)"; deactivate;" imsuser | tee -a $logfile;
