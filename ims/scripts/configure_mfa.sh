#! /bin/bash
folder=$(dirname $(readlink -f $0)); # Should be /etc/zc/ims/scripts
parentfolder=$(dirname $folder); # Should be /etc/zc/ims
# Find the .env file
if [[ -f "$parentfolder/.env" ]]; then
    envfile="$parentfolder/.env";
else
    echo "ERROR: IMS .env File Not Found!";
    exit 1;
fi
if [[ ! $(grep -e "LOG_DIR" $envfile) == "" ]]; then
    logfile=$(echo $(grep -e "LOG_DIR" $envfile | cut -d'=' -f2 | sed -e "s/\/$//" -e "s/'//g")"/install.log");
else
    logfile="/var/log/zc/ims/install.log";
fi
if [[ ! -d $(dirname $logfile) ]]; then
    logfile="$parentfolder/install.log";
fi
if [ ! $3 ]; then
    echo "Provide the domain name and site title to register for the MFA tokens:";
    read -p "domain: " domain;
    read -p "site title (IMS): " title;
    echo "";
    read -p "Enforce MFA for all accounts?(Y/n): " enforce;
    if [[ $title == "" ]]; then title='IMS'; fi
    if [[ $enforce == "" ]]; then enforce=Y; fi
else
    domain=$1;
    title=$2;
    enforce=$3;
fi
echo "$(date)" >> $logfile;
echo "Configuring MFA settings ($envfile)..." >> $logfile;
if [[ ! $(grep -e "MFA_ENABLED" $envfile) == "" ]]; then
    echo "$(grep -e "MFA_" $envfile)" >> $logfile;
fi
if [[ ${enforce,,} == "y" ]]; then 
    enforce='True';
    if [[ ! $(grep -e "MIDDLEWARE" "$envfile") == "" ]]; then
        existing=$(grep 'MIDDLEWARE' $envfile);
        existing=${existing:12:-1};
        IFS=',' read -ra MIDDLEWARE <<EOF
$existing
EOF
        OTHER_MIDS="";
        for mid in "${MIDDLEWARE[@]}"; do
            if [ ! $(echo $mid | grep 'mfa.middleware.MFAEnforceMiddleware') ]; then
                OTHER_MIDS+="$mid,";
            fi
        done
        if [ "$OTHER_MIDS" != "" ]; then
            OTHER_MIDS=${OTHER_MIDS:0:-1};
            sed -i -e "s/MIDDLEWARE=.*/MIDDLEWARE=\"${OTHER_MIDS},mfa.middleware.MFAEnforceMiddleware\"/" $envfile;
        else
            sed -i -e "s/MIDDLEWARE=.*/MIDDLEWARE=\"mfa.middleware.MFAEnforceMiddleware\"/" $envfile;
        fi
    else
        sed -i -e "s/# Security Settings/# Security Settings\nMIDDLEWARE=\"mfa.middleware.MFAEnforceMiddleware\"/" $envfile;
    fi
else 
    enforce='False';
    if [[ ! $(grep -e "mfa.middleware.MFAEnforceMiddleware" "$envfile") == "" ]]; then
        existing=$(grep 'MIDDLEWARE' $envfile);
        existing=${existing:12:-1};
        IFS=',' read -ra MIDDLEWARE <<EOF
$existing
EOF
        OTHER_MIDS="";
        for mid in "${MIDDLEWARE[@]}"; do
            if [ ! $(echo $mid | grep 'mfa.middleware.MFAEnforceMiddleware') ]; then
                OTHER_MIDS+="$mid,";
            fi
        done
        if [ "$OTHER_MIDS" != "" ]; then
            OTHER_MIDS=${OTHER_MIDS:0:-1};
            sed -i -e "s/MIDDLEWARE=.*/MIDDLEWARE=\"${OTHER_MIDS}\"/" $envfile;
        else
            sed -i -e "s/MIDDLEWARE=.*/MIDDLEWARE=\"\"/" $envfile;
        fi
    fi
fi
if [[ ! $(grep -e "# MFA Settings" "$envfile") ]]; then
    cp $envfile $envfile.tmp;
    awk '/MFA_/{next;}1' $envfile.tmp > $envfile;
    rm -f $envfile.tmp;
    echo -e "# MFA Settings\nMFA_ENABLED=True\nMFA_DOMAIN=$domain\nMFA_SITE_TITLE=$title\nMFA_ENFORCE=$enforce\n###\n" >> $envfile;
else
    if [[ ! $(grep -e "MFA_ENFORCE" "$envfile") == "" ]]; then
        sed -i -e "s/MFA_ENFORCE=.*$/MFA_ENFORCE=$enforce/" $envfile;
    else
        sed -i -e "s/# MFA Settings/# MFA Settings\nMFA_ENFORCE=$enforce/" $envfile;
    fi
    if [[ ! $(grep -e "MFA_SITE_TITLE" "$envfile") == "" ]]; then
        sed -i -e "s/MFA_SITE_TITLE=.*$/MFA_SITE_TITLE=$title/" $envfile;
    else
        sed -i -e "s/# MFA Settings/# MFA Settings\nMFA_SITE_TITLE=$title/" $envfile;
    fi
    if [[ ! $(grep -e "MFA_DOMAIN" "$envfile") == "" ]]; then
        sed -i -e "s/MFA_DOMAIN=.*$/MFA_DOMAIN=$domain/" $envfile;
    else
        sed -i -e "s/# MFA Settings/# MFA Settings\nMFA_DOMAIN=$domain/" $envfile;
    fi
    if [[ ! $(grep -e "MFA_ENABLED" "$envfile") == "" ]]; then
        sed -i -e "s/MFA_ENABLED=.*$/MFA_ENABLED=True/" $envfile;
    else
        sed -i -e "s/# MFA Settings/# MFA Settings\nMFA_ENABLED=True/" $envfile;
    fi
fi
echo "Restart the IMS service to apply the new settings"
