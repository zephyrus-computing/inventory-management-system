#! /bin/bash
folder=$(dirname $(readlink -f $0)); # Should be /etc/zc/ims/scripts
parentfolder=$(dirname $folder); # Should be /etc/zc/ims
# Find the .env file
if [[ -f "$parentfolder/.env" ]]; then
    envfile="$parentfolder/.env";
else
    echo "ERROR: IMS .env File Not Found!";
    exit 1;
fi
if [[ !$(grep -e "LOG_DIR" $envfile) == "" ]]; then
    logfile=$(echo $(grep -e "LOG_DIR" $envfile | cut -d'=' -f2 | sed -e "s/\/$//" -e "s/'//g")"/install.log");
else
    logfile="/var/log/zc/ims/install.log";
fi
if [[ ! -d $(dirname $logfile) ]]; then
    [[ -d "$parentfolder/logs/" ]] && logfile="$parentfolder/logs/install.log" || logfile="$folder/install.log";
fi
if [ ! $1 ]; then
    read -s -p "Provide a password: " password;
else
    password=$1;
fi
echo "$(date)" >> $logfile;
echo "Configuring Postgresql..." >> $logfile;
# Run as postgres, create the database and role
su -c "echo \"SELECT 'CREATE DATABASE imsdb' WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'imsdb')\gexec\"|psql" postgres | tee -a $logfile;
su -c "echo \"SELECT 'CREATE USER imsuser' WHERE NOT EXISTS (SELECT FROM pg_user WHERE usename = 'imsuser')\gexec\"|psql" postgres | tee -a $logfile;
su -c "psql -b -c \"ALTER ROLE imsuser WITH PASSWORD '$password';\"" postgres | tee -a $logfile;
su -c "psql -b -c \"ALTER ROLE imsuser SET client_encoding TO 'utf8';\"" postgres | tee -a $logfile;
su -c "psql -b -c \"ALTER ROLE imsuser SET default_transaction_isolation TO 'read committed';\"" postgres | tee -a $logfile;
su -c "psql -b -c \"ALTER ROLE imsuser SET timezone TO 'utc';\"" postgres | tee -a $logfile;
su -c "psql -b -c \"GRANT ALL PRIVILEGES ON DATABASE imsdb TO imsuser;\"" postgres | tee -a $logfile;
su -c "psql -d imsdb -c \"CREATE SCHEMA IF NOT EXISTS imsschema AUTHORIZATION imsuser\"" postgres | tee -a $logfile;
echo "Configuring ims settings ($envfile)" >> $logfile;
# Update settings.py with PostgreSQL settings for database backend
EXISTING=$(grep -e 'IMS_DATABASE' $envfile | sed -e "s/imsuser:.*@localhost/imsuser@localhost/");
echo "$EXISTING" >> $logfile;
cp -f $envfile $envfile.old;
if [[ ! $(grep -e "IMS_DATABASE" "$envfile") == "" ]]; then
    sed -i -e "s/IMS_DATABASE=.*$/IMS_DATABASE=\"postgresql:\/\/imsuser:$password@localhost\/imsdb?options=-c search_path=imsschema\"/" $envfile;
else
    if [[ $(grep -e "# Database Settings" "$envfile") ]]; then
        sed -i -e "s/# Database Settings/# Database Settings\nIMS_DATABASE=\"postgresql:\/\/imsuser:$password@localhost\/imsdb?options=-c search_path=imsschema\"/" $envfile;
    else
        echo -e "# Database Settings\nIMS_DATABASE=\"postgresql://imsuser:$password@localhost/imsdb?options=-c search_path=imsschema\"\n###\n" >> $envfile;
    fi
fi
echo "Configuring ims Envrionment..." >> $logfile;
# Run as imsuser, install packages and migrate
su -c "source $parentfolder/venv/bin/activate; pip uninstall mysqlclient; pip install psycopg2-binary; if [ $? == 0 ]; then python $parentfolder/manage.py migrate; python $parentfolder/manage.py shell < $parentfolder/scripts/create_builtin_groups.py; else echo 'There was an error installing the psycopg2-binary package.'; fi; deactivate;" imsuser | tee -a $logfile;
if [[ ! $(grep -e "psycopg2-binary" "$parentfolder/local-requirements.txt") ]]; then
    echo "psycopg2-binary>=2.9.9" >> "$parentfolder/local-requirements.txt";
fi
if [[ ! $(grep -e "mysqlclient" "$parentfolder/local-requirements.txt") == "" ]]; then
    echo "WARNING: Manual intervention is required to remove 'mysqlclient' from $parentfolder/local-requirements.txt file.";
fi