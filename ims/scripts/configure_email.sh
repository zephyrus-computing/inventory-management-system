#!/bin/bash
folder=$(dirname $0); # Should be /etc/ims/scripts
parentfolder=$(dirname $folder); # Should be /etc/ims
# Find the .env file
if [[ -f "$parentfolder/.env" ]]; then
    envfile="$parentfolder/.env";
else
    echo "ERROR: IMS .env File Not Found!";
    exit 1;
fi
if [[ ! $(grep -e "LOG_DIR" $envfile) == "" ]]; then
    logfile=$(echo $(grep -e "LOG_DIR" $envfile | cut -d'=' -f2 | sed -e "s/\/$//" -e "s/'//g")"/install.log");
else
    logfile="/var/log/zc/ims/install.log";
fi
if [[ ! -d $(dirname $logfile) ]]; then
    [[ -d "$parentfolder/logs/" ]] && logfile="$parentfolder/logs/install.log" || logfile="$folder/install.log";
fi
if [ ! $1 ]; then
    read -p "Select delivery option (file,smtp): " selection;
else
    selection=$1;
fi
list=(file smtp)
if [[ ! " ${list[*]} " =~ " ${selection} " ]]; then
    echo "Invalid argument.";
    echo "$0 [file] [directory/path]";
    echo "$0 [smtp] [host] [username] [password] [port] ([from_address] [use_tls])";
    exit;
fi

# Update the email.conf with the specified Email settings
echo "$(date)" >> $logfile;
echo "Configuring email settings ($envfile)" >> $logfile;
if [ $selection == "file" ]; then
    if [ ! $2 ]; then
        read -p "Target Directory Path: " emaildirectory;
    else
        emaildirectory=$2;
    fi
    EXISTING=$(grep -e "EMAIL_" -e "DEFAULT_FROM_EMAIL" $envfile | awk "/EMAIL_HOST_PASSWORD/{next}1");
    echo "$EXISTING" >> $logfile;
    if [[ ! $(grep -e "# Email Settings" "$envfile") ]]; then
        cp $envfile $envfile.tmp;
        awk '/EMAIL_/{next;}/DEFAULT_FROM_EMAIL/{next;}1' $envfile.tmp > $envfile;
        rm -f $envfile.tmp;
        echo -e "# Email Settings\nEMAIL_BACKEND=django.core.mail.backends.filebased.EmailBackend\nEMAIL_FILE_PATH=${emaildirectory//\//\\\/}\nDEFAULT_FROM_EMAIL=support@zephyruscomputing.com\n###\n" >> $envfile;
    else
        cp $envfile $envfile.tmp;
        awk '/EMAIL_HOST/{next;}/EMAIL_PORT/{next;}/EMAIL_USE_TLS/{next;}1' $envfile.tmp > $envfile;
        rm -f $envfile.tmp;
        if [[ ! $(grep -e "EMAIL_BACKEND" "$envfile") == "" ]]; then
            sed -i -e "s/EMAIL_BACKEND=.*$/EMAIL_BACKEND=django.core.mail.backends.filebased.EmailBackend/" $envfile;
        else
            sed -i -e "s/# Email Settings/# Email Settings\nEMAIL_BACKEND=django.core.mail.backends.filebased.EmailBackend/" $envfile;
        fi
        if [[ !$(grep -e "EMAIL_FILE_PATH" "$envfile") == "" ]]; then
            sed -i -e "s/EMAIL_FILE_PATH=.*$/EMAIL_FILE_PATH=${emaildirectory//\//\\\/}/" $envfile;
        else
            sed -i -e "s/# Email Settings/# Email Settings\nEMAIL_FILE_PATH=${emaildirectory//\//\\\/}/" $envfile;
        fi
        if [[ ! $(grep -e "DEFAULT_FROM_EMAIL" "$envfile") == "" ]]; then
            sed -i -e "s/DEFAULT_FROM_EMAIL=.*$/DEFAULT_FROM_EMAIL=$from/" $envfile;
        else
            sed -i -e "s/# Email Settings/# Email Settings\nDEFAULT_FROM_EMAIL=$from/" $envfile;
        fi
    fi
fi
if [ $selection == "smtp" ]; then
    if [ ! $5 ]; then
        read -p "Target Host Address: " host;
        read -p "Email Account User: " username;
        read -p -s "Email Account Password: " password;
        read -p "Target Port: " port;
    else
        host=$2;
        username=$3;
        password=$4;
        port=$5;
    fi
    if [ ! $6 ]; then
        read -p "Configure optional parameters? (y/N): " optional;
        if [[ "$optional" == "y" || "$optional" == "Y" ]]; then
            read -p "Use TLS (true/false): " tls;
            read -p "Default From Address: " from;
        else
            [[ "$port" == "25" ]] && tls="False";
            [[ "$port" == "587" ]] && tls="True";
            from=$username;
        fi
    else
        tls=$6;
        [[ $7 ]] && from=$7;
    fi
    EXISTING=$(grep -e "EMAIL_" -e "DEFAULT_FROM_EMAIL" $envfile | awk "/EMAIL_HOST_PASSWORD/{next}1");
    echo "$EXISTING" >> $logfile;
    [[ ! $from ]] && from="support@zephyruscomputing.com";
    if [[ ! $(grep -e "# Email Settings" "$envfile") ]]; then
        cp $envfile $envfile.tmp;
        awk '/EMAIL_/{next;}/DEFAULT_FROM_EMAIL/{next;}1' $envfile.tmp > $envfile;
        rm -f $envfile.tmp;
        echo -e "# Email Settings\nEMAIL_BACKEND=django.core.mail.backends.smtp.EmailBackend\nEMAIL_HOST=$host\nEMAIL_HOST_USER=$username\nEMAIL_HOST_PASSWORD=$password\nEMAIL_PORT=$port\nEMAIL_USE_TLS=$tls\nDEFAULT_FROM_EMAIL=$from\n###\n" >> $envfile;
    else
        cp $envfile $envfile.tmp;
        awk '/EMAIL_FILE_PATH/{next;}1' $envfile.tmp > $envfile;
        rm -f $envfile.tmp;
        if [[ ! $(grep -e "EMAIL_BACKEND" "$envfile") == "" ]]; then
            sed -i -e "s/EMAIL_BACKEND=.*$/EMAIL_BACKEND=django.core.mail.backends.smtp.EmailBackend/" $envfile;
        else
            sed -i -e "s/# Email Settings/# Email Settings\nEMAIL_BACKEND=django.core.mail.backends.smtp.EmailBackend/" $envfile;
        fi
        if [[ ! $(grep -e "EMAIL_HOST=" "$envfile") == "" ]]; then
            sed -i -e "s/EMAIL_HOST=.*$/EMAIL_HOST=$host/" $envfile;
        else
            sed -i -e "s/# Email Settings/# Email Settings\nEMAIL_HOST=$host/" $envfile;
        fi
        if [[ ! $(grep -e "EMAIL_HOST_USER" "$envfile") == "" ]]; then
            sed -i -e "s/EMAIL_HOST_USER=.*$/EMAIL_HOST_USER=$username/" $envfile;
        else
            sed -i -e "s/# Email Settings/# Email Settings\nEMAIL_HOST_USER=$username/" $envfile;
        fi
        if [[ ! $(grep -e "EMAIL_HOST_PASSWORD" "$envfile") == "" ]]; then
            sed -i -e "s/EMAIL_HOST_PASSWORD=.*$/EMAIL_HOST_PASSWORD=$password/" $envfile;
        else
            sed -i -e "s/# Email Settings/# Email Settings\nEMAIL_HOST_PASSWORD=$password/" $envfile;
        fi
        if [[ ! $(grep -e "EMAIL_PORT" "$envfile") == "" ]]; then
            sed -i -e "s/EMAIL_PORT=.*$/EMAIL_PORT=$port/" $envfile;
        else
            sed -i -e "s/# Email Settings/# Email Settings\nEMAIL_PORT=$port/" $envfile;
        fi
        if [[ ! $(grep -e "EMAIL_USE_TLS" "$envfile") == "" ]]; then
            sed -i -e "s/EMAIL_USE_TLS=.*$/EMAIL_USE_TLS=$tls/" $envfile;
        else
            sed -i -e "s/# Email Settings/# Email Settings\nEMAIL_USE_TLS=$tls/" $envfile;
        fi
        if [[ ! $(grep -e "DEFAULT_FROM_EMAIL" "$envfile") == "" ]]; then
            sed -i -e "s/DEFAULT_FROM_EMAIL=.*$/DEFAULT_FROM_EMAIL=$from/" $envfile;
        else
            sed -i -e "s/# Email Settings/# Email Settings\nDEFAULT_FROM_EMAIL=$from/" $envfile;
        fi
    fi
fi
echo "Restart the IMS service to apply the new settings";
