#! /bin/bash
folder=$(dirname $0); # Should be /etc/zc/ims/scripts
parentfolder=$(dirname $folder); # Should be /etc/zc/ims
if [[ ! $(grep -e "LOG_DIR" $envfile) == "" ]]; then
    logfile=$(echo $(grep -e "LOG_DIR" $envfile | cut -d'=' -f2 | sed -e "s/\/$//" -e "s/'//g")"/install.log");
else
    logfile="/var/log/zc/ims/install.log";
fi
if [[ ! -d $(dirname $logfile) ]]; then
    [[ -d "$parentfolder/logs/" ]] && logfile="$parentfolder/logs/install.log" || logfile="$folder/install.log";
fi
# Move the apache2/httpd config file
echo "$(date)" >> $logfile;
echo "Determining the apache2 vs httpd..." >> $logfile;
ls /lib/systemd/system/ | grep httpd > /dev/null;
if [ $? == 0 ]; then
    echo "Copying httpd configuration to /etc/httpd/conf.d/..." >> $logfile;
    cp "${parentfolder}/httpd-ims.conf" /etc/httpd/conf.d/ | tee -a $logfile;
    # Restart the httpd service
    service httpd restart | tee -a $logfile;
else
    echo "Copying httpd configuration to /etc/apache2/sites-available/ and linking..." >> $logfile;
    cp "${parentfolder}/httpd-ims.conf" /etc/apache2/sites-available/ | tee -a $logfile;
    ln -s /etc/apache2/sites-available/httpd-ims.conf /etc/apache2/sites-enabled/ | tee -a $logfile;
    # Restart the apache2 service
    echo "Enabling apache2 mods..." >> $logfile;
    a2enmod proxy proxy_http | tee -a $logfile;
    service apache2 restart | tee -a $logfile;
fi
