#! /bin/bash
# Ensure that the user executing this script is 'imsuser'
if [ "$USER" != "imsuser" ]; then
    echo "This script needs to be run as imsuser!";
    exit;
fi
folder=$(dirname $0); # Should be /etc/zc/ims/scripts
parentfolder=$(dirname $folder); # Should be /etc/zc/ims
# Find the .env files
if [[ -f "$parentfolder/.env" ]]; then
    envfile="$parentfolder/.env";
else
    echo "ERROR: IMS .env File Not Found!";
    exit 1;
fi
if [[ ! $(grep -e "LOG_DIR" $envfile) == ""]]; then
    logdir=$(grep -e "LOG_DIR" $envfile | cut -d'=' -f2);
    logfile=$(echo $(echo $logdir | sed -e "s/\/$//")"/install.log");
else
    logdir="/var/log/zc/ims/";
    logfile="/var/log/zc/ims/install.log";
fi
if [[ ! -d $(dirname $logfile) ]]; then
    [[ -d "$parentfolder/logs/" ]] && logfile="$parentfolder/logs/install.log" || logfile="$folder/install.log";
fi
filename="$parentfolder/$(hostname)-$(date +%j%H%M%S).tar";
# Create the support bundle archive with the install.log
if [[ -d $(dirname $logfile) ]]; then
    tar -cf $filename -C $(dirname $logfile) install.log 2&> /dev/null;
else
    echo "ERROR: IMS install.log File Not Found!";
    exit 1;
fi
# Append to the archive access and error logs of the webserver
tar -rf $filename -C $logdir *-access.log 2&> /dev/null;
tar -rf $filename -C $logdir *-error.log 2&> /dev/null;
tar -rf $filename -C $logdir gunicorn.log 2&> /dev/null;
# Append to the archive the database schema
$folder/get_database_schema.sh;
tar -rf $filename -C $logdir db_schema.txt 2&> /dev/null;

# Prompt for permission to copy .env
read -p "Include a sanitized copy of .env? (Y/n)" include;
if [[ $include == "" ]]; then include=Y; fi
if [[ ${include,,} == "y" ]]; then
    # Append to the archive the sanitized contents of .env
    sed -e "s/,PASSWORD=.*,HOST/,HOST/" $envfile > $envfile.sanitized;
    tar -rf $filename -C $(dirname $envfile) .env.sanitized 2&> /dev/null;
fi

# gunzip the archive
tar -czf $filename.gz $filename 2&> /dev/null;

# Delete the created files
rm -f $envfile.sanitized;
rm -f $logdir/db_schema.txt;
rm -f $filename;
