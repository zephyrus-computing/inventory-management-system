#!/usr/bin/env python3
import os
import socket
import uuid

def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.settimeout(0)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except Exception:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP

def get_fqdn():
    return socket.getfqdn()

def get_uuid():
    u = uuid.uuid4()
    return str(u)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

ENV_FILE = os.path.join(BASE_DIR, '.env')
if not os.path.exists(ENV_FILE):
    with open(ENV_FILE, 'w') as settings:
        print('# Local host information', file=settings)
        print("LOCAL_HOSTNAME='{}'".format(get_fqdn()), file=settings)
        print("LOCAL_IPADDR='{}'".format(get_ip()), file=settings)
        print('###', file=settings)
        print('', file=settings)
        print('# Installed Apps and Third Party Libraries', file=settings)
        print('INSTALLED_APPS=""', file=settings)
        print('THIRD_PARTY_LIBRARIES=""', file=settings)
        print('###', file=settings)
        print('', file=settings)
        print('# Security Settings', file=settings)
        print("DJANGO_SECRET_KEY='{}'\n".format(get_uuid()), file=settings)
        print('###', file=settings)
        print('', file=settings)
        print('# MFA Settings', file=settings)
        print('MFA_ENFORCE=False', file=settings)
        print('###', file=settings)
        print('', file=settings)
        print('# Database Settings', file=settings)
        print('', file=settings)
        print('###', file=settings)
        print('', file=settings)
        print('# Logging Settings', file=settings)
        print('', file=settings)
        print('###', file=settings)
        print('', file=settings)
        print('# Email Settings', file=settings)
        print('', file=settings)
        print('###', file=settings)
        print('', file=settings)
        print('# Addons', file=settings)
        print('', file=settings)
        print('###', file=settings)
        print('', file=settings)
        print('# Debug and Development variables', file=settings)
        print('DEBUG=False', file=settings)
        print('DEVENV=False', file=settings)
        print('###', file=settings)
        print('', file=settings)
        print('# IMS Settings', file=settings)
        print('ALLOW_ANONYMOUS=True', file=settings)
        print('ALLOW_USER_API_KEYS=True', file=settings)
        print("LOGGING_LEVEL='INFO'", file=settings)
        print('###', file=settings)
        print('', file=settings)
