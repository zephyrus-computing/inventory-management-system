#! /bin/bash
folder=$(dirname $0); # Should be /etc/zc/ims/scripts
parentfolder=$(dirname $folder); # Should be /etc/zc/ims
# Find the .env file
if [[ -f "$parentfolder/.env" ]]; then
    envfile="$parentfolder/.env";
else
    echo "ERROR: IMS .env File Not Found!";
    exit 1;
fi
if [[ ! $(grep -e "LOG_DIR" $envfile) == "" ]]; then
    logfile=$(echo $(grep -e "LOG_DIR" $envfile | cut -d'=' -f2 | sed -e "s/\/$//" -e "s/'//g")"/install.log");
else
    logfile="/var/log/zc/ims/install.log";
fi
if [[ ! -d $(dirname $logfile) ]]; then 
    [[ -d "$parentfolder/logs/" ]] && logfile="$parentfolder/logs/install.log" || logfile="$folder/install.log";
fi
# Move the nginx config file to /etc/nginx/conf.d/
echo "$(date)" >> $logfile;
echo "Copying nginx configuration to /etc/nginx/conf.d/..." >> $logfile;
cp "$parentfolder/nginx-ims.conf" /etc/nginx/conf.d/ | tee -a $logfile;
# Restart nginx service
service nginx restart | tee -a $logfile;
