#! /bin/bash
folder=$(dirname $(readlink -f $0)); # Should be /etc/zc/ims/scripts
parentfolder=$(dirname $folder); # Should be /etc/zc/ims
# Find the .env file
if [[ -f "$parentfolder/.env" ]]; then
    envfile="$parentfolder/.env";
else
    echo "ERROR: IMS .env File Not Found!";
    exit 1;
fi
if [[ ! $(grep -e "LOG_DIR" $envfile) == "" ]]; then
    logfile=$(echo $(grep -e "LOG_DIR" $envfile | cut -d'=' -f2 | sed -e "s/\/$//" -e "s/'//g")"/install.log");
else
    logfile="/var/log/zc/ims/install.log";
fi
if [[ ! -d $(dirname $logfile) ]]; then
    [[ -d "$parentfolder/logs/" ]] && logfile="$parentfolder/logs/install.log" || logfile="$folder/install.log";
fi
# Update settings.py with MariaDB settings for database backend
echo "$(date)" >> $logfile;
echo "Configuring ims settings ($envfile)" >> $logfile;
EXISTING=$(grep -e 'IMS_DATABASE' $envfile | sed -e "s/imsuser:.*@localhost/imsuser@localhost/");
echo "$EXISTING" >> $logfile;
cp -f $envfile $envfile.old;
if [[ ! $(grep -e "IMS_DATABASE" "$envfile") = "" ]]; then
    sed -i -e "s/IMS_DATABASE=.*$/IMS_DATABASE=\"sqlite:\/\/\/\/$parentfolder\/db.sqlite3\"/" $envfile;
else
    if [[ ! $(grep -e "# Database Settings" "$envfile") ]]; then
        sed -i -e "s/# Database Settings/# Database Settings\nIMS_DATABASE=\"sqlite:\/\/\/\/$parentfolder\/db.sqlite3\"/" $envfile;
    else
        echo -e "# Database Settings\nIMS_DATABASE=\"sqlite:////$parentfolder/db.sqlite3\"\n###\n" >> $envfile;
    fi
fi

echo "Configuring ims Envrionment..." >> $logfile;
# Run as imsuser, install packages and migrate
su -c "source $parentfolder/venv/bin/activate; pip uninstall mysqlclient psycopg2-binary; python $parentfolder/manage.py migrate; python $parentfolder/manage.py shell < $parentfolder/scripts/create_builtin_groups.py; deactivate;" imsuser | tee -a $logfile;
