#! /usr/bin/env python

from itertools import chain
from inventory.models import Part,Storage,AlternateSKU,PartStorage,PartAlternateSKU,Assembly,Kit,KitPartStorage,get_sentinel_part,get_sentinel_storage,get_sentinel_sku,get_sentinel_kit,get_sentinel_partstorage
from inventory.api.serializers import PartStorageReadSerializer,PartAlternateSKUReadSerializer,AssemblyReadSerializer,KitPartStorageReadSerializer

# Get Sentinel objects
sentinelpart = get_sentinel_part()
sentinelstorage = get_sentinel_storage()
sentinelsku = get_sentinel_sku()
sentinelkit = get_sentinel_kit()
sentinelpartstorage = get_sentinel_partstorage()
# Get PartStorage where either Part or Storage is Sentinel object
partstorages = PartStorage.objects.filter(part=sentinelpart)
partstorages = chain(partstorages, PartStorage.objects.filter(storage=sentinelstorage))
# Get PartAlternateSKU where either Part or AlternateSKU is Sentinel object
partaltskus = PartAlternateSKU.objects.filter(part=sentinelpart)
partaltskus = chain(partaltskus, PartAlternateSKU.objects.filter(alt_sku=sentinelsku))
# Get Assembly where either Parent or Part is Sentinel object
assemblies = Assembly.objects.filter(parent=sentinelpart)
assemblies = chain(assemblies, Assembly.objects.filter(part=sentinelpart))
# Get KitPartStorage where either Kit or PartStorage is Sentinel object
kitpartstorages = KitPartStorage.objects.filter(kit=sentinelkit)
kitpartstorages = chain(kitpartstorages, KitPartStorage.objects.filter(partstorage=sentinelpartstorage))

# Output the JSON serialized objects with stale relationships
for partstorage in partstorages:
    print(PartStorageReadSerializer(partstorage).data)

for partaltsku in partaltskus:
    print(PartAlternateSKUReadSerializer(partaltsku).data)

for assembly in assemblies:
    print(AssemblyReadSerializer(assembly).data)

for kitpartstorage in kitpartstorages:
    print(KitPartStorageReadSerializer(kitpartstorage).data)

