import django
django.setup()
from django.conf import settings
from django.contrib.auth.models import AnonymousUser, User
from django.test import RequestFactory, TestCase, override_settings
from os import popen,remove
from os.path import exists,join
from random import randint
from rest_framework import status
from inventory.models import Part, Storage, AlternateSKU, PartAlternateSKU, PartStorage, Assembly

# Test ims view redirect
class ImsScriptTests(TestCase):
    def setUp(self):
        self.script_list = ['configure_apache2.sh','configure_email.sh','configure_mariadb.sh','configure_nginx.sh','configure_postgresql.sh','configure_rabbitmq.sh','configure_redis.sh','create_builtin_groups.py','create_email_templates.py','create_support_bundle.sh','get_database_schema.sh','ims_logrotate.conf','ims_logrotate.sh','setup.sh','upgrade.sh']

    def test_script_exists(self):
        # Test that each file exists
        for script in self.script_list:
            p = join(settings.BASE_DIR, 'scripts', script)
            print(p)
            self.assertTrue(exists(p))

# Test configure_apache2.sh
class ImsScriptConfApache2Tests(TestCase):
    def setUp(self):
        self.script = join(settings.BASE_DIR, 'scripts/configure_apache2.sh')
        # Remove the target files if they already exist
        if exists('/etc/httpd/conf.d'):
            remove('/etc/httpd/conf.d/*.*')
        if exists('/etc/apache2/sites-available'):
            remove('/etc/apache2/sites-enabled/*.*')
        if exists('/etc/apache2/sites-available'):
            remove('/etc/apache2/sites-available/*.*')
        if exists('/etc/apache2/conf.d'):
            remove('/etc/apache2/conf.d/*.*')

    def test_apache2_execution(self):
        # Execute the script and record its output
        result = popen(self.script).read()
        # Test if target files exist
        if not exists('/etc/httpd/conf.d/httpd-ims.conf') and not exists('/etc/apache2/sites-available/httpd-ims.conf'):
            self.fail()

class ImsScriptConfNginxTests(TestCase):
    def setUp(self):
        self.script = join(settings.BASE_DIR, 'scripts/configure_nginx.sh')
        # Remove the target files if they already exist
        if exists('/etc/nginx/conf.d/nginx-ims.conf'):
            remove('/etc/nginx/conf.d/*.*')

    def test_nginx_execution(self):
        # Execute the script and record its output
        result = popen(self.script).read()
        # Test if target files exist
        if not exists('/etc/nginx/conf.d/nginx-ims.conf'):
            self.fail()

class ImsScriptConfEmailTests(TestCase):
    def setUp(self):
        self.script = join(settings.BASE_DIR, 'scripts/configure_email.sh')

    def test_email_execution_bad(self):
        # Execute the script and record its output
        result = popen(self.script + ' banana').read()
        self.assertIn('Invalid argument.', result)
        self.assertIn('[file] [directory/path]', result)
        self.assertIn('[smtp] [host] [username] [password] [port] ([from_address] [use_tls])', result)

    def test_email_execution_good(self):
        # Execute the script and record its output
        result = popen(self.script + ' file /opt/zc/ims/logs/email/').read()
        # Check the settings config to see if the script updated it correctly
        self.assertEqual(settings.EMAIL_TYPE, 'file')
        self.assertEqual(settings.EMAIL_DIRECTORY, '/opt/zc/ims/logs/email/')
        result = popen(self.script + ' smtp smtp.office365.com support@zephyruscomputing.com pasword123 587 support@zephyruscomputing.com True').read()
        # Check the settings config to see if the script updated it correctly
        #self.assertEqual(settings.EMAIL_TYPE, 'smtp')
        #self.assertEqual(settings.EMAIL_HOST, 'smtp.office365.com')
        #self.assertEqual(settings.EMAIL_HOST_USER, 'support@zephyruscomputing.com')
        #self.assertEqual(settings.EMAIL_HOST_PASSWORD, 'password123')
        #self.assertEqual(settings.EMAIL_PORT, '587')
        #self.assertEqual(settings.EMAIL_USE_TLS, 'True')
        #self.assertEqual(settings.DEFAULT_FROM_ADDRESS, 'support@zephyruscomputing.com')
        #result = popen(self.script + ' smtp smtp.office365.com sales@zephyruscomputing.com pasword123 25').read()
        # Check the settings config to see if the script updated it correctly
        #self.assertEqual(settings.EMAIL_TYPE, 'smtp')
        #self.assertEqual(settings.EMAIL_HOST, 'smtp.office365.com')
        #self.assertEqual(settings.EMAIL_HOST_USER, 'sales@zephyruscomputing.com')
        #self.assertEqual(settings.EMAIL_HOST_PASSWORD, 'password123')
        #self.assertEqual(settings.EMAIL_PORT, '25')
        #self.assertEqual(settings.EMAIL_USE_TLS, 'False')
        #self.assertEqual(settings.DEFAULT_FROM_ADDRESS, 'sales@zephyruscomputing.com')

class ImsScriptConfMariaDbTests(TestCase):
    def setUp(self):
        self.script = join(settings.BASE_DIR, 'scripts/configure_mariadb.sh')
        popen('useradd imsuser; echo -e "password123\npassword123" | passwd imsuser')
        popen("mysql -e \"GRANT ALL PRIVILEGES ON *.* TO 'imsuser'@'localhost' IDENTIFIED BY 'password123' WITH GRANT OPTION;\"")

    def test_maria_execution(self):
        # Execute the script and record its output
        result = popen(self.script + ' imsuser password123 dbpassword').read()
        # Check results for keywords
        self.assertNotIn('ERROR', result)
        self.assertNotIn('Errno', result)
        # Check the settings config to see if the script updated it correctly
        self.assertEqual(settings.DATABASES['default']['ENGINE'], 'django.db.backends.mysql')
        self.assertEqual(settings.DATABASES['default']['NAME'], 'imsdb')
        self.assertEqual(settings.DATABASES['default']['PASSWORD'], 'dbpassword')
        self.assertEqual(settings.DATABASES['default']['USERNAME'], 'imsuser')
        self.assertEqual(settings.DATABASES['default']['HOST'], 'localhost')
        # Execute the script again to change the password
        result = popen(self.script + ' imsuser password123 newpassword').read()
        self.assertEqual(settings.DATABASES['default']['PASSWORD'], 'newpassword')

    def tearDown(self):
        popen('userdel imsuser')

class ImsScriptConfPostgreSqlTests(TestCase):
    def setUp(self):
        self.script = join(settings.BASE_DIR, 'scripts/configure_postgresql.sh')

    def test_postgresql_execution(self):
        # Execute the script and record its output
        result = popen(self.script + ' dbpassword').read()
        # Check results for keywords
        self.assertIn('CREATE DATABASE', result)
        self.assertIn('CREATE USER', result)
        self.assertIn('ALTER ROLE', result)
        self.assertIn('GRANT', result)
        self.assertNotIn('Errno', result)
        # Check the settings config to see if the script updated it correctly
        self.assertEqual(settings.DATABASES['default']['ENGINE'], 'django.db.backends.postgresql_psycopg2')
        self.assertEqual(settings.DATABASES['default']['NAME'], 'imsdb')
        self.assertEqual(settings.DATABASES['default']['PASSWORD'], 'dbpassword')
        self.assertEqual(settings.DATABASES['default']['USERNAME'], 'imsuser')
        self.assertEqual(settings.DATABASES['default']['HOST'], 'localhost')
        # Execute the script again to change the password
        result = popen(self.script + ' newpassword').read()
        self.assertEqual(settings.DATABASES['default']['PASSWORD'], 'newpassword')

class ImsScriptConfSqlite3Tests(TestCase):
    def setUp(self):
        self.script = join(settings.BASE_DIR, 'scripts/configure_sqlite3.sh')

    def test_sqlite3_execution(self):
        # Execute the script and record its output
        result = popen(self.script).read()
        # Check results for keywords
        self.assertNotIn('Errno', result)
        # Check the settings config to see if the script updated it correctly
        #self.assertEqual(settings.DATABASES['default']['ENGINE'], 'django.db.backends.sqlite3')
        #self.assertEqual(settings.DATABASES['default']['NAME'], '/opt/zc/ims/db.sqlite3')

class ImsScriptConfRabbitMqTests(TestCase):
    def setUp(self):
        self.script = join(settings.BASE_DIR, 'scripts/configure_rabbitmq.sh')

    def test_rabbitmq_execution_bad(self):
        # Execute the script and record its output
        result = popen(self.script + ' banana')
        # Check results for keywords

    def test_rabbitmq_execution_good(self):
        # Execute the script and record its output
        result = popen(self.script + ' backend')
        # Check results for keywords
        # Check the settings config to see if the script updated it correctly
        result = popen(self.script + ' broker')
        # Check results for keywords
        # Check the settings config to see if the script updated it correctly

class ImsScriptConfRedisTests(TestCase):
    def setUp(self):
        self.script = join(settings.BASE_DIR, 'scripts/configure_redis.sh')

    def test_redis_execution_bad(self):
        # Execute the script and record its output
        result = popen(self.script + ' banana')
        # Check results for keywords

    def test_redis_execution_good(self):
        # Execute the script and record its output
        result = popen(self.script + ' backend')
        # Check results for keywords
        # Check the settings config to see if the script updated it correctly
        result = popen(self.script + ' broker')
        # Check results for keywords
        # Check the settings config to see if the script updated it correctly

class ImsScriptSupportTests(TestCase):
    def setUp(self):
        self.script = join(settings.BASE_DIR, 'scripts/create_support_bundle.sh')

    def test_support_execution(self):
        # Execute the script and record its output
        result = popen(self.script)
        # Check results for keywords
        # Check the support file to see if the script created the contents correctly


