#! /bin/bash
folder=$(dirname $(readlink -f $0)); # Should be /etc/zc/ims/scripts
parentfolder=$(dirname $folder); # Should be /etc/zc/ims
# Find the .env file
if [[ -f "$parentfolder/.env" ]]; then
    envfile="$parentfolder/.env";
else
    echo "ERROR: IMS .env File Not Found!";
    exit 1;
fi
if [[ ! $(grep -e "LOG_DIR" $envfile) == "" ]]; then
    logfile=$(echo $(grep -e "LOG_DIR" $envfile | cut -d'=' -f2 | sed -e "s/\/$//" -e "s/'//g")"/install.log");
else
    logfile="/var/log/zc/ims/install.log";
fi
if [[ ! -d $(dirname $logfile) ]]; then
    [[ -d "$parentfolder/logs/" ]] && logfile="$parentfolder/logs/install.log" || logfile="$folder/install.log";
fi
if [ ! $3 ]; then
    echo "Provide credentials to run mysql commands:";
    read -p "username: " username;
    read -s -p "password: " userpass;
    echo "";
    read -s -p "Provide a password for connecting to the new database: " password;
else
    username=$1;
    userpass=$2;
    password=$3;
fi
echo "$(date)" >> $logfile;
echo "Configuring MariaDB..." >> $logfile;
# Run as mysql, create the database and role. Should have these permissions
# GRANT ALL PRIVILEGES ON *.* TO '$username'@'localhost' IDENTIFIED BY PASSWORD '*****' WITH GRANT OPTION
mysql --user=$username --password=$userpass -e "CREATE DATABASE IF NOT EXISTS imsdb CHARACTER SET utf8;" | tee -a $logfile;
mysql --user=$username --password=$userpass -e "CREATE USER IF NOT EXISTS 'imsuser'@'localhost';" | tee -a $logfile;
mysql --user=$username --password=$userpass -e "ALTER USER 'imsuser'@'localhost' IDENTIFIED BY '$password';" | tee -a $logfile;
mysql --user=$username --password=$userpass -e "GRANT ALL PRIVILEGES ON imsdb.* TO 'imsuser'@'localhost';" | tee -a $logfile;
# Update settings.py with MariaDB settings for database backend
echo "Configuring ims settings ($envfile)" >> $logfile;
EXISTING=$(grep -e 'IMS_DATABASE' $envfile | sed -e "s/imsuser:.*@localhost/imsuser@localhost/");
echo "$EXISTING" >> $logfile;
cp -f $envfile $envfile.old;
if [[ ! $(grep -e "IMS_DATABASE" "$envfile") == "" ]]; then
    sed -i -e "s/IMS_DATABASE=.*$/IMS_DATABASE=\"mysql:\/\/imsuser:$password@localhost\/imsdb\"/" $envfile;
else
    if [[ $(grep -e "# Database Settings" "$envfile") ]]; then
        sed -i -e "s/# Database Settings/# Database Settings\nIMS_DATABASE=\"mysql:\/\/imsuser:$password@localhost\/imsdb\"/" $envfile;
    else
        echo -e "# Database Settings\nIMS_DATABASE=\"mysql://imsuser:$password@localhost/imsdb\"\n###\n" >> $envfile;
    fi
fi
echo "Configuring ims Environment..." >> $logfile;
# Install packages needed to utilize mysql/mariadb
apt install -y python3-dev default-libmysqlclient-dev build-essential | tee -a $logfile;
# Run as imsuser, install packages and migrate
su -c "source $parentfolder/venv/bin/activate; pip uninstall psycopg2-binary; pip install mysqlclient; if [ $? == 0 ]; then python $parentfolder/manage.py migrate; python $parentfolder/manage.py shell < $parentfolder/scripts/create_builtin_groups.py; else echo 'There was an error while installing the mysql-python package.'; fi; deactivate;" imsuser | tee -a $logfile;
if [[ ! $(grep -e "mysqlclient" "$parentfolder/local-requirements.txt") ]]; then
    echo "mysqlclient>=2.2.0" >> "$parentfolder/local-requirements.txt";
fi
if [[ ! $(grep -e "psycopg2-binary" "$parentfolder/local-requirements.txt") == "" ]]; then
    echo "WARNING: Manual intervention is required to remove 'psycopg2-binary' from $parentfolder/local-requirements.txt file.";
fi