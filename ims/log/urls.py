from django.urls import re_path
from log import views

app_name='log'
urlpatterns = [
    re_path(r'^$', views.log_list, name='logs'),
]
