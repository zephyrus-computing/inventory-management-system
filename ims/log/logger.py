from django.contrib.auth.models import AnonymousUser, User
from django.conf import settings
from log.models import Log, _levelToName, _nameToLevel, NOTSET, DEBUG, INFO, WARNING, ERROR, CRITICAL

def getLevelName(level):
    result = _levelToName.get(level)
    if result is not None:
        return result
    result = _nameToLevel.get(level)
    if result is not None:
        return result
    return "Level %s" % level

def _checkLevel(level):
    if isinstance(level, int):
        rv = level
    elif str(level) == level:
        if level not in _nameToLevel:
            raise ValueError("Unknown level: %r" % level)
        rv = _nameToLevel[level]
    else:
        raise TypeError("Level not an integer or a valid string: %r" % (level,))
    return rv

def getLogger(name):
    if not isinstance(name, str):
        raise TypeError("Name must be a string")
    if isinstance(settings.LOGGING_LEVEL, str):
        return Logger(name, level=settings.LOGGING_LEVEL)
    if isinstance(settings.LOGGING_LEVEL, list):
        for i in range(0,len(settings.LOGGING_LEVEL)):
            if settings.LOGGING_LEVEL[i]['name'] == name:
                return Logger(name, level=settings.LOGGING_LEVEL[i]['level'])
    return Logger(name, level='INFO')


class Logger(object):
    def __init__(self, name, level=NOTSET):
        self.name = name
        self.level = _checkLevel(level)

    def setLevel(self, level):
        self.level = _checkLevel(level)

    def isEnabledFor(self, level):
        return level >= self.level

    def log(self, level, user, msg, *args):
        if not isinstance(level, int):
            raise TypeError("level must be an integer")
        if not isinstance(user, int) and not isinstance(user, User) and not isinstance(user, AnonymousUser):
            raise TypeError("user must be an integer, AnonymousUser, or User")
        if isinstance(user, int):
            if user != 0:
                try:
                    user = User.objects.get(id=user)
                except User.DoesNotExist as dne:
                    Log.objects.create(severity=ERROR,module=self.name,user=None,message=dne)
                    Log.objects.create(severity=CRITICAL,module=self.name,user=None,message='Unable to log the following event for the specified user id!: severity={},user={},message={}'.format(level,user,msg))
                    return
            else:
                user = None
        if isinstance(user, AnonymousUser):
            user = None
        if self.isEnabledFor(level):
            try:
                Log.objects.create(severity=level,module=self.name,user=user,message=msg.format(args))
            except Exception as err:
                raise err

    def debug(self, user, msg, *args):
        if self.isEnabledFor(DEBUG):
            self.log(DEBUG, user, msg, args)
    
    def info(self, user, msg, *args):
        if self.isEnabledFor(INFO):
            self.log(INFO, user, msg, args)
    
    def warning(self, user, msg, *args):
        if self.isEnabledFor(WARNING):
            self.log(WARNING, user, msg, args)
    
    def error(self, user, msg, *args):
        if self.isEnabledFor(ERROR):
            self.log(ERROR, user, msg, args)
    
    def critical(self, user, msg, *args):
        if self.isEnabledFor(CRITICAL):
            self.log(CRITICAL, user, msg, args)

