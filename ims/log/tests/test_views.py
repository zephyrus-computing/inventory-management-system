import django
django.setup()
from django.contrib.auth.models import AnonymousUser, User, Permission
from django.test import TestCase, override_settings
from django.utils.timezone import now
from random import choice, randint
from rest_framework import status
from log.models import Log, SEVERITY, _levelToName

baseurl = '/logs/'

class LogLogViewTests(TestCase):
    def setUp(self):
        user = User.objects.get_or_create(username='testuser')[0]
        for i in range(1,100):
            module = choice(['inventory-admin','inventory-api'])
            Log.objects.create(severity=20,module=module,user=user,message='Created Part id {}'.format(i))
            Log.objects.create(severity=20,module=module,user=user,message='Created Storage id {}'.format(i))
            Log.objects.create(severity=20,module=module,user=user,message='Created PartStorage id {}'.format(i))
            Log.objects.create(severity=20,module=module,user=user,message='Created Kit id {}'.format(i))
            Log.objects.create(severity=20,module=module,user=user,message='Created KitPartStorage id {}'.format(i))
            Log.objects.create(severity=20,module=module,user=user,message='Created AlternateSKU id {}'.format(i))
            Log.objects.create(severity=20,module=module,user=user,message='Created PartAlternateSKU id {}'.format(i))
            Log.objects.create(severity=20,module=module,user=user,message='Created Assembly id {}'.format(i))
        for i in range(0,200):
            module = choice(['inventory-admin','inventory-api'])
            obj = choice(['Part','Storage','PartStorage','Kit','KitPartStorage','AlternateSKU','PartAlternateSKU','Assembly'])
            Log.objects.create(severity=20,module=module,user=user,message='Updated {} id {}'.format(obj, randint(1,99)))
        for i in range(0,30):
            action = choice(['Create','Change'])
            obj = choice(['Part','Storage','PartStorage','Kit','KitPartStorage','AlternateSKU','PartAlternateSKU','Assembly'])
            Log.objects.create(severity=30,module='inventory-admin',user=user,message='{} {} Form not valid'.format(action, obj))
        for i in range(0,30):
            action = choice(['Create','Change'])
            obj = choice(['Part','Storage','PartStorage','Kit','KitPartStorage','AlternateSKU','PartAlternateSKU','Assembly'])
            Log.objects.create(severity=10,module='inventory-admin',user=user,message='Request to view Admin {} {} page for id {}'.format(action, obj, randint(1,99)))
        for i in range(0,5):
            Log.objects.create(severity=40,module='inventory',user=user,message='A Test Exception Occurred: Test-{}'.format(i))

    def test_log_list(self):
        response = self.client.get(baseurl)
        self.assertRedirects(response, '/accounts/login/?next={}'.format(baseurl))
        user = User.objects.get_or_create(username='testuser')[0]
        self.client.force_login(user)
        response = self.client.get(baseurl)
        self.assertRedirects(response, '/accounts/login/?next={}'.format(baseurl))
        auditor = User.objects.get_or_create(username='testauditor')[0]
        auditor.user_permissions.add(Permission.objects.get(name='Can view log'))
        self.client.force_login(auditor)
        response = self.client.get(baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Positive Tests
        datetime = Log.objects.first().created
        response = self.client.get('{}?created={}'.format(baseurl, datetime.date()))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, datetime.date())
        response = self.client.get('{}?created={}'.format(baseurl, datetime.time()))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '{}:{}'.format(str(datetime.time()).split('.')[0].split(':')[0],str(datetime.time()).split('.')[0].split(':')[1]))
        for sev in 'info','WARN','Debug':
            response = self.client.get('{}?severity={}'.format(baseurl, sev))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertContains(response, sev.upper())
        response = self.client.get('{}?severity=error'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'ERROR')
        response = self.client.get('{}?severity=error&module=inventory-admin'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, 'ERROR')
        response = self.client.get('{}?user=TESTUSER'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'testuser')
        response = self.client.get('{}?user=TESTAUDITOR'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'testauditor', count=10)
        response = self.client.get('{}?message=CREATED'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Created')
        self.assertNotContains(response, 'Updated')
        response = self.client.get('{}?message=UP'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Updated')
        self.assertNotContains(response, 'Created')
        response = self.client.get('{}?page=2'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, 'Created')
        response = self.client.get('{}?page=2000'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Created')
        response = self.client.get('{}?page=-1'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Created', count=15)
        # Negative Tests
        response = self.client.get('{}?datetime={}'.format(baseurl,now().year +1))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Page 1 of 1.')
        response = self.client.get('{}?severity={}'.format(baseurl,'banana'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Page 1 of 1.')
        response = self.client.get('{}?module={}'.format(baseurl, 'ims'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Page 1 of 1.')
        response = self.client.get('{}?user={}'.format(baseurl, 'test_staff'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Page 1 of 1.')
        response = self.client.get('{}?message={}'.format(baseurl, 'notarealwordorlogmessage'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Page 1 of 1.')
        response = self.client.get('{}?user=jimbob&page=10000'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Page 1 of 1.')
        self.client.force_login(User.objects.get_or_create(username='test_user')[0])
        response = self.client.get(baseurl)
        self.assertRedirects(response, '/accounts/login/?next={}'.format(baseurl))

