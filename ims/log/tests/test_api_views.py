import django
django.setup()
from django.contrib.auth.models import AnonymousUser, User
from django.test import TestCase, override_settings
from django.utils.timezone import now
from random import choice, randint
from rest_framework import status
from log.models import Log, SEVERITY, _levelToName
from log.api.serializers import LogSerializer
import json

api_version = 'v1'
api_module = 'log'

api_baseurl = '/api/{}/{}/'.format(api_version, api_module)

class LogLogViewTests(TestCase):
    def setUp(self):
        self.baseurl = api_baseurl
        user = User.objects.get_or_create(username='testuser')[0]
        for i in range(1,100):
            module = choice(['inventory-admin','inventory-api'])
            Log.objects.create(severity=20,module=module,user=user,message='Created Part id {}'.format(i))
            Log.objects.create(severity=20,module=module,user=user,message='Created Storage id {}'.format(i))
            Log.objects.create(severity=20,module=module,user=user,message='Created PartStorage id {}'.format(i))
            Log.objects.create(severity=20,module=module,user=user,message='Created Kit id {}'.format(i))
            Log.objects.create(severity=20,module=module,user=user,message='Created KitPartStorage id {}'.format(i))
            Log.objects.create(severity=20,module=module,user=user,message='Created AlternateSKU id {}'.format(i))
            Log.objects.create(severity=20,module=module,user=user,message='Created PartAlternateSKU id {}'.format(i))
            Log.objects.create(severity=20,module=module,user=user,message='Created Assembly id {}'.format(i))
        for i in range(0,200):
            module = choice(['inventory-admin','inventory-api'])
            obj = choice(['Part','Storage','PartStorage','Kit','KitPartStorage','AlternateSKU','PartAlternateSKU','Assembly'])
            Log.objects.create(severity=20,module=module,user=user,message='Updated {} id {}'.format(obj, randint(1,99)))
        for i in range(0,30):
            action = choice(['Create','Change'])
            obj = choice(['Part','Storage','PartStorage','Kit','KitPartStorage','AlternateSKU','PartAlternateSKU','Assembly'])
            Log.objects.create(severity=30,module='inventory-admin',user=user,message='{} {} Form not valid'.format(action, obj))
        for i in range(0,30):
            action = choice(['Create','Change'])
            obj = choice(['Part','Storage','PartStorage','Kit','KitPartStorage','AlternateSKU','PartAlternateSKU','Assembly'])
            Log.objects.create(severity=10,module='inventory-admin',user=user,message='Request to view Admin {} {} page for id {}'.format(action, obj, randint(1,99)))
        for i in range(0,5):
            Log.objects.create(severity=40,module='inventory',user=user,message='A Test Exception Occurred: Test-{}'.format(i))

    def test_apiview_logs(self):
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        user = User.objects.get_or_create(username='testuser')[0]
        self.client.force_login(user)
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        staff = User.objects.get_or_create(username='teststaff',is_staff=True)[0]
        self.client.force_login(staff)
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Positive Tests
        datetime = Log.objects.first().created
        response = self.client.get('{}?created={}'.format(self.baseurl, datetime.date()))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, datetime.date())
        response = self.client.get('{}?created={}'.format(self.baseurl, datetime.time()))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '{}:{}'.format(str(datetime.time()).split('.')[0].split(':')[0],str(datetime.time()).split('.')[0].split(':')[1]))
        for sev in '20','30','10':
            response = self.client.get('{}?severity={}'.format(self.baseurl, sev))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertContains(response, sev.upper())
        response = self.client.get('{}?severity=40'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '40')
        response = self.client.get('{}?severity=40&module=inventory-admin'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, '40')
        response = self.client.get('{}?user=TESTUSER'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"user":{}'.format(user.id))
        response = self.client.get('{}?user=TESTSTAFF'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"user":{}'.format(staff.id), count=11)
        response = self.client.get('{}?message=Created'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Created')
        self.assertNotContains(response, 'Updated')
        response = self.client.get('{}?message=UP'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Updated')
        self.assertNotContains(response, 'Created')
        response = self.client.get('{}?page=3'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Created')
        response = self.client.get('{}?page=2000'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Created')
        response = self.client.get('{}?page=-1'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Created', count=1)
        response = self.client.get(api_baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        sev = choice([10,20,30,40])
        log = Log.objects.filter(severity=sev).last()
        serialized = json.dumps(LogSerializer(log).data, separators=(",",":"))[:-1]
        self.assertContains(response, serialized)
        response = self.client.get('{}?created={}'.format(api_baseurl,log.created.year))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, serialized)
        response = self.client.get('{}?severity={}'.format(api_baseurl,sev))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, serialized)
        response = self.client.get('{}?module={}'.format(api_baseurl, log.module))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, serialized)
        response = self.client.get('{}?user={}'.format(api_baseurl, log.user))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, serialized)
        response = self.client.get('{}?message={}'.format(api_baseurl, log.message))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, serialized)
        # Negative Tests
        response = self.client.get('{}?created={}-{}-{}'.format(api_baseurl,now().year +1,now().month,now().day))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.content), 2)
        response = self.client.get('{}?severity={}'.format(api_baseurl,'banana'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.content), 2)
        response = self.client.get('{}?module={}'.format(api_baseurl, 'ims'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.content), 2)
        response = self.client.get('{}?user={}'.format(api_baseurl, 'test_bob'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.content), 2)
        response = self.client.get('{}?message={}'.format(api_baseurl, 'notarealwordorlogmessage'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.content), 206)
        response = self.client.get('{}?user=jimbob&page=10000'.format(api_baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.content), 2)
        log = Log.objects.last()
        serialized = json.dumps(LogSerializer(log).data, separators=(",",":"))[:-1]
        response = self.client.get('{}?limit=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, serialized)
        response = self.client.get('{}?limit=20&offset=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertNotContains(response, serialized)
        response = self.client.get('{}?limit=20&sort=created'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertNotContains(response, serialized)
        response = self.client.get('{}?limit=20&sort=-created'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, serialized)
        response = self.client.get('{}?offset=2000'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        self.assertNotContains(response, serialized)
        response = self.client.get('{}?limit=20&sort=-user'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        response = self.client.get('{}?sort=-banana'.format(self.baseurl))
        self.assertEqual(100, len(json.loads(response.content)))
        self.assertContains(response, serialized)

    def test_apiview_logs_pk(self):
        response = self.client.get('{}{}/'.format(self.baseurl, Log.objects.last().id))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        user = User.objects.get_or_create(username='testuser')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/'.format(self.baseurl, Log.objects.last().id))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        staff = User.objects.get_or_create(username='teststaff',is_staff=True)[0]
        self.client.force_login(staff)
        response = self.client.get('{}{}/'.format(self.baseurl, Log.objects.last().id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/'.format(self.baseurl, '-1'))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
