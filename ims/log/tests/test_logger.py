import django
django.setup()
from django.test import TestCase, override_settings
from inventory.models import Part, Storage, AlternateSKU
from log.logger import Logger, getLogger, getLevelName, _checkLevel
from log.models import Log, _levelToName, _nameToLevel, NOTSET, DEBUG, INFO, WARNING, ERROR, CRITICAL

class LoggerUnitTests(TestCase):
    def setUp(self):
        pass

    def test_getLevelName(self):
        self.assertEqual(getLevelName('NOTSET'), NOTSET)
        self.assertEqual(getLevelName('DEBUG'), DEBUG)
        self.assertEqual(getLevelName('INFO'), INFO)
        self.assertEqual(getLevelName('WARNING'), WARNING)
        self.assertEqual(getLevelName('ERROR'), ERROR)
        self.assertEqual(getLevelName('CRITICAL'), CRITICAL)
        self.assertEqual(getLevelName(NOTSET), 'NOTSET')
        self.assertEqual(getLevelName(DEBUG), 'DEBUG')
        self.assertEqual(getLevelName(INFO), 'INFO')
        self.assertEqual(getLevelName(WARNING), 'WARNING')
        self.assertEqual(getLevelName(ERROR), 'ERROR')
        self.assertEqual(getLevelName(CRITICAL), 'CRITICAL')
        self.assertEqual(getLevelName('info'), 'Level info')
        self.assertEqual(getLevelName('Banana'), 'Level Banana')
        self.assertEqual(getLevelName(12), 'Level 12')
        self.assertEqual(getLevelName(None), 'Level None')

    def test_checkLevel(self):
        self.assertEqual(_checkLevel(NOTSET), 0)
        self.assertEqual(_checkLevel(DEBUG), 10)
        self.assertEqual(_checkLevel(INFO), 20)
        self.assertEqual(_checkLevel(WARNING), 30)
        self.assertEqual(_checkLevel(ERROR), 40)
        self.assertEqual(_checkLevel(CRITICAL), 50)
        self.assertEqual(_checkLevel('NOTSET'), 0)
        self.assertEqual(_checkLevel('DEBUG'), 10)
        self.assertEqual(_checkLevel('INFO'), 20)
        self.assertEqual(_checkLevel('WARNING'), 30)
        self.assertEqual(_checkLevel('ERROR'), 40)
        self.assertEqual(_checkLevel('CRITICAL'), 50)
        self.assertEqual(_checkLevel(12), 12)
        with self.assertRaises(ValueError):
            _checkLevel('Banana')
        with self.assertRaises(TypeError):
            _checkLevel([0,1,2])
        with self.assertRaises(TypeError):
            _checkLevel(None)

    def test_getLogger(self):
        logger = getLogger('test1')
        self.assertEqual(logger.name, 'test1')
        self.assertEqual(logger.level, INFO)
        with self.assertRaises(TypeError):
            getLogger(12)
        with override_settings(LOGGING_LEVEL=None):
            logger = getLogger('test2')
            self.assertEqual(logger.name, 'test2')
            self.assertEqual(logger.level, INFO)
        with override_settings(LOGGING_LEVEL=DEBUG):
            logger = getLogger('test3')
            self.assertEqual(logger.name, 'test3')
            self.assertEqual(logger.level, INFO)
        with override_settings(LOGGING_LEVEL='DEBUG'):
            logger = getLogger('test4')
            self.assertEqual(logger.name, 'test4')
            self.assertEqual(logger.level, DEBUG)
        with override_settings(LOGGING_LEVEL=[{'name':'test5','level':'WARNING'},{'name':'test6','level':ERROR}]):
            logger = getLogger('test5')
            self.assertEqual(logger.name, 'test5')
            self.assertEqual(logger.level, WARNING)
            logger = getLogger('test6')
            self.assertEqual(logger.name, 'test6')
            self.assertEqual(logger.level, ERROR)


    def test_Logger_debug(self):
        logger = getLogger('DebugLogger')
        logger.setLevel(DEBUG)
        self.assertEqual(Log.objects.all().count(), 0)
        logger.debug(0,'debug log')
        logger.info(0,'info log')
        logger.warning(0,'warning log')
        logger.error(0,'error log')
        logger.critical(0,'critical log')
        self.assertEqual(Log.objects.all().count(), 5)
        with override_settings(LOGGING_LEVEL=[{'name':'DebugLogger','level':10},{'name':'InfoLogger','level':20},{'name':'WarningLogger','level':30},{'name':'ErrorLogger','level':40},{'name':'CriticalLogger','level':50},]):
            Log.objects.all().delete()
            logger = getLogger('DebugLogger')
            logger.debug(0,'debug log')
            logger.info(0,'info log')
            logger.warning(0,'warning log')
            logger.error(0,'error log')
            logger.critical(0,'critical log')
            self.assertEqual(Log.objects.all().count(), 5)

    def test_Logger_info(self):
        logger = getLogger('InfoLogger')
        logger.setLevel(INFO)
        self.assertEqual(Log.objects.all().count(), 0)
        logger.debug(0,'debug log')
        logger.info(0,'info log')
        logger.warning(0,'warning log')
        logger.error(0,'error log')
        logger.critical(0,'critical log')
        self.assertEqual(Log.objects.all().count(), 4)
        with override_settings(LOGGING_LEVEL=[{'name':'DebugLogger','level':10},{'name':'InfoLogger','level':20},{'name':'WarningLogger','level':30},{'name':'ErrorLogger','level':40},{'name':'CriticalLogger','level':50},]):
            Log.objects.all().delete()
            logger = getLogger('InfoLogger')
            logger.debug(0,'debug log')
            logger.info(0,'info log')
            logger.warning(0,'warning log')
            logger.error(0,'error log')
            logger.critical(0,'critical log')
            self.assertEqual(Log.objects.all().count(), 4)

    def test_Logger_warning(self):
        logger = getLogger('WarningLogger')
        logger.setLevel(WARNING)
        self.assertEqual(Log.objects.all().count(), 0)
        logger.debug(0,'debug log')
        logger.info(0,'info log')
        logger.warning(0,'warning log')
        logger.error(0,'error log')
        logger.critical(0,'critical log')
        self.assertEqual(Log.objects.all().count(), 3)
        with override_settings(LOGGING_LEVEL=[{'name':'DebugLogger','level':10},{'name':'InfoLogger','level':20},{'name':'WarningLogger','level':30},{'name':'ErrorLogger','level':40},{'name':'CriticalLogger','level':50},]):
            Log.objects.all().delete()
            logger = getLogger('WarningLogger')
            logger.debug(0,'debug log')
            logger.info(0,'info log')
            logger.warning(0,'warning log')
            logger.error(0,'error log')
            logger.critical(0,'critical log')
            self.assertEqual(Log.objects.all().count(), 3)

    def test_Logger_error(self):
        logger = getLogger('ErrorLogger')
        logger.setLevel(ERROR)
        self.assertEqual(Log.objects.all().count(), 0)
        logger.debug(0,'debug log')
        logger.info(0,'info log')
        logger.warning(0,'warning log')
        logger.error(0,'error log')
        logger.critical(0,'critical log')
        self.assertEqual(Log.objects.all().count(), 2)
        with override_settings(LOGGING_LEVEL=[{'name':'DebugLogger','level':10},{'name':'InfoLogger','level':20},{'name':'WarningLogger','level':30},{'name':'ErrorLogger','level':40},{'name':'CriticalLogger','level':50},]):
            Log.objects.all().delete()
            logger = getLogger('ErrorLogger')
            logger.debug(0,'debug log')
            logger.info(0,'info log')
            logger.warning(0,'warning log')
            logger.error(0,'error log')
            logger.critical(0,'critical log')
            self.assertEqual(Log.objects.all().count(), 2)

    def test_Logger_critical(self):
        logger = getLogger('CriticalLogger')
        logger.setLevel(CRITICAL)
        self.assertEqual(Log.objects.all().count(), 0)
        logger.debug(0,'debug log')
        logger.info(0,'info log')
        logger.warning(0,'warning log')
        logger.error(0,'error log')
        logger.critical(0,'critical log')
        self.assertEqual(Log.objects.all().count(), 1)
        with override_settings(LOGGING_LEVEL=[{'name':'DebugLogger','level':10},{'name':'InfoLogger','level':20},{'name':'WarningLogger','level':30},{'name':'ErrorLogger','level':40},{'name':'CriticalLogger','level':50},]):
            Log.objects.all().delete()
            logger = getLogger('CriticalLogger')
            logger.debug(0,'debug log')
            logger.info(0,'info log')
            logger.warning(0,'warning log')
            logger.error(0,'error log')
            logger.critical(0,'critical log')
            self.assertEqual(Log.objects.all().count(), 1)

    def test_Logger_notset(self):
        logger = getLogger('NotSetLogger')
        logger.setLevel(NOTSET)
        self.assertEqual(Log.objects.all().count(), 0)
        logger.debug(0,'debug log')
        logger.info(0,'info log')
        logger.warning(0,'warning log')
        logger.error(0,'error log')
        logger.critical(0,'critical log')
        self.assertEqual(Log.objects.all().count(), 5)
        with override_settings(LOGGING_LEVEL=[{'name':'DebugLogger','level':10},{'name':'InfoLogger','level':20},{'name':'WarningLogger','level':30},{'name':'ErrorLogger','level':40},{'name':'CriticalLogger','level':50},]):
            Log.objects.all().delete()
            logger = getLogger('NotSetLogger')
            logger.debug(0,'debug log')
            logger.info(0,'info log')
            logger.warning(0,'warning log')
            logger.error(0,'error log')
            logger.critical(0,'critical log')
            self.assertEqual(Log.objects.all().count(), 4)

    def test_Logger_custom(self):
        logger = getLogger('ZeroLogger')
        logger.setLevel(0)
        self.assertEqual(Log.objects.all().count(), 0)
        logger.log(1,0,'verbose verbose log')
        logger.log(5,0,'verbose log')
        logger.debug(0,'debug log')
        logger.info(0,'info log')
        logger.log(25,0,'yellow alert log')
        logger.warning(0,'warning log')
        logger.log(39,0,'red alert log')
        logger.error(0,'error log')
        logger.critical(0,'critical log')
        logger.log(51,0,'more than critical log')
        self.assertEqual(Log.objects.all().count(), 10)

    def test_Logger_UserDoesNotExist(self):
        logger = getLogger('BadLogger')
        self.assertEqual(Log.objects.all().count(), 0)
        logger.setLevel(-1) # Cause this should work but is not recommended
        logger.debug(100, 'debug log gonna make ERROR and CRITICAL logs instead')
        logger.info(101, 'info log gonna make ERROR and CRITICAL logs instead')
        logger.warning(102, 'warning log gonna make ERROR and CRITICAL logs instead')
        logger.error(103, 'error log gonna make ERROR and CRITICAL logs instead')
        logger.critical(104, 'critical log gonna make ERROR and CRITICAL logs instead')
        logger.log(NOTSET, 105, 'notset log gonna make ERROR and CRITICAL logs instead')
        self.assertEqual(Log.objects.all().count(), 12)

    def test_Logger_bad_logs(self):
        logger = getLogger('BadLogger2')
        with self.assertRaises(TypeError):
            logger.log([0,1],0,'bad log level')
        with self.assertRaises(TypeError):
            logger.log(0,[99],'bad log user')