from django.contrib import admin
from log.models import Log

class LogAdmin(admin.ModelAdmin):
    list_display = ['created', 'severity', 'module', 'user', 'message']
    search_fields = ['severity', 'module', 'user', 'message']
    list_filter = ['severity', 'module', 'user']

admin.site.register(Log, LogAdmin)
