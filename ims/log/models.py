from django.contrib.auth.models import User
from django.db import models

CRITICAL = 50
ERROR = 40
WARNING = 30
INFO = 20
DEBUG = 10
NOTSET = 0
_levelToName = {
    CRITICAL: 'CRITICAL',
    ERROR: 'ERROR',
    WARNING: 'WARNING',
    INFO: 'INFO',
    DEBUG: 'DEBUG',
    NOTSET: 'NOTSET',
}
_nameToLevel = {
    'CRITICAL': CRITICAL,
    'ERROR': ERROR,
    'WARNING': WARNING,
    'INFO': INFO,
    'DEBUG': DEBUG,
    'NOTSET': NOTSET,
}
SEVERITY = [
    (DEBUG,'DEBUG'),
    (INFO,'INFO'),
    (WARNING,'WARNING'),
    (ERROR,'ERROR'),
    (CRITICAL,'CRITICAL'),
]

class Log(models.Model):
    severity = models.CharField(max_length=2,choices=SEVERITY,default=INFO,editable=False)
    created = models.DateTimeField(auto_now_add=True,editable=False)
    module = models.CharField(max_length=50,editable=False)
    user = models.ForeignKey(User,on_delete=models.SET_NULL,blank=True,null=True,related_name='+',editable=False)
    message = models.TextField(editable=False)
    
    class Meta:
        ordering = ['created']

    def get_severity(self):
        return _levelToName.get(int(self.severity))

