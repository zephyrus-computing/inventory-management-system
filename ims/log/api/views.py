from django.conf import settings
from django.core.exceptions import FieldError
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
import logging

from inventory.models import IsInt
from inventory.search import Search
from log.api.serializers import LogSerializer
from log.logger import getLogger
from log.models import Log
import inventory.helpers as helpers

logger = logging.getLogger(__name__)
logger2 = getLogger('log-api')

class LogList(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug(request.user, 'GET Request to {} API'.format(cname))
        if request.user.is_staff:
            logger.debug('Generating list of Log')
            logs = Log.objects.all().order_by('-created')
            logmsg = 'Accessed Logs List API with filters:'
            for field in ['created','severity','module','user','message']:
                value = request.GET.get(field)
                if value != None:
                    logger.debug('Filtering list of Log with {} query'.format(field))
                    logmsg += ' {}=\'{}\','.format(field, value)
                    if field == 'user':
                        sf = Search().filter(['user__id','user__username','user__first_name','user__last_name','user__email'], value)
                    else:
                        sf = Search().filter([field], value)
                    logs = logs.filter(sf)
            sort = request.GET.get('sort')
            if sort != None:
                try:
                    logs = logs.order_by(sort)
                    logmsg += ' sort=\'{}\','.format(sort)
                except FieldError:
                    logger.warning('Unable to sort Log based on field "{}"'.format(sort))
                    logger2.warning(request.user, 'Unable to sort Log based on field "{}"'.format(sort))
            limit = request.GET.get('limit')
            if not helpers.is_int(limit): limit = settings.DEFAULT_API_RESULTS
            else: limit = int(limit)
            offset = request.GET.get('offset')
            if not helpers.is_int(offset): offset = 0
            else: offset = int(offset)
            limit += offset
            logmsg += ' limit=\'{}\', offset=\'{}\''.format(limit, offset)
            logs = logs[offset:limit]
            logger.debug('Responding (200) to request for {} APIView'.format(cname))
            if request.GET:
                logger2.info(request.user, logmsg)
            else:
                logger2.info(request.user, 'Accessed Logs List API')
            return Response(LogSerializer(logs, many=True).data, status.HTTP_200_OK)
        else:
            return Response({}, status.HTTP_401_UNAUTHORIZED)

class LogDetail(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug(request.user, 'GET Request to {} API'.format(cname))
        if request.user.is_staff:
            logger.debug('Getting requested Log')
            log = get_object_or_404(Log, id=id)
            logger.debug('Responding (200) to request for {} APIView'.format(cname))
            logger2.info(request.user, 'Accessed Log Detail for id {}'.format(cname, id))
            return Response(LogSerializer(log).data, status.HTTP_200_OK)
        else:
            return Response({}, status.HTTP_401_UNAUTHORIZED)
