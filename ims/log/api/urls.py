from django.urls import re_path
from log.api import views

app_name = 'logapi'
urlpatterns = [
    re_path(r'^$', views.LogList.as_view(), name='log_list'),
    re_path(r'^(?P<id>[\.\d]+)/$', views.LogDetail.as_view(), name='log_detail'),
]
