from django.contrib.auth.decorators import login_required,permission_required
from django.conf import settings
from django.core.exceptions import PermissionDenied, FieldError
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404, redirect
from itertools import chain
import logging

from inventory.search import Search
from log.logger import getLogger, getLevelName
from log.models import Log

logger = logging.getLogger(__name__)
logger2 = getLogger("log")

@permission_required('log.view_log')
def log_list(request):
    logger.debug('Request made to log_list view')
    logger.debug('Generating object_list of Part')
    logger2.debug(request.user, 'Request made to log_list view')
    object_list = Log.objects.all().order_by('-created')
    dateTime = request.GET.get('datetime')
    if dateTime != None:
        logger.debug('Filtering object_list of Log with DateTime query')
        sf = Search().filter(['created'], dateTime)
        object_list = object_list.filter(sf)
    severity = request.GET.get('severity')
    if severity != None:
        logger.debug('Filtering object_list of Log with Severity query')
        level = getLevelName(severity.upper())
        sf = Search().filter(['severity'], str(level))
        object_list = object_list.filter(sf)
    module = request.GET.get('module')
    if module != None:
        logger.debug('Filtering object_list of Log with Module query')
        sf = Search().filter(['module'], module)
        object_list = object_list.filter(sf)
    user = request.GET.get('user')
    if user != None and user != "None":
        logger.debug('Filtering object_list of Log with User query')
        sf = Search().filter(['user__username'], user)
        object_list = object_list.filter(sf)
    message = request.GET.get('message')
    if message != None:
        logger.debug('Filtering object_list of Log with Message query')
        sf = Search().filter(['message'], message)
        object_list = object_list.filter(sf)
    logger.debug('Paginate object_list of Log')
    paginator = Paginator(object_list, settings.LOG_PAGE_SIZE)
    page = request.GET.get('page')
    # Try to display the page or catch and process the errors
    try:
        logs = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        logs = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page ' + str(paginator.num_pages))
        logs = paginator.page(paginator.num_pages)
    # Return the list of parts
    logger.info('Rendering request for log_list view')
    if request.GET:
        logmessage = 'Accessed Logs with filters: '
        if page is not None:
            logmessage += 'page=\'{}\','.format(page)
        if dateTime is not None:
            logmessage += 'datetime=\'{}\','.format(dateTime)
        if severity is not None:
            logmessage += 'severity=\'{}\','.format(severity)
        if module is not None:
            logmessage += 'module=\'{}\','.format(module)
        if user is not None:
            logmessage += 'user=\'{}\','.format(user)
        if message is not None:
            logmessage += 'message=\'{}\''.format(message)
        logger2.info(request.user, logmessage.strip(','))
    else:
        logger2.info(request.user, 'Accessed Logs')
    return render(request, 'log/list.html', {'page': page, 'logs': logs, 'DateTime': dateTime, 'Severity': severity, 'Module': module, 'User': user, 'Message': message })
