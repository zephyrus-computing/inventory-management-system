from django.conf import settings
from django.contrib.auth.models import User, Group, Permission
from django.core.exceptions import FieldError
from django.core.mail import send_mail
from django.shortcuts import render
import logging

from accounts.models import Invite, Token
from accounts.forms import InviteForm, TokenForm, ExtendedUserCreationForm, GroupCreationForm
from inventory.helpers import is_int
from inventory.search import Search
from log.logger import getLogger

logger = logging.getLogger(__name__)
logger2 = getLogger('accounts')

# Return parsed permission name
def get_admin_permission(app, model, action):
    return '{}.{}_{}'.format(app.lower(), action.lower(), model.lower())

# Return a specific object by id
def get_object(objtype, objid):
    logger.debug('get_object called')
    try:
        if objtype == 'Group':
            return Group.objects.get(id=objid)
        if objtype == 'Invite':
            return Invite.objects.get(id=objid)
        if objtype == 'Token':
            return Token.objects.get(id=objid)
        if objtype == 'User':
            return User.objects.get(id=objid)
        return ''
    except Exception as err:
        if type(err) in [Group.DoesNotExist, Invite.DoesNotExist, Token.DoesNotExist, User.DoesNotExist]:
            logger.warning('Unable to locate {} with id {}. Error message: {}'.format(objtype, objid, err))
        else:
            logger.warning('There was an unknown problem in processing get_object')
            logger.debug(err)
        raise
    finally:
        logger.debug('get_object completed')

# Return list of objects of the specified type
def get_object_list(objtype):
    logger.debug('get_object_list called')
    try:
        if objtype == 'Group':
            return Group.objects.all()
        if objtype == 'Invite':
            return Invite.objects.all()
        if objtype == 'Token':
            return Token.objects.all()
        if objtype == 'User':
            return User.objects.all()
        return ''
    except Exception as err:
        logger.warning('There was an unknown problem in processing get_object_list')
        logger.debug(err)
        raise
    finally:
        logger.debug('get_object_list completed')

# Return list of fields for use with Search queries
def get_search_fields(model):
    logger.debug('get_search_fields called')
    try:
        if model == 'Group':
            return ['name']
        if model == 'Invite':
            return ['email','group__name','expires']
        if model == 'User':
            return ['username','first_name','last_name','email']
        if model == 'Token':
            return ['user__username','user__first_name','user__last_name','user__email']
        return []
    except Exception as err:
        logger.warning('There was an unknown problem in processing get_search_fields')
        logger.debug(err)
        raise
    finally:
        logger.debug('get_search_fields completed')
    

# Return a Form object for a Patch/Post/Put request
def get_admin_form_p(model, data=None, instance=None):
    logger.debug('get_admin_form_p called')
    try:
        if model == 'Group':
            form = GroupCreationForm(data=data,instance=instance)
        if model == 'Invite':
            form = InviteForm(data=data,instance=instance)
        if model == 'Token':
            form = TokenForm(data=data,instance=instance)
        if model == 'User':
            form = ExtendedUserCreationForm(data=data,instance=instance)
        return form
    except Exception as err:
        logger.warning('There was an unknown problem in processing get_admin_form_p')
        logger.debug(err)
        raise
    finally:
        logger.debug('get_admin_form completed')

# Return a Form object for a Get request
def get_admin_form_g(model, GET=None, instance=None):
    logger.debug('get_admin_form_g called')
    try:
        if model == 'Group':
            form = GroupCreationForm(initial=GET,instance=instance)
        if model == 'Invite':
            form = InviteForm(initial=GET,instance=instance)
        if model == 'Token':
            form = TokenForm(initial=GET,instance=instance)
        if model == 'User':
            form = ExtendedUserCreationForm(initial=GET,instance=instance)
        return form
    except Exception as err:
        logger.warning('There was an unknown problem in processing get_admin_form_g')
        logger.debug(err)
        raise
    finally:
        logger.debug('get_admin_form completed')

# Return the name of an object from the form data
def get_form_object_name(form):
    logger.debug('get_form_object_name called')
    try:
        if form.__class__.__name__ == 'GroupCreationForm':
            return '{}'.format(form.cleaned_data['name'])
        if form.__class__.__name__ == 'InviteForm':
            return '{}'.format(form.cleaned_data['email'])
        if form.__class__.__name__ == 'TokenForm':
            return '{}'.format(form.cleaned_data['user'])
        if form.__class__.__name__ == 'ExtendedUserCreationForm':
            return '{}'.format(form.cleaned_data['username'])
        return ''
    except Exception as err:
        logger.warning('There was an unknown problem in processing get_form_object_name')
        logger.debug(err)
        raise
    finally:
        logger.debug('get_form_object_name completed')

# Send Email message
def send_email_from_template(to_addrs, template_file, from_addr = settings.DEFAULT_FROM_EMAIL, subject = settings.EMAIL_DEFAULT_SUBJECT, **kwargs):
    mail_sent = -1
    cname = 'send_email_from_template'
    logger.debug('{} called'.format(cname))
    try:
        context = {key: value for key, value in kwargs.items()}
        content = render(None, template_file, context).content.decode('utf-8')
        if settings.EMAIL_HOST_USER != '' and settings.EMAIL_HOST_PASSWORD != '':
            mail_sent = send_mail(subject, '', from_addr, to_addrs, auth_user=settings.EMAIL_HOST_USER, auth_password=settings.EMAIL_HOST_PASSWORD, html_message=content)
        else:
            mail_sent = send_mail(subject, '', from_addr, to_addrs, html_message=content)
    except Exception as err:
        logger.warning('There was an Error while processing {}. Error Message: {}'.format(cname, err))
        mail_sent = -1
    finally:
        logger.debug('{} completed'.format(cname))
        return mail_sent

# Compares the difference of two lists and returns items to be removed and added
def compare_lists(old, new):
    to_be_removed = []
    to_be_added = []
    for obj in old:
        if not new.contains(obj):
            to_be_removed.append(obj)
    for obj in new:
        if not old.contains(obj):
            to_be_added.append(obj)
    return to_be_removed, to_be_added

def compare_groups(user, selected):
    existing_groups = user.groups.all()
    ids = []
    for group in selected:
        if not group in ids:
            ids.append(group)
    selected_groups = Group.objects.filter(id__in=ids)
    return compare_lists(existing_groups, selected_groups)

def compare_permissions(obj, selected):
    if obj.__class__.__name__ == 'User':
        existing_perms = obj.user_permissions.all()
    elif obj.__class__.__name__ == 'Group':
        existing_perms = obj.permissions.all()
    else:
        return [], []
    ids = []
    for perm in selected:
        if not perm in ids:
            ids.append(perm)
    selected_perms = Permission.objects.filter(id__in=ids)
    return compare_lists(existing_perms, selected_perms)

# Return the query type for use with Search
def parse_querytype(value):
    if value == 1 or value == '1':
        return "^"
    if value == 2 or value == '2':
        return "="
    return ""

# Apply filters to an API request
def filter_api_request(request, logmsg, model, objects):
    logger.debug('filter_api_request called')
    fields = get_search_fields(model)
    query = request.GET.get('search')
    if query != None:
        logmsg += ' query={},'.format(query)
        logger.debug('Filtering list of {} with query'.format(model))
        sf = Search().filter(fields, query)
        objects = objects.filter(sf)
    for field in fields:
        value = request.GET.get(field)
        if value != None:
            logmsg += ' {}={},'.format(field, value)
            logger.debug('Filtering list of {} with query on field "{}"'.format(model, field))
            sf = Search().filter([field], value)
            objects = objects.filter(sf)
    sort = request.GET.get('sort')
    if sort != None:
        try:
            objects = objects.order_by(sort)
            logmsg += ' sort={},'.format(sort)
        except FieldError:
            logger.warning('Unable to sort {} based on field "{}". Request made by {}'.format(model, sort, (request.user or 0)))
    limit = request.GET.get('limit')
    if not is_int(limit): limit = settings.DEFAULT_API_RESULTS
    else: limit = int(limit)
    offset = request.GET.get('offset')
    if not is_int(offset): offset = 0
    else: offset = int(offset)
    logmsg += ' limit={}, offset={}'.format(limit, offset)
    limit += offset
    objects = objects[offset:limit]
    logger.debug('filter_api_request completed')
    return logmsg, objects

# For future use with IP abuse restrictions
# def get_client_ip(request):
#     x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
#     if x_forwarded_for:
#         ip = x_forwarded_for.split(',')[-1].strip()
#     else:
#         ip = request.META.get('REMOTE_ADDR')
#     return ip
