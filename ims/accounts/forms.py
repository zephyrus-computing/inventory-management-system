from datetime import timedelta
from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.forms import UsernameField
from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

from accounts.models import Invite, Token

class ExtendedUserCreationForm(forms.ModelForm):
    error_messages = {
        "password_mismatch": _("The two password fields didn't match."),
        "invalid_email": _("Format of email address is not valid."),
    }
    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label=_("Confirm Password"),
        strip=False,
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
        help_text=_("Enter the same password as before, for verification"),
    )
    email = forms.CharField(
        label=_("Email address"),
        strip=False,
        widget=forms.EmailInput(attrs={"autocomplete": "email"}),
    )
    first_name = forms.CharField(
        label=_("First name"),
        strip=False,
        widget=forms.TextInput(attrs={"autocomplete": "first-name"}),
    )
    last_name = forms.CharField(
        label=_("Last name"),
        strip=False,
        widget=forms.TextInput(attrs={"autocomplete": "given-name"}),
    )

    class Meta:
        model = User
        fields = ("username","first_name","last_name","email",)
        field_classes = {"username": UsernameField}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self._meta.model.USERNAME_FIELD in self.fields:
            self.fields[self._meta.model.USERNAME_FIELD].widget.attrs["autofocus"] = True
    
    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise ValidationError(
                self.error_messages["password_mismatch"],
                code="password_mismatch",
            )
        return password2
    
    def _post_clean(self):
        super()._post_clean()
        password = self.cleaned_data.get("password2")
        if password:
            try:
                password_validation.validate_password(password, self.instance)
            except ValidationError as error:
                self.add_error("password2", error)

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
            if hasattr(self, "save_m2m"):
                self.save_m2m()
        return user
    
    def clean_username(self):
        username = self.cleaned_data.get("username")
        if (
            username
            and self._meta.model.objects.filter(username__iexact=username).exists()
        ):
            self._update_errors(
                ValidationError(
                    {
                        "username": self.instance.unique_error_message(
                            self._meta.model, ["username"]
                        )
                    }
                )
            )
        else:
            return username

class GroupCreationForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ("name",)

    # def clean_name(self):
    #     name = self.cleaned_data.get("name")
    #     if 'name' in self.changed_data:
    #         if (
    #             name
    #             and self._meta.model.objects.filter(username__iexact=name).exists()
    #         ):
    #             self._update_errors(
    #                 ValidationError(
    #                     {
    #                         "name": self.instance.unique_error_message(
    #                             self._meta.model, ["name"]
    #                         )
    #                     }
    #                 )
    #             )
    #     return name

class UserCreationAdminForm(forms.ModelForm):
    error_messages = {
        "invalid_email": _("Format of email address is not valid."),
    }
    password = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.TextInput(attrs={"autocomplete": "password"}),
    )
    email = forms.CharField(
        label=_("Email address"),
        strip=False,
        widget=forms.EmailInput(attrs={"autocomplete": "email"}),
    )
    first_name = forms.CharField(
        label=_("First name"),
        strip=False,
        widget=forms.TextInput(attrs={"autocomplete": "first-name"}),
    )
    last_name = forms.CharField(
        label=_("Last name"),
        strip=False,
        widget=forms.TextInput(attrs={"autocomplete": "given-name"}),
    )
    is_active = forms.BooleanField(
        label=_("Enabled"),
    )

    class Meta:
        model = User
        fields = ("username","first_name","last_name","email","is_active")
        field_classes = {"username": UsernameField}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self._meta.model.USERNAME_FIELD in self.fields:
            self.fields[self._meta.model.USERNAME_FIELD].widget.attrs["autofocus"] = True
        self.fields['first_name'].required = False
        self.fields['last_name'].required = False
        self.fields['is_active'].required = False
        if self.instance:
            self.fields.pop('password')
        else:
            self.fields['password'].initial = User.objects.make_random_password()

    def save(self, commit=True):
        user = super().save(commit=False)
        if 'password' in self.cleaned_data.keys():
            user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
            if hasattr(self, "save_m2m"):
                self.save_m2m()
        return user
    
    def clean_username(self):
        username = self.cleaned_data.get("username")
        if 'username' in self.changed_data:
            if (
                username
                and self._meta.model.objects.filter(username__iexact=username).exists()
            ):
                self._update_errors(
                    ValidationError(
                        {
                            "username": self.instance.unique_error_message(
                                self._meta.model, ["username"]
                            )
                        }
                    )
                )
        return username
    
class UserEditForm(forms.ModelForm):
    error_messages = {
        "invalid_email": _("Format of email address is not valid."),
    }
    email = forms.CharField(
        label=_("Email address"),
        strip=False,
        widget=forms.EmailInput(attrs={"autocomplete": "email"}),
    )
    first_name = forms.CharField(
        label=_("First name"),
        strip=False,
        widget=forms.TextInput(attrs={"autocomplete": "first-name"}),
    )
    last_name = forms.CharField(
        label=_("Last name"),
        strip=False,
        widget=forms.TextInput(attrs={"autocomplete": "given-name"}),
    )

    class Meta:
        model = User
        fields = ("first_name","last_name","email")
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        user = super().save(commit=False)
        if commit:
            user.save()
            if hasattr(self, "save_m2m"):
                self.save_m2m()
        return user
   
class UserPasswordForm(forms.Form):
    error_messages = {
        "password_mismatch": _("The two password fields didn't match."),
    }
    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label=_("Confirm Password"),
        strip=False,
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
        help_text=_("Enter the same password as before, for verification"),
    )
    
    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise ValidationError(
                self.error_messages["password_mismatch"],
                code="password_mismatch",
            )
        return password2
    
    def _post_clean(self):
        super()._post_clean()
        password = self.cleaned_data.get("password2")
        if password:
            try:
                password_validation.validate_password(password)
            except ValidationError as error:
                self.add_error("password2", error)
        
class UserGroupForm(forms.Form):
    groups = forms.MultipleChoiceField(
        label=_("Groups"),
        widget=forms.SelectMultiple()
    )
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        _choices = []
        _groups = Group.objects.order_by('name')
        for g in _groups:
            _choices.append((g.id, g.name))
        self.fields['groups'].choices = _choices
        self.fields['groups'].required = False
        self.fields['groups'].widget.attrs.update({"data-multi-select":""})
        self.fields['groups'].widget.attrs.update({"data-placeholder":"Groups"})

class PermissionForm(forms.Form):
    perms = forms.MultipleChoiceField(
        label=_("Permissions"),
    )
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        _choices = []
        _perms = Permission.objects.filter(content_type__app_label__in=['inventory','accounts','log','auth'])
        for p in _perms:
            _choices.append((p.id, p.name))
        self.fields['perms'].choices = _choices
        self.fields['perms'].required = False
        self.fields['perms'].widget.attrs.update({"data-multi-select":""})
        self.fields['perms'].widget.attrs.update({"data-placeholder":"Permissions"})

class UserMetadataForm(forms.Form):
    must_change_password = forms.BooleanField(
        label=_("Force Password Change"),
        required=False
    )
    bad_password_attempts = forms.CharField(
        label=_("Bad Password Attempts"),
        strip=False,
        widget=forms.TextInput(attrs={"readonly": "true"}),
        required=False
    )
    last_bad_password_datetime = forms.CharField(
        label=_("Last Bad Password Attempt"),
        strip=False,
        widget=forms.TextInput(attrs={"readonly": "true"}),
        required=False
    )
    last_lockout_datetime = forms.CharField(
        label=_("Last Lockout"),
        strip=False,
        widget=forms.TextInput(attrs={"readonly": "true"}),
        required=False
    )
    last_set_password_datetime = forms.CharField(
        label=_("Password Last Set"),
        strip=False,
        widget=forms.TextInput(attrs={"readonly": "true"}),
        required=False
    )


class InviteForm(forms.ModelForm):
    class Meta:
        model = Invite
        fields = ("email","group","expires",)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['expires'] = forms.DateTimeField(widget=forms.TextInput,initial=(now() + timedelta(hours=1)).strftime("%Y-%m-%d %H:%M:%S"))
        self.fields['expires'].required = False

class TokenForm(forms.ModelForm):
    class Meta:
        model = Token
        fields = ("user","expires")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['expires'] = forms.DateTimeField(widget=forms.TextInput,initial=(now() + timedelta(days=60)).strftime("%Y-%m-%d %H:%M:%S"))
        self.fields['expires'].required = False