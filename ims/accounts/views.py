from datetime import timedelta
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, get_object_or_404, redirect
from django.template.response import TemplateResponse
from django.utils.timezone import now
from mfa.views import LoginView as MfaLoginView
import logging

from accounts.forms import ExtendedUserCreationForm, UserEditForm
from accounts.models import Invite, Token
from log.logger import getLogger

logger = logging.getLogger(__name__)
logger2 = getLogger('accounts')

@login_required()
def user_profile(request):
    cname = 'user_profile'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view User Profile')
    api_keys = ""
    if settings.ALLOW_USER_API_KEYS:
        api_keys = Token.objects.filter(user=request.user)
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, 'Accessed User Profile')
    return render(request, 'registration/profile.html', {'apikeys': api_keys})

@login_required()
def user_edit(request):
    cname = 'user_edit'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to edit User Profile')
    if not settings.ALLOW_USER_EDIT_PROFILE:
        logger2.info(request.user, 'Attempted to update User Profile, but feature is disabled')
        return redirect('/accounts/profile/')
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        user_form = UserEditForm(request.POST, instance=request.user)
        if user_form.is_valid():
            logger.debug('{} is valid.'.format(user_form.__class__.__name__))
            logger.info('Updated User Profile')
            user_form.save()
            logger2.info(request.user, 'Updated User Profile')
            return redirect('/accounts/profile/')
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'User Edit Form not valid')
        return render(request, 'registration/edit_profile.html', {'name': request.user.username, 'user_form': user_form})
    user_form = UserEditForm(instance=request.user)
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, 'Accessed Edit User Profile')
    return render(request, 'registration/edit_profile.html', {'name': request.user.username, 'user_form': user_form})

def logout_view(request):
    if request.user.is_authenticated:
        logger2.info(request.user, 'User Logged Off')
        logout(request)
    return redirect('/accounts/login/?next=/inventory/')

def sign_up(request):
    cname = 'sign_up'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug((request.user or 0), 'Request to view Sign Up page')
    if not settings.ALLOW_USER_SIGN_UP:
        logger2.info((request.user or 0), 'Attempted to Sign Up, but feature is disabled')
        return redirect('/accounts/login/?next=/inventory/')
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        form = ExtendedUserCreationForm(request.POST)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Creating new User "{}"'.format(form.cleaned_data.get('username')))
            user = form.save()
            logger2.info(user, 'Self Service Sign Up')
            return redirect('/accounts/profile/')
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning((request.user or 0), 'Create User Form not valid')
        return render(request, 'registration/signup.html', {'form': form})
    form = ExtendedUserCreationForm()
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info((request.user or 0), 'Accessed Sign Up Page')
    return render(request, 'registration/signup.html', {'form': form})

def invite_code(request, token):
    cname = 'invite_code'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug((request.user or 0), 'Request to view Invite Code page')
    try:
        invite = Invite.objects.get(code=token)
        form = ExtendedUserCreationForm(initial={'email':invite.email,'username':invite.email.split('@')[0]})
    except Invite.DoesNotExist:
        logger.debug('Invalid code with request made to {} view'.format(cname))
        logger2.warning((request.user or 0), 'Request made to Invite Code page with invalid code')
        return render(request, 'registration/signup.html', {'message': 'Invite has expired. Please request another or visit the Sign Up page.'})
    if invite.expires != None and invite.expires.timestamp() < now().timestamp():
        logger.debug('Expired code with request made to {} view'.format(cname))
        logger2.warning((request.user or 0), 'Request made to Invite Code page with an expired code')
        return render(request, 'registration/signup.html', {'message': 'Invite has expired. Please request another or visit the Sign Up page.'})
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        form = ExtendedUserCreationForm(request.POST)
        if form.is_valid():
            if form.cleaned_data.get('email') != invite.email:
                logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
                logger2.warning((request.user or 0), 'Create User Form not valid')
                return render(request, 'registration/signup.html', {'form': form, 'message': 'Email Address does not match invite.'})
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Creating new User "{}"'.format(form.cleaned_data.get('username')))
            user = form.save()
            logger2.info(user, 'Self Service Sign Up via Invite')
            user.groups.add(invite.group)
            logger.info('Added User "{}" to group "{}"'.format(user.username, invite.group.name))
            logger2.info(user, 'Added to group "{}" via Invite'.format(invite.group.name))
            invite.delete()
            return redirect('/accounts/profile/')
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning((request.user or 0), 'Create User Form not valid')
        return render(request, 'registration/signup.html', {'form': form})
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info((request.user or 0), 'Accessed Signup Page')
    return render(request, 'registration/signup.html', {'form': form})

def ajax_create_token(request):
    cname = 'ajax_create_token'
    logger.debug('Request made to {} view'.format(cname))
    if not request.user.is_authenticated:
        logger.debug('Access was denied to {} view'.format(cname))
        logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
        return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Access Denied'})
    if settings.ALLOW_USER_API_KEYS:
        logger2.debug(request.user, 'Request to create API Token')
        Token.objects.create(user=request.user, expires=now() + timedelta(days=settings.DEFAULT_API_KEY_EXPIRATION))
        logger.info('Created new API Key')
        logger2.info(request.user, 'Created new API Key')
        return render(request, 'inventory/ajax_result.html', {'color': 'green', 'result': 'Created New API Key'})
    return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Access Denied'})

def ajax_view_tokens(request):
    cname = 'ajax_view_token'
    logger.debug('Request made to {} view'.format(cname))
    if not request.user.is_authenticated:
        logger.debug('Access was denied to {} view'.format(cname))
        logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
        return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Access Denied'})
    if settings.ALLOW_USER_API_KEYS:
        logger2.debug(request.user, 'Request to retrieve available API Tokens')
        tokens = Token.objects.filter(user=request.user)
        logger2.info(request.user, 'Accessed available API Tokens')
        return render(request, 'inventory/ajax_result2.html', {'type': 'token', 'apikeys': tokens})
    return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Access Denied'})

@login_required()
def view_token(request, id):
    cname = 'view_token'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view User Token')
    if settings.ALLOW_USER_API_KEYS:
        key = Token.objects.get(id=id)
        if key.user != request.user:
            logger.warning('Request for token id {} by unassociated user {}!'.format(id, request.user))
            logger2.warning(request.user, 'Unauthorized attempt to access Token id {}'.format(id))
            return redirect('/accounts/profile/')
    else:
        logger.warning('Request for token id {} by {}. ALLOW_USER_API_KEYS is disabled'.format(id, request.user))
        logger2.info(request.user, 'Attempt to access Token id {} when ALLOW_USER_API_KEYS is disabled'.format(id))
        return redirect('/accounts/profile/')
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, 'Accessed User Token')
    return render(request, 'registration/api_key.html', {'key': key})

def ajax_delete_token(request, id):
    cname = 'ajax_delete_token'
    logger.debug('Request made to {} view'.format(cname))
    if not request.user.is_authenticated:
        logger.debug('Access was denied to {} view'.format(cname))
        logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
        return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Access Denied'})
    if settings.ALLOW_USER_API_KEYS:
        logger2.debug((request.user or 0), 'Request to delete API Token')
        token = get_object_or_404(Token, id=id)
        if token.user != request.user:
            logger.debug('Requesting and Token User mismatch to {} view'.format(cname))
            logger2.warning(0, 'User mismatch with request to {}'.format(request.path))
            return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Access Denied'})
        token.delete()
        logger.info('Deleted API Key')
        logger2.info(request.user, 'Deleted API Key')
        return render(request, 'inventory/ajax_result.html', {'color': 'green', 'result': 'Deleted API Key'})
    return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Access Denied'})

class LoginView(MfaLoginView):
    def get(self, request, *args, **kwargs):
        logger.debug('Request made to LoginView')
        logger2.info((request.user or 0), 'Accessed Login Page')
        return super().get(request, args, kwargs)
    
    def post(self, request, *args, **kwargs):
        if settings.BAD_PASSWORD_ACCOUNT_LOCKOUT:
            try:
                user = User.objects.get(username=request.POST['username'])
                if user.metadata.is_locked_out:
                    logger2.info(0, 'Failed Login Attempt for {}; Account is locked out'.format(user.username))
                    form = AuthenticationForm(data=request.POST)
                    if form.errors != {}:
                        form.errors.pop('__all__')
                    form.add_error(None,'This account is locked out')
                    response = TemplateResponse(request,"registration/login.html",{'form':form},status=200,charset="utf-8")
                    return response
            except ObjectDoesNotExist:
                pass
            except:
                raise
        result = super().post(request, args, kwargs)
        if result.__class__.__name__ == 'HttpResponseRedirect':
            logger2.info(request.user, 'Successfully Logged In')
            request.user.metadata.bad_password_attempts = 0
            request.user.metadata.save()
        else:
            logger2.info(0, 'Failed Login Attempt for {}'.format(request.POST['username']))
            # Get user, increment bad_password_attempts, set current time for last_bad_password_datetime
            try:
                user = User.objects.get(username=request.POST['username'])
                user.metadata.bad_password_attempts += 1
                user.metadata.last_bad_password_datetime = now()
                if user.metadata.bad_password_attempts >= settings.BAD_PASSWORD_ATTEMPTS:
                    user.metadata.is_locked_out = True
                user.metadata.save()
            except ObjectDoesNotExist:
                pass
            except:
                raise
        return result