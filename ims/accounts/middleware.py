from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.core.exceptions import ImproperlyConfigured, ObjectDoesNotExist
from django.http import HttpResponseRedirect
from django.utils.timezone import now
from datetime import timedelta

class PasswordExpirationMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response
    
    def process_view(self, request, view_func, view_args, view_kwargs):
        if not hasattr(request, "user"):
            raise ImproperlyConfigured(
                "The Accounts middleware requires Django Authentication "
                "middleware to be installed. Edit your MIDDLEWARE setting to "
                "insert "
                "'django.contrib.auth.middleware.AuthenticationMiddleware' before "
                "'accounts.middleware.PasswordExpirationMiddleware'."
            )
        try:
            if not request.user.__class__ == AnonymousUser:
                if not request.path == '/accounts/password_change/':
                    if request.user.metadata.must_change_password:
                        return HttpResponseRedirect('/accounts/password_change/')
                    if settings.USER_PASSWORDS_EXPIRE:
                        delta = timedelta(days=settings.MAX_PASSWORD_AGE)
                        if request.user.metadata.last_set_password_datetime + delta < now():
                            request.user.metadata.must_change_password = True
                            request.user.metadata.save()
        except ObjectDoesNotExist:
            pass

# For future use with IP abuse restrictions
# class IPRestrictionMiddleware:
#     def __init__(self, get_response):
#         self.get_response = get_response

#     def __call__(self, request):
#         response = self.get_response(request)
#         return response
    
#     def process_view(self, request, view_func, view_args, view_kwargs):
#         pass