import django
import json
django.setup()
from datetime import timedelta
from django.conf import settings
from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase, override_settings
from django.utils.timezone import now
from os import listdir
from random import randint
from rest_framework import status
from accounts.models import Token, UserMetadata, Invite
from accounts.api.serializers import UserSerializer, UserMetadataSerializer, GroupSerializer, InviteSerializer, TokenSerializer, PermissionSerializer

baseurl = '/api/v1/accounts/'

# Test module root view
class AccountsRootViewTests(TestCase):
    def setUp(self):
        for i in range(1020,1030):
            user=User.objects.create(username='test_user_{}'.format(i))
            user.set_password('Zephyrus1')
            user.save()
        Token.objects.create(user=User.objects.first(),expires=now() + timedelta(days=7))
        Token.objects.create(user=User.objects.last())
        self.baseurl = '{}token-auth/'.format(baseurl)

    def test_apiview_auth_token(self):
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        user = User.objects.first()
        token = Token.objects.get(user=user)
        payload = {'username': user.username, 'password': 'Zephyrus1'}
        response = self.client.post(self.baseurl,payload)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response,token.key)
        self.assertGreater(token.expires,now())
        user = User.objects.last()
        token = Token.objects.get(user=user)
        payload = {'username': user.username, 'password': 'Zephyrus1'}
        response = self.client.post(self.baseurl,payload)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response,token.key)
        self.assertIsNone(token.expires)
        Token.objects.all().delete()
        response = self.client.post(self.baseurl,payload)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        token = json.loads(response.content)['token']
        self.assertIsNotNone(Token.objects.filter(key=token))

class AccountsUserApiViewTests(TestCase):
    def setUp(self):
        for i in range(1030,1040):
            user=User.objects.create(username='test_user_{}'.format(i),first_name='test',last_name='user_{}'.format(i),email='support+{}@zephyruscomputing.com'.format(i))
            group=Group.objects.create(name='test_group_{}'.format(i))
            user.groups.add(group)
            user.save()
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view user','Can add user','Can change user','Can delete user']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.user.save()
        self.baseurl = '{}user/'.format(baseurl)
        self.client.force_login(self.user)

    def test_apiview_user_get(self):
        user = User.objects.first()
        # Run Generic Tests
        TestGenerics().get(self, self.baseurl, user)
        TestGenerics().get(self, '{}?search={}'.format(self.baseurl, user.username[0:4]), user)
        TestGenerics().get(self, '{}?username={}'.format(self.baseurl, user.username), user)
        TestGenerics().get(self, '{}?first_name={}'.format(self.baseurl, user.first_name[0:4]), user)
        TestGenerics().get(self, '{}?email={}'.format(self.baseurl, user.email[0:3]), user)
        response = self.client.get('{}?limit=10'.format(self.baseurl))
        self.assertEqual(10, len(json.loads(response.content)))
        self.assertContains(response, user.username)
        response = self.client.get('{}?limit=10&offset=10'.format(self.baseurl))
        self.assertEqual(1, len(json.loads(response.content)))
        self.assertNotContains(response, user.username)
        response = self.client.get('{}?limit=10&sort=username'.format(self.baseurl))
        self.assertEqual(10, len(json.loads(response.content)))
        self.assertContains(response, user.username)
        response = self.client.get('{}?limit=10&sort=-username'.format(self.baseurl))
        self.assertEqual(10, len(json.loads(response.content)))
        self.assertNotContains(response, user.username)
        response = self.client.get('{}?offset=1000'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        self.assertNotContains(response, user.username)
        response = self.client.get('{}?sort=-banana'.format(self.baseurl))
        self.assertEqual(11, len(json.loads(response.content)))
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_user_post(self):
        # Make Data
        data = {'username':'Test_View_User_New','first_name':'Test_View','last_name':'User_New','email':'support+new@zephyruscomputing.com','password':'Z3phyrus!'}
        serializer = UserSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        # Run Generic Tests
        TestGenerics().post(self, self.baseurl, data, False) # This is set to False because of the Unique Constraint on the 'sku' field which causes a 400 reply instead of the usual expected 200
        # Run Specific Tests
        self.assertIsNotNone(User.objects.filter(username='Test_View_User_New').first())
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_user_pk_get(self):
        # Get Object
        user = User.objects.first()
        # Run Generic Tests
        TestGenerics().pk_get(self, self.baseurl, user)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_user_pk_put(self):
        # Get Object
        user = User.objects.first()
        user.first_name = 'updated'
        # Run Generic Tests
        TestGenerics().pk_put(self, self.baseurl, user)
        # Run Specific Tests
        user.refresh_from_db()
        self.assertEqual(user.first_name, 'updated')
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.put('{}{}/'.format(self.baseurl, user.id), data=UserSerializer(user).data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_user_pk_patch(self):
        # Get Object and Make Data
        user = User.objects.first()
        data = {'id': user.id, 'last_name': 'updated'}
        bad_data = {'garbage_in': 'garbage_out'}
        response = self.client.patch('{}{}/'.format(self.baseurl, user.id), data=data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.patch('{}{}/'.format(self.baseurl, user.id), data=bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)#For some reason, no usable data at all still responds with a 200
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.patch('{}{}/'.format(self.baseurl, user.id), data=data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_user_pk_delete(self):
        # Get Object
        user = User.objects.first()
        # Build Test Specific Objects
        group = Group.objects.first()
        perm = Permission.objects.last()
        user.groups.add(group)
        user.user_permissions.add(perm)
        user.save()
        # Run Generic Tests
        TestGenerics().pk_delete(self, self.baseurl, user)
        # Run Specific Tests
        self.assertIsNone(User.objects.filter(username=user.username).first())
        response = self.client.delete('{}{}/'.format(self.baseurl, self.user.id))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.delete('{}{}/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_user_pk_groups_get(self):
        # Get Object
        user = User.objects.first()
        # Build Test Specific Objects
        group = Group.objects.last()
        user.groups.add(group)
        user.save()
        # Run Specific Tests
        response = self.client.get('{}{}/groups/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.user.user_permissions.add(Permission.objects.get(codename='view_group'))
        self.user.save()
        response = self.client.get('{}{}/groups/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/groups/'.format(self.baseurl, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/groups/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_user_pk_groups_add_get(self):
        # Get Object
        user = User.objects.first()
        # Build Test Specific Objects
        group = Group.objects.last()
        if group in user.groups.all():
            group = Group.objects.exclude(id__in=(ids.id for ids in user.groups.all())).first()
        # Run Specific Tests
        response = self.client.get('{}{}/groups/add/{}/'.format(self.baseurl, user.id, group.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.user.user_permissions.add(Permission.objects.get(codename='change_group'))
        self.user.save()
        response = self.client.get('{}{}/groups/add/{}/'.format(self.baseurl, user.id, group.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/groups/add/{}/'.format(self.baseurl, user.id, group.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/groups/add/{}/'.format(self.baseurl, user.id, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/groups/add/{}/'.format(self.baseurl, user.id, group.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_user_pk_groups_remove_get(self):
        # Get Object
        user = User.objects.first()
        # Build Test Specific Objects
        group = Group.objects.last()
        user.groups.add(group)
        user.save()
        # Run Specific Tests
        response = self.client.get('{}{}/groups/remove/{}/'.format(self.baseurl, user.id, group.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.user.user_permissions.add(Permission.objects.get(codename='change_group'))
        self.user.save()
        response = self.client.get('{}{}/groups/remove/{}/'.format(self.baseurl, user.id, group.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/groups/remove/{}/'.format(self.baseurl, user.id, group.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/groups/remove/{}/'.format(self.baseurl, user.id, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/groups/remove/{}/'.format(self.baseurl, user.id, group.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_user_pk_permissions_get(self):
        # Get Object
        user = User.objects.first()
        # Build Test Specific Objects
        permission = Permission.objects.last()
        user.user_permissions.add(permission)
        user.save()
        # Run Specific Tests
        response = self.client.get('{}{}/permissions/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.user.user_permissions.add(Permission.objects.get(codename='view_permission'))
        self.user.save()
        response = self.client.get('{}{}/permissions/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/permissions/'.format(self.baseurl, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/permissions/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


    def test_apiview_user_pk_permissions_add_get(self):
        # Get Object
        user = User.objects.first()
        # Build Test Specific Objects
        permission = Permission.objects.last()
        if permission in user.user_permissions.all():
            permission = Permission.objects.exclude(id__in=(ids.id for ids in user.user_permissions.all())).first()
        # Run Specific Tests
        response = self.client.get('{}{}/permissions/add/{}/'.format(self.baseurl, user.id, permission.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.user.user_permissions.add(Permission.objects.get(codename='change_permission'))
        self.user.save()
        response = self.client.get('{}{}/permissions/add/{}/'.format(self.baseurl, user.id, permission.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/permissions/add/{}/'.format(self.baseurl, user.id, permission.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/permissions/add/{}/'.format(self.baseurl, user.id, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/permissions/add/{}/'.format(self.baseurl, user.id, permission.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_user_pk_permissions_remove_get(self):
        # Get Object
        user = User.objects.first()
        # Build Test Specific Objects
        permission = Permission.objects.last()
        user.user_permissions.add(permission)
        user.save()
        # Run Specific Tests
        response = self.client.get('{}{}/permissions/remove/{}/'.format(self.baseurl, user.id, permission.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.user.user_permissions.add(Permission.objects.get(codename='change_permission'))
        self.user.save()
        response = self.client.get('{}{}/permissions/remove/{}/'.format(self.baseurl, user.id, permission.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/permissions/remove/{}/'.format(self.baseurl, user.id, permission.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/permissions/remove/{}/'.format(self.baseurl, user.id, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/permissions/remove/{}/'.format(self.baseurl, user.id, permission.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    
    def test_apiview_user_pk_disable_get(self):
        # Get Object
        user = User.objects.first()
        # Build Test Specific Objects
        user.is_active = True
        user.save()
        # Run Specific Tests
        response = self.client.get('{}{}/disable/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        user.refresh_from_db()
        self.assertFalse(user.is_active)
        response = self.client.get('{}{}/disable/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/disable/'.format(self.baseurl, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        response = self.client.get('{}{}/disable/'.format(self.baseurl, self.user.id))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/disable/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    
    def test_apiview_user_pk_enable_get(self):
        # Get Object
        user = User.objects.first()
        # Build Test Specific Objects
        user.is_active = False
        user.save()
        # Run Specific Tests
        response = self.client.get('{}{}/enable/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        user.refresh_from_db()
        self.assertTrue(user.is_active)
        response = self.client.get('{}{}/enable/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/enable/'.format(self.baseurl, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        response = self.client.get('{}{}/enable/'.format(self.baseurl, self.user.id))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/enable/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    
    def test_apiview_user_pk_lock_get(self):
        # Get Object
        user = User.objects.first()
        # Build Test Specific Objects
        self.assertEqual(user.metadata.last_lockout_datetime, None)
        # Run Specific Tests
        response = self.client.get('{}{}/lock/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        user.refresh_from_db()
        self.assertTrue(user.metadata.is_locked_out)
        self.assertNotEqual(user.metadata.last_lockout_datetime, None)
        response = self.client.get('{}{}/lock/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/lock/'.format(self.baseurl, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        response = self.client.get('{}{}/lock/'.format(self.baseurl, self.user.id))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/lock/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    
    def test_apiview_user_pk_unlock_get(self):
        # Get Object
        user = User.objects.first()
        # Build Test Specific Objects
        user.metadata.is_locked_out = True
        user.metadata.save()
        # Run Specific Tests
        response = self.client.get('{}{}/unlock/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        user.refresh_from_db()
        self.assertFalse(user.metadata.is_locked_out)
        response = self.client.get('{}{}/unlock/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/unlock/'.format(self.baseurl, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        response = self.client.get('{}{}/unlock/'.format(self.baseurl, self.user.id))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/unlock/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class AccountsGroupApiViewTests(TestCase):
    def setUp(self):
        for i in range(1040,1050):
            user=User.objects.create(username='test_user_{}'.format(i),first_name='test',last_name='user_{}'.format(i),email='support+{}@zephyruscomputing.com'.format(i))
            group=Group.objects.create(name='test_group_{}'.format(i))
            user.groups.add(group)
            user.save()
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view group','Can add group','Can change group','Can delete group']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.user.save()
        self.baseurl = '{}group/'.format(baseurl)
        self.client.force_login(self.user)

    def test_apiview_group_get(self):
        group = Group.objects.first()
        # Run Generic Tests
        TestGenerics().get(self, self.baseurl, group)
        TestGenerics().get(self, '{}?search={}'.format(self.baseurl, group.name[0:4]), group)
        TestGenerics().get(self, '{}?name={}'.format(self.baseurl, group.name), group)
        response = self.client.get('{}?limit=10'.format(self.baseurl))
        self.assertEqual(10, len(json.loads(response.content)))
        self.assertContains(response, group.name)
        response = self.client.get('{}?limit=10&offset=9'.format(self.baseurl))
        self.assertEqual(1, len(json.loads(response.content)))
        self.assertNotContains(response, group.name)
        response = self.client.get('{}?limit=10&sort=name'.format(self.baseurl))
        self.assertEqual(10, len(json.loads(response.content)))
        self.assertContains(response, group.name)
        response = self.client.get('{}?limit=9&sort=-name'.format(self.baseurl))
        self.assertEqual(9, len(json.loads(response.content)))
        self.assertNotContains(response, group.name)
        response = self.client.get('{}?offset=1000'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        self.assertNotContains(response, group.name)
        response = self.client.get('{}?sort=-banana'.format(self.baseurl))
        self.assertEqual(10, len(json.loads(response.content)))
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_group_post(self):
        # Make Data
        data = {'name':'Test_View_Group_New'}
        bad_data = {'no_name':'present_here'}
        serializer = GroupSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        response = self.client.post('{}'.format(self.baseurl), data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNotNone(Group.objects.filter(name='Test_View_Group_New').first())
        response = self.client.post('{}'.format(self.baseurl), data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        response = self.client.post('{}'.format(self.baseurl), data=bad_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # Run Specific Tests
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_group_pk_get(self):
        # Get Object
        group = Group.objects.first()
        # Run Generic Tests
        TestGenerics().pk_get(self, self.baseurl, group)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/'.format(self.baseurl, group.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_group_pk_put(self):
        # Get Object
        group = Group.objects.first()
        group.name = 'updated'
        # Run Specific Tests
        response = self.client.patch('{}{}/'.format(self.baseurl, group.id), data=GroupSerializer(group).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.patch('{}{}/'.format(self.baseurl, group.id), data=GroupSerializer(group).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_apiview_group_pk_patch(self):
        # Get Object and Make Data
        group = Group.objects.first()
        data = {'id': group.id, 'name': 'updated'}
        response = self.client.patch('{}{}/'.format(self.baseurl, group.id), data=data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.patch('{}{}/'.format(self.baseurl, group.id), data=data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_apiview_group_pk_delete(self):
        # Get Object
        group = Group.objects.first()
        # Build Test Specific Objects
        user = User.objects.last()
        perm = Permission.objects.last()
        group.user_set.add(user)
        group.permissions.add(perm)
        group.save()
        # Run Generic Tests
        TestGenerics().pk_delete(self, self.baseurl, group)
        # Run Specific Tests
        self.assertIsNone(Group.objects.filter(name=group.name).first())
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.delete('{}{}/'.format(self.baseurl, group.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_group_pk_users_get(self):
        # Get Object
        group = Group.objects.first()
        # Build Test Specific Objects
        user = User.objects.last()
        user.groups.add(group)
        user.save()
        # Run Specific Tests
        response = self.client.get('{}{}/users/'.format(self.baseurl, group.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.user.user_permissions.add(Permission.objects.get(codename='view_user'))
        self.user.save()
        response = self.client.get('{}{}/users/'.format(self.baseurl, group.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/users/'.format(self.baseurl, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/users/'.format(self.baseurl, group.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_group_pk_users_add_get(self):
        # Get Object
        group = Group.objects.first()
        # Build Test Specific Objects
        user = User.objects.last()
        if user in group.user_set.all():
            user = User.objects.exclude(id__in=(ids.id for ids in group.user_set.all())).first()
        # Run Specific Tests
        response = self.client.get('{}{}/users/add/{}/'.format(self.baseurl, group.id, user.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.user.user_permissions.add(Permission.objects.get(codename='change_user'))
        self.user.save()
        response = self.client.get('{}{}/users/add/{}/'.format(self.baseurl, group.id, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/users/add/{}/'.format(self.baseurl, group.id, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/users/add/{}/'.format(self.baseurl, group.id, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/users/add/{}/'.format(self.baseurl, group.id, user.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_group_pk_users_remove_get(self):
        # Get Object
        group = Group.objects.first()
        # Build Test Specific Objects
        user = User.objects.last()
        group.user_set.add(user)
        group.save()
        # Run Specific Tests
        response = self.client.get('{}{}/users/remove/{}/'.format(self.baseurl, group.id, user.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.user.user_permissions.add(Permission.objects.get(codename='change_user'))
        self.user.save()
        response = self.client.get('{}{}/users/remove/{}/'.format(self.baseurl, group.id, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/users/remove/{}/'.format(self.baseurl, group.id, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/users/remove/{}/'.format(self.baseurl, group.id, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/users/remove/{}/'.format(self.baseurl, group.id, user.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_group_pk_permissions_get(self):
        # Get Object
        group = Group.objects.first()
        # Build Test Specific Objects
        permission = Permission.objects.last()
        group.permissions.add(permission)
        group.save()
        # Run Specific Tests
        response = self.client.get('{}{}/permissions/'.format(self.baseurl, group.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.user.user_permissions.add(Permission.objects.get(codename='view_permission'))
        self.user.save()
        response = self.client.get('{}{}/permissions/'.format(self.baseurl, group.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/permissions/'.format(self.baseurl, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/permissions/'.format(self.baseurl, group.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_group_pk_permissions_add_get(self):
        # Get Object
        group = Group.objects.first()
        # Build Test Specific Objects
        permission = Permission.objects.last()
        if permission in group.permissions.all():
            permission = Permission.objects.exclude(id__in=(ids.id for ids in group.permissions.all())).first()
        # Run Specific Tests
        response = self.client.get('{}{}/permissions/add/{}/'.format(self.baseurl, group.id, permission.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.user.user_permissions.add(Permission.objects.get(codename='change_permission'))
        self.user.save()
        response = self.client.get('{}{}/permissions/add/{}/'.format(self.baseurl, group.id, permission.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/permissions/add/{}/'.format(self.baseurl, group.id, permission.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/permissions/add/{}/'.format(self.baseurl, group.id, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/permissions/add/{}/'.format(self.baseurl, group.id, permission.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_group_pk_permissions_remove_get(self):
        # Get Object
        group = Group.objects.first()
        # Build Test Specific Objects
        permission = Permission.objects.last()
        group.permissions.add(permission)
        group.save()
        # Run Specific Tests
        response = self.client.get('{}{}/permissions/remove/{}/'.format(self.baseurl, group.id, permission.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.user.user_permissions.add(Permission.objects.get(codename='change_permission'))
        self.user.save()
        response = self.client.get('{}{}/permissions/remove/{}/'.format(self.baseurl, group.id, permission.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/permissions/remove/{}/'.format(self.baseurl, group.id, permission.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/permissions/remove/{}/'.format(self.baseurl, group.id, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/permissions/remove/{}/'.format(self.baseurl, group.id, permission.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class AccountsPermissionApiViewTests(TestCase):
    def setUp(self):
        for i in range(1050,1060):
            user=User.objects.create(username='test_user_{}'.format(i),first_name='test',last_name='user_{}'.format(i),email='support+{}@zephyruscomputing.com'.format(i))
            group=Group.objects.create(name='test_group_{}'.format(i))
            user.groups.add(group)
            user.save()
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view permission','Can add permission','Can change permission','Can delete permission']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.user.save()
        self.baseurl = '{}permission/'.format(baseurl)
        self.client.force_login(self.user)

    def test_apiview_permission_get(self):
        permission = Permission.objects.first()
        # Run Generic Tests
        response = self.client.get('{}'.format(self.baseurl))
        self.assertContains(response, permission.codename)
        #response = self.client.get('{}?search={}'.format(self.baseurl, permission.codename[0:4]), permission)
        self.assertContains(response, permission.codename)
        #response = self.client.get('{}?codename={}'.format(self.baseurl, permission.codename), permission)
        self.assertContains(response, permission.codename)
        response = self.client.get('{}?limit=10'.format(self.baseurl))
        self.assertEqual(10, len(json.loads(response.content)))
        self.assertContains(response, permission.codename)
        response = self.client.get('{}?limit=10&offset=10'.format(self.baseurl))
        self.assertEqual(10, len(json.loads(response.content)))
        self.assertNotContains(response, permission.codename)
        response = self.client.get('{}?limit=10&sort=codename'.format(self.baseurl))
        self.assertEqual(10, len(json.loads(response.content)))
        self.assertContains(response, permission.codename)
        response = self.client.get('{}?limit=10&sort=-codename'.format(self.baseurl))
        self.assertEqual(10, len(json.loads(response.content)))
        self.assertNotContains(response, permission.name)
        response = self.client.get('{}?offset=1000'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        self.assertNotContains(response, permission.codename)
        response = self.client.get('{}?sort=-banana'.format(self.baseurl))
        self.assertEqual(64, len(json.loads(response.content)))
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_permission_post(self):
        # Make Data
        data = {'codename':'Test_View_Permission_New','content_type_id':7,'name':'Test View Permission New'}
        serializer = PermissionSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        response = self.client.post('{}'.format(self.baseurl), data=data)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # Run Specific Tests
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_apiview_permission_pk_get(self):
        # Get Object
        permission = Permission.objects.first()
        # Run Generic Tests
        response = self.client.get('{}{}/'.format(self.baseurl, permission.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, permission.codename)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/'.format(self.baseurl, permission.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_permission_pk_put(self):
        # Get Object
        permission = Permission.objects.first()
        permission.codename = 'updated'
        # Run Specific Tests
        response = self.client.patch('{}{}/'.format(self.baseurl, permission.id), data=PermissionSerializer(permission).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.patch('{}{}/'.format(self.baseurl, permission.id), data=PermissionSerializer(permission).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_apiview_permission_pk_patch(self):
        # Get Object and Make Data
        permission = Permission.objects.first()
        data = {'id': permission.id, 'codename': 'updated'}
        response = self.client.patch('{}{}/'.format(self.baseurl, permission.id), data=data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.patch('{}{}/'.format(self.baseurl, permission.id), data=data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_apiview_permission_pk_delete(self):
        # Get Object
        perm = Permission.objects.last()
        # Build Test Specific Objects
        user = User.objects.last()
        group = Group.objects.first()
        perm.group_set.add(group)
        perm.user_set.add(user)
        perm.save()
        # Run Specific Tests
        response = self.client.delete('{}{}/'.format(self.baseurl, perm.id))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.delete('{}{}/'.format(self.baseurl, perm.id))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_apiview_permission_pk_users_get(self):
        # Get Object
        permission = Permission.objects.first()
        # Build Test Specific Objects
        user = User.objects.last()
        permission.user_set.add(user)
        permission.save()
        # Run Specific Tests
        response = self.client.get('{}{}/users/'.format(self.baseurl, permission.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.user.user_permissions.add(Permission.objects.get(codename='view_user'))
        self.user.save()
        response = self.client.get('{}{}/users/'.format(self.baseurl, permission.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/users/'.format(self.baseurl, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/users/'.format(self.baseurl, permission.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_permission_pk_users_add_get(self):
        # Get Object
        permission = Permission.objects.first()
        # Build Test Specific Objects
        user = User.objects.last()
        if user in permission.user_set.all():
            user = User.objects.exclude(id__in=(ids.id for ids in permission.user_set.all())).first()
        # Run Specific Tests
        response = self.client.get('{}{}/users/add/{}/'.format(self.baseurl, permission.id, user.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.user.user_permissions.add(Permission.objects.get(codename='change_user'))
        self.user.save()
        response = self.client.get('{}{}/users/add/{}/'.format(self.baseurl, permission.id, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/users/add/{}/'.format(self.baseurl, permission.id, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/users/add/{}/'.format(self.baseurl, permission.id, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/users/add/{}/'.format(self.baseurl, permission.id, user.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_permission_pk_users_remove_get(self):
        # Get Object
        permission = Permission.objects.first()
        # Build Test Specific Objects
        user = User.objects.last()
        permission.user_set.add(user)
        permission.save()
        # Run Specific Tests
        response = self.client.get('{}{}/users/remove/{}/'.format(self.baseurl, permission.id, user.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.user.user_permissions.add(Permission.objects.get(codename='change_user'))
        self.user.save()
        response = self.client.get('{}{}/users/remove/{}/'.format(self.baseurl, permission.id, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/users/remove/{}/'.format(self.baseurl, permission.id, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/users/remove/{}/'.format(self.baseurl, permission.id, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/users/remove/{}/'.format(self.baseurl, permission.id, user.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_permission_pk_groups_get(self):
        # Get Object
        permission = Permission.objects.last()
        # Build Test Specific Objects
        group = Group.objects.last()
        permission.group_set.add(group)
        permission.save()
        # Run Specific Tests
        response = self.client.get('{}{}/groups/'.format(self.baseurl, permission.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.user.user_permissions.add(Permission.objects.get(codename='view_group'))
        self.user.save()
        response = self.client.get('{}{}/groups/'.format(self.baseurl, permission.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/groups/'.format(self.baseurl, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/groups/'.format(self.baseurl, permission.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_permission_pk_groups_add_get(self):
        # Get Object
        permission = Permission.objects.last()
        # Build Test Specific Objects
        group = Group.objects.last()
        if group in permission.group_set.all():
            group = Group.objects.exclude(id__in=(ids.id for ids in permission.group_set.all())).first()
        # Run Specific Tests
        response = self.client.get('{}{}/groups/add/{}/'.format(self.baseurl, permission.id, group.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.user.user_permissions.add(Permission.objects.get(codename='change_group'))
        self.user.save()
        response = self.client.get('{}{}/groups/add/{}/'.format(self.baseurl, permission.id, group.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/groups/add/{}/'.format(self.baseurl, permission.id, group.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/groups/add/{}/'.format(self.baseurl, permission.id, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/groups/add/{}/'.format(self.baseurl, permission.id, group.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_permission_pk_groups_remove_get(self):
        # Get Object
        permission = Permission.objects.last()
        # Build Test Specific Objects
        group = Group.objects.first()
        permission.group_set.add(group)
        permission.save()
        # Run Specific Tests
        response = self.client.get('{}{}/groups/remove/{}/'.format(self.baseurl, permission.id, group.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.user.user_permissions.add(Permission.objects.get(codename='change_group'))
        self.user.save()
        response = self.client.get('{}{}/groups/remove/{}/'.format(self.baseurl, permission.id, group.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/groups/remove/{}/'.format(self.baseurl, permission.id, group.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}{}/groups/remove/{}/'.format(self.baseurl, permission.id, 1000))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/groups/remove/{}/'.format(self.baseurl, permission.id, group.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class AccountsInviteApiViewTests(TestCase):
    def setUp(self):
        for i in range(1060,1070):
            group=Group.objects.create(name='test_group_{}'.format(i))
            Invite.objects.create(email='support+{}@zephyruscomputing.com'.format(i),group=group,expires=(now() + timedelta(hours=1)))
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view invite','Can add invite','Can change invite','Can delete invite']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.user.save()
        self.baseurl = '{}invite/'.format(baseurl)
        self.client.force_login(self.user)

    def test_apiview_invite_get(self):
        invite = Invite.objects.first()
        # Run Generic Tests
        TestGenerics().get(self, self.baseurl, invite)
        TestGenerics().get(self, '{}?search={}'.format(self.baseurl, invite.email[0:4]), invite)
        TestGenerics().get(self, '{}?group={}'.format(self.baseurl, invite.group), invite)
        TestGenerics().get(self, '{}?expires={}'.format(self.baseurl, str(invite.expires)[0:4]), invite)
        TestGenerics().get(self, '{}?email={}'.format(self.baseurl, invite.email[0:3]), invite)
        response = self.client.get('{}?limit=10'.format(self.baseurl))
        self.assertEqual(10, len(json.loads(response.content)))
        self.assertContains(response, invite.email)
        response = self.client.get('{}?limit=10&offset=10'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        self.assertNotContains(response, invite.email)
        response = self.client.get('{}?limit=10&sort=email'.format(self.baseurl))
        self.assertEqual(10, len(json.loads(response.content)))
        self.assertContains(response, invite.email)
        response = self.client.get('{}?limit=9&sort=-email'.format(self.baseurl))
        self.assertEqual(9, len(json.loads(response.content)))
        self.assertNotContains(response, invite.email)
        response = self.client.get('{}?offset=1000'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        self.assertNotContains(response, invite.email)
        response = self.client.get('{}?sort=-banana'.format(self.baseurl))
        self.assertEqual(10, len(json.loads(response.content)))
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_invite_post(self):
        # Make Data
        data = {'email':'support+new@zephyruscomputing.com','group':Group.objects.first().id,'expires':str(now() + timedelta(hours=1))}
        serializer = InviteSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        TestGenerics().post(self, self.baseurl, data, True)
        # Run Specific Tests
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_invite_pk_get(self):
        # Get Object
        invite = Invite.objects.first()
        # Run Generic Tests
        TestGenerics().pk_get(self, self.baseurl, invite)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/'.format(self.baseurl, invite.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_invite_pk_put(self):
        # Get Object
        invite = Invite.objects.first()
        invite.email = 'updated@zephyruscomputing.com'
        invite.expires = None
        # Run Specific Tests
        response = self.client.put('{}{}/'.format(self.baseurl, invite.id), data=InviteSerializer(invite).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.put('{}{}/'.format(self.baseurl, invite.id), data=InviteSerializer(invite).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        invite.refresh_from_db()
        self.assertEqual(invite.email, 'updated@zephyruscomputing.com')
        self.assertEqual(invite.expires, None)

    def test_apiview_invite_pk_patch(self):
        # Get Object and Make Data
        invite = Invite.objects.first()
        data = {'id': invite.id, 'email': 'updated2@zephyruscomputing.com', 'expires': None}
        response = self.client.patch('{}{}/'.format(self.baseurl, invite.id), data=data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.patch('{}{}/'.format(self.baseurl, invite.id), data=data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        invite.refresh_from_db()
        self.assertEqual(invite.email, 'updated2@zephyruscomputing.com')
        self.assertEqual(invite.expires, None)

    def test_apiview_invite_pk_delete(self):
        # Get Object
        invite = Invite.objects.last()
        # Run Specific Tests
        TestGenerics().pk_delete(self, self.baseurl, invite)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.delete('{}{}/'.format(self.baseurl, invite.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class AccountsTokenApiViewTests(TestCase):
    def setUp(self):
        for i in range(1070,1080):
            user=User.objects.create(username='test_user_{}'.format(i),first_name='test',last_name='user_{}'.format(i),email='support+{}@zephyruscomputing.com'.format(i))
            Token.objects.create(user=user,expires=now() + timedelta(hours=1))
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view Token','Can add Token','Can change Token','Can delete Token']:
            p = Permission.objects.get(name=name,content_type__app_label='accounts')
            self.user.user_permissions.add(p)
        self.user.save()
        self.baseurl = '{}token/'.format(baseurl)
        self.client.force_login(self.user)

    def test_apiview_token_get(self):
        token = Token.objects.first()
        # Run Generic Tests
        response = self.client.get(self.baseurl)
        self.assertContains(response, str(token.expires).replace(' ','T').replace('+00:00','Z'))
        response = self.client.get('{}?search={}'.format(self.baseurl, token.user.id))
        self.assertContains(response, token.user.id)
        response = self.client.get('{}?expires={}'.format(self.baseurl, str(token.expires)[0:4]))
        self.assertContains(response, str(token.expires).replace(' ','T').replace('+00:00','Z'))
        response = self.client.get('{}?limit=10'.format(self.baseurl))
        self.assertEqual(10, len(json.loads(response.content)))
        self.assertContains(response, str(token.expires).replace(' ','T').replace('+00:00','Z'))
        response = self.client.get('{}?limit=10&offset=10'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        self.assertNotContains(response, str(token.expires).replace(' ','T').replace('+00:00','Z'))
        response = self.client.get('{}?limit=10&sort=created'.format(self.baseurl))
        self.assertEqual(10, len(json.loads(response.content)))
        self.assertContains(response, str(token.expires).replace(' ','T').replace('+00:00','Z'))
        response = self.client.get('{}?limit=9&sort=-expires'.format(self.baseurl))
        self.assertEqual(9, len(json.loads(response.content)))
        self.assertNotContains(response, str(token.expires).replace(' ','T').replace('+00:00','Z'))
        response = self.client.get('{}?offset=1000'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        self.assertNotContains(response, str(token.expires).replace(' ','T').replace('+00:00','Z'))
        response = self.client.get('{}?sort=-banana'.format(self.baseurl))
        self.assertEqual(10, len(json.loads(response.content)))
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_token_post(self):
        # Make Data
        user = User.objects.first()
        data = {'user':user.id,'expires':str(now() + timedelta(hours=1))}
        serializer = TokenSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        TestGenerics().post(self, self.baseurl, data, True)
        # Run Specific Tests
        data = {'user':user.id}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertGreaterEqual(len(Token.objects.filter(user=user)),2)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_token_pk_get(self):
        # Get Object
        token = Token.objects.first()
        # Run Generic Tests
        response = self.client.get('{}{}/'.format(self.baseurl, token.id))
        self.assertContains(response, token.user.id)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}{}/'.format(self.baseurl, token.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_token_pk_put(self):
        # Get Object
        token = Token.objects.first()
        token.expires = None
        # Run Specific Tests
        response = self.client.put('{}{}/'.format(self.baseurl, token.id), data=TokenSerializer(token).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.put('{}{}/'.format(self.baseurl, token.id), data=TokenSerializer(token).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        token.refresh_from_db()
        self.assertNotEqual(token.expires, None)

    def test_apiview_token_pk_patch(self):
        # Get Object and Make Data
        token = Token.objects.first()
        data = {'expires': None}
        response = self.client.patch('{}{}/'.format(self.baseurl, token.id), data=data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.patch('{}{}/'.format(self.baseurl, token.id), data=data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        token.refresh_from_db()
        self.assertNotEqual(token.expires, None)

    def test_apiview_token_pk_delete(self):
        # Get Object
        token = Token.objects.last()
        # Run Specific Tests
        TestGenerics().pk_delete(self, self.baseurl, token)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.delete('{}{}/'.format(self.baseurl, token.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class TestGenerics():
    def get_serialized_data(self, obj):
    # Determine the model and return the data serialized
        if obj.__class__.__name__ == 'User':
            return UserSerializer(obj).data
        if obj.__class__.__name__ == 'Group':
            return GroupSerializer(obj).data
        if obj.__class__.__name__ == 'Invite':
            return InviteSerializer(obj).data
        if obj.__class__.__name__ == 'Token':
            return TokenSerializer(obj).data
        if obj.__class__.__name__ == 'Permission':
            return PermissionSerializer(obj).data
        else:
            return json.dumps(obj)

    def get_completed_url(self, url, obj_or_data):
        serialized = self.get_serialized_data(obj_or_data)
        if isinstance(serialized, str):
            serialized = json.loads(serialized)
        return '{}{}/'.format(url, serialized.get('id'))

    def get(self, case, url, obj):
        # GET Valid Request
        response = case.client.get(url)
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        case.assertContains(response, obj)
        if '?' in url:
            response = case.client.get('{}&limit=1'.format(url))
            case.assertEqual(response.status_code, status.HTTP_200_OK)
            case.assertContains(response, obj)
            response = case.client.get('{}&limit=1&offset=2'.format(url))
            case.assertEqual(response.status_code, status.HTTP_200_OK)
            case.assertNotContains(response, obj)
        else: 
            response = case.client.get('{}?limit=1'.format(url))
            case.assertEqual(response.status_code, status.HTTP_200_OK)
            case.assertContains(response, obj)
            response = case.client.get('{}?limit=1&offset=2'.format(url))
            case.assertEqual(response.status_code, status.HTTP_200_OK)
            case.assertNotContains(response, obj)
        # Not Implemented Yet
        # Test User without Permissions
        #good_user = case.user
        #bad_user = User.objects.get_or_create(username='bad_user')[0]
        #case.client.force_login(bad_user)
        #response = case.client.get(valid)
        #case.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        #case.client.force_login(good_user)

    def post(self, case, url, data, dup=True):
        # POST Valid Data
        response = case.client.post(url, data)
        case.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # POST Duplicate Data
        if dup:
            response = case.client.post(url, data)
            case.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # POST Invalid Data
        bad_data = {'name': 'bad'}
        response = case.client.post(url, bad_data)
        case.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # Test User without Permissions
        good_user = case.user
        bad_user = User.objects.get_or_create(username='bad_user')[0]
        case.client.force_login(bad_user)
        response = case.client.post(url, data)
        case.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        case.client.force_login(good_user)

    def pk_get(self, case, url, obj):
        # GET Valid Request
        valid = self.get_completed_url(url, obj)
        response = case.client.get(valid)
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        case.assertContains(response, obj)
        # GET Invalid Request
        response = case.client.get(url + '0/')
        case.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # Not Implemented Yet
        # Test User without Permissions
        #good_user = case.user
        #bad_user = User.objects.get_or_create(username='bad_user')[0]
        #case.client.force_login(bad_user)
        #response = case.client.get(valid)
        #case.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        #case.client.force_login(good_user)

    def pk_put(self, case, url, obj):
        serialized = self.get_serialized_data(obj)
        # PUT Valid Request
        valid = self.get_completed_url(url, obj)
        response = case.client.put(valid, serialized, content_type='application/json')
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        # PUT Invalid Request
        bad_data = {'id': obj.id, 'name': 'bad'.zfill(100)}
        response = case.client.put(valid, bad_data, content_type='application/json')
        case.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # Test User without Permissions
        good_user = case.user
        bad_user = User.objects.get_or_create(username='bad_user')[0]
        case.client.force_login(bad_user)
        response = case.client.put(valid, serialized, content_type='application/json')
        case.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        case.client.force_login(good_user)

    def pk_patch(self, case, url, data):
        # PATCH Valid Data
        valid = self.get_completed_url(url, data)
        response = case.client.patch(valid, data, content_type='application/json')
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        # PATCH Invalid Data
        bad_data = {'name': 'bad'.zfill(1000)}
        response = case.client.patch(valid, bad_data, content_type='application/json')
        case.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # Test User without Permissions
        good_user = case.user
        bad_user = User.objects.get_or_create(username='bad_user')[0]
        case.client.force_login(bad_user)
        response = case.client.patch(valid, data, content_type='application/json')
        case.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        case.client.force_login(good_user)

    def pk_delete(self, case, url, obj):
        # DELETE Valid Request
        valid = self.get_completed_url(url, obj)
        response = case.client.delete(valid)
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        # Test User without Permissions
        good_user = case.user
        bad_user = User.objects.get_or_create(username='bad_user')[0]
        case.client.force_login(bad_user)
        response = case.client.delete(valid)
        case.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        case.client.force_login(good_user)
