import django
django.setup()
from datetime import timedelta
from django.contrib.auth.models import User
from django.test import TestCase, override_settings
from django.utils.timezone import now
from datetime import timedelta
from random import randint
from rest_framework.exceptions import AuthenticationFailed
from accounts.models import Token
from accounts.authentication import ImsTokenAuthentication
import accounts.helpers as helpers

# Unit test for IMSTokenAuthentication methods
class TestIMSTokenAuthentication(TestCase):
    def setUp(self):
        for i in range(20,30):
            User.objects.create(username='test_user_{}'.format(i),first_name='test',last_name='user',email='support+{}@zephyruscomputing.com'.format(i))

    def test_authenticate_credentials(self):
        ims_token = ImsTokenAuthentication()
        expected_user = User.objects.all()[randint(0,User.objects.count() -1)]
        expected_key = randint(0,1000000)
        token = Token.objects.create(user=expected_user, key=expected_key)
        user, key = ims_token.authenticate_credentials(expected_key)
        self.assertEqual(expected_user, user)
        self.assertEqual(str(expected_key), key.key)
        token.delete()
        with self.assertRaises(AuthenticationFailed):
            ims_token.authenticate_credentials(key)
        with self.assertRaises(AuthenticationFailed):
            expected_user = User.objects.all()[randint(0,User.objects.count() -1)]
            token = Token.objects.create(user=expected_user)
            expected_user.is_active = False
            expected_user.save()
            ims_token.authenticate_credentials(token.key)
        with self.assertRaises(AuthenticationFailed):
            expected_user = User.objects.all()[randint(0,User.objects.count() -1)]
            while not expected_user.is_active:
                expected_user = User.objects.all()[randint(0,User.objects.count() -1)]
            token = Token.objects.create(user=expected_user, expires=now() - timedelta(hours=1))
            ims_token.authenticate_credentials(token.key)

@override_settings(USER_PASSWORDS_EXPIRE=False,MAX_PASSWORD_AGE=1)
class TestPasswordExpirationMiddleware(TestCase):
    def setUp(self):
        for i in range(30,40):
            User.objects.create(username='test_user_{}'.format(i),first_name='test',last_name='user',email='support+{}@zephyruscomputing.com'.format(i))

    def test_must_change_password(self):
        user = User.objects.all()[randint(0,User.objects.count() -1)]
        self.client.force_login(user)
        response = self.client.get('/accounts/profile/')
        self.assertEqual(response.status_code, 200)
        user.metadata.must_change_password = True
        user.metadata.save()
        response = self.client.get('/accounts/profile/')
        self.assertEqual(response.status_code, 302)
        
    def test_user_expired_password(self):
        user = User.objects.all()[randint(0,User.objects.count() -1)]
        self.client.force_login(user)
        response = self.client.get('/accounts/profile/')
        self.assertEqual(response.status_code, 200)
        delta = timedelta(days=2)
        user.metadata.last_set_password_datetime = now() - delta
        user.metadata.save()
        with override_settings(USER_PASSWORDS_EXPIRE=True):
            response = self.client.get('/accounts/profile/')
            self.assertEqual(response.status_code, 200)
            user.refresh_from_db()
            self.assertTrue(user.metadata.must_change_password)
            response = self.client.get('/accounts/profile/')
            self.assertEqual(response.status_code, 302)
        