import django
django.setup()
from datetime import timedelta
from django.conf import settings
from django.contrib.auth.models import User, Group, Permission
from django.contrib.auth.tokens import default_token_generator
from django.test import TestCase, override_settings
from django.utils.timezone import now
from os import listdir
from os.path import join
from random import randint,choice
from rest_framework import status
from accounts.models import Invite, Token
from log.models import Log

baseurl = '/accounts/'

@override_settings(ALLOW_ANONYMOUS=True)
class ProfileViewTests(TestCase):
    def test_user_profile(self):
        url = '{}profile/'.format(baseurl)
        response = self.client.get(url)
        self.assertRedirects(response, '/accounts/login/?next={}'.format(url))
        user = User.objects.get_or_create(username='testuser')[0]
        self.client.force_login(user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Token.objects.filter(user=user).count(), 0)
        token = Token.objects.get_or_create(user=user)[0]
        with override_settings(ALLOW_USER_API_KEYS = False):
            response = self.client.get(url)
            self.assertNotContains(response, token.key)
            self.assertNotContains(response, token.key[:5])
            self.assertNotContains(response, token.key[-5:])
        with override_settings(ALLOW_USER_API_KEYS = True):
            response = self.client.get(url)
            self.assertNotContains(response, token.key)
            self.assertContains(response, token.key[:5])
            self.assertContains(response, token.key[-5:])

    def test_edit_user(self):
        url = '{}profile/edit/'.format(baseurl)
        response = self.client.get(url)
        self.assertRedirects(response, '/accounts/login/?next={}'.format(url))
        user = User.objects.get_or_create(username='testuser')[0]
        self.client.force_login(user)
        with override_settings(ALLOW_USER_EDIT_PROFILE = False):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        with override_settings(ALLOW_USER_EDIT_PROFILE = True):
            response = self.client.get(url)
            csrf_token = str(response.content.split(b'csrfmiddlewaretoken" value="')[1].split(b'">\n')[0])[2:-1]
            response = self.client.post(url, {'csrfmiddlewaretoken':csrf_token,'banana':'first', 'last_name':-1,'email':'not_a_real_email_address'})
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            response = self.client.get(url)
            csrf_token = str(response.content.split(b'csrfmiddlewaretoken" value="')[1].split(b'">\n')[0])[2:-1]
            response = self.client.post(url, {'csrfmiddlewaretoken':csrf_token,'first_name':'new_first', 'last_name':'new_last','email':'support@zephyruscomputing.com'})
            self.assertEqual(response.status_code, status.HTTP_302_FOUND)

    def test_ajax_create_token(self):
        url = '{}profile/token/create/'.format(baseurl)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertContains(response, 'red')
        user = User.objects.get_or_create(username='testuser')[0]
        self.client.force_login(user)
        self.assertEqual(Token.objects.filter(user=user).count(), 0)
        with override_settings(ALLOW_USER_API_KEYS = False):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTemplateUsed(response, 'inventory/ajax_result.html')
            self.assertContains(response, 'red')
        with override_settings(ALLOW_USER_API_KEYS = True):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTemplateUsed(response, 'inventory/ajax_result.html')
            self.assertContains(response, 'green')
            self.assertEqual(Token.objects.filter(user=user).count(), 1)
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTemplateUsed(response, 'inventory/ajax_result.html')
            self.assertContains(response, 'green')
            self.assertEqual(Token.objects.filter(user=user).count(), 2)

    def test_ajax_view_tokens(self):
        url = '{}profile/token/'.format(baseurl)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertContains(response, 'red')
        user = User.objects.get_or_create(username='testuser')[0]
        self.client.force_login(user)
        token = Token.objects.get_or_create(user=user)[0]
        with override_settings(ALLOW_USER_API_KEYS = False):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTemplateUsed(response, 'inventory/ajax_result.html')
            self.assertContains(response, 'red')
        with override_settings(ALLOW_USER_API_KEYS = True):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTemplateUsed(response, 'inventory/ajax_result2.html')
            self.assertNotContains(response, token.key)
            self.assertContains(response, token.key[:5])
            self.assertContains(response, token.key[-5:])
            token2 = Token.objects.create(user=user,expires=now() + timedelta(hours=1))
            response = self.client.get(url)
            self.assertNotContains(response, token.key)
            self.assertContains(response, token.key[:5])
            self.assertContains(response, token.key[-5:])
            self.assertNotContains(response, token2.key)
            self.assertContains(response, token2.key[:5])
            self.assertContains(response, token2.key[-5:])

    def test_view_token(self):
        user = User.objects.get_or_create(username='testuser')[0]
        self.client.force_login(user)
        token = Token.objects.get_or_create(user=user)[0]
        url = '{}profile/token/{}/'.format(baseurl, token.id)
        with override_settings(ALLOW_USER_API_KEYS = False):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        with override_settings(ALLOW_USER_API_KEYS = True):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTemplateUsed(response, 'registration/api_key.html')
            self.assertContains(response, token.key)
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_302_FOUND)

    def test_ajax_delete_token(self):
        user = User.objects.get_or_create(username='testuser')[0]
        token = Token.objects.get_or_create(user=user)[0]
        url = '{}profile/token/{}/delete/'.format(baseurl, token.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertContains(response, 'red')
        self.client.force_login(user)
        self.assertEqual(Token.objects.filter(user=user).count(), 1)
        with override_settings(ALLOW_USER_API_KEYS = False):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTemplateUsed(response, 'inventory/ajax_result.html')
            self.assertContains(response, 'red')
        with override_settings(ALLOW_USER_API_KEYS = True):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTemplateUsed(response, 'inventory/ajax_result.html')
            self.assertContains(response, 'green')
            self.assertEqual(Token.objects.filter(user=user).count(), 0)
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
            token = Token.objects.get_or_create(user=user)[0]
            url = '{}profile/token/{}/delete/'.format(baseurl, token.id)
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTemplateUsed(response, 'inventory/ajax_result.html')
            self.assertContains(response, 'red')

@override_settings(ALLOW_ANONYMOUS=True,ALLOW_USER_SIGN_UP=True)
class SignupViewTests(TestCase):
    def setUp(self):
        User.objects.create(username='testuser',first_name='test',last_name='user',email='support@zephyruscomputing.com')

    def test_signup_get(self):
        response = self.client.get('{}signup/'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'registration/signup.html')
        with override_settings(ALLOW_ANONYMOUS=False):
            response = self.client.get('{}signup/'.format(baseurl))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTemplateUsed(response, 'registration/signup.html')
        with override_settings(ALLOW_USER_SIGN_UP=False):
            response = self.client.get('{}signup/'.format(baseurl))
            self.assertEqual(response.status_code, status.HTTP_302_FOUND)

    def test_signup_post(self):
        # Empty Test
        data = {'username':'','first_name':'','last_name':'','email':'','password1':'','password2':''}
        response = self.client.post('{}signup/'.format(baseurl),data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'registration/signup.html')
        self.assertContains(response,'Please correct the errors below.')
        # Duplicate Username Test
        data = {'username':'testuser','first_name':'test','last_name':'user','email':'support@zephyruscomputing.com','password1':'xzsawq21','password2':'xzsawq21'}
        response = self.client.post('{}signup/'.format(baseurl),data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'registration/signup.html')
        self.assertContains(response,'Please correct the error below.')
        self.assertContains(response,'A user with that username already exists.')
        # Bad Email Test
        data = {'username':'testuser2','first_name':'test','last_name':'user','email':'support+2@zephyruscomputing','password1':'xzsawq21','password2':'xzsawq21'}
        response = self.client.post('{}signup/'.format(baseurl),data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'registration/signup.html')
        self.assertContains(response,'Please correct the error below.')
        self.assertContains(response,'Enter a valid email address.')
        # Password Similar to Username or First/Last Name
        data = {'username':'testuser2','first_name':'test','last_name':'user','email':'support+2@zephyruscomputing.com','password1':'testuser123','password2':'testuser123'}
        response = self.client.post('{}signup/'.format(baseurl),data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'registration/signup.html')
        self.assertContains(response,'Please correct the error below.')
        self.assertContains(response,'The password is too similar')
        # Password Mismatch
        data = {'username':'testuser2','first_name':'test','last_name':'user','email':'support+2@zephyruscomputing.com','password1':'xzsawq21','password2':'xzsawq22'}
        response = self.client.post('{}signup/'.format(baseurl),data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'registration/signup.html')
        self.assertContains(response,'Please correct the error below.')
        self.assertContains(response,'The two password fields didn&#x27;t match.')
        # Success
        data = {'username':'testuser2','first_name':'test','last_name':'user','email':'support+2@zephyruscomputing.com','password1':'xzsawq21','password2':'xzsawq21'}
        response = self.client.post('{}signup/'.format(baseurl),data=data)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        with override_settings(ALLOW_ANONYMOUS=False):
            data = {'username':'testuser3','first_name':'test','last_name':'user','email':'support+3@zephyruscomputing.com','password1':'xzsawq21','password2':'xzsawq21'}
            response = self.client.post('{}signup/'.format(baseurl),data=data)
            self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        with override_settings(ALLOW_USER_SIGN_UP=False):
            response = self.client.get('{}signup/'.format(baseurl))
            self.assertEqual(response.status_code, status.HTTP_302_FOUND)

@override_settings(ALLOW_ANONYMOUS=True)
class InviteCodeViewTests(TestCase):
    def setUp(self):
        User.objects.create(username='testuser',first_name='test',last_name='user',email='support@zephyruscomputing.com')
        group = Group.objects.create(name='test_group')
        Invite.objects.create(email='support+2@zephyruscomputing.com',group=group)
        Invite.objects.create(email='support+3@zephyruscomputing.com',group=group,expires=now())
        Invite.objects.create(email='support+4@zephyruscomputing.com',group=group,expires=now() + timedelta(hours=1))

    def test_get_invite_code_bad_code(self):
        response = self.client.get('{}invite/banana-hammock/'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'registration/signup.html')
        self.assertNotContains(response, '@zephyruscomputing.com')
        self.assertContains(response, 'Invite has expired. Please request another or visit the Sign Up page.')
        with override_settings(ALLOW_ANONYMOUS=False):
            response = self.client.get('{}invite/banana-hammock/'.format(baseurl))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTemplateUsed(response, 'registration/signup.html')
            self.assertNotContains(response, '@zephyruscomputing.com')
            self.assertContains(response, 'Invite has expired. Please request another or visit the Sign Up page.')

    def test_get_invite_code_expires_none(self):
        invite = Invite.objects.get(email='support+2@zephyruscomputing.com')
        response = self.client.get('{}invite/{}/'.format(baseurl, invite.code))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'registration/signup.html')
        self.assertContains(response, 'support+2@zephyruscomputing.com')
        self.assertNotContains(response, 'Invite has expired. Please request another or visit the Sign Up page.')
        with override_settings(ALLOW_ANONYMOUS=False):
            response = self.client.get('{}invite/{}/'.format(baseurl, invite.code))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTemplateUsed(response, 'registration/signup.html')
            self.assertContains(response, 'support+2@zephyruscomputing.com')
            self.assertNotContains(response, 'Invite has expired. Please request another or visit the Sign Up page.')

    def test_get_invite_code_expires_now(self):
        invite = Invite.objects.get(email='support+3@zephyruscomputing.com')
        response = self.client.get('{}invite/{}/'.format(baseurl, invite.code))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'registration/signup.html')
        self.assertContains(response, 'Invite has expired. Please request another or visit the Sign Up page.')
        with override_settings(ALLOW_ANONYMOUS=False):
            response = self.client.get('{}invite/{}/'.format(baseurl, invite.code))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTemplateUsed(response, 'registration/signup.html')
            self.assertContains(response, 'Invite has expired. Please request another or visit the Sign Up page.')

    def test_get_invite_code_expires_hour(self):
        invite = Invite.objects.get(email='support+4@zephyruscomputing.com')
        response = self.client.get('{}invite/{}/'.format(baseurl, invite.code))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'registration/signup.html')
        self.assertContains(response, 'support+4@zephyruscomputing.com')
        self.assertNotContains(response, 'Invite has expired. Please request another or visit the Sign Up page.')
        with override_settings(ALLOW_ANONYMOUS=False):
            response = self.client.get('{}invite/{}/'.format(baseurl, invite.code))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTemplateUsed(response, 'registration/signup.html')
            self.assertContains(response, 'support+4@zephyruscomputing.com')
            self.assertNotContains(response, 'Invite has expired. Please request another or visit the Sign Up page.')

    def test_post_invite_code_bad_code(self):
        # Invalid Invite Tests
        data = {'username':'testuser2','first_name':'test','last_name':'user','email':'support+2@zephyruscomputing.com','password1':'xzsawq21','password2':'xzsawq21'}
        response = self.client.post('{}invite/banana-hammock/'.format(baseurl), data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'registration/signup.html')
        self.assertContains(response, 'Invite has expired. Please request another or visit the Sign Up page.')
        with override_settings(ALLOW_ANONYMOUS=False):
            response = self.client.post('{}invite/banana-hammock/'.format(baseurl), data=data)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTemplateUsed(response, 'registration/signup.html')
            self.assertContains(response, 'Invite has expired. Please request another or visit the Sign Up page.')

    def test_post_invite_code_expires_none(self):
        # Email Mismatch Test
        invite = Invite.objects.get(email='support+2@zephyruscomputing.com')
        group = invite.group
        data = {'username':'testuser2','first_name':'test','last_name':'user','email':'support+3@zephyruscomputing.com','password1':'xzsawq21','password2':'xzsawq21'}
        response = self.client.post('{}invite/{}/'.format(baseurl, invite.code), data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'registration/signup.html')
        self.assertContains(response, 'Email Address does not match invite.')
        self.assertNotContains(response, 'Invite has expired. Please request another or visit the Sign Up page.')
        self.assertIn(invite, Invite.objects.all())
        with override_settings(ALLOW_ANONYMOUS=False):
            # Success
            data = {'username':'testuser2','first_name':'test','last_name':'user','email':'support+2@zephyruscomputing.com','password1':'xzsawq21','password2':'xzsawq21'}
            response = self.client.post('{}invite/{}/'.format(baseurl, invite.code), data=data)
            self.assertEqual(response.status_code, status.HTTP_302_FOUND)
            self.assertFalse(len(Invite.objects.filter(email='support+2@zephyruscomputing.com')) > 0)
            self.assertIsNotNone(User.objects.filter(username='testuser2'))
            self.assertIn(group, User.objects.get(username='testuser2').groups.all())

    def test_post_invite_code_expires_now(self):
        # Expired Invite Tests
        invite = Invite.objects.get(email='support+3@zephyruscomputing.com')
        data = {'username':'testuser2','first_name':'test','last_name':'user','email':'support+2@zephyruscomputing.com','password1':'xzsawq21','password2':'xzsawq21'}
        response = self.client.post('{}invite/{}/'.format(baseurl, invite.code), data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'registration/signup.html')
        self.assertContains(response, 'Invite has expired. Please request another or visit the Sign Up page.')
        self.assertIn(invite, Invite.objects.all())
        with override_settings(ALLOW_ANONYMOUS=False):
            response = self.client.post('{}invite/{}/'.format(baseurl, invite.code), data=data)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTemplateUsed(response, 'registration/signup.html')
            self.assertContains(response, 'Invite has expired. Please request another or visit the Sign Up page.')
            self.assertIn(invite, Invite.objects.all())
    
    def test_post_invite_code_expires_hour(self):
        # Empty Test
        invite = Invite.objects.get(email='support+4@zephyruscomputing.com')
        group = invite.group
        data = {'username':'','first_name':'','last_name':'','email':'','password1':'','password2':''}
        response = self.client.post('{}invite/{}/'.format(baseurl, invite.code), data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'registration/signup.html')
        self.assertNotContains(response, 'Invite has expired. Please request another or visit the Sign Up page.')
        self.assertContains(response,'Please correct the errors below.')
        data = {'username':'testuser2','first_name':'test','last_name':'user','email':'support+4@zephyruscomputing.com','password1':'xzsawq21','password2':'xzsawq21'}
        response = self.client.post('{}invite/{}/'.format(baseurl, invite.code), data=data)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertFalse(len(Invite.objects.filter(email='support+4@zephyruscomputing.com')) > 0)
        self.assertIsNotNone(User.objects.filter(username='testuser2'))
        self.assertIn(group, User.objects.get(username='testuser2').groups.all())
        with override_settings(ALLOW_ANONYMOUS=False):
            response = self.client.post('{}invite/{}/'.format(baseurl, invite.code), data=data)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTemplateUsed(response, 'registration/signup.html')
            self.assertContains(response, 'Invite has expired. Please request another or visit the Sign Up page.')
            self.assertNotIn(invite, Invite.objects.all())


@override_settings(ALLOW_ANONYMOUS=True,ALLOW_USER_SIGN_UP=True,ALLOW_USER_PASSWORD_RESET=True,BAD_PASSWORD_ACCOUNT_LOCKOUT=False)
class LoginViewTests(TestCase):
    def test_user_login(self):
        url = '{}login/?next=/inventory/'.format(baseurl)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Forgot Password')
        self.assertContains(response, 'Sign Up')
        user = User.objects.get_or_create(username='testuser')[0]
        password = ''.join([choice('1234567890abcefghijklmnopqrstuvwxyz') for i in range(16)])
        user.set_password(password)
        user.save()
        csrf_token = str(response.content.split(b'csrfmiddlewaretoken" value="')[1].split(b'">\n')[0])[2:-1]
        response = self.client.post(url,data={'username':'admin','password':'notthepassword','csrfmiddlewaretoken':csrf_token})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        logs = Log.objects.filter(message='Failed Login Attempt for admin')
        self.assertGreater(len(logs), 0)
        found = False
        for log in logs:
            if log.created > (now() - timedelta(minutes=1)):
                found = True
                break
        self.assertTrue(found)
        response = self.client.get(url)
        csrf_token = str(response.content.split(b'csrfmiddlewaretoken" value="')[1].split(b'">\n')[0])[2:-1]
        response = self.client.post(url,data={'username':user.username,'password':password,'csrfmiddlewaretoken':csrf_token})
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        logs = Log.objects.filter(message='Successfully Logged In',user=user)
        self.assertGreater(len(logs), 0)
        found = False
        for log in logs:
            if log.created > (now() - timedelta(minutes=1)):
                found = True
                break
        self.assertTrue(found)
        with override_settings(ALLOW_USER_SIGN_UP=False):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertNotContains(response, 'Sign Up')
        with override_settings(ALLOW_USER_PASSWORD_RESET=False):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertNotContains(response, 'Forgot Password')
        with override_settings(BAD_PASSWORD_ACCOUNT_LOCKOUT=True,BAD_PASSWORD_ATTEMPTS=3):
            for i in range(0,3):
                user.refresh_from_db()
                self.assertEqual(user.metadata.bad_password_attempts,i)
                response = self.client.get(url)
                csrf_token = str(response.content.split(b'csrfmiddlewaretoken" value="')[1].split(b'">\n')[0])[2:-1]
                bad_password = ''.join([choice('1234567890abcefghijklmnopqrstuvwxyz') for i in range(16)])
                response = self.client.post(url,data={'username':user.username,'password':bad_password,'csrfmiddlewaretoken':csrf_token})
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                self.assertNotContains(response, 'account is locked out')
                self.assertContains(response, 'Your username and password didn\'t match.')
            response = self.client.get(url)
            csrf_token = str(response.content.split(b'csrfmiddlewaretoken" value="')[1].split(b'">\n')[0])[2:-1]
            response = self.client.post(url,data={'username':user.username,'password':password,'csrfmiddlewaretoken':csrf_token})
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertContains(response, 'account is locked out')

    def test_user_logoff(self):
        url = '{}logout/?next=/inventory/'.format(baseurl)
        user = User.objects.get_or_create(username='testuser')[0]
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        logs = Log.objects.filter(message='User Logged Off',user=user)
        self.assertEqual(len(logs), 0)
        self.client.force_login(user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        logs = Log.objects.filter(message='User Logged Off',user=user)
        self.assertGreater(len(logs), 0)
        found = False
        for log in logs:
            if log.created > (now() - timedelta(minutes=1)):
                found = True
        self.assertTrue(found)

@override_settings(ALLOW_ANONYMOUS=True)
class UserPasswordTests(TestCase):
    def test_user_change_password(self):
        url = '{}password_change/'.format(baseurl)
        user = User.objects.get_or_create(username='testuser')[0]
        password = ''.join([choice('1234567890abcefghijklmnopqrstuvwxyz') for i in range(16)])
        user.set_password(password)
        user.save()
        self.client.force_login(user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        newpassword = ''.join([choice('1234567890abcefghijklmnopqrstuvwxyz') for i in range(16)])
        csrf_token = str(response.content.split(b'csrfmiddlewaretoken" value="')[1].split(b'">\n')[0])[2:-1]
        response = self.client.post(url,data={'old_password':password,'new_password1':newpassword,'new_password2':newpassword,'csrfmiddlewaretoken':csrf_token})
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        user.refresh_from_db()
        self.assertTrue(user.check_password(newpassword))
        response = self.client.post(url,data={'old_password':password,'new_password1':newpassword,'new_password2':newpassword,'csrfmiddlewaretoken':csrf_token})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'registration/password_change_form.html')
        self.assertContains(response, "Your old password was entered incorrectly. Please enter it again.")
        response = self.client.post(url,data={'old_password':newpassword,'new_password1':'1234','new_password2':'1234','csrfmiddlewaretoken':csrf_token})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'registration/password_change_form.html')
        self.assertNotContains(response, "Your old password was entered incorrectly. Please enter it again.")
        self.assertContains(response, "This password is too short. It must contain at least 8 characters.")
        self.assertContains(response, "This password is too common.")
        self.assertContains(response, "This password is entirely numeric.")
        response = self.client.post(url,data={'old_password':newpassword,'new_password1':'1234','new_password2':'4321','csrfmiddlewaretoken':csrf_token})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'registration/password_change_form.html')
        self.assertContains(response, "The two password fields didn’t match.")
        self.assertNotContains(response, "Your old password was entered incorrectly. Please enter it again.")
        self.assertNotContains(response, "This password is too short. It must contain at least 8 characters.")
        self.assertNotContains(response, "This password is too common.")
        self.assertNotContains(response, "This password is entirely numeric.")

    def test_user_reset_password(self):
        url = '{}password_reset/'.format(baseurl)
        user = User.objects.get_or_create(username='testuser',email='support@zephyruscomputing.com')[0]
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        csrf_token = str(response.content.split(b'csrfmiddlewaretoken" value="')[1].split(b'">\n')[0])[2:-1]
        response = self.client.post(url,data={'email':user.email,'csrfmiddlewaretoken':csrf_token})
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        response = self.client.post(url,data={'email':'banana@zephyruscomputing','csrfmiddlewaretoken':csrf_token})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'registration/password_reset_form.html')
        self.assertContains(response, "Enter a valid email address.")
        code = default_token_generator.make_token(user)
        reset_url = '{}reset/MQ/{}/'.format(baseurl,code)
        response = self.client.get(reset_url)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        response = self.client.get(reset_url, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        csrf_token = str(response.content.split(b'csrfmiddlewaretoken" value="')[1].split(b'">\n')[0])[2:-1]
        password = ''.join([choice('1234567890abcefghijklmnopqrstuvwxyz') for i in range(16)])
        url = '{}reset/MQ/set-password/'.format(baseurl)
        response = self.client.post(url,data={'username':user.username,'new_password1':'1234','new_password2':'1234','csrfmiddlewaretoken':csrf_token})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'registration/password_reset_confirm.html')
        self.assertContains(response, "This password is too short. It must contain at least 8 characters.")
        self.assertContains(response, "This password is too common.")
        self.assertContains(response, "This password is entirely numeric.")
        response = self.client.post(url,data={'username':user.username,'new_password1':'1234','new_password2':'4321','csrfmiddlewaretoken':csrf_token})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'registration/password_reset_confirm.html')
        self.assertContains(response, "The two password fields didn’t match.")
        self.assertNotContains(response, "This password is too short. It must contain at least 8 characters.")
        self.assertNotContains(response, "This password is too common.")
        self.assertNotContains(response, "This password is entirely numeric.")
        response = self.client.post(url,data={'username':user.username,'new_password1':password,'new_password2':password,'csrfmiddlewaretoken':csrf_token})
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        user.refresh_from_db()
        self.assertTrue(user.check_password(password))