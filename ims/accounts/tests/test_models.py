import django
django.setup()
from datetime import timedelta
from django.contrib.auth.models import Group
from django.test import TestCase
from django.urls import reverse
from django.utils.timezone import now
from random import randint
from accounts.models import Invite

# Unit tests for Invite
class InviteUnitTests(TestCase):
    def setUp(self):
        for i in range(1,11):
            Group.objects.get_or_create(name='test_group_{}'.format(i))
        for i in range(1,101):
            j = randint(1,10)
            Invite.objects.create(email='support+{}@zephyruscomputing.com'.format(i),group=Group.objects.get(name='test_group_{}'.format(j)))

    def test_invite_str(self):
        i = randint(1,100)
        email = 'support+{}@zephyruscomputing.com'.format(i)
        invite = Invite.objects.get(email=email)
        self.assertEqual(invite.__str__(), email)

    def test_invite_abosolute_url(self):
        i = randint(1,100)
        invite = Invite.objects.get(email='support+{}@zephyruscomputing.com'.format(i))
        self.assertEqual(invite.get_absolute_url(), reverse('accounts:invite_code', args=[invite.code]))

    def test_invite_save(self):
        i = randint(1,100)
        expires = now() + timedelta(hours=2)
        invite = Invite.objects.get(email='support+{}@zephyruscomputing.com'.format(i))
        invite.expires = expires
        invite.save()
        invite.refresh_from_db()
        self.assertEqual(invite.expires.timestamp(), expires.timestamp())

    def test_invite_delete(self):
        i = randint(1,100)
        invite = Invite.objects.get(email='support+{}@zephyruscomputing.com'.format(i))
        invite.delete()
        self.assertFalse(len(Invite.objects.filter(email='support+{}@zephyruscomputing.com')) > 0)
