import django
django.setup()
from datetime import timedelta
from django.conf import settings
from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase, override_settings
from django.utils.timezone import now
from os import listdir
from random import randint,choice
from rest_framework import status
from accounts.models import Invite, Token

baseurl = '/accounts/admin/'

# Test module root view
class AccountsRootViewTests(TestCase):
    def test_view_root(self):
        response = self.client.get(baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user = User.objects.get_or_create(username='testuser')[0]
        permission = Permission.objects.get(codename='view_invite')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get(baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'accounts/admin/home.html')

# Test Invite Admin views
class AccountsInviteAdminViewTests(TestCase):
    def setUp(self):
        for i in range(0,10):
            Group.objects.create(name='Test_View_Group_{}'.format(i))
        for i in range(1000,1010):
            j = randint(0,9)
            Invite.objects.create(email='support+{}@zephyruscomputing.com'.format(i), group=Group.objects.get(name='Test_View_Group_{}'.format(j)), expires=now() + timedelta(hours=1))
        self.baseurl = '{}invite/'.format(baseurl)

    def test_view_invite_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_invite')
        TestGenerics().get(self, self.baseurl, permission, 'accounts/admin/view.html')
        # Test specifics
        response = self.client.get('{}?q=Test&page=10'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_invite_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_invite')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'accounts/admin/create.html')
        # Test email gets sent out
        with override_settings(EMAIL_BACKEND='django.core.mail.backends.filebased.EmailBackend',EMAIL_FILE_PATH='{}/../logs/email/'.format(settings.BASE_DIR)):
            datetimestamp = now().strftime('%Y%m%d-%H%M')
            group = Group.objects.get(name='Test_View_Group_{}'.format(randint(0,9)))
            data = {'email':'support@zephyruscomputing.com','group':group.id,'expires':''}
            TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
            passed=False
            invite = Invite.objects.get(email='support@zephyruscomputing.com')
            for file in listdir('{}/../logs/email/'.format(settings.BASE_DIR)):
                if datetimestamp in file:
                    with open('{}/../logs/email/{}'.format(settings.BASE_DIR, file)) as f:
                        if invite.code in f.read():
                            passed=True
            self.assertTrue(passed)
        data = {'email': 'banana'}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_invite_change(self):
        # Test the change view
        permission = Permission.objects.get(codename='change_invite')
        invite = Invite.objects.last()
        TestGenerics().get(self, '{}change/{}/'.format(self.baseurl, invite.id), permission, 'accounts/admin/update.html')
        data = {'email':'sales@zephyruscomputing.com','group':invite.group.id,'expires':invite.expires}
        old = invite.email
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, invite.id), data, permission)
        invite.refresh_from_db()
        self.assertEqual(invite.email, 'sales@zephyruscomputing.com')
        data = {'group': ''}
        self.client.post('{}change/{}/'.format(self.baseurl, invite.id), data)

    def test_view_invite_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_invite')
        invite = Invite.objects.last()
        response = self.client.delete('{}delete/{}/'.format(self.baseurl, invite.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Permission Denied')
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, invite.id), permission)

# Test Token Admin views
class AccountsTokenAdminViewTests(TestCase):
    def setUp(self):
        for i in range(1010,1020):
            User.objects.create(username='test_user_{}'.format(i))
        for i in range(0,10):
            j = randint(0,9)
            Token.objects.create(user=User.objects.all()[j], expires=now() + timedelta(hours=1))
        self.baseurl = '{}token/'.format(baseurl)

    def test_view_token_view(self):
        # Test the view view
        permission = Permission.objects.filter(codename='view_token')[0]
        TestGenerics().get(self, self.baseurl, permission, 'accounts/admin/view.html')
        # Test specifics
        response = self.client.get('{}?q=Test&page=10'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_token_create(self):
        # Test the create view
        permission = Permission.objects.filter(codename='add_token')[0]
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'accounts/admin/create.html')
        data = {'user':User.objects.last().id,'expires':''}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)

    def test_view_token_delete(self):
        # Test the delete view
        permission = Permission.objects.filter(codename='delete_token')[0]
        token = Token.objects.last()
        response = self.client.delete('{}delete/{}/'.format(self.baseurl, token.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Permission Denied')
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, token.id), permission)

# Test User Admin Views
class AccountsUserAdminViewTests(TestCase):
    def setUp(self):
        permission = Permission.objects.get(codename='view_storage')
        for i in range(10,20):
            Group.objects.create(name='test_group_{}'.format(i))
        for i in range(0,5):
            user = User.objects.create(username='test_user-{}'.format(i),email='support+{}@zephyruscomputing.com'.format(i),first_name='test',last_name='user')
            user.user_permissions.add(permission)
            user.groups.add(Group.objects.last())
        self.baseurl = '{}user/'.format(baseurl)

    def test_view_user_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_user')
        TestGenerics().get(self, self.baseurl, permission, 'accounts/admin/view.html')
        # Test specifics
        response = self.client.get('{}?q=Test&page=10'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_user_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_user')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'accounts/admin/user_create.html')
        groups = []
        for i in range(0,randint(1,4)):
            j = randint(0,8)
            groups.append(Group.objects.all()[j].id)
        perms = []
        for i in range(0,randint(1,5)):
            j = randint(0,15)
            perms.append(Permission.objects.filter(content_type__in=ContentType.objects.filter(app_label='inventory'))[j].id)
        data = {'username':'new_test_user','first_name':'new','last_name':'test_user','email':'support@zephyruscomputing.com','groups[]':groups,'perms[]':perms}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        response = self.client.get('{}create/'.format(self.baseurl))
        self.assertNotContains(response, 'Can view log entry')
        self.assertNotContains(response, 'Can add content type')
        self.assertNotContains(response, 'Can delete session')
        groups.append(0)
        perms.append(0)
        data = {'username':'new_test_user','first_name':'new','last_name':'test_user','email':'support@zephyruscomputing.com','groups[]':groups,'perms[]':perms}
        response = self.client.post('{}create/'.format(self.baseurl), data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_user_change(self):
        # Test the change view
        user = User.objects.first()
        permission = Permission.objects.get(codename='change_user')
        TestGenerics().get(self, '{}change/{}/'.format(self.baseurl, user.id), permission, 'accounts/admin/user_update.html')
        groups = []
        for i in range(0,randint(1,4)):
            j = randint(0,9)
            groups.append(Group.objects.all()[j].id)
        perms = []
        for i in range(0,randint(1,5)):
            j = randint(0,15)
            perms.append(Permission.objects.filter(content_type__in=ContentType.objects.filter(app_label='inventory'))[j].id)
        data = {'username':user.username,'first_name':'new','last_name':user.last_name,'email':user.email,'groups[]':groups,'perms[]':perms,'must_change_password':True}
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, user.id), data, permission)
        response = self.client.get('{}change/{}/'.format(self.baseurl, user.id))
        self.assertNotContains(response, 'Can view log entry')
        self.assertNotContains(response, 'Can add content type')
        self.assertNotContains(response, 'Can delete session')
        user.refresh_from_db()
        self.assertTrue(user.metadata.must_change_password)
        groups.append(0)
        perms.append(0)
        data = {'username':user.username,'first_name':'new','last_name':user.last_name,'email':user.email,'groups[]':groups,'perms[]':perms,}
        response = self.client.post('{}change/{}/'.format(self.baseurl, user.id), data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_user_change_password(self):
        # Test the change view
        user = User.objects.first()
        permission = Permission.objects.get(codename='change_user')
        TestGenerics().get(self, '{}change/{}/password/'.format(self.baseurl, user.id), permission, 'accounts/admin/user_update.html')
        password = ''.join([choice('1234567890abcefghijklmnopqrstuvwxyz') for i in range(16)])
        data = {'password1':password,'password2':password}
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, user.id), data, permission)
        response = self.client.get('{}change/{}/password/'.format(self.baseurl, user.id))
        csrf_token = str(response.content.split(b'csrfmiddlewaretoken" value="')[1].split(b'">\n')[0])[2:-1]
        data = {'csrf_token':csrf_token,'password1':password,'password2':'B@nana'}
        response = self.client.post('{}change/{}/password/'.format(self.baseurl, user.id), data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "The two password fields didn&#x27;t match.")
        response = self.client.get('{}change/{}/password/'.format(self.baseurl, user.id))
        csrf_token = str(response.content.split(b'csrfmiddlewaretoken" value="')[1].split(b'">\n')[0])[2:-1]
        data = {'csrf_token':csrf_token,'password1':'1234','password2':'1234'}
        response = self.client.post('{}change/{}/password/'.format(self.baseurl, user.id), data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "This password is too short. It must contain at least 8 characters.")
        self.assertContains(response, "This password is too common.")
        self.assertContains(response, "This password is entirely numeric.")
        response = self.client.get('{}change/{}/password/'.format(self.baseurl, user.id))
        csrf_token = str(response.content.split(b'csrfmiddlewaretoken" value="')[1].split(b'">\n')[0])[2:-1]
        data = {'csrf_token':csrf_token,'password1':password,'password2':password}
        response = self.client.post('{}change/{}/password/'.format(self.baseurl, user.id), data=data)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

    def test_view_user_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_user')
        user = User.objects.last()
        response = self.client.delete('{}delete/{}/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Permission Denied')
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, user.id), permission)

    def test_view_user_unlock(self):
        # Test the unlock view
        permission = Permission.objects.get(codename='change_user')
        user = User.objects.last()
        response = self.client.get('{}unlock/{}/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Permission Denied')
        admin = User.objects.get_or_create(username='testuser')[0]
        admin.user_permissions.add(permission)
        self.client.force_login(admin)
        response = self.client.get('{}unlock/{}/'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, 'Permission Denied')
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')

# Test Group Admin Views
class AccountsGroupAdminViewTests(TestCase):
    def setUp(self):
        permission = Permission.objects.get(codename='view_storage')
        for i in range(10,20):
            group = Group.objects.create(name='test_group_{}'.format(i))
            group.permissions.add(permission)
        self.baseurl = '{}group/'.format(baseurl)

    def test_view_group_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_group')
        TestGenerics().get(self, self.baseurl, permission, 'accounts/admin/view.html')
        # Test specifics
        response = self.client.get('{}?q=Test&page=10'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_group_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_group')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'accounts/admin/user_create.html')
        perms = []
        for i in range(0,randint(1,5)):
            j = randint(0,15)
            perms.append(Permission.objects.filter(content_type__in=ContentType.objects.filter(app_label='inventory'))[j].id)
        data = {'name':'new_test_group','perms[]':perms}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        response = self.client.get('{}create/'.format(self.baseurl))
        self.assertNotContains(response, 'Can view log entry')
        self.assertNotContains(response, 'Can add content type')
        self.assertNotContains(response, 'Can delete session')
        perms.append(0)
        data = {'name':'new_test_group','perms[]':perms}
        response = self.client.post('{}create/'.format(self.baseurl), data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_group_change(self):
        # Test the change view
        group = Group.objects.first()
        permission = Permission.objects.get(codename='change_group')
        TestGenerics().get(self, '{}change/{}/'.format(self.baseurl, group.id), permission, 'accounts/admin/user_update.html')
        perms = []
        for i in range(0,randint(1,5)):
            j = randint(0,15)
            perms.append(Permission.objects.filter(content_type__in=ContentType.objects.filter(app_label='inventory'))[j].id)
        data = {'name':group.name,'perms[]':perms}
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, group.id), data, permission)
        response = self.client.get('{}change/{}/'.format(self.baseurl, group.id))
        self.assertNotContains(response, 'Can view log entry')
        self.assertNotContains(response, 'Can add content type')
        self.assertNotContains(response, 'Can delete session')
        perms.append(0)
        data = {'name':group.name,'perms[]':perms}
        response = self.client.post('{}change/{}/'.format(self.baseurl, group.id), data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_group_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_group')
        group = Group.objects.last()
        response = self.client.delete('{}delete/{}/'.format(self.baseurl, group.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Permission Denied')
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, group.id), permission)


class TestGenerics():
    def get(self, case, url, perm, temp):
        response = case.client.get(url)
        case.assertRedirects(response, '/accounts/login/?next={}'.format(url))
        user = User.objects.get_or_create(username='testuser')[0]
        user.user_permissions.add(perm)
        case.client.force_login(user)
        response = case.client.get(url)
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        case.assertTemplateUsed(response, temp)

    def post(self, case, url, data, perm):
        user = User.objects.get_or_create(username='testuser')[0]
        user.user_permissions.add(perm)
        case.client.force_login(user)
        response = case.client.post(url, data)
        case.assertNotEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        case.assertNotEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        case.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        case.assertNotEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        case.assertNotEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)

    def delete(self, case, url, perm):
        user = User.objects.get_or_create(username='testuser')[0]
        user.user_permissions.add(perm)
        case.client.force_login(user)
        response = case.client.get(url)
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        case.assertTemplateUsed(response, 'inventory/ajax_result.html')
        case.assertContains(response, 'green')
