import django
django.setup()
from django.contrib.auth.models import User, Group, Permission
from django.test import TestCase
from accounts.models import Invite
from accounts.forms import InviteForm, GroupCreationForm, ExtendedUserCreationForm, TokenForm
import accounts.helpers as helpers

# Unit test for lookup methods
class LookupMethodsUnitTests(TestCase):
    def setUp(self):
        for i in range(1,10):
            Group.objects.create(name='test_group_{}'.format(i))
            Invite.objects.create(email='support+{}@zephyruscomputing.com'.format(i),group=Group.objects.last())
            User.objects.create(username='test_user_{}'.format(i),first_name='test',last_name='user',email='support+{}@zephyruscomputing.com'.format(i))

    def test_get_admin_permission(self):
        actions = ['view','add','change','delete']
        models = ['Invite','Token']
        for model in models:
            for action in actions:
                result = helpers.get_admin_permission('accounts',model, action)
                self.assertNotEqual(result, 'accounts.{}_{}'.format(action, model))
                self.assertEqual(result, 'accounts.{}_{}'.format(action, model.lower()))
        models = ['Group','User']
        for model in models:
            for action in actions:
                result = helpers.get_admin_permission('auth',model, action)
                self.assertNotEqual(result, 'auth.{}_{}'.format(action, model))
                self.assertEqual(result, 'auth.{}_{}'.format(action, model.lower()))

    def test_get_object(self):
        expected = Invite.objects.last()
        result = helpers.get_object('Invite', expected.id)
        self.assertEqual(expected, result)
        expected = Group.objects.last()
        result = helpers.get_object('Group', expected.id)
        self.assertEqual(expected, result)
        expected = User.objects.last()
        result = helpers.get_object('User', expected.id)
        self.assertEqual(expected, result)
        result = helpers.get_object('Cat', expected.id)
        self.assertEqual('', result)
        with self.assertRaises(Group.DoesNotExist):
            helpers.get_object('Group', 0)
        with self.assertRaises(User.DoesNotExist):
            helpers.get_object('User', 0)

    def test_get_object_list(self):
        expected = Invite.objects.last()
        result = helpers.get_object_list('Invite')
        self.assertIn(expected, result)
        expected = Group.objects.last()
        result = helpers.get_object_list('Group')
        self.assertIn(expected, result)
        expected = User.objects.last()
        result = helpers.get_object_list('User')
        self.assertIn(expected, result)
        result = helpers.get_object_list('Cat')
        self.assertEqual('', result)

    def test_get_search_fields(self):
        for model in ['Group','User','Cat']:
            result = helpers.get_search_fields(model)
            if model == 'Invite':
                self.assertTrue('email' in result)
                self.assertTrue('group__name' in result)
                self.assertTrue('expires' in result)
            if model == 'Group':
                self.assertTrue('name' in result)
            if model == 'User':
                self.assertTrue('username' in result)
                self.assertTrue('first_name' in result)
                self.assertTrue('last_name' in result)
                self.assertTrue('email' in result)
            if model == 'Cat':
                self.assertEqual(result, [])

    def test_get_admin_form_p(self):
        result = helpers.get_admin_form_p('Invite')
        self.assertIsInstance(result, InviteForm)
        result = helpers.get_admin_form_p('Group')
        self.assertIsInstance(result, GroupCreationForm)
        result = helpers.get_admin_form_p('User')
        self.assertIsInstance(result, ExtendedUserCreationForm)
        with self.assertRaises(Exception):
            result = helpers.get_admin_form_p('Cat')

    def test_get_admin_form_g(self):
        result = helpers.get_admin_form_g('Invite')
        self.assertIsInstance(result, InviteForm)
        result = helpers.get_admin_form_g('Group')
        self.assertIsInstance(result, GroupCreationForm)
        result = helpers.get_admin_form_g('User')
        self.assertIsInstance(result, ExtendedUserCreationForm)
        with self.assertRaises(Exception):
            result = helpers.get_admin_form_g('Cat')

    def test_get_form_object_name(self):
        form = GroupCreationForm({'name':'new_group'})
        if not form.is_valid():
            self.fail
        result = helpers.get_form_object_name(form)
        self.assertEqual(result, 'new_group')
        form = InviteForm({'email':'support@zephyruscomputing.com','code':'123','expires':None,'group':Group.objects.first().id})
        if not form.is_valid():
            self.fail
        result = helpers.get_form_object_name(form)
        self.assertEqual(result,'support@zephyruscomputing.com')
        form = TokenForm({'expires':None,'key':'123','user':User.objects.first().id})
        if not form.is_valid():
            self.fail
        result = helpers.get_form_object_name(form)
        self.assertEqual(result, User.objects.first().username)
        form = ExtendedUserCreationForm({'username':'new_user','first_name':'new','last_name':'user','email':'support@zephyruscomputing.com','password1':'123','password2':'123'})
        if not form.is_valid():
            self.fail
        result = helpers.get_form_object_name(form)
        self.assertEqual(result, 'new_user')
        result = helpers.get_form_object_name(User.objects.first())
        self.assertEqual(result, '')

    def test_compare_lists(self):
        list1 = Group.objects.filter(name__in=['test_group_1','test_group_2'])
        list2 = Group.objects.filter(name__in=['test_group_1','test_group_2'])
        result1, result2 = helpers.compare_lists(list1, list2)
        self.assertEqual(result1, [])
        self.assertEqual(result2, [])
        list1 = Group.objects.filter(name__in=['test_group_1','test_group_2'])
        list2 = Group.objects.filter(name__in=['test_group_2','test_group_9'])
        result1, result2 = helpers.compare_lists(list1, list2)
        self.assertEqual(result1, [Group.objects.first()])
        self.assertEqual(result2, [Group.objects.last()])

    def test_compare_groups(self):
        user = User.objects.first()
        result_remove, result_add = helpers.compare_groups(user, [Group.objects.get(name='test_group_7').id])
        self.assertEqual(result_remove, [])
        self.assertEqual(result_add, [Group.objects.get(name='test_group_7')])
        user.groups.add(Group.objects.get(name='test_group_7'))
        user.save()
        result_remove, result_add = helpers.compare_groups(user, [Group.objects.get(name='test_group_1').id])
        self.assertEqual(result_remove, [Group.objects.get(name='test_group_7')])
        self.assertEqual(result_add, [Group.objects.get(name='test_group_1')])

    def test_compare_permissions(self):
        user = User.objects.last()
        result_remove, result_add = helpers.compare_permissions(user, [Permission.objects.first().id])
        self.assertEqual(result_remove, [])
        self.assertEqual(result_add, [Permission.objects.first()])
        user.user_permissions.add(Permission.objects.first())
        user.save()
        perms = Permission.objects.filter(codename__contains='kit')
        pids = []
        plist = []
        for perm in perms:
            plist.append(perm)
            pids.append(perm.id)
        result_remove, result_add = helpers.compare_permissions(user, pids)
        self.assertEqual(result_remove, [Permission.objects.first()])
        self.assertEqual(result_add, plist)

    def test_parse_querytype(self):
        result = helpers.parse_querytype(1)
        self.assertEqual(result, '^')
        result = helpers.parse_querytype('1')
        self.assertEqual(result, '^')
        result = helpers.parse_querytype(2)
        self.assertEqual(result, '=')
        result = helpers.parse_querytype('2')
        self.assertEqual(result, '=')
        result = helpers.parse_querytype(3)
        self.assertEqual(result, '')
        result = helpers.parse_querytype('3')
        self.assertEqual(result, '')
        result = helpers.parse_querytype('banana')
        self.assertEqual(result, '')
        result = helpers.parse_querytype([])
        self.assertEqual(result, '')
        result = helpers.parse_querytype(None)
        self.assertEqual(result, '')