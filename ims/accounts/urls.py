from django.conf import settings
from django.urls import path,re_path
from accounts import views

app_name='accounts'
urlpatterns = [
    path('invite/<token>/', views.invite_code, name='invite_code'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.logout_view, name='logout'),
    path('profile/', views.user_profile, name='user_profile'),
    path('profile/edit/', views.user_edit, name='user_edit'),
    path('profile/token/', views.ajax_view_tokens, name='ajax_view_tokens'),
    path('profile/token/create/', views.ajax_create_token, name='ajax_create_token'),
    re_path(r'^profile/token/(?P<id>[\d]+)/$', views.view_token, name='view_token'),
    re_path(r'^profile/token/(?P<id>[\d]+)/delete/$', views.ajax_delete_token, name='ajax_delete_token'),
    path('signup/', views.sign_up, name='sign_up'),
]
