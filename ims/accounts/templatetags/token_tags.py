from django import forms, template
from django.utils.safestring import mark_safe
register = template.Library()

@register.filter('obfuscate_token')
def obfuscate_token(obj):
    return '{}{}{}'.format(obj[:5],'*'*30,obj[-5:])

