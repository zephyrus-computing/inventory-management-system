from binascii import hexlify
from django.conf import settings
from django.contrib.auth.models import Group, User
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from os import urandom
from secrets import token_urlsafe
import logging

logger = logging.getLogger(__name__)


class Invite(models.Model):
    email = models.EmailField()
    code = models.CharField(max_length=86)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    expires = models.DateTimeField(default=None,editable=True,blank=True,null=True)
    created = models.DateTimeField(auto_now_add=True,editable=False)
    lastmodified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['email']

    def save(self, *args, **kwargs):
        if self.id != None:
            logger.info('Saving changes made to Invite id {}'.format(self.id))
        else:
            self.code = token_urlsafe(48)
            logger.info('Creating new Invite "{}"'.format(self.email))
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        logger.info('Deleting Invite id {}'.format(self.id))
        super().delete(*args, **kwargs)

    def __str__(self):
        return self.email

    def get_absolute_url(self):
        logger.debug('Generating absolute URL for Invite id {}'.format(self.id))
        return reverse('accounts:invite_code', args=[self.code])

class Token(models.Model):
    key = models.CharField(_("Key"), max_length=40)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='ims_authtoken', on_delete=models.CASCADE, verbose_name=_("User"))
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    expires = models.DateTimeField(_("Expires"), default=None,editable=True,blank=True,null=True)

    class Meta:
        abstract = 'rest_framework.authtoken' not in settings.INSTALLED_APPS
        ordering = ['expires']
        verbose_name = _("Token")
        verbose_name_plural = _("Tokens")

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super().save(*args, **kwargs)
    
    @classmethod
    def generate_key(cls):
        return hexlify(urandom(20)).decode()
    
    def __str__(self):
        return self.key
    
class UserMetadata(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE,primary_key=True,related_name='metadata')
    bad_password_attempts = models.PositiveSmallIntegerField(default=0)
    is_locked_out = models.BooleanField(default=False)
    last_set_password_datetime = models.DateTimeField(auto_now_add=True, editable=False)
    last_lockout_datetime = models.DateTimeField(editable=False, blank=True, null=True,)
    last_bad_password_datetime = models.DateTimeField(editable=False, blank=True, null=True)
    must_change_password = models.BooleanField(default=False)