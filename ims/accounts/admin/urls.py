from django.urls import path,re_path
from accounts.admin import views

app_name='accounts.admin'
urlpatterns = [
    path('', views.home, name='home'),
    path('invite/', views.admin_invite_view, name='admin_invite_view'),
    path('invite/create/', views.admin_invite_create, name='admin_invite_create'),
    re_path(r'^invite/change/(?P<id>[\d]+)/$', views.admin_invite_change, name='admin_invite_change'),
    re_path(r'^invite/delete/(?P<id>[\d]+)/$', views.ajax_invite_delete, name='ajax_invite_delete'),
    path('token/', views.admin_token_view, name='admin_token_view'),
    path('token/create/', views.admin_token_create, name='admin_token_create'),
    re_path(r'^token/delete/(?P<id>[\d]+)/$', views.ajax_token_delete, name='ajax_token_delete'),
    path('group/', views.admin_group_view, name='admin_group_view'),
    path('group/create/', views.admin_group_create, name='admin_group_create'),
    re_path(r'^group/change/(?P<id>[\d]+)/$', views.admin_group_change, name='admin_group_change'),
    re_path(r'^group/delete/(?P<id>[\d]+)/$', views.ajax_group_delete, name='ajax_group_delete'),
    path('user/', views.admin_user_view, name='admin_user_view'),
    path('user/create/', views.admin_user_create, name='admin_user_create'),
    re_path(r'^user/change/(?P<id>[\d]+)/password/$', views.admin_user_change_password, name='admin_user_change_password'),
    re_path(r'^user/change/(?P<id>[\d]+)/$', views.admin_user_change, name='admin_user_change'),
    re_path(r'^user/delete/(?P<id>[\d]+)/$', views.ajax_user_delete, name='ajax_user_delete'),
    re_path(r'^user/unlock/(?P<id>[\d]+)/$', views.ajax_user_unlock, name='ajax_user_unlock'),
]