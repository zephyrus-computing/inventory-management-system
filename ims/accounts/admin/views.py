from django.contrib.auth.decorators import login_required,permission_required
from django.contrib.auth.models import User, Group, Permission
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
import logging

from accounts.forms import GroupCreationForm, PermissionForm, UserCreationAdminForm, UserGroupForm, UserMetadataForm, UserPasswordForm
from accounts.models import Invite
from inventory.search import Search
from log.logger import getLogger

import accounts.helpers as helpers

logger = logging.getLogger(__name__)
logger2 = getLogger('accounts-admin')

@permission_required('accounts.view_invite')
def admin_invite_view(request):
    return admin_generic_view('admin_invite_view', 'Invite', request, 'accounts.view_invite')

@permission_required('accounts.add_invite')
def admin_invite_create(request):
    rendered = admin_generic_create('admin_invite_create', 'Invite', request, 'accounts.add_invite')
    if type(rendered) == HttpResponseRedirect:
        email = request.POST.get('email')
        invite = Invite.objects.filter(email=email).last()
        helpers.send_email_from_template([email], 'accounts/invite.html', subject='Create Your Account', invite_code=invite.code, email=email, support_email=settings.DEFAULT_FROM_EMAIL, expiration_date=invite.expires, site_url=request.build_absolute_uri('/'))
    return rendered

@permission_required('accounts.change_invite')
def admin_invite_change(request, id):
    return admin_generic_change('admin_invite_change', 'Invite', request, id, 'accounts.change_invite')

def ajax_invite_delete(request, id):
    return ajax_generic_delete('ajax_invite_delete', 'Invite', request, id, 'accounts.delete_invite')

@permission_required('accounts.view_token')
def admin_token_view(request):
    return admin_generic_view('admin_token_view', 'Token', request, 'accounts.view_token')

@permission_required('accounts.add_token')
def admin_token_create(request):
    return admin_generic_create('admin_token_create', 'Token', request, 'accounts.add_token')

def ajax_token_delete(request, id):
    return ajax_generic_delete('ajax_token_delete', 'Token', request, id, 'accounts.delete_token')

@permission_required('auth.add_user')
def admin_user_create(request):
    cname = 'admin_user_create'
    model = 'User'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view Admin Create {} page'.format(model))
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        user_form = UserCreationAdminForm(request.POST)
        if request.POST.__contains__('groups[]'):
            request.POST._mutable = True
            request.POST.setlist('groups',request.POST.getlist('groups[]'))
            request.POST.pop('groups[]')
            request.POST._mutable = False
        group_form = UserGroupForm(request.POST)
        if request.POST.__contains__('perms[]'):
            request.POST._mutable = True
            request.POST.setlist('perms',request.POST.getlist('perms[]'))
            request.POST.pop('perms[]')
            request.POST._mutable = False
        permission_form = PermissionForm(request.POST)
        if user_form.is_valid() and group_form.is_valid() and permission_form.is_valid():
            try:
                logger.debug('{} is valid.'.format(user_form.__class__.__name__))
                logger.info('Creating new User "{}"'.format(model, helpers.get_form_object_name(user_form)))
                inst = user_form.save()
                logger2.info(request.user, 'Created {} id {}'.format(model, inst.pk))
                for group in group_form.cleaned_data['groups']:
                    g = Group.objects.get(id=group)
                    if not g.user_set.contains(inst):
                        g.user_set.add(inst)
                        logger.debug('Added new user {} to group {}'.format(inst.username, g.name))
                        logger2.info(request.user, 'Added user "{}" to group "{}"'.format(inst.username, g.name))
                for perm in permission_form.cleaned_data['perms']:
                    p = Permission.objects.get(id=perm)
                    if not p.user_set.contains(inst):
                        p.user_set.add(inst)
                        logger.debug('Added new user {} to permission {}'.format(inst.username, p.name))
                        logger2.info(request.user, 'Added user "{}" to permission "{}"'.format(inst.username, p.name))
            except Exception as err:
                logger.error(err)
                logger2.warning(request.user, 'Issue with creating new user account: {}'.format(err))
                return render(request, 'accounts/admin/user_create.html', {'name': model, 'user_form': user_form, 'group_form': group_form, 'permission_form': permission_form, 'message': 'There was an error with creating the user account.'})
        else:
            logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
            logger2.warning(request.user, 'Create {} Form not valid'.format(model))
            return render(request, 'accounts/admin/user_create.html', {'name': model, 'user_form': user_form, 'group_form': group_form, 'permission_form': permission_form, 'message': 'Form is not valid.'})
        return redirect('/accounts/admin/{}/'.format(model.lower()))
    else:
        user_form = UserCreationAdminForm()
        group_form = UserGroupForm()
        permission_form = PermissionForm()
        logger.debug('Generated new forms to create a User')
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Create {} page'.format(model))
        return render(request, 'accounts/admin/user_create.html', {'name': model, 'user_form': user_form, 'group_form': group_form, 'permission_form': permission_form})

@permission_required('auth.view_user')
def admin_user_view(request):
    return admin_generic_view('admin_user_view', 'User', request, 'auth.view_user')

@permission_required('auth.change_user')
def admin_user_change(request, id):
    cname = 'admin_user_change'
    model = 'User'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view Admin Change {} page'.format(model))
    user = get_object_or_404(User, id=id)
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        user_form = UserCreationAdminForm(request.POST, instance=user)
        if request.POST.__contains__('groups[]'):
            request.POST._mutable = True
            request.POST.setlist('groups',request.POST.getlist('groups[]'))
            request.POST.pop('groups[]')
            request.POST._mutable = False
        group_form = UserGroupForm(request.POST)
        if request.POST.__contains__('perms[]'):
            request.POST._mutable = True
            request.POST.setlist('perms',request.POST.getlist('perms[]'))
            request.POST.pop('perms[]')
            request.POST._mutable = False
        permission_form = PermissionForm(request.POST)
        meta_form = UserMetadataForm(request.POST)
        if user_form.is_valid() and group_form.is_valid() and permission_form.is_valid():
            try:
                logger.debug('{} is valid.'.format(user_form.__class__.__name__))
                logger.info('Updating User "{}"'.format(model, helpers.get_form_object_name(user_form)))
                user_form.save()
                logger2.info(request.user, 'Updated {} id {}: {}'.format(model, id, user_form.changed_data))
                remove, add = helpers.compare_groups(user, group_form.cleaned_data['groups'])
                for group in remove:
                    group.user_set.remove(user)
                    logger.debug('Removed user {} from group {}'.format(user.username, group.name))
                    logger2.info(request.user, 'Removed user "{}" from group "{}"'.format(user.username, group.name))
                for group in add:
                    group.user_set.add(user)
                    logger.debug('Added user {} to group {}'.format(user.username, group.name))
                    logger2.info(request.user, 'Added user "{}" to group "{}"'.format(user.username, group.name))
                remove, add = helpers.compare_permissions(user, permission_form.cleaned_data['perms'])
                for perm in remove:
                    perm.user_set.remove(user)
                    logger.debug('Removed permission {} from user {}'.format(perm.name, user.username))
                    logger2.info(request.user, 'Removed permission "{}" from user "{}"'.format(perm.name, user.username))
                for perm in add:
                    perm.user_set.add(user)
                    logger.debug('Added permission {} to user {}'.format(perm.name, user.username))
                    logger2.info(request.user, 'Added permission "{}" to user "{}"'.format(perm.name, user.username))
                if meta_form.is_valid():
                    mcp = meta_form.cleaned_data['must_change_password']
                    if user.metadata.must_change_password != mcp:
                        logger.debug('Set user must change password to {} for user {}'.format(mcp, user.username))
                        logger2.info(request.user, 'Set user must change password to {} for user {}'.format(mcp, user.username))
                        user.metadata.must_change_password = mcp
                        user.metadata.save()
            except Exception as err:
                logger.error(err)
                logger2.warning(request.user, 'Issue with updating user account: {}'.format(err))
                return render(request, 'accounts/admin/user_update.html', {'objtype': model, 'name': user.username, 'user_form': user_form, 'group_form': group_form, 'permission_form': permission_form, 'metadata_form': meta_form, 'message': 'There was an error with updating the user account.'})
        else:
            logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
            logger2.warning(request.user, 'Update {} Form not valid'.format(model))
            return render(request, 'accounts/admin/user_update.html', {'objtype': model, 'name': user.username, 'user_form': user_form, 'group_form': group_form, 'permission_form': permission_form, 'metadata_form': meta_form, 'message': 'Form is not valid.'})
        return redirect('/accounts/admin/{}/'.format(model.lower()))
    else:
        user_form = UserCreationAdminForm(instance=user)
        meta_form = UserMetadataForm(initial={'must_change_password':user.metadata.must_change_password,
                                              'bad_password_attempts':user.metadata.bad_password_attempts,
                                              'last_bad_password_datetime':user.metadata.last_bad_password_datetime,
                                              'last_lockout_datetime':user.metadata.last_lockout_datetime,
                                              'last_set_password_datetime':user.metadata.last_set_password_datetime
        })
        group_form = UserGroupForm()
        _initial = []
        _groups = user.groups.all()
        for g in _groups:
            _initial.append(g.id)
        group_form.fields['groups'].initial = _initial
        permission_form = PermissionForm()
        _initial = []
        _perms = user.user_permissions.all()
        for p in _perms:
            _initial.append(p.id)
        permission_form.fields['perms'].initial = _initial
        logger.debug('Generated new forms to change a User')
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Change {} page'.format(model))
        return render(request, 'accounts/admin/user_update.html', {'objtype': model, 'name': user.username, 'user_form': user_form, 'group_form': group_form, 'permission_form': permission_form, 'metadata_form': meta_form})

@permission_required('auth.change_user')
def admin_user_change_password(request, id):
    cname = 'admin_user_change_password'
    model = 'User'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view Admin Change {} page'.format(model))
    user = get_object_or_404(User, id=id)
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        user_form = UserPasswordForm(request.POST)
        if user_form.is_valid():
            try:
                logger.debug('{} is valid.'.format(user_form.__class__.__name__))
                logger.info('Updating User\'s Password "{}"'.format(model, user.username))
                user.set_password(user_form.cleaned_data['password2'])
                user.save()
                logger2.info(request.user, 'Updated {} id {}: Password'.format(model, id))
            except Exception as err:
                logger.error(err)
                logger2.warning(request.user, 'Issue with updating user account password: {}'.format(err))
                return render(request, 'accounts/admin/user_update.html', {'objtype': model, 'name': user.username, 'user_form': user_form, 'message': 'There was an error with updating the user account.'})
        else:
            logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
            logger2.warning(request.user, 'Update {} Password Form not valid'.format(model))
            return render(request, 'accounts/admin/user_update.html', {'objtype': model, 'name': user.username, 'user_form': user_form, 'message': 'Form is not valid.'})
        return redirect('/accounts/admin/{}/'.format(model.lower()))
    else:
        user_form = UserPasswordForm()
        logger.debug('Generated new forms to change a User Password')
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Change {} Password page'.format(model))
        return render(request, 'accounts/admin/user_update.html', {'objtype': model, 'name': user.username, 'user_form': user_form})

def ajax_user_delete(request, id):
    return ajax_generic_delete('ajax_user_delete', 'User', request, id, 'auth.delete_user')

def ajax_user_unlock(request, id):
    cname = 'ajax_user_unlock'
    model = 'User'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to delete {} with id {}'.format(model, id))
    if not request.user.has_perm('auth.change_user'):
        logger.debug('User does not have permissions to unlock {}'.format(model))
        logger2.warning(request.user, 'User does not have permissions to unlock {} id {}'.format(model, id))
        return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Error: Permission Denied'})
    logger.debug('Getting requested {}'.format(model))
    obj = helpers.get_object(model, id)
    logger.info('Unlock request for {} id {}'.format(model, id))
    obj.metadata.is_locked_out = False
    obj.metadata.bad_password_attempts = 0
    obj.metadata.save()
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, 'Unlocked {} id {}'.format(model, id))
    return render(request, 'inventory/ajax_result.html', {'color': 'green', 'result': 'Successfully Unlocked'})

@permission_required('auth.add_group')
def admin_group_create(request):
    cname = 'admin_group_create'
    model = 'Group'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view Admin Create {} page'.format(model))
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        group_form = GroupCreationForm(request.POST)
        if request.POST.__contains__('perms[]'):
            request.POST._mutable = True
            request.POST.setlist('perms',request.POST.getlist('perms[]'))
            request.POST.pop('perms[]')
            request.POST._mutable = False
        permission_form = PermissionForm(request.POST)
        if group_form.is_valid() and permission_form.is_valid():
            try:
                logger.debug('{} is valid.'.format(group_form.__class__.__name__))
                logger.info('Creating new Group "{}"'.format(model, helpers.get_form_object_name(group_form)))
                inst = group_form.save()
                logger2.info(request.user, 'Created {} id {}'.format(model, inst.pk))
                for perm in permission_form.cleaned_data['perms']:
                    p = Permission.objects.get(id=perm)
                    if not p.group_set.contains(inst):
                        p.group_set.add(inst)
                        logger.debug('Added permission {} to new group {}'.format(p.name, inst.name))
                        logger2.info(request.user, 'Added permission "{}" to group "{}"'.format(p.name, inst.name))
            except Exception as err:
                logger.error(err)
                logger2.warning(request.user, 'Issue with creating new group: {}'.format(err))
                return render(request, 'accounts/admin/user_create.html', {'name': model, 'group_form': group_form, 'permission_form': permission_form, 'message': 'There was an error with creating the group.'})
        else:
            logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
            logger2.warning(request.user, 'Create {} Form not valid'.format(model))
            return render(request, 'accounts/admin/user_create.html', {'name': model, 'group_form': group_form, 'permission_form': permission_form, 'message': 'Form is not valid.'})
        return redirect('/accounts/admin/{}/'.format(model.lower()))
    else:
        group_form = GroupCreationForm()
        permission_form = PermissionForm()
        logger.debug('Generated new forms to create a Group')
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Create {} page'.format(model))
        return render(request, 'accounts/admin/user_create.html', {'name': model, 'group_form': group_form, 'permission_form': permission_form})

@permission_required('auth.view_group')
def admin_group_view(request):
    return admin_generic_view('admin_group_view', 'Group', request, 'auth.view_group')

@permission_required('auth.change_group')
def admin_group_change(request, id):
    cname = 'admin_group_change'
    model = 'Group'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view Admin Change {} page'.format(model))
    group = get_object_or_404(Group, id=id)
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        group_form = GroupCreationForm(request.POST, instance=group)
        if request.POST.__contains__('perms[]'):
            request.POST._mutable = True
            request.POST.setlist('perms',request.POST.getlist('perms[]'))
            request.POST.pop('perms[]')
            request.POST._mutable = False
        permission_form = PermissionForm(request.POST)
        if group_form.is_valid() and permission_form.is_valid():
            try:
                logger.debug('{} is valid.'.format(group_form.__class__.__name__))
                logger.info('Updating Group "{}"'.format(model, helpers.get_form_object_name(group_form)))
                group_form.save()
                logger2.info(request.user, 'Updated {} id {}'.format(model, id))
                remove, add = helpers.compare_permissions(group, permission_form.cleaned_data['perms'])
                for perm in remove:
                    perm.group_set.remove(group)
                    logger.debug('Removed permission {} from group {}'.format(perm.name, group.name))
                    logger2.info(request.user, 'Removed permission "{}" from group "{}"'.format(perm.name, group.name))
                for perm in add:
                    perm.group_set.add(group)
                    logger.debug('Added permission {} to group {}'.format(perm.name, group.name))
                    logger2.info(request.user, 'Added permission "{}" to group "{}"'.format(perm.name, group.name))
            except Exception as err:
                logger.error(err)
                logger2.warning(request.user, 'Issue with updating group: {}'.format(err))
                return render(request, 'accounts/admin/user_update.html', {'objtype': model, 'name': group.name, 'group_form': group_form, 'permission_form': permission_form, 'message': 'There was an error with updating the group.'})
        else:
            logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
            logger2.warning(request.user, 'Update {} Form not valid'.format(model))
            return render(request, 'accounts/admin/user_update.html', {'objtype': model, 'name': group.name, 'group_form': group_form, 'permission_form': permission_form, 'message': 'Form is not valid.'})
        return redirect('/accounts/admin/{}/'.format(model.lower()))
    else:
        group_form = GroupCreationForm(instance=group)
        permission_form = PermissionForm()
        _initial = []
        _perms = group.permissions.all()
        for p in _perms:
            _initial.append(p.id)
        permission_form.fields['perms'].initial = _initial
        logger.debug('Generated new forms to create a Group')
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Change {} page'.format(model))
        return render(request, 'accounts/admin/user_update.html', {'objtype': model, 'name': group.name, 'group_form': group_form, 'permission_form': permission_form})

def ajax_group_delete(request, id):
    return ajax_generic_delete('ajax_group_delete', 'Group', request, id, 'auth.delete_group')

def home(request):
    cname = 'home'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view Admin Home')
    if not (request.user.has_perm('accounts.view_invite') or request.user.has_perm('accounts.view_token')):
        raise PermissionDenied()
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, 'Accessed Accounts Home')
    return render(request, 'accounts/admin/home.html')

def admin_generic_view(cname, model, request, perm):
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view Admin {} List'.format(model))
    if not request.user.has_perm(perm):
        raise PermissionDenied()
    cperms = ('view')
    if model in ['User','Group']:
        module = 'auth'
    else:
        module = 'accounts'
    for t in ['add','change','delete']:
        if request.user.has_perm(helpers.get_admin_permission(module,model,t)):
            cperms += t
    allowupdate = True
    if model == 'Token':
        allowupdate = False
    logmsg = 'Accessed Admin {} List'.format(model)
    object_list = helpers.get_object_list(model)
    query = request.GET.get('q')
    if query != None:
        logmsg = '{} with query \'{}\''.format(logmsg, query)
        logger.debug('Filtering object_list of {} with query'.format(model))
        querytype = request.GET.get('t')
        qt = helpers.parse_querytype(querytype)
        fields = helpers.get_search_fields(model)
        sf = Search().filter(fields, qt + query)
        object_list = object_list.filter(sf)
    paginator = Paginator(object_list, settings.ADMIN_PAGE_SIZE)
    page = request.GET.get('page')
    # Try to display the page or catch and process the errors
    try:
        object_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        object_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
        object_list = paginator.page(paginator.num_pages)
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, logmsg)
    return render(request, 'accounts/admin/view.html', {'query': query, 'name': model, 'objs': object_list, 'allowupdate': allowupdate, 'cperms': cperms})

def admin_generic_create(cname, model, request, perm):
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view Admin Create {} page'.format(model))
    if not request.user.has_perm(perm):
        raise PermissionDenied()
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        form = helpers.get_admin_form_p(model, request.POST)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Creating new {} "{}"'.format(model, helpers.get_form_object_name(form)))
            inst = form.save()
            logger2.info(request.user, 'Created {} id {}'.format(model, inst.pk))
            return redirect('/accounts/admin/{}/'.format(model.lower()))
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Create {} Form not valid'.format(model))
        return render(request, 'accounts/admin/create.html', {'name': model, 'form': form, 'message': 'Form is not valid.'})
    else:
        form = helpers.get_admin_form_g(model)
        logger.debug('Generated a new {} and checking for provided variables'.format(form.__class__.__name__))
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Create {} page'.format(model))
        return render(request, 'accounts/admin/create.html', {'name': model, 'form': form})
    
def admin_generic_change(cname, model, request, id, perm):
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view Admin Change {} page for id {}'.format(model, id))
    if not request.user.has_perm(perm):
        raise PermissionDenied()
    obj = helpers.get_object(model, id)
    if request.method == 'POST':
        form = helpers.get_admin_form_p(model, request.POST, instance=obj)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Updating {} id {}'.format(model, id))
            message = 'Updated {}'.format(model)
            try:
                form.save()
            except Exception as err:
                logger.warning(err)
                logger2.error(request.user, err)
                message = 'Failed to update {}'.format(model)
            logger.debug('Rendering request for {} view'.format(cname))
            logger2.info(request.user, '{} id {}'.format(message, id))
            return render(request, 'accounts/admin/update.html', {'objtype': model, 'name': str(obj), 'form': form, 'message': message})
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Change {} Form not valid for id {}'.format(model, id))
        return render(request, 'accounts/admin/update.html', {'objtype': model, 'name': str(obj), 'form': form, 'message': 'Form is not valid.'})
    else:
        form = helpers.get_admin_form_g(model, instance=obj)
        logger.debug('Populated {} for id {}'.format(form.__class__.__name__, id))
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Change {} page for id {}'.format(model, id))
        return render(request, 'accounts/admin/update.html', {'objtype': model, 'name': str(obj), 'form': form})
    
def ajax_generic_delete(cname, model, request, id, perm):
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to delete {} with id {}'.format(model, id))
    if not request.user.has_perm(perm):
        logger.debug('User does not have permissions to delete {}'.format(model))
        logger2.warning(request.user, 'User does not have permissions to delete {} id {}'.format(model, id))
        return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Error: Permission Denied'})
    logger.debug('Getting requested {}'.format(model))
    obj = helpers.get_object(model, id)
    logger.info('DELETE request for {} id {}'.format(model, id))
    obj.delete()
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, 'Deleted {} id {}'.format(model, id))
    return render(request, 'inventory/ajax_result.html', {'color': 'green', 'result': 'Successfully Deleted'})