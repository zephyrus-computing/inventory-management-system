from django.contrib.auth.models import User
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.utils.timezone import now
import logging
from accounts.models import UserMetadata
from log.logger import getLogger

logger = logging.getLogger(__name__)
logger2 = getLogger('accounts')

@receiver(pre_save, sender=User)
def user_updated(sender, **kwargs):
    user = kwargs.get('instance', None)
    if user:
        if user._password is None or user.pk is None:
            return
        logger.info('Password has been updated for user "{}"'.format(user.username))
        logger2.info(user, 'Updated Password')
        user.metadata.last_set_password_datetime = now()
        user.metadata.must_change_password = False
        user.metadata.save()
    return

@receiver(post_save, sender=User)
def user_created(sender, **kwargs):
    if kwargs.get('created', None):
        user = kwargs.get('instance', None)
        if user:
            logger.info('User "{}" and associated metadata has been created'.format(user.username))
            logger2.info(user, 'User and metadata created')
            UserMetadata.objects.create(user=user)
    return