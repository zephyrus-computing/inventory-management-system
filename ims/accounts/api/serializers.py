from django.contrib.auth import authenticate
from django.contrib.auth.models import User, Group, Permission
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers

from accounts.models import Token, Invite, UserMetadata


class AuthTokenSerializer(serializers.Serializer):
    username = serializers.CharField(
        label=_("Username"),
        write_only=True
    )
    password = serializers.CharField(
        label=_("Password"),
        style={'input_type': 'password'},
        trim_whitespace=False,
        write_only=True
    )
    token = serializers.CharField(
        label=_("Token"),
        read_only=True
    )
    expiration = serializers.CharField(
        label=_("Expiration"),
        read_only=True
    )

    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')

        if username and password:
            user = authenticate(request=self.context.get('request'),
                                username=username, password=password)

            # The authenticate call simply returns None for is_active=False
            # users. (Assuming the default ModelBackend authentication
            # backend.)
            if not user:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = _('Must include "username" and "password".')
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('id','name')

    def create(self, validated_data):
        return Group.objects.create(**validated_data)

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id','username','first_name','last_name','email','is_active')

    def create(self, validated_data):
        return User.objects.create(**validated_data)
    
    def update(self, instance, validated_data):
        instance.username = validated_data.get('username', instance.username)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.email = validated_data.get('email', instance.email)
        instance.is_active = validated_data.get('is_active', instance.is_active)
        instance.save()
        return instance

class UserMetadataSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserMetadata
        fields = ('id','user','bad_password_attempts','is_locked_out','last_set_password_datetime','last_lockout_datetime','last_bad_password_datetime','must_change_password')

class TokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Token
        fields = ('id','user','created','expires')

    def create(self, validated_data):
        return Token.objects.create(**validated_data)

class InviteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invite
        fields = ('id','email','group','created','expires','lastmodified')

    def create(self, validated_data):
        return Invite.objects.create(**validated_data)
    
    def update(self, instance, validated_data):
        instance.email = validated_data.get('email', instance.email)
        instance.group = validated_data.get('group', instance.group)
        instance.expires = validated_data.get('expires', instance.expires)
        instance.save()
        return instance

class PermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Permission
        fields = ('id','codename','content_type_id','name')