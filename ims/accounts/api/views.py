from datetime import timedelta
from django.conf import settings
from django.contrib.auth.models import User,Group,Permission
from django.shortcuts import get_object_or_404
from django.utils.timezone import now
from rest_framework import parsers, renderers
from rest_framework.compat import coreapi, coreschema
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.schemas import ManualSchema
from rest_framework.schemas import coreapi as coreapi_schema
from rest_framework.views import APIView
from rest_framework import status
import logging

from accounts.api.serializers import AuthTokenSerializer,GroupSerializer,UserSerializer,UserMetadataSerializer,TokenSerializer,InviteSerializer,PermissionSerializer
from accounts.models import Token,Invite
from log.logger import getLogger
import accounts.helpers as helpers

logger = logging.getLogger(__name__)
logger2 = getLogger('inventory-api')

class ObtainAuthToken(APIView):
    throttle_classes = ()
    permission_classes = ()
    parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = AuthTokenSerializer

    if coreapi_schema.is_enabled():
        schema = ManualSchema(
            fields=[
                coreapi.Field(
                    name="username",
                    required=True,
                    location='form',
                    schema=coreschema.String(
                        title="Username",
                        description="Valid username for authentication",
                    ),
                ),
                coreapi.Field(
                    name="password",
                    required=True,
                    location='form',
                    schema=coreschema.String(
                        title="Password",
                        description="Valid password for authentication",
                    ),
                ),
            ],
            encoding="application/json",
        )

    def get_serializer_context(self):
        return {
            'request': self.request,
            'format': self.format_kwarg,
            'view': self
        }

    def get_serializer(self, *args, **kwargs):
        kwargs['context'] = self.get_serializer_context()
        return self.serializer_class(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token = Token.objects.filter(user=user).last()
        if not token or token.expires is not None and token.expires < now():
            token = Token.objects.create(user=user,expires=now() + timedelta(days=settings.DEFAULT_API_KEY_EXPIRATION))               
        return Response({'token': token.key,'expiration': token.expires})
    
obtain_auth_token = ObtainAuthToken.as_view()

class GroupList(APIView):
    permission_classes = (IsAuthenticated,)
    model_class = Group
    serializer_class = GroupSerializer
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_class, 'auth.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_class, 'auth.add_{}'.format(self.model_class.__name__.lower()))

class GroupDetail(APIView):
    permission_classes = (IsAuthenticated,)
    model_class = Group
    serializer_class = GroupSerializer
    def get(self, request, gid):
        return _pk_get(request, gid, self.__class__.__name__, self.model_class, self.serializer_class, 'auth.view_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, gid):
        return _pk_delete(request, gid, self.__class__.__name__, self.model_class, 'auth.delete_{}'.format(self.model_class.__name__.lower()))

class UserList(APIView):
    permission_classes = (IsAuthenticated,)
    model_class = User
    serializer_class = UserSerializer
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_class, 'auth.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_class, 'auth.add_{}'.format(self.model_class.__name__.lower()))

class UserDetail(APIView):
    permission_classes = (IsAuthenticated,)
    model_class = User
    serializer_class = UserSerializer
    def get(self, request, uid):
        return _pk_get(request, uid, self.__class__.__name__, self.model_class, self.serializer_class, 'auth.view_{}'.format(self.model_class.__name__.lower()))
    def put(self, request, uid):
        return _pk_put(request, uid, self.__class__.__name__, self.model_class, self.serializer_class, 'auth.change_{}'.format(self.model_class.__name__.lower()))
    def patch(self, request, uid):
        return _pk_patch(request, uid, self.__class__.__name__, self.model_class, self.serializer_class, 'auth.change_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, uid):
        return _pk_delete(request, uid, self.__class__.__name__, self.model_class, 'auth.delete_{}'.format(self.model_class.__name__.lower()))

class InviteList(APIView):
    permission_classes = (IsAuthenticated,)
    model_class = Invite
    serializer_class = InviteSerializer
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_class, 'accounts.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_class, 'accounts.add_{}'.format(self.model_class.__name__.lower()))

class InviteDetail(APIView):
    permission_classes = (IsAuthenticated,)
    model_class = Invite
    serializer_class = InviteSerializer
    def get(self, request, iid):
        return _pk_get(request, iid, self.__class__.__name__, self.model_class, self.serializer_class, 'accounts.view_{}'.format(self.model_class.__name__.lower()))
    def put(self, request, iid):
        return _pk_put(request, iid, self.__class__.__name__, self.model_class, self.serializer_class, 'accounts.change_{}'.format(self.model_class.__name__.lower()))
    def patch(self, request, iid):
        return _pk_patch(request, iid, self.__class__.__name__, self.model_class, self.serializer_class, 'accounts.change_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, iid):
        return _pk_delete(request, iid, self.__class__.__name__, self.model_class, 'accounts.delete_{}'.format(self.model_class.__name__.lower()))

class TokenList(APIView):
    permission_classes = (IsAuthenticated,)
    model_class = Token
    serializer_class = TokenSerializer
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_class, 'accounts.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_class, 'accounts.add_{}'.format(self.model_class.__name__.lower()))

class TokenDetail(APIView):
    permission_classes = (IsAuthenticated,)
    model_class = Token
    serializer_class = TokenSerializer
    def get(self, request, tid):
        return _pk_get(request, tid, self.__class__.__name__, self.model_class, self.serializer_class, 'accounts.view_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, tid):
        return _pk_delete(request, tid, self.__class__.__name__, self.model_class, 'accounts.delete_{}'.format(self.model_class.__name__.lower()))

class PermissionList(APIView):
    permission_classes = (IsAuthenticated,)
    model_class = Permission
    serializer_class = PermissionSerializer
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_class, 'auth.view_{}'.format(self.model_class.__name__.lower()))

class PermissionDetail(APIView):
    permission_classes = (IsAuthenticated,)
    model_class = Permission
    serializer_class = PermissionSerializer
    def get(self, request, pid):
        return _pk_get(request, pid, self.__class__.__name__, self.model_class, self.serializer_class, 'auth.view_{}'.format(self.model_class.__name__.lower()))

class GroupUserDetail(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, gid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.view_group') or not request.user.has_perm('auth.view_user'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed {} API'.format(cname)
        logger.debug('Generating list of User')
        group = get_object_or_404(Group, id=gid)
        objs = group.user_set.all()
        logmsg, objs = helpers.filter_api_request(request, logmsg, 'User', objs)
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response(UserSerializer(objs, many=True).data, status.HTTP_200_OK)
    
class GroupUserAdd(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, gid, uid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.change_group') or not request.user.has_perm('auth.change_user'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        group = get_object_or_404(Group, id=gid)
        user = get_object_or_404(User, id=uid)
        logmsg = 'successfully added user {} to group {}'.format(user.id, group.id)
        group.user_set.add(user)
        group.save()
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response({'message':logmsg}, status.HTTP_200_OK)
    
class GroupUserRemove(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, gid, uid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.change_group') or not request.user.has_perm('auth.change_user'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        group = get_object_or_404(Group, id=gid)
        user = get_object_or_404(User, id=uid)
        logmsg = 'successfully removed user {} from group {}'.format(user.id, group.id)
        group.user_set.remove(user)
        group.save()
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response({'message':logmsg}, status.HTTP_200_OK)

class GroupPermissionDetail(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, gid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.view_group') or not request.user.has_perm('auth.view_permission'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed {} API'.format(cname)
        logger.debug('Generating list of Permission')
        group = get_object_or_404(Group, id=gid)
        objs = group.permissions.all()
        logmsg, objs = helpers.filter_api_request(request, logmsg, 'Permission', objs)
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response(PermissionSerializer(objs, many=True).data, status.HTTP_200_OK)
    
class GroupPermissionAdd(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, gid, pid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.change_group') or not request.user.has_perm('auth.change_permission'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        group = get_object_or_404(Group, id=gid)
        permission = get_object_or_404(Permission, id=pid)
        logmsg = 'successfully added permission {} to group {}'.format(permission.id, group.id)
        group.permissions.add(permission)
        group.save()
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response({'message':logmsg}, status.HTTP_200_OK)
    
class GroupPermissionRemove(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, gid, pid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.change_group') or not request.user.has_perm('auth.change_permission'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        group = get_object_or_404(Group, id=gid)
        permission = get_object_or_404(Permission, id=pid)
        logmsg = 'successfully removed permission {} from group {}'.format(permission.id, group.id)
        group.permissions.remove(permission)
        group.save()
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response({'message':logmsg}, status.HTTP_200_OK)
    
class PermissionGroupDetail(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, pid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.view_permission') or not request.user.has_perm('auth.view_group'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed {} API'.format(cname)
        logger.debug('Generating list of Group')
        permission = get_object_or_404(Permission, id=pid)
        objs = permission.group_set.all()
        logmsg, objs = helpers.filter_api_request(request, logmsg, 'Group', objs)
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response(GroupSerializer(objs, many=True).data, status.HTTP_200_OK)
    
class PermissionGroupAdd(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, pid, gid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.change_permission') or not request.user.has_perm('auth.change_group'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        permission = get_object_or_404(Permission, id=pid)
        group = get_object_or_404(Group, id=gid)
        logmsg = 'successfully added group {} to permission {}'.format(group.id, permission.id)
        permission.group_set.add(group)
        permission.save()
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response({'message':logmsg}, status.HTTP_200_OK)
    
class PermissionGroupRemove(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, pid, gid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.change_permission') or not request.user.has_perm('auth.change_group'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        permission = get_object_or_404(Permission, id=pid)
        group = get_object_or_404(Group, id=gid)
        logmsg = 'successfully removed group {} from permission {}'.format(group.id, permission.id)
        permission.group_set.remove(group)
        permission.save()
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response({'message':logmsg}, status.HTTP_200_OK)

class PermissionUserDetail(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, pid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.view_permission') or not request.user.has_perm('auth.view_user'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed {} API'.format(cname)
        logger.debug('Generating list of User')
        permission = get_object_or_404(Permission, id=pid)
        objs = permission.user_set.all()
        logmsg, objs = helpers.filter_api_request(request, logmsg, 'User', objs)
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response(UserSerializer(objs, many=True).data, status.HTTP_200_OK)
    
class PermissionUserAdd(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, pid, uid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.change_permission') or not request.user.has_perm('auth.change_user'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        permission = get_object_or_404(Permission, id=pid)
        user = get_object_or_404(User, id=uid)
        logmsg = 'successfully added user {} to permission {}'.format(user.id, permission.id)
        permission.user_set.add(user)
        permission.save()
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response({'message':logmsg}, status.HTTP_200_OK)
    
class PermissionUserRemove(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, pid, uid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.change_permission') or not request.user.has_perm('auth.change_user'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        permission = get_object_or_404(Permission, id=pid)
        user = get_object_or_404(User, id=uid)
        logmsg = 'successfully removed user {} from permission {}'.format(user.id, permission.id)
        permission.user_set.remove(user)
        permission.save()
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response({'message':logmsg}, status.HTTP_200_OK)

class UserGroupDetail(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, uid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.view_user') or not request.user.has_perm('auth.view_group'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed {} API'.format(cname)
        logger.debug('Generating list of Group')
        user = get_object_or_404(User, id=uid)
        objs = user.groups.all()
        logmsg, objs = helpers.filter_api_request(request, logmsg, 'Group', objs)
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response(GroupSerializer(objs, many=True).data, status.HTTP_200_OK)
    
class UserGroupAdd(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, uid, gid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.change_user') or not request.user.has_perm('auth.change_group'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        user = get_object_or_404(User, id=uid)
        group = get_object_or_404(Group, id=gid)
        logmsg = 'successfully added group {} to user {}'.format(group.id, user.id)
        user.groups.add(group)
        user.save()
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response({'message':logmsg}, status.HTTP_200_OK)
    
class UserGroupRemove(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, uid, gid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.change_user') or not request.user.has_perm('auth.change_group'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        user = get_object_or_404(User, id=uid)
        group = get_object_or_404(Group, id=gid)
        logmsg = 'successfully removed group {} from user {}'.format(group.id, user.id)
        user.groups.remove(group)
        user.save()
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response({'message':logmsg}, status.HTTP_200_OK)

class UserPermissionDetail(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, uid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.view_user') or not request.user.has_perm('auth.view_permission'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed {} API'.format(cname)
        logger.debug('Generating list of Permission')
        user = get_object_or_404(User, id=uid)
        objs = user.user_permissions.all()
        logmsg, objs = helpers.filter_api_request(request, logmsg, 'Permission', objs)
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response(PermissionSerializer(objs, many=True).data, status.HTTP_200_OK)
    
class UserPermissionAdd(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, uid, pid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.change_user') or not request.user.has_perm('auth.change_permission'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        user = get_object_or_404(User, id=uid)
        permission = get_object_or_404(Permission, id=pid)
        logmsg = 'successfully added permission {} to user {}'.format(permission.id, user.id)
        user.user_permissions.add(permission)
        user.save()
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response({'message':logmsg}, status.HTTP_200_OK)
    
class UserPermissionRemove(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, uid, pid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.change_user') or not request.user.has_perm('auth.change_permission'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        user = get_object_or_404(User, id=uid)
        permission = get_object_or_404(Permission, id=pid)
        logmsg = 'successfully removed permission {} from user {}'.format(permission.id, user.id)
        user.user_permissions.remove(permission)
        user.save()
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response({'message':logmsg}, status.HTTP_200_OK)

class UserDisable(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, uid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.change_user'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        user = get_object_or_404(User, id=uid)
        if user == request.user:
            logger.debug('Cannot disable own User object')
            logger2.info(request.user, 'Attempted to disable own User object'.format(fname, cname))
            return Response({'message': 'You cannot disable your own User object.'}, status.HTTP_400_BAD_REQUEST)
        logmsg = 'successfully disabled user {}'.format(user.id)
        user.is_active = False
        user.save()
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response({'message':logmsg}, status.HTTP_200_OK)

class UserEnable(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, uid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.change_user'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        user = get_object_or_404(User, id=uid)
        if user == request.user:
            logger.debug('Cannot enable own User object')
            logger2.info(request.user, 'Attempted to enable own User object'.format(fname, cname))
            return Response({'message': 'You cannot enable your own User object.'}, status.HTTP_400_BAD_REQUEST)
        logmsg = 'successfully enabled user {}'.format(user.id)
        user.is_active = True
        user.save()
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response({'message':logmsg}, status.HTTP_200_OK)
    
class UserLock(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, uid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.change_user'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        user = get_object_or_404(User, id=uid)
        if user == request.user:
            logger.debug('Cannot lock own User object')
            logger2.info(request.user, 'Attempted to lock own User object'.format(fname, cname))
            return Response({'message': 'You cannot lock your own User object.'}, status.HTTP_400_BAD_REQUEST)
        logmsg = 'successfully locked user {}'.format(user.id)
        user.metadata.is_locked_out = True
        user.metadata.last_lockout_datetime = now()
        user.metadata.save()
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response({'message':logmsg}, status.HTTP_200_OK)
    
class UserUnlock(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, uid):
        cname = self.__class__.__name__
        fname = 'GET'
        logger.info('{} Request made to {} APIView'.format(fname, cname))
        logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
        if not request.user.has_perm('auth.change_user'):
            logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
            logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        user = get_object_or_404(User, id=uid)
        if user == request.user:
            logger.debug('Cannot unlock own User object')
            logger2.info(request.user, 'Attempted to unlock own User object'.format(fname, cname))
            return Response({'message': 'You cannot unlock your own User object.'}, status.HTTP_400_BAD_REQUEST)
        logmsg = 'successfully unlocked user {}'.format(user.id)
        user.metadata.is_locked_out = False
        user.metadata.save()
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, logmsg)
        return Response({'message':logmsg}, status.HTTP_200_OK)

def _get(request, cname, model, serializer, permission):
    fname = 'GET'
    logger.info('{} Request made to {} APIView'.format(fname, cname))
    logger2.debug((request.user or 0), '{} Request to {} API'.format(fname, cname))
    if not request.user.has_perm(permission):
        logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
        logger2.warning((request.user or 0), 'Access Denied for {} request to {} API'.format(fname, cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logmsg = 'Accessed {} API'.format(cname)
    logger.debug('Generating list of {}'.format(model.__name__))
    if not model.__name__ == "Permission":
        objs = model.objects.all()
    else:
        objs = model.objects.filter(content_type__app_label__in=['inventory','accounts','log','auth'])
    logmsg, objs = helpers.filter_api_request(request, logmsg, model.__name__, objs)
    logger.debug('Responding (200) to request for {} APIView'.format(cname))
    logger2.info((request.user or 0), logmsg)
    return Response(serializer(objs, many=True).data, status.HTTP_200_OK)
def _post(request, cname, model, serializer, permission):
    fname = 'POST'
    logger.info('{} Request made to {} APIView'.format(fname, cname))
    logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
    if not request.user.has_perm(permission):
        logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
        logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logger.debug('Serializing data in request')
    serializer = serializer(data=request.data)
    if serializer.is_valid():
        logger.debug('Serializer is valid')
        logger.info('Creating new {} "{}"'.format(model.__name__, serializer.validated_data.get('name')))
        inst = serializer.save()
        logger.debug('Responding (201) to request for {} APIView'.format(cname))
        logger2.info(request.user, 'Created {} id {}'.format(model.__name__, inst.id))
        return Response(serializer.data, status.HTTP_201_CREATED)
    logger.info('HTTP_400_BAD_REQUEST response for {} APIView'.format(cname))
    logger.debug('Responding (400) to request for {} APIView'.format(cname))
    logger2.warning(request.user, 'Bad {} Request to {} API'.format(fname, cname))
    return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)
def _pk_get(request, id, cname, model, serializer, permission):
    fname = 'GET'
    logger.info('{} Request made to {} APIView'.format(fname, cname))
    logger2.debug((request.user or 0), '{} Request to {} API for id {}'.format(fname, cname, id))
    if not request.user.has_perm(permission):
        logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
        logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logger.debug('Getting requested {}'.format(model.__name__))
    obj = get_object_or_404(model, id=id)
    if not model.__name__ == 'Permission' or obj.content_type.app_label in ['inventory','accounts','log','auth']:
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), 'Accessed {} API for id {}'.format(cname, id))
        return Response(serializer(obj).data, status.HTTP_200_OK)
    else:
        logger.debug('User attempted to access permission "{}" which is not allowed through this API'.format(obj))
        logger2.warning(request.user, 'Access attempt to restricted object id {} with {} API'.format(id, cname))
        return Response({}, status.HTTP_404_NOT_FOUND) 

def _pk_put(request, id, cname, model, serializer, permission):
    return _pk_update('PUT', request, id, cname, model, serializer, permission, False)
def _pk_patch(request, id, cname, model, serializer, permission):
    return _pk_update('PATCH', request, id, cname, model, serializer, permission, True)
def _pk_update(fname, request, id, cname, model, serializer, permission, partial):
    logger.info('{} Request made to {} APIView'.format(fname, cname))
    logger2.debug(request.user, '{} Request to {} API for id {}'.format(fname, cname, id))
    if not request.user.has_perm(permission):
        logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
        logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logger.debug('Getting requested {}'.format(model.__name__))
    obj = get_object_or_404(model, id=id)
    logger.debug('Serializing data in request')
    serializer = serializer(obj, data=request.data, partial=partial)
    if serializer.is_valid():
        logger.debug('Serializer is valid')
        serializer.save()
        logger.info('{} request successfully updated {} id {}'.format(fname, model.__name__, id))
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, 'Updated {} for id {}'.format(cname, id))
        return Response(serializer.data, status.HTTP_200_OK)
    logger.info('HTTP_400_BAD_REQUEST response for {} APIView'.format(cname))
    logger.debug('Responding (400) to request for {} APIView'.format(cname))
    logger2.warning(request.user, 'Bad {} Request to {} API'.format(fname, cname))
    return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)
def _pk_delete(request, id, cname, model, permission):
    fname = 'DELETE'
    logger.info('{} Request made to {} APIView'.format(fname, cname))
    logger2.debug(request.user, '{} Request to {} API for id {}'.format(fname, cname, id))
    if not request.user.has_perm(permission):
        logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
        logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logger.debug('Getting requested {}'.format(model.__name__))
    obj = get_object_or_404(model, id=id)
    message = {
        'message': '{} has sucessfully been deleted'.format(model.__name__),
    }
    if model.__name__ == 'User':
        if obj.id == request.user.id:
            logger.debug('Responding (400) to request for {} APIView'.format(cname))
            logger2.info(request.user, 'Attempted to delete own User object')
            return Response({'message': 'You cannot delete your own User object.'}, status.HTTP_400_BAD_REQUEST)
        message = {
            'message': '{} has successfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'group': GroupSerializer((g for g in obj.groups.all()), many=True).data,
            'permission': PermissionSerializer((p for p in obj.user_permissions.all()), many=True).data,
            'token': TokenSerializer((t for t in obj.ims_authtoken.all()), many=True).data,
        }
    if model.__name__ == 'Group':
        message = {
            'message': '{} has successfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'user': UserSerializer((u for u in obj.user_set.all()), many=True).data,
            'permission': PermissionSerializer((p for p in obj.permissions.all()), many=True).data,
            'invite': InviteSerializer((i for i in obj.invite_set.all()), many=True).data,
        }
    if model.__name__ == 'Token':
        message = {
            'message': '{} has successfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'user': UserSerializer(obj.user).data,
        }
    if model.__name__ == 'Invite':
        message = {
            'message': '{} has successfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'group': GroupSerializer(obj.group).data,
        }
    logger.info('DELETE request for {} id {}'.format(model.__name__, id))
    obj.delete()
    logger.debug('Responding (200) to request for {} APIView'.format(cname))
    logger2.info(request.user, 'Deleted {} id {}'.format(model.__name__, id))
    return Response(message, status.HTTP_200_OK)