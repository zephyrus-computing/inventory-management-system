var coll = document.getElementsByClassName("collapsible");

coll.forEach(addCollapsible);

function addCollapsible(item) {
    item.addEventListener("click", function () {
        this.classList.toggle("active");
        var content = this.nextElementSibling;
        if (content.style.maxHeight) {
            content.style.maxHeight = null;
        } else {
            content.style.maxHeight = content.scrollHeight + "px";
        }
    });
}

