from django.contrib.auth.decorators import login_required, permission_required
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, redirect
import logging

from inventory.search import Search
from log.logger import getLogger
import inventory.helpers as helpers

logger = logging.getLogger(__name__)
logger2 = getLogger('inventory-admin')

@login_required
def home(request):
    cname = 'home'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view Admin Home')
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, 'Accessed Admin Home')
    return render(request, 'inventory/admin/home.html')

@permission_required('inventory.view_part')
def admin_part_view(request):
    return admin_generic_view('admin_part_view', 'Part', request)

@permission_required('inventory.add_part')
def admin_part_create(request):
    return admin_generic_create('admin_part_create', 'Part', request)

@permission_required('inventory.change_part')
def admin_part_change(request, id):
    return admin_generic_change('admin_part_change', 'Part', request, id)

def ajax_part_delete(request, id):
    return ajax_generic_delete('ajax_part_delete', 'Part', request, id)

@permission_required('inventory.view_assembly')
def admin_assembly_view(request):
    return admin_generic_view('admin_assembly_view', 'Assembly', request)

@permission_required('inventory.add_assembly')
def admin_assembly_create(request):
    return admin_generic_create('admin_assembly_create', 'Assembly', request)

@permission_required('inventory.change_assembly')
def admin_assembly_change(request, id):
    return admin_generic_change('admin_assembly_change', 'Assembly', request, id)

def ajax_assembly_delete(request, id):
    return ajax_generic_delete('ajax_assembly_delete', 'Assembly', request, id)

@permission_required('inventory.view_storage')
def admin_storage_view(request):
    return admin_generic_view('admin_storage_view', 'Storage', request)

@permission_required('inventory.add_storage')
def admin_storage_create(request):
    return admin_generic_create('admin_storage_create', 'Storage', request)

@permission_required('inventory.change_storage')
def admin_storage_change(request, id):
    return admin_generic_change('admin_storage_change', 'Storage', request, id)

def ajax_storage_delete(request, id):
    return ajax_generic_delete('ajax_storage_delete', 'Storage', request, id)

@permission_required('inventory.view_alternatesku')
def admin_alternatesku_view(request):
    return admin_generic_view('admin_alternatesku_view', 'AlternateSKU', request)

@permission_required('inventory.add_alternatesku')
def admin_alternatesku_create(request):
    return admin_generic_create('admin_alternatesku_create', 'AlternateSKU', request)

def ajax_alternatesku_delete(request, id):
    return ajax_generic_delete('ajax_alternatesku_delete', 'AlternateSKU', request, id)

@permission_required('inventory.view_partstorage')
def admin_partstorage_view(request):
    return admin_generic_view('admin_partstorage_view', 'PartStorage', request)

@permission_required('inventory.add_partstorage')
def admin_partstorage_create(request):
    return admin_generic_create('admin_partstorage_create', 'PartStorage', request)

@permission_required('inventory.change_partstorage')
def admin_partstorage_change(request, id):
    return admin_generic_change('admin_partstorage_change', 'PartStorage', request, id)

def ajax_partstorage_delete(request, id):
    return ajax_generic_delete('ajax_partstorage_delete', 'PartStorage', request, id)

@permission_required('inventory.view_partalternatesku')
def admin_partalternatesku_view(request):
    return admin_generic_view('admin_partalternatesku_view', 'PartAlternateSKU', request)

@permission_required('inventory.add_partalternatesku')
def admin_partalternatesku_create(request):
    return admin_generic_create('admin_partalternatesku_create', 'PartAlternateSKU', request)

def ajax_partalternatesku_delete(request, id):
    return ajax_generic_delete('ajax_partalternatesku_delete', 'PartAlternateSKU', request, id)

@permission_required('inventory.view_kit')
def admin_kit_view(request):
    return admin_generic_view('admin_kit_view', 'Kit', request)

@permission_required('inventory.add_kit')
def admin_kit_create(request):
    return admin_generic_create('admin_kit_create', 'Kit', request)

@permission_required('inventory.change_kit')
def admin_kit_change(request, id):
    return admin_generic_change('admin_kit_change', 'Kit', request, id)

def ajax_kit_delete(request, id):
    return ajax_generic_delete('ajax_kit_delete', 'Kit', request, id)

@permission_required('inventory.view_kitpartstorage')
def admin_kitpartstorage_view(request):
    return admin_generic_view('admin_kitpartstorage_view', 'KitPartStorage', request)

@permission_required('inventory.add_kitpartstorage')
def admin_kitpartstorage_create(request):
    return admin_generic_create('admin_kitpartstorage_create', 'KitPartStorage', request)

@permission_required('inventory.change_kitpartstorage')
def admin_kitpartstorage_change(request, id):
    return admin_generic_change('admin_kitpartstorage_change', 'KitPartStorage', request, id)

def ajax_kitpartstorage_delete(request, id):
    return ajax_generic_delete('ajax_kitpartstorage_delete', 'KitPartStorage', request, id)

def admin_generic_view(cname, model, request):
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view Admin {} List'.format(model))
    perm = helpers.get_admin_permission('inventory',model, 'view')
    if not request.user.has_perm(perm):
        raise PermissionDenied()
    cperms = ('view')
    for t in ['add','change','delete']:
        if request.user.has_perm(helpers.get_admin_permission('inventory',model, t)):
            cperms += t
    allowupdate = True
    if 'alternatesku' in model.lower():
        allowupdate = False
    logmsg = 'Accessed Admin {} List'.format(model)
    object_list = helpers.get_object_list(model)
    query = request.GET.get('q')
    if query != None:
        logmsg = '{} with query \'{}\''.format(logmsg, query)
        logger.debug('Filtering object_list of {} with query'.format(model))
        querytype = request.GET.get('t')
        qt = helpers.parse_querytype(querytype)
        fields = helpers.get_search_fields(model)
        sf = Search().filter(fields, qt + query)
        object_list = object_list.filter(sf)
    paginator = Paginator(object_list, settings.ADMIN_PAGE_SIZE)
    page = request.GET.get('page')
    # Try to display the page or catch and process the errors
    try:
        object_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        object_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
        object_list = paginator.page(paginator.num_pages)
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, logmsg)
    return render(request, 'inventory/admin/view.html', {'query': query, 'name': model, 'objs': object_list, 'allowupdate': allowupdate, 'cperms': cperms})

def admin_generic_create(cname, model, request):
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view Admin Create {} page'.format(model))
    perm = helpers.get_admin_permission('inventory',model, 'add')
    if not request.user.has_perm(perm):
        raise PermissionDenied()
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        form = helpers.get_admin_form_p(model, request.POST)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Creating new {} "{}"'.format(model, helpers.get_form_object_name(form)))
            inst = form.save()
            logger2.info(request.user, 'Created {} id {}'.format(model, inst.id))
            return redirect('/inventory/admin/{}/'.format(model.lower()))
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Create {} Form not valid'.format(model))
        return render(request, 'inventory/admin/create.html', {'name': model, 'form': form, 'message': 'Form is not valid.'})
    else:
        form = helpers.get_admin_form_g(model)
        logger.debug('Generated a new {} and checking for provided variables'.format(form.__class__.__name__))
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Create {} page'.format(model))
        return render(request, 'inventory/admin/create.html', {'name': model, 'form': form})
    
def admin_generic_change(cname, model, request, id):
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view Admin Change {} page for id {}'.format(model, id))
    perm = helpers.get_admin_permission('inventory',model, 'change')
    if not request.user.has_perm(perm):
        raise PermissionDenied()
    obj = helpers.get_object(model, id)
    if request.method == 'POST':
        form = helpers.get_admin_form_p(model, request.POST, instance=obj)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Updating {} id {}'.format(model, id))
            message = 'Updated {}'.format(model)
            try:
                form.save()
            except Exception as err:
                logger.warning(err)
                logger2.error(request.user, err)
                message = 'Failed to update {}'.format(model)
            logger.debug('Rendering request for {} view'.format(cname))
            logger2.info(request.user, '{} id {}'.format(message, id))
            return render(request, 'inventory/admin/update.html', {'objtype': model, 'name': str(obj), 'form': form, 'message': message})
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Change {} Form not valid for id {}'.format(model, id))
        return render(request, 'inventory/admin/update.html', {'objtype': model, 'name': str(obj), 'form': form, 'message': 'Form is not valid.'})
    else:
        form = helpers.get_admin_form_g(model, instance=obj)
        logger.debug('Populated {} for id {}'.format(form.__class__.__name__, id))
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Change {} page for id {}'.format(model, id))
        return render(request, 'inventory/admin/update.html', {'objtype': model, 'name': str(obj), 'form': form})
    
def ajax_generic_delete(cname, model, request, id):
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to delete {} with id {}'.format(model, id))
    perm = helpers.get_admin_permission('inventory',model, 'delete')
    if not request.user.has_perm(perm):
        logger.debug('User does not have permissions to delete {}'.format(model))
        logger2.warning(request.user, 'User does not have permissions to delete {} id {}'.format(model, id))
        return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Error: Permission Denied'})
    logger.debug('Getting requested {}'.format(model))
    obj = helpers.get_object(model, id)
    logger.info('DELETE request for {} id {}'.format(model, id))
    obj.delete()
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, 'Deleted {} id {}'.format(model, id))
    return render(request, 'inventory/ajax_result.html', {'color': 'green', 'result': 'Successfully Deleted'})