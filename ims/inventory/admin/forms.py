from django.forms import ModelForm

from inventory.models import Part, Assembly, Storage, AlternateSKU, PartStorage, PartAlternateSKU, Kit, KitPartStorage

class PartForm(ModelForm):
    class Meta:
        model = Part
        exclude = ['created', 'lastmodified']

class AssemblyForm(ModelForm):
    class Meta:
        model = Assembly
        exclude = ['created', 'lastmodified']

class StorageForm(ModelForm):
    class Meta:
        model = Storage
        exclude = ['created', 'lastmodified']
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['parent'].required = False

class AlternateSKUForm(ModelForm):
    class Meta:
        model = AlternateSKU
        exclude = ['created', 'lastmodified']

class PartStorageForm(ModelForm):
    class Meta:
        model = PartStorage
        exclude = ['created', 'lastmodified']

class PartAlternateSKUForm(ModelForm):
    class Meta:
        model = PartAlternateSKU
        exclude = ['created', 'lastmodified']

class KitForm(ModelForm):
    class Meta:
        model = Kit
        exclude = ['created', 'lastmodified']

class KitPartStorageForm(ModelForm):
    class Meta:
        model = KitPartStorage
        exclude = ['created', 'lastmodified']

