from django.conf import settings
from django.core.exceptions import FieldError
from django.shortcuts import render
import logging

from inventory.admin.forms import PartForm, StorageForm, AlternateSKUForm, KitForm, AssemblyForm, PartStorageForm, PartAlternateSKUForm, KitPartStorageForm
from inventory.models import Part, Storage, AlternateSKU, Kit, Assembly, PartStorage, PartAlternateSKU, KitPartStorage
from inventory.search import Search
from log.logger import getLogger

logger = logging.getLogger(__name__)
logger2 = getLogger('inventory')

def lookup_dictionary(dic, v):
    for k in dic:
        if k[0] == v:
            return k[1]
    return 'unknown'

def rlookup_dictionary(dic, v):
    for k in dic:
        if k[1] == v:
            return k[0]
    return ''

# Return parsed permission name
def get_admin_permission(app, model, action):
    return '{}.{}_{}'.format(app.lower(), action.lower(), model.lower())

# Return a specific object by id
def get_object(objtype, objid):
    logger.debug('get_object called')
    try:
        if objtype == 'AlternateSKU':
            return AlternateSKU.objects.get(id=objid)
        if objtype == 'Assembly':
            return Assembly.objects.get(id=objid)
        if objtype == 'Kit':
            return Kit.objects.get(id=objid)
        if objtype == 'KitPartStorage':
            return KitPartStorage.objects.get(id=objid)
        if objtype == 'Part':
            return Part.objects.get(id=objid)
        if objtype == 'PartAlternateSKU':
            return PartAlternateSKU.objects.get(id=objid)
        if objtype == 'PartStorage':
            return PartStorage.objects.get(id=objid)
        if objtype == 'Storage':
            return Storage.objects.get(id=objid)
        return ''
    except Exception as err:
        if type(err) in [AlternateSKU.DoesNotExist, Assembly.DoesNotExist, Kit.DoesNotExist, 
                         KitPartStorage.DoesNotExist, Part.DoesNotExist, PartAlternateSKU.DoesNotExist,
                         PartStorage.DoesNotExist, Storage.DoesNotExist]:
            logger.warning('Unable to locate {} with id {}. Error message: {}'.format(objtype, objid, err))
        else:
            logger.warning('There was an unknown problem in processing get_object')
            logger.debug(err)
        raise
    finally:
        logger.debug('get_object completed')

# Return list of objects of the specified type
def get_object_list(objtype):
    logger.debug('get_object_list called')
    try:
        if objtype == 'AlternateSKU':
            return AlternateSKU.objects.all()
        if objtype == 'Assembly':
            return Assembly.objects.all()
        if objtype == 'Kit':
            return Kit.objects.all()
        if objtype == 'KitPartStorage':
            return KitPartStorage.objects.all()
        if objtype == 'Part':
            return Part.objects.all()
        if objtype == 'PartAlternateSKU':
            return PartAlternateSKU.objects.all()
        if objtype == 'PartStorage':
            return PartStorage.objects.all()
        if objtype == 'Storage':
            return Storage.objects.all()
        return ''
    except Exception as err:
        logger.warning('There was an unknown problem in processing get_object_list')
        logger.debug(err)
        raise
    finally:
        logger.debug('get_object_list completed')

# Return list of fields for use with Search queries
def get_search_fields(model):
    logger.debug('get_search_fields called')
    try:
        if model == 'AlternateSKU':
            return ['manufacturer', 'sku']
        if model == 'Assembly':
            return ['parent__name', 'parent__sku', 'parent__description', 'part__name', 'part__sku', 'part__description']
        if model == 'Kit':
            return ['name', 'sku', 'description']
        if model == 'KitPartStorage':
            return ['kit__name', 'kit__sku', 'kit__description', 'partstorage__part__name', 'partstorage__part__sku', 'partstorage__part__description', 'partstorage__storage__name', 'partstorage__storage__description']
        if model == 'Part':
            return ['name', 'sku', 'description']
        if model == 'PartAlternateSKU':
            return ['part__name', 'part__sku', 'part__description', 'alt_sku__manufacturer', 'alt_sku__sku']
        if model == 'PartStorage':
            return ['part__name', 'part__sku', 'part__description', 'storage__name', 'storage__description']
        if model == 'Storage':
            return ['name', 'description']
        return []
    except Exception as err:
        logger.warning('There was an unknown problem in processing get_search_fields')
        logger.debug(err)
        raise
    finally:
        logger.debug('get_search_fields completed')
    

# Return a Form object for a Patch/Post/Put request
def get_admin_form_p(model, data=None, instance=None):
    logger.debug('get_admin_form_p called')
    try:
        if model == 'AlternateSKU':
            form = AlternateSKUForm(data=data,instance=instance)
        if model == 'Assembly':
            form = AssemblyForm(data=data,instance=instance)
        if model == 'Kit':
            form = KitForm(data=data,instance=instance)
        if model == 'KitPartStorage':
            form = KitPartStorageForm(data=data,instance=instance)
        if model == 'Part':
            form = PartForm(data=data,instance=instance)
        if model == 'PartAlternateSKU':
            form = PartAlternateSKUForm(data=data,instance=instance)
        if model == 'PartStorage':
            form = PartStorageForm(data=data,instance=instance)
        if model == 'Storage':
            form = StorageForm(data=data,instance=instance)
        return form
    except Exception as err:
        logger.warning('There was an unknown problem in processing get_admin_form_p')
        logger.debug(err)
        raise
    finally:
        logger.debug('get_admin_form completed')

# Return a Form object for a Get request
def get_admin_form_g(model, GET=None, instance=None):
    logger.debug('get_admin_form_g called')
    try:
        if model == 'AlternateSKU':
            form = AlternateSKUForm(initial=GET,instance=instance)
        if model == 'Assembly':
            form = AssemblyForm(initial=GET,instance=instance)
        if model == 'Kit':
            form = KitForm(initial=GET,instance=instance)
        if model == 'KitPartStorage':
            form = KitPartStorageForm(initial=GET,instance=instance)
        if model == 'Part':
            form = PartForm(instance=instance)
        if model == 'PartAlternateSKU':
            form = PartAlternateSKUForm(initial=GET,instance=instance)
        if model == 'PartStorage':
            form = PartStorageForm(initial=GET,instance=instance)
        if model == 'Storage':
            form = StorageForm(initial=GET,instance=instance)
        return form
    except Exception as err:
        logger.warning('There was an unknown problem in processing get_admin_form_g')
        logger.debug(err)
        raise
    finally:
        logger.debug('get_admin_form completed')

# Return the name of an object from the form data
def get_form_object_name(form):
    logger.debug('get_form_object_name called')
    try:
        if form.__class__.__name__ == 'AlternateSKU':
            return '{}-{}'.format(form.cleaned_data['manufacturer'], form.cleaned_data['sku'])
        if form.__class__.__name__ == 'AssemblyForm':
            return '{}-{}'.format(form.cleaned_data['parent'], form.cleaned_data['part'])
        if form.__class__.__name__ == 'KitForm':
            return '{}'.format(form.cleaned_data['name'])
        if form.__class__.__name__ == 'KitPartStorageForm':
            return '{}-{}'.format(form.cleaned_data['kit'], form.cleaned_data['partstorage'])
        if form.__class__.__name__ == 'PartForm':
            return '{}'.format(form.cleaned_data['name'])
        if form.__class__.__name__ == 'PartAlternateSKUForm':
            return '{}-{}'.format(form.cleaned_data['part'], form.cleaned_data['alt_sku'])
        if form.__class__.__name__ == 'PartStorageForm':
            return '{}-{}'.format(form.cleaned_data['part'], form.cleaned_data['storage'])
        if form.__class__.__name__ == 'StorageForm':
            return '{}'.format(form.cleaned_data['name'])
        return ''
    except Exception as err:
        logger.warning('There was an unknown problem in processing get_form_object_name')
        logger.debug(err)
        raise
    finally:
        logger.debug('get_form_object_name completed')

# Apply filters to an API request
def filter_api_request(request, logmsg, model, objects):
    logger.debug('filter_api_request called')
    fields = get_search_fields(model)
    query = request.GET.get('search')
    if query != None:
        logmsg += ' query={},'.format(query)
        logger.debug('Filtering list of {} with query'.format(model))
        sf = Search().filter(fields, query)
        objects = objects.filter(sf)
    for field in fields:
        value = request.GET.get(field)
        if value != None:
            logmsg += ' {}={},'.format(field, value)
            logger.debug('Filtering list of {} with query on field "{}"'.format(model, field))
            sf = Search().filter([field], value)
            objects = objects.filter(sf)
    sort = request.GET.get('sort')
    if sort != None:
        try:
            objects = objects.order_by(sort)
            logmsg += ' sort={},'.format(sort)
        except FieldError:
            logger.warning('Unable to sort {} based on field "{}". Request made by {}'.format(model, sort, (request.user or 0)))
    limit = request.GET.get('limit')
    if not is_int(limit): limit = settings.DEFAULT_API_RESULTS
    else: limit = int(limit)
    offset = request.GET.get('offset')
    if not is_int(offset): offset = 0
    else: offset = int(offset)
    logmsg += ' limit={}, offset={}'.format(limit, offset)
    limit += offset
    objects = objects[offset:limit]
    logger.debug('filter_api_request completed')
    return logmsg, objects

# Return bool if provided object is an int
def is_int(i):
    try:
        int(i)
        return True
    except ValueError:
        return False
    except TypeError:
        return False

# Return the query type for use with Search
def parse_querytype(value):
    if value == 1 or value == '1':
        return "^"
    if value == 2 or value == '2':
        return "="
    return ""
