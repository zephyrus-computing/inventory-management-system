from django.urls import re_path
from inventory import views

app_name='inventory'
urlpatterns = [
    re_path(r'^$', views.part_list, name='home'),
    re_path(r'^about/$', views.about_ims, name='about'),
    #re_path(r'^about/theme/$', views.ajax_theme, name='about'),
    re_path(r'^part/$', views.part_list, name='part_list'),
    re_path(r'^part/(?P<id>[\d]+)/$', views.part_detail, name='part_detail'),
    re_path(r'^container/$', views.storage_hierarchy_list, name='storage_hierarchy_list'),
    re_path(r'^container/(?P<id>[\d]+)/$', views.storage_hierarchy_detail, name='storage_hierarchy_detail'),
    re_path(r'^storage/$', views.storage_list, name='storage_list'),
    re_path(r'^storage/(?P<id>[\d]+)/$', views.storage_detail, name='storage_detail'),
    re_path(r'^partstorage/(?P<id>[\d]+)/add/(?P<inc>[\d]+)/$', views.ajax_partstorage_add, name='ajax_partstorage_add'),
    re_path(r'^partstorage/(?P<id>[\d]+)/sub/(?P<inc>[\d]+)/$', views.ajax_partstorage_sub, name='ajax_partstorage_sub'),
    re_path(r'^partstorage/(?P<id>[\d]+)/update/(?P<section>[\w]+)/$', views.ajax_partstorage_update, name='ajax_partstorage_update'),
    re_path(r'^altsku/$', views.altsku_list, name='altsku_list'),
    re_path(r'^assembly/$', views.assembly_list, name='assembly_list'),
    re_path(r'^assembly/(?P<id>[\d]+)/$', views.assembly_detail, name='assembly_detail'),
    re_path(r'^kit/$', views.kit_list, name='kit_list'),
    re_path(r'^kit/(?P<id>[\d]+)/$', views.kit_detail, name='kit_detail'),
    re_path(r'^kit/(?P<id>[\d]+)/add/(?P<inc>[\d]+)/$', views.ajax_kit_add, name='ajax_kit_add'),
    re_path(r'^kit/(?P<id>[\d]+)/sub/(?P<inc>[\d]+)/$', views.ajax_kit_subtract, name='ajax_kit_subtract'),
    re_path(r'^kit/(?P<id>[\d]+)/update/(?P<section>[\w]+)/$', views.ajax_kit_update, name='ajax_kit_update'),
    re_path(r'^search/$', views.search, name='search'),
]
