from django.db.models import Q
from functools import reduce

class Search():
    NULL_FILTER = Q(pk=None)
    def filter(self, search_fields, query):
        query = query.strip()
        filters = []
        first = True
        if isinstance(query, str):
            query = query.split(',')
        for bit in query:
            queries = [Q(**{self.search_param(field_name, first): bit})
                for field_name in search_fields]
            filters.append(reduce(Q.__or__, queries))
            first = False
        return reduce(Q.__and__, filters) if len(filters) else NULL_FILTER
    def search_param(self, field_name, is_first_word):
        if field_name.startswith('^') and is_first_word:
            return "%s__istartswith" % field_name[1:]
        elif field_name.startswith('@'):
            return "%s__search" % field_name[1:]
        elif field_name.startswith('='):
            return "%s__iexact" % field_name[1:]
        else:
            return "%s__icontains" % field_name

