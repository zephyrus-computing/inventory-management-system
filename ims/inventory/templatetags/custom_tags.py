from django import forms, template
from django.template.defaulttags import CommentNode
from django.template.loader_tags import do_include
from django.urls import reverse
from django.utils.safestring import mark_safe
register = template.Library()

@register.filter('input_type')
def input_type(ob):
    '''
    Extract form field type
    :param ob: form field
    :return: string of form field widget type
    '''
    return ob.field.widget.__class__.__name__

@register.filter(name='add_classes')
def add_classes(value, arg):
    '''
    Add provided classes to form field
    :param value: bound field
    :param arg: string of classes seperated by ' '
    :return: edited field
    '''
    css_classes = value.field.widget.attrs.get('class', '')
    # check if class is set or empty and split its content to list (or init list)
    if css_classes:
        css_classes = css_classes.split(' ')
    else:
        css_classes = []
    # prepare new classes to list
    args = arg.split(' ')
    for a in args:
        if a not in css_classes:
            css_classes.append(a)
    # join back to single string
    return value.as_widget(attrs={'class': ' '.join(css_classes)})

@register.filter(name='add_classes_and_auto_format')
def add_classes_and_auto_format(value, arg):
    '''
    Add provided classes to and generate other attrs for form field
    :param value: bound field
    :param classes: string of classes separated by ' '
    :return: edited field
    '''
    css_classes = value.field.widget.attrs.get('class', '')
    # check if class is set or empty and split its content to list (or init list)
    if css_classes:
        css_classes = css_classes.split(' ')
    else:
        css_classes = []
    # prepare new classes to list
    args = arg.split(' ')
    for a in args:
        if a not in css_classes:
            css_classes.append(a)
    # generate the placeholder value
    placeholder = value.name.capitalize()
    if type(value.field) == forms.DecimalField:
        placeholder = 0.00
    elif type(value.field) == forms.IntegerField:
        placeholder = 0
    # join back to single string
    return value.as_widget(attrs={'class': ' '.join(css_classes), 'title': value.name, 'placeholder': placeholder})

@register.filter(name='get_dict_item')
def get_dict_item(dictionary, key):
    return dictionary.get(key)

@register.tag(name='include_maybe')
def do_include_maybe(parser, token):
    "Source: http://stackoverflow.com/a/18951166/15690"
    bits = token.split_contents()
    if len(bits) < 2:
        raise template.TemplateSyntaxError(
            "%r tag takes at least one argument: "
            "the name of the template to be included." % bits[0])
        
    try:
        silent_node = do_include(parser, token)
    except template.TemplateDoesNotExist:
        # Django < 1.7
        return ''
    
    _orig_render = silent_node.render
    def wrapped_render(*args, **kwargs):
        try:
            return _orig_render(*args, **kwargs)
        except template.TemplateDoesNotExist:
            return ''
    silent_node.render = wrapped_render
    return silent_node

@register.filter(name='get_class_name')
def get_class_name(object):
    try:
        return object.__class__.__name__
    except:
        return object

# Can be in one of the following formats
# {% if perms|check_permissions:'inventory' %}something inventory related{% endif %}
# {% if perms|check_permissions:'inventory view_part add_part change_part delete_part %}something inventory and part related{% endif %}
# {% if perms|check_permissions:'accounts.view_invite accounts.view_token auth.view_user auth.view_group %}something account related{% endif %}
@register.filter(name='check_permissions')
def check_permission(user_perms, app_label_or_permissions):
    args = app_label_or_permissions.split(' ')
    # Check if we are dealing with more than one permission
    if len(args) > 1:
        # Check if we are starting with an app_label or permission
        if '.' in args[0]:
            for perm in args:
                print(perm)
                if user_perms.__contains__(perm):
                    print(True)
                    return True
            return False
        else:
            app_label = args.pop(0)
            for perm in args:
                if user_perms.__contains__('{}.{}'.format(app_label,perm)):
                    return True
            return False
    else:
        return user_perms.__contains__(args[0])