import django
django.setup()
from django.contrib.auth.models import AnonymousUser, User, Permission
from django.core.exceptions import FieldError
from django.test import TestCase, override_settings
from random import randint, choice
from rest_framework import status
from inventory.models import Part, Storage, AlternateSKU, PartAlternateSKU, PartStorage, Assembly, Kit, KitPartStorage
from inventory.api.serializers import PartSerializer, StorageSerializer, AlternateSKUSerializer, PartAlternateSKUSerializer, PartStorageSerializer, AssemblySerializer, KitSerializer, KitPartStorageSerializer, PartStoragePartCountSerializer, PartStorageStorageCountSerializer
from log.models import Log
import json

api_version = 'v1'
api_module = 'inventory'

api_baseurl = '/api/{}/{}/'.format(api_version, api_module)

# Test Inventory API Part Views
class InventoryApiPartViewTests(TestCase):
    def setUp(self):
        for i in range(0,101):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is an api view TestCase test part', sku=str(i).zfill(4), price=10.00, cost=5.00)
        self.baseurl = '{}part/'.format(api_baseurl)
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view part','Can add part','Can change part','Can delete part']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_part_get(self):
        # Get Object
        part = Part.objects.first()
        # Run Generic Tests
        TestGenerics().get(self, self.baseurl, part)
        TestGenerics().get(self, '{}?search={}'.format(self.baseurl, part.name[0:4]), part)
        TestGenerics().get(self, '{}?name={}'.format(self.baseurl, part.name), part)
        TestGenerics().get(self, '{}?description={}'.format(self.baseurl, part.description[0:4]), part)
        TestGenerics().get(self, '{}?sku={}'.format(self.baseurl, part.sku[0:3]), part)
        response = self.client.get('{}?limit=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, part.name)
        response = self.client.get('{}?limit=20&offset=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertNotContains(response, part.name)
        response = self.client.get('{}?limit=20&sort=name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, part.name)
        response = self.client.get('{}?limit=20&sort=-name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertNotContains(response, part.name)
        response = self.client.get('{}?offset=1000'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        self.assertNotContains(response, part.name)
        response = self.client.get('{}?limit=20&sort=-name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertNotContains(response, part.name)
        response = self.client.get('{}?sort=-banana'.format(self.baseurl))
        self.assertEqual(100, len(json.loads(response.content)))
        with override_settings(ALLOW_ANONYMOUS=False):
            self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_part_post(self):
        # Make Data
        data = {'name': 'Test_View_Part_New', 'description': 'This is a new api view TestCase test part', 'sku': 'new', 'price': '10.00', 'cost': '5.00'}
        serializer = PartSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        # Run Generic Tests
        TestGenerics().post(self, self.baseurl, data, False) # This is set to False because of the Unique Constraint on the 'sku' field which causes a 400 reply instead of the usual expected 200
        # Run Specific Tests
        self.assertIsNotNone(Part.objects.filter(name='Test_View_Part_New').first())
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.post(self.baseurl, data=data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_part_pk_get(self):
        # Get Object
        part = Part.objects.first()
        # Run Generic Tests
        TestGenerics().pk_get(self, self.baseurl, part)
        with override_settings(ALLOW_ANONYMOUS=False):
            self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
            response = self.client.get('{}{}/'.format(self.baseurl, part.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_part_pk_put(self):
        # Get Object
        part = Part.objects.first()
        part.name = 'updated'
        # Run Generic Tests
        TestGenerics().pk_put(self, self.baseurl, part)
        # Run Specific Tests
        part.refresh_from_db()
        self.assertEqual(part.name, 'updated')
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.put('{}{}/'.format(self.baseurl, part.id), data=PartSerializer(part).data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_part_pk_patch(self):
        # Get Object and Make Data
        part = Part.objects.first()
        data = {'id': part.id, 'sku': part.sku, 'name': 'updated'}
        # Run Generic Tests
        TestGenerics().pk_patch(self, self.baseurl, data)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.patch('{}{}/'.format(self.baseurl, part.id), data=data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_part_pk_delete(self):
        # Get Object
        part = Part.objects.first()
        # Build Test Specific Objects
        storage = Storage.objects.create(name='Test_View_Storage_1', description='This is a short-lived container')
        altsku = AlternateSKU.objects.create(manufacturer='Zephryus Computing', sku='00001')
        PartStorage.objects.create(part=part, storage=storage, count=1)
        PartAlternateSKU.objects.create(part=part, alt_sku=altsku)
        # Run Generic Tests
        TestGenerics().pk_delete(self, self.baseurl, part)
        # Run Specific Tests
        self.assertIsNone(Part.objects.filter(name=part.name).first())
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.delete('{}{}/'.format(self.baseurl, part.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_part_pk_count(self):
        # Get Object
        part = Part.objects.first()
        # Build Test Specific Objects
        storage = Storage.objects.create(name='Test_View_Storage_1', description='This is a short-lived container')
        PartStorage.objects.create(part=part,storage=storage,count=5)
        storage = Storage.objects.create(name='Test_View_Storage_2', description='This is a short-lived container')
        ps = PartStorage.objects.create(part=part,storage=storage,count=5)
        # Run Specific Tests
        response = self.client.get('{}{}/count/'.format(self.baseurl,part.id))
        self.assertEqual(response.content, b'{"count":10}')
        ps.subtract(3)
        response = self.client.get('{}{}/count/'.format(self.baseurl,part.id))
        self.assertEqual(response.content, b'{"count":7}')
        ps.delete()
        response = self.client.get('{}{}/count/'.format(self.baseurl,part.id))
        self.assertEqual(response.content, b'{"count":5}')
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/count/'.format(self.baseurl, part.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_part_pk_assembly(self):
        self.user.user_permissions.add(Permission.objects.get(name='Can view assembly'))
        # Get Object
        part = Part.objects.first()
        # Build Test Specific Objects
        parent = Part.objects.create(name='Test_View_Part_Assembly',description='This is a short-lived parent part',sku='Assembly',price=10,cost=5)
        child = Part.objects.create(name='Test_View_Part_Child',description='This is a short-lived child part',sku='Child',price=1,cost=0.5)
        Assembly.objects.create(parent=parent,part=part,count=1)
        Assembly.objects.create(parent=parent,part=child,count=1)
        # Run Specific Tests
        response = self.client.get('{}{}/assembly/'.format(self.baseurl,part.id))
        self.assertContains(response, parent.name)
        self.assertNotContains(response, part.name)
        self.assertNotContains(response, child.name)
        response = self.client.get('{}{}/assembly/?search=Test_View_Part_Ass&limit=1&sort=sku'.format(self.baseurl,part.id))
        self.assertContains(response, parent.name)
        response = self.client.get('{}{}/assembly/?name=Test_View_Part_Ass&offset=1&sort=-name'.format(self.baseurl,part.id))
        self.assertNotContains(response, parent.name)
        with self.assertRaises(FieldError):
            response = self.client.get('{}{}/assembly/?sku=Assem&sort=banana'.format(self.baseurl,part.id))
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/assembly/'.format(self.baseurl, part.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            user.user_permissions.add(Permission.objects.get(name='Can view part'))
            response = self.client.get('{}{}/assembly/'.format(self.baseurl, part.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_part_pk_component(self):
        self.user.user_permissions.add(Permission.objects.get(name='Can view assembly'))
        # Get Object
        part = Part.objects.first()
        # Build Test Specific Objects
        parent = Part.objects.create(name='Test_View_Part_Assembly',description='This is a short-lived parent part',sku='Assembly',price=10,cost=5)
        child = Part.objects.create(name='Test_View_Part_Child',description='This is a short-lived child part',sku='Child',price=1,cost=0.5)
        Assembly.objects.create(parent=parent,part=part,count=1)
        Assembly.objects.create(parent=part,part=child,count=1)
        # Run Specific Tests
        response = self.client.get('{}{}/component/'.format(self.baseurl,part.id))
        self.assertContains(response, child.name)
        self.assertNotContains(response, part.name)
        self.assertNotContains(response, parent.name)
        response = self.client.get('{}{}/component/?search=Test_View_Part_Chi&limit=1&sort=sku'.format(self.baseurl,part.id))
        self.assertContains(response, child.name)
        response = self.client.get('{}{}/component/?name=Test_View_Part_Chi&offset=1&sort=-name'.format(self.baseurl,part.id))
        self.assertNotContains(response, child.name)
        with self.assertRaises(FieldError):
            response = self.client.get('{}{}/component/?sku=Chil&sort=banana'.format(self.baseurl,part.id))
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/component/'.format(self.baseurl, part.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            user.user_permissions.add(Permission.objects.get(name='Can view part'))
            response = self.client.get('{}{}/component/'.format(self.baseurl, part.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_part_pk_storage(self):
        self.user.user_permissions.add(Permission.objects.get(name='Can view storage'))
        # Get Object
        part = Part.objects.first()
        # Build Test Specific Objects
        storage = Storage.objects.create(name='Test_View_Storage_1', description='This is a short-lived container')
        ps = PartStorage.objects.create(part=part,storage=storage,count=5)
        storage = Storage.objects.create(name='Test_View_Storage_2', description='This is a short-lived container')
        ex = PartStorage.objects.create(part=Part.objects.last(),storage=storage,count=5)
        # Run Specific Tests
        response = self.client.get('{}{}/storage/'.format(self.baseurl,part.id))
        self.assertContains(response, ps.storage.name)
        self.assertNotContains(response, ps.part.name)
        self.assertNotContains(response, ex.storage.name)
        response = self.client.get('{}{}/storage/?search=Test_View_Storage&limit=1&sort=description'.format(self.baseurl,part.id))
        self.assertContains(response, ps.storage.name)
        with self.assertRaises(FieldError):
            response = self.client.get('{}{}/storage/?name=Test_View_Storage&offset=1&sort=banana'.format(self.baseurl,part.id))
        response = self.client.get('{}{}/storage/?description={}&sort=-name'.format(self.baseurl,part.id,'This is a short'))
        self.assertContains(response, ps.storage.name)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/storage/'.format(self.baseurl, part.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            user.user_permissions.add(Permission.objects.get(name='Can view part'))
            response = self.client.get('{}{}/storage/'.format(self.baseurl, part.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_part_pk_kit(self):
        self.user.user_permissions.add(Permission.objects.get(name='Can view kit'))
        # Get Object
        part = Part.objects.first()
        # Build Test Specific Objects
        kit = Kit.objects.create(name='Test_View_Kit_1', description='This is a short-lived kit', sku='Kit1', price=1)
        ex = Kit.objects.create(name='Test_View_Kit_2', description='This is a short-lived kit', sku='Kit2', price=1)
        storage = Storage.objects.create(name='Test_View_Storage_1', description='This is a short-lived container')
        ps = PartStorage.objects.create(part=part,storage=storage,count=5)
        KitPartStorage.objects.create(kit=kit,partstorage=ps,count=1)
        storage = Storage.objects.create(name='Test_View_Storage_2', description='This is a short-lived container')
        ps = PartStorage.objects.create(part=Part.objects.last(),storage=storage,count=5)
        KitPartStorage.objects.create(kit=ex,partstorage=ps,count=1)
        # Run Specific Tests
        response = self.client.get('{}{}/kit/'.format(self.baseurl,part.id))
        self.assertContains(response, kit.name)
        self.assertNotContains(response, ex.name)
        response = self.client.get('{}{}/kit/?search=Test_View_Kit&limit=1&sort=description'.format(self.baseurl,part.id))
        self.assertContains(response, kit.name)
        with self.assertRaises(FieldError):
            response = self.client.get('{}{}/kit/?name=Test_View_Kit&offset=1&sort=banana'.format(self.baseurl,part.id))
        response = self.client.get('{}{}/kit/?description={}&sort=-name'.format(self.baseurl,part.id,'This is a short'))
        self.assertContains(response, kit.name)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/kit/'.format(self.baseurl, part.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            user.user_permissions.add(Permission.objects.get(name='Can view part'))
            response = self.client.get('{}{}/kit/'.format(self.baseurl, part.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_part_pk_kit_sub(self):
        self.user.user_permissions.add(Permission.objects.get(name='Can decrement count kit'))
        # Get Object
        part = Part.objects.first()
        # Build Test Specific Objects
        kit = Kit.objects.create(name='Test_View_Kit_1', description='This is a short-lived kit', sku='Kit1', price=1)
        storage = Storage.objects.create(name='Test_View_Storage_1', description='This is a short-lived container')
        ps = PartStorage.objects.create(part=part,storage=storage,count=5)
        KitPartStorage.objects.create(kit=kit,partstorage=ps,count=2)
        # Run Specific Tests
        response = self.client.get('{}{}/kit/{}/subtract/2/'.format(self.baseurl,part.id,kit.id))
        self.assertContains(response, 'Part counts have been decremented for 2 number of Kit {}'.format(kit.name))
        self.assertNotContains(response, 'Kit cannot be decremented by 2, there are not enough parts available.')
        response = self.client.get('{}{}/kit/{}/subtract/1/'.format(self.baseurl,part.id,kit.id))
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/kit/{}/subtract/2/'.format(self.baseurl, part.id, kit.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            user.user_permissions.add(Permission.objects.get(name='Can view part'))
            response = self.client.get('{}{}/kit/{}/subtract/2/'.format(self.baseurl, part.id, kit.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_part_pk_kit_add(self):
        self.user.user_permissions.add(Permission.objects.get(name='Can increment count kit'))
        # Get Object
        part = Part.objects.first()
        # Build Test Specific Objects
        kit = Kit.objects.create(name='Test_View_Kit_1', description='This is a short-lived kit', sku='Kit1', price=1)
        storage = Storage.objects.create(name='Test_View_Storage_1', description='This is a short-lived container')
        ps = PartStorage.objects.create(part=part,storage=storage,count=5)
        KitPartStorage.objects.create(kit=kit,partstorage=ps,count=2)
        # Run Specific Tests
        response = self.client.get('{}{}/kit/{}/add/2/'.format(self.baseurl,part.id,kit.id))
        self.assertContains(response, 'Part counts have been incremented for 2 number of Kit {}'.format(kit.name))
        response = self.client.get('{}{}/kit/{}/add/1/'.format(self.baseurl,part.id,kit.id))
        self.assertContains(response, 'Part counts have been incremented for 1 number of Kit {}'.format(kit.name))
        self.assertNotContains(response, 'Kit cannot be decremented by 1, there are not enough parts available.')
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/kit/{}/add/1/'.format(self.baseurl, part.id, kit.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            user.user_permissions.add(Permission.objects.get(name='Can view part'))
            response = self.client.get('{}{}/kit/{}/add/1/'.format(self.baseurl, part.id, kit.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

# Test Inventory API Storage Views
class InventoryApiStorageViewTests(TestCase):
    def setUp(self):
        for i in range(0,50):
            Storage.objects.create(name='Test_View_Storage_' + str(i), description='This is an api view TestCase test storage')
        self.baseurl = api_baseurl + 'storage/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view storage','Can add storage','Can change storage','Can delete storage']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_storage_get(self):
        # Get Object
        storage = Storage.objects.first()
        # Run Generic Tests
        TestGenerics().get(self, self.baseurl, storage)
        TestGenerics().get(self, '{}?search={}'.format(self.baseurl, storage.name[0:4]), storage)
        TestGenerics().get(self, '{}?name={}'.format(self.baseurl, storage.name), storage)
        TestGenerics().get(self, '{}?description={}'.format(self.baseurl, storage.description[0:4]), storage)
        response = self.client.get('{}?limit=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, storage.name)
        response = self.client.get('{}?limit=20&offset=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertNotContains(response, storage.name)
        response = self.client.get('{}?limit=20&sort=name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, storage.name)
        response = self.client.get('{}?limit=20&sort=-name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertNotContains(response, storage.name)
        response = self.client.get('{}?offset=1000'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        self.assertNotContains(response, storage.name)
        response = self.client.get('{}?limit=20&sort=-name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertNotContains(response, storage.name)
        response = self.client.get('{}?sort=-banana'.format(self.baseurl))
        self.assertEqual(50, len(json.loads(response.content)))
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_storage_post(self):
        # Make Data
        data = {'name': 'Test_View_Storage_New', 'description': 'This is a new api view TestCase test storage'}
        serializer = StorageSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        # Run Generic Tests
        TestGenerics().post(self, self.baseurl, data)
        # Run Specific Tests
        self.assertIsNotNone(Storage.objects.filter(name='Test_View_Storage_New').first())
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.post(self.baseurl, data=data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_storage_pk_get(self):
        # Get Object
        storage = Storage.objects.first()
        # Run Generic Tests
        TestGenerics().pk_get(self, self.baseurl, storage)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/'.format(self.baseurl, storage.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_storage_pk_put(self):
        # Get Object
        storage = Storage.objects.first()
        storage.description = 'This is an updated description'
        # Run Generic Tests
        TestGenerics().pk_put(self, self.baseurl, storage)
        # Run Specific Tests
        storage.refresh_from_db()
        self.assertEqual(storage.description, 'This is an updated description')
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.put('{}{}/'.format(self.baseurl, storage.id), data=StorageSerializer(storage).data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_storage_pk_patch(self):
        # Get Object and Make Data
        storage = Storage.objects.first()
        data = {'id': str(storage.id), 'description': 'This is an updated description'}
        # Run Generic Tests
        TestGenerics().pk_patch(self, self.baseurl, data)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.patch('{}{}/'.format(self.baseurl, storage.id), data=data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_storage_pk_delete(self):
        # Get Object
        storage = Storage.objects.first()
        # Build Test Specific Objects
        part = Part.objects.create(name='Test_View_Part_1', description='This is a short-lived part', sku='00003', price=10.00, cost=5.00)
        PartStorage.objects.create(part=part, storage=storage, count=1)
        # Run Generic Tests
        TestGenerics().pk_delete(self, self.baseurl, storage)
        # Run Specific Tests
        self.assertIsNone(Storage.objects.filter(name=storage.name).first())
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.delete('{}{}/'.format(self.baseurl, storage.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_storage_pk_part(self):
        self.user.user_permissions.add(Permission.objects.get(name='Can view part'))
        # Get Object
        storage = Storage.objects.first()
        # Build Test Specific Objects
        part = Part.objects.create(name='Test_View_Part_1',description='This is a short-lived part',sku='Part1',price=2,cost=1)
        ps = PartStorage.objects.create(part=part,storage=storage,count=5)
        part = Part.objects.create(name='Test_View_Part_2',description='This is a short-lived part',sku='Part2',price=2,cost=1)
        ex = PartStorage.objects.create(part=part,storage=Storage.objects.last(),count=5)
        # Run Specific Tests
        response = self.client.get('{}{}/part/'.format(self.baseurl,storage.id))
        self.assertContains(response, ps.part.name)
        self.assertNotContains(response, ex.part.name)
        self.assertNotContains(response, storage.name)
        response = self.client.get('{}{}/part/?search=Test_View_Part&limit=1&sort=description'.format(self.baseurl,storage.id))
        self.assertContains(response, ps.part.name)
        with self.assertRaises(FieldError):
            response = self.client.get('{}{}/part/?name=Test_View_Part&offset=1&sort=banana'.format(self.baseurl,storage.id))
        response = self.client.get('{}{}/part/?description={}&sort=-name'.format(self.baseurl,storage.id,'This is a short'))
        self.assertContains(response, ps.part.name)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/part/'.format(self.baseurl, storage.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            user.user_permissions.add(Permission.objects.get(name='Can view storage'))
            response = self.client.get('{}{}/part/'.format(self.baseurl, storage.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


# Test Inventory API AlternateSKU Views
class InventoryApiAlternateSKUViewTests(TestCase):
    def setUp(self):
        for i in range(0,30):
            AlternateSKU.objects.create(manufacturer='Zephyrus Computing', sku=str(i).zfill(3))
        self.baseurl = api_baseurl + 'altsku/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view alternate sku','Can add alternate sku','Can change alternate sku','Can delete alternate sku']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_alternatesku_get(self):
        # Get Object
        sku = AlternateSKU.objects.first()
        # Run Specific Tests
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"manufacturer":"{}'.format(sku.manufacturer))
        self.assertContains(response, '"sku":"{}'.format(sku.sku))
        response = self.client.get('{}?search={}'.format(self.baseurl, sku.manufacturer[0:8]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"manufacturer":"{}'.format(sku.manufacturer))
        self.assertContains(response, '"sku":"{}'.format(sku.sku))
        response = self.client.get('{}?manufacturer={}'.format(self.baseurl, sku.manufacturer))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"sku":"' + sku.sku)
        response = self.client.get('{}?sku={}'.format(self.baseurl, sku.sku))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"sku":"' + sku.sku)
        response = self.client.get('{}?limit=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, sku.sku)
        response = self.client.get('{}?limit=20&offset=20'.format(self.baseurl))
        self.assertEqual(10, len(json.loads(response.content)))
        self.assertNotContains(response, sku.sku)
        response = self.client.get('{}?limit=20&sort=sku'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, sku.sku)
        response = self.client.get('{}?limit=20&sort=-sku'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertNotContains(response, sku.sku)
        response = self.client.get('{}?offset=1000'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        self.assertNotContains(response, sku.sku)
        response = self.client.get('{}?limit=20&sort=-sku'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertNotContains(response, sku.sku)
        response = self.client.get('{}?sort=-banana'.format(self.baseurl))
        self.assertEqual(30, len(json.loads(response.content)))
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_alternatesku_post(self):
        # Make Data
        data = {'manufacturer': 'Zephyrus Computing', 'sku': 'abc123'}
        serializer = AlternateSKUSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        # Run Generic Tests
        TestGenerics().post(self, self.baseurl, data)
        # Run Specific Tests
        self.assertIsNotNone(AlternateSKU.objects.filter(sku='abc123').first())
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.post(self.baseurl, data=data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_alternatesku_pk_get(self):
        # Get Object
        sku = AlternateSKU.objects.first()
        # Run Specific Tests
        response = self.client.get('{}{}/'.format(self.baseurl, sku.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"manufacturer":"' + sku.manufacturer)
        self.assertContains(response, '"sku":"' + sku.sku)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/'.format(self.baseurl, sku.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_alternatesku_pk_put(self):
        # Get Object
        sku = AlternateSKU.objects.first()
        sku.manufacturer = 'Zephyrus Computing LLC'
        # Run Specific Tests
        response = self.client.put('{}{}/'.format(self.baseurl, sku.id), AlternateSKUSerializer(sku).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'manufacturer': sku.manufacturer, 'sku': sku.sku.zfill(1000)}
        response = self.client.put('{}{}/'.format(self.baseurl, sku.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        sku.refresh_from_db()
        self.assertEqual(sku.manufacturer, 'Zephyrus Computing LLC')
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.put('{}{}/'.format(self.baseurl, sku.id), data=AlternateSKUSerializer(sku).data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_alternatesku_pk_patch(self):
        # Get Object and Make Data
        sku = AlternateSKU.objects.first()
        data = {'id': str(sku.id), 'manufacturer': 'Zephyrus Computing LLC'}
        # Run Specific Tests
        response = self.client.patch('{}{}/'.format(self.baseurl, sku.id), data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': str(sku.id), 'sku': sku.sku.zfill(1000)}
        response = self.client.patch('{}{}/'.format(self.baseurl, sku.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.patch('{}{}/'.format(self.baseurl, sku.id), data=data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    
    def test_apiview_alternatesku_pk_delete(self):
        # Get Object
        sku = AlternateSKU.objects.first()
        # Build Test Specific Objects
        part = Part.objects.create(name='Test_View_Part_1', description='This is a short-lived part', sku='00005', price=10.00, cost=5.00)
        PartAlternateSKU.objects.create(part=part, alt_sku=sku)
        # Run Generic Tests
        TestGenerics().pk_delete(self, self.baseurl, sku)
        # Run Specific Tests
        self.assertIsNone(AlternateSKU.objects.filter(id=sku.id).first())
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.delete('{}{}/'.format(self.baseurl, sku.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

# Test Inventory API PartStorage Views
class InventoryApiPartStorageViewTests(TestCase):
    def setUp(self):
        for i in range(0,101):
            Part.objects.create(name='Test_View_Part_' + str(i), description='This is an api view TestCase test part', sku=str(i*10).zfill(5), price=10.00, cost=5.00)
        for i in range(0,5):
            Storage.objects.create(name='Test_View_Storage_' + str(i), description='This is an api view TestCase test storage')
        for i in range(0,151):
            j = randint(1,100)
            k = randint(1,4)
            PartStorage.objects.create(part=Part.objects.get(name='Test_View_Part_' + str(j)), storage=Storage.objects.get(name='Test_View_Storage_' + str(k)), count=12)
        self.baseurl = api_baseurl + 'partstorage/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view part storage','Can add part storage','Can change part storage','Can delete part storage']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_partstorage_get(self):
        # Get Object
        ps = PartStorage.objects.first()
        # Run Specific Tests
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "\"part\":{}\"id\":{}".format('{', ps.part.id))
        self.assertContains(response, "\"storage\":{}\"id\":{}".format('{', ps.storage.id))
        response = self.client.get("{}?search={}".format(self.baseurl,ps. part.name[0:4]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "\"part\":{}\"id\":{}".format('{', ps.part.id))
        self.assertContains(response, "\"storage\":{}\"id\":{}".format('{', ps.storage.id))
        response = self.client.get('{}?storage__name={}'.format(self.baseurl, ps.storage.name))
        self.assertContains(response, ps.storage.name)
        response = self.client.get('{}?part__name={}'.format(self.baseurl, ps.part.name))
        self.assertContains(response, ps.part.name)
        response = self.client.get('{}?storage__description={}'.format(self.baseurl, ps.storage.description[0:8]))
        self.assertContains(response, ps.storage.description)
        response = self.client.get('{}?part__description={}'.format(self.baseurl, ps.part.description[0:8]))
        self.assertContains(response, ps.part.description)
        response = self.client.get('{}?part__sku={}'.format(self.baseurl, ps.part.sku))
        self.assertContains(response, ps.part.sku)
        response = self.client.get('{}?limit=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, ps.storage.name)
        response = self.client.get('{}?limit=20&offset=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        response = self.client.get('{}?limit=20&sort=storage__name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, ps.storage.name)
        response = self.client.get('{}?limit=20&sort=-storage__name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        response = self.client.get('{}?offset=1000'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        response = self.client.get('{}?limit=20&sort=-part__name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        response = self.client.get('{}?sort=-banana'.format(self.baseurl))
        self.assertEqual(100, len(json.loads(response.content)))
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstorage_post(self):
        # Get Objects and Make Data
        part = Part.objects.first()
        storage = Storage.objects.first()
        data = {'part': str(part.id), 'storage': str(storage.id), 'count': 3}
        # Run Specific Tests
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNotNone(PartStorage.objects.filter(part=part, storage=storage, count=3).first())
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.post(self.baseurl, {'part':0,'storage':0,'count':0})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.post(self.baseurl, data=data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstorage_pk_get(self):
        # Get Object
        ps = PartStorage.objects.first()
        # Run Specific Tests
        response = self.client.get("{}{}/".format(self.baseurl, ps.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "\"part\":{}\"id\":{}".format('{', ps.part.id))
        self.assertContains(response, "\"storage\":{}\"id\":{}".format('{', ps.storage.id))
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/'.format(self.baseurl, ps.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstorage_pk_put(self):
        # Get Object
        ps = PartStorage.objects.first()
        ps.count = 0
        # Run Specific Tests
        response = self.client.put("{}{}/".format(self.baseurl, ps.id), PartStorageSerializer(ps).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': str(ps.id), 'count': 0}
        response = self.client.put("{}{}/".format(self.baseurl, ps.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        ps.refresh_from_db()
        self.assertEqual(ps.count, 0)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.put('{}{}/'.format(self.baseurl, ps.id), data=PartStorageSerializer(ps).data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstorage_pk_patch(self):
        # Get Object and Make Data
        ps = PartStorage.objects.first()
        data = {'id': ps.id, 'count': 0}
        # Run Specific Tests
        response = self.client.patch("{}{}/".format(self.baseurl, ps.id), data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': ps.id, 'part': 0}
        response = self.client.patch("{}{}/".format(self.baseurl, ps.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.patch('{}{}/'.format(self.baseurl, ps.id), data=data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        
    def test_apiview_partstorage_pk_delete(self):
        # Get Object
        ps = PartStorage.objects.first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, self.baseurl, ps)
        # Run Specific Tests
        self.assertIsNone(PartStorage.objects.filter(id=ps.id).first())
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.delete('{}{}/'.format(self.baseurl, ps.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstorage_add(self):
        self.user.user_permissions.add(Permission.objects.get(name='Can increment count part storage'))
        # Get Object
        ps = PartStorage.objects.first()
        k = ps.count + 8
        # Build URL
        url = "{}addpart/{}/{}/8/".format(api_baseurl, ps.storage.id, ps.part.id)
        # Build New Storage object for failure testing
        s = Storage.objects.create(name='Test_View_Storage_New', description='This is a new api view TestCase test storage')
        # Build failure URL
        bad_url1 = "{}addpart/{}/{}/1/".format(api_baseurl, s.id, ps.part.id)
        # Run Specific Tests
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        ps.refresh_from_db()
        self.assertEqual(ps.count, k)
        response = self.client.get(bad_url1)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}addpart/{}/{}/1/'.format(api_baseurl, ps.storage.id, ps.part.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstorage_sub(self):
        self.user.user_permissions.add(Permission.objects.get(name='Can decrement count part storage'))
        # Get Object
        ps = PartStorage.objects.last()
        k = ps.count - 1
        # Build URL
        url = '{}subpart/{}/{}/{}/'.format(api_baseurl, ps.storage.id, ps.part.id, k)
        # Build New Part object for failure testing
        p = Part.objects.create(name='Test_View_Part_New', description='This is a new api view TestCase test part', sku='NEW', price=10.00, cost=5.00)
        # Build failure URL
        bad_url1 = '{}subpart/{}/{}/1/'.format(api_baseurl, p.id, ps.storage.id)
        # Run Specific Tests
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get(bad_url1)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        ps.refresh_from_db()
        self.assertEqual(ps.count, 1)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}subpart/{}/{}/1/'.format(api_baseurl, ps.storage.id, ps.part.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstorage_add2(self):
        self.user.user_permissions.add(Permission.objects.get(name='Can increment count part storage'))
        # Get Object
        ps = PartStorage.objects.first()
        k = ps.count + 8
        # Build URL
        url = "{}{}/add/8/".format(self.baseurl, ps.id)
        # Build failure URL
        bad_url1 = "{}0/add/1/".format(self.baseurl)
        # Run Specific Tests
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        ps.refresh_from_db()
        self.assertEqual(ps.count, k)
        response = self.client.get(bad_url1)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/add/1/'.format(self.baseurl, ps.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstorage_sub2(self):
        self.user.user_permissions.add(Permission.objects.get(name='Can decrement count part storage'))
        # Get Object
        ps = PartStorage.objects.last()
        k = ps.count - 1
        # Build URL
        url = '{}{}/subtract/{}/'.format(self.baseurl, ps.id, k)
        # Build failure URL
        bad_url1 = '{}0/subtract/1/'.format(self.baseurl)
        # Run Specific Tests
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get(bad_url1)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        ps.refresh_from_db()
        self.assertEqual(ps.count, 1)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/subtract/1/'.format(self.baseurl, ps.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

# Test PartAlternateSKU Views
class InventoryApiPartAlternateSKUViewTests(TestCase):
    def setUp(self):
        for i in range(0,301):
            Part.objects.create(name='Test_View_Part_' + str(i), description='This is an api view TestCase test part', sku=str(i*3).zfill(6), price=10.00, cost=5.00)
        for i in range(0,60):
            j = randint(1,300)
            AlternateSKU.objects.create(manufacturer='Zephyrus Computing', sku='aa' + str(i).zfill(4))
            PartAlternateSKU.objects.create(part=Part.objects.all()[j], alt_sku=AlternateSKU.objects.last())
        self.baseurl = api_baseurl + 'partaltsku/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view part alternate sku','Can add part alternate sku','Can change part alternate sku','Can delete part alternate sku']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_partaltsku_get(self):
        # Get Object
        pa = PartAlternateSKU.objects.first()
        # Run Specific Tests
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"part":' + json.dumps(PartSerializer(pa.part).data, separators=(",",":"))[:-1])
        self.assertContains(response, '"alt_sku":' + json.dumps(AlternateSKUSerializer(pa.alt_sku).data, separators=(",",":"))[:-1])
        response = self.client.get(self.baseurl + '?search=' + pa.part.name[0:4])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"part":' + json.dumps(PartSerializer(pa.part).data, separators=(",",":"))[:-1])
        self.assertContains(response, '"alt_sku":' + json.dumps(AlternateSKUSerializer(pa.alt_sku).data, separators=(",",":"))[:-1])
        response = self.client.get('{}?alt_sku__sku={}'.format(self.baseurl, pa.alt_sku.sku))
        self.assertContains(response, pa.alt_sku.sku)
        response = self.client.get('{}?alt_sku__manufacturer={}'.format(self.baseurl, pa.alt_sku.manufacturer))
        self.assertContains(response, pa.alt_sku.manufacturer)
        response = self.client.get('{}?part__name={}'.format(self.baseurl, pa.part.name))
        self.assertContains(response, pa.part.name)
        response = self.client.get('{}?part__description={}'.format(self.baseurl, pa.part.description[0:8]))
        self.assertContains(response, pa.part.description)
        response = self.client.get('{}?part__sku={}'.format(self.baseurl, pa.part.sku))
        self.assertContains(response, pa.part.sku)
        response = self.client.get('{}?limit=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, pa.part.name)
        response = self.client.get('{}?limit=20&offset=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        response = self.client.get('{}?limit=20&sort=part__name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, pa.part.name)
        response = self.client.get('{}?limit=20&sort=-part__name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        response = self.client.get('{}?offset=1000'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        response = self.client.get('{}?limit=20&sort=-altsku__sku'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        response = self.client.get('{}?sort=-banana'.format(self.baseurl))
        self.assertEqual(60, len(json.loads(response.content)))
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partaltsku_post(self):
        # Get Objects and Make Data
        part = Part.objects.first()
        altsku = AlternateSKU.objects.first()
        data = {'part': part.id, 'alt_sku': altsku.id}
        # Run Specific Tests
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNotNone(PartAlternateSKU.objects.filter(part=part, alt_sku=altsku).first())
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.post(self.baseurl, {'part':0,'alt_sku':0})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.post(self.baseurl, data=data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partaltsku_pk_get(self):
        # Get Object
        pa = PartAlternateSKU.objects.first()
        # Run Specific Tests
        response = self.client.get(self.baseurl + str(pa.id) + '/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"part":' + json.dumps(PartSerializer(pa.part).data, separators=(",",":"))[:-1])
        self.assertContains(response, '"alt_sku":' + json.dumps(AlternateSKUSerializer(pa.alt_sku).data, separators=(",",":"))[:-1])
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/'.format(self.baseurl, pa.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partaltsku_pk_put(self):
        # Get Objects
        p = Part.objects.first()
        pa = PartAlternateSKU.objects.first()
        if pa.part == p:
            p = Part.objects.last()
        pa.part = p
        # Run Specific Tests
        response = self.client.put(self.baseurl + str(pa.id) + '/', PartAlternateSKUSerializer(pa).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_apiview_partaltsku_pk_patch(self):
        # Get Objects and Make Data
        altsku = AlternateSKU.objects.first()
        # Run Specific Tests
        pa = PartAlternateSKU.objects.first()
        if pa.alt_sku == altsku:
            altsku = AlternateSKU.objects.last()
        data = {'id': str(pa.id), 'alt_sku': altsku.id}
        # Run Specific Tests
        response = self.client.patch(self.baseurl + str(pa.id) + '/', data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_apiview_partaltsku_pk_delete(self):
        # Get Object
        pa = PartAlternateSKU.objects.first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, self.baseurl, pa)
        # Run Specific Tests
        self.assertIsNone(PartAlternateSKU.objects.filter(id=pa.id).first())
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.delete('{}{}/'.format(self.baseurl, pa.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


# Test Assembly Views
class InventoryApiAssemblyViewTests(TestCase):
    def setUp(self):
        for i in range(0,101):
            Part.objects.create(name='Test_View_Part_' + str(i), description='This is an api view TestCase test part', sku=str(i*3).zfill(6), price=10.00, cost=5.00)
        for i in range(0,16):
            for j in range(0, randint(1,10)):
                k = randint(0,99)
                Assembly.objects.create(parent=Part.objects.all()[i], part=Part.objects.all()[k], count=randint(1,5))
        self.baseurl = api_baseurl + 'assembly/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view assembly','Can add assembly','Can change assembly','Can delete assembly']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_assembly_get(self):
        # Get Object
        a = Assembly.objects.first()
        # Run Specific Tests
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"parent":' + json.dumps(PartSerializer(a.parent).data, separators=(",",":"))[:-1])
        self.assertContains(response, '"part":' + json.dumps(PartSerializer(a.part).data, separators=(",",":"))[:-1])
        response = self.client.get(self.baseurl + '?search=' + a.parent.name[0:4])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"parent":' + json.dumps(PartSerializer(a.parent).data, separators=(",",":"))[:-1])
        self.assertContains(response, '"part":' + json.dumps(PartSerializer(a.part).data, separators=(",",":"))[:-1])
        response = self.client.get('{}?parent__name={}'.format(self.baseurl, a.parent.name))
        self.assertContains(response, a.parent.name)
        response = self.client.get('{}?parent__description={}'.format(self.baseurl, a.parent.description[0:8]))
        self.assertContains(response, a.parent.description)
        response = self.client.get('{}?parent__sku={}'.format(self.baseurl, a.parent.sku))
        self.assertContains(response, a.parent.sku)
        response = self.client.get('{}?part__name={}'.format(self.baseurl, a.part.name))
        self.assertContains(response, a.part.name)
        response = self.client.get('{}?part__description={}'.format(self.baseurl, a.part.description[0:8]))
        self.assertContains(response, a.part.description)
        response = self.client.get('{}?part__sku={}'.format(self.baseurl, a.part.sku))
        self.assertContains(response, a.part.sku)
        response = self.client.get('{}?limit=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, a.part.name)
        response = self.client.get('{}?limit=20&offset=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        response = self.client.get('{}?limit=20&sort=parent__name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, a.parent.name)
        response = self.client.get('{}?limit=20&sort=-part__name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        response = self.client.get('{}?offset=1000'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        response = self.client.get('{}?limit=20&sort=-parent__sku'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        response = self.client.get('{}?sort=-banana'.format(self.baseurl))
        self.assertGreater(len(json.loads(response.content)), 20)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_assembly_post(self):
        # Get Objects and Make Data
        part = Part.objects.last()
        assembly = Assembly.objects.first()
        data = {'parent': assembly.parent.id, 'part': part.id, 'count': 1}
        # Run Specific Tests
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNotNone(Assembly.objects.filter(parent=assembly.parent, part=part).first())
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.post(self.baseurl, {'parent':0,'part':0})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.post(self.baseurl, data=data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_assembly_pk_get(self):
        # Get Object
        a = Assembly.objects.first()
        # Run Specific Tests
        response = self.client.get(self.baseurl + str(a.id) + '/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"parent":' + json.dumps(PartSerializer(a.parent).data, separators=(",",":"))[:-1])
        self.assertContains(response, '"part":' + json.dumps(PartSerializer(a.part).data, separators=(",",":"))[:-1])
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/'.format(self.baseurl, a.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_assembly_pk_put(self):
        # Get Objects
        p = Part.objects.first()
        a = Assembly.objects.first()
        if a.part == p:
            p = Part.objects.last()
        p2 = a.part
        a.part = p
        # Run Specific Tests
        response = self.client.put(self.baseurl + str(a.id) + '/', AssemblySerializer(a).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': str(a.id), 'part': a.part.id}
        response = self.client.put(self.baseurl + str(a.id) + '/', bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        a.refresh_from_db()
        self.assertEqual(a.part, p2)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.put('{}{}/'.format(self.baseurl, a.id), data=AssemblySerializer(a).data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_assembly_pk_patch(self):
        # Get Objects and Make Data
        p = Part.objects.first()
        a = Assembly.objects.first()
        if a.parent == p:
            p = Part.objects.last()
        data = {'id': str(a.id), 'parent': p.id}
        # Run Specific Tests
        response = self.client.patch(self.baseurl + str(a.id) + '/', data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': str(a.id), 'parent': 0}
        response = self.client.patch(self.baseurl + str(a.id) + '/', bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.patch('{}{}/'.format(self.baseurl, a.id), data=data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_assembly_pk_delete(self):
        # Get Object
        a = Assembly.objects.first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, self.baseurl, a)
        # Run Specific Tests
        self.assertIsNone(Assembly.objects.filter(id=a.id).first())
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.delete('{}{}/'.format(self.baseurl, a.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_assembly_pk_part(self):
        self.user.user_permissions.add(Permission.objects.get(name='Can view part'))
        # Get Object
        a = Assembly.objects.first()
        child = Part.objects.create(name='Test_View_Part_Child',description='This is a short-lived child part',sku='Child',price=1,cost=0.5)
        Assembly.objects.create(parent=a.parent,part=child,count=1)
        # Run Specific Tests
        response = self.client.get('{}{}/part/'.format(self.baseurl,a.id))
        self.assertContains(response, child.name)
        self.assertNotContains(response, a.parent.name)
        response = self.client.get('{}{}/part/?search=Test_View_Part_Chi&limit=1&sort=sku'.format(self.baseurl,a.id))
        self.assertContains(response, child.name)
        response = self.client.get('{}{}/part/?name=Test_View_Part_Chi&offset=1&sort=-name'.format(self.baseurl,a.id))
        self.assertNotContains(response, child.name)
        with self.assertRaises(FieldError):
            response = self.client.get('{}{}/part/?sku=Chil&sort=banana'.format(self.baseurl,a.id))
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/part/'.format(self.baseurl, a.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            user.user_permissions.add(Permission.objects.get(name='Can view assembly'))
            response = self.client.get('{}{}/part/'.format(self.baseurl, a.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

# Test Kit Views
class InventoryApiKitViewTests(TestCase):
    def setUp(self):
        for i in range(0,101):
            Kit.objects.create(name='Test_View_Kit_' + str(i), description='This is an api view TestCase test kit', sku=str(i).zfill(6), price=10.00)
        self.baseurl = '{}kit/'.format(api_baseurl)
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view kit','Can add kit','Can change kit','Can delete kit']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_kit_get(self):
        # Get Object
        kit = Kit.objects.first()
        # Run Generic Tests
        TestGenerics().get(self, self.baseurl, kit)
        TestGenerics().get(self, '{}?search={}'.format(self.baseurl, kit.name[0:4]), kit)
        TestGenerics().get(self, '{}?name={}'.format(self.baseurl, kit.name), kit)
        TestGenerics().get(self, '{}?description={}'.format(self.baseurl, kit.description[0:4]), kit)
        TestGenerics().get(self, '{}?sku={}'.format(self.baseurl, kit.sku[0:3]), kit)
        response = self.client.get('{}?limit=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, kit.name)
        response = self.client.get('{}?limit=20&offset=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertNotContains(response, kit.name)
        response = self.client.get('{}?limit=20&sort=name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, kit.name)
        response = self.client.get('{}?limit=20&sort=-name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertNotContains(response, kit.name)
        response = self.client.get('{}?offset=1000'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        self.assertNotContains(response, kit.name)
        response = self.client.get('{}?limit=20&sort=-name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertNotContains(response, kit.name)
        response = self.client.get('{}?sort=-banana'.format(self.baseurl))
        self.assertEqual(100, len(json.loads(response.content)))
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kit_post(self):
        # Make Data
        data = {'name': 'Test_View_Kit_New', 'description': 'This is a new api view TestCase test kit', 'sku': 'new', 'price': '10.00'}
        serializer = KitSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        # Run Generic Tests
        TestGenerics().post(self, self.baseurl, data, False) # This is set to False because of the Unique Constraint on the 'sku' field which causes a 400 reply instead of the usual expected 200
        # Run Specific Tests
        self.assertIsNotNone(Kit.objects.filter(name='Test_View_Kit_New').first())
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.post(self.baseurl, data=data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kit_pk_get(self):
        # Get Object
        kit = Kit.objects.first()
        # Run Generic Tests
        TestGenerics().pk_get(self, self.baseurl, kit)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/'.format(self.baseurl, kit.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kit_pk_put(self):
        # Get Object
        kit = Kit.objects.first()
        kit.name = 'updated'
        # Run Generic Tests
        TestGenerics().pk_put(self, self.baseurl, kit)
        # Run Specific Tests
        kit.refresh_from_db()
        self.assertEqual(kit.name, 'updated')
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.put('{}{}/'.format(self.baseurl, kit.id), data=KitSerializer(kit).data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kit_pk_patch(self):
        # Get Object and Make Data
        kit = Kit.objects.first()
        data = {'id': kit.id, 'sku': kit.sku, 'name': 'updated'}
        # Run Generic Tests
        TestGenerics().pk_patch(self, self.baseurl, data)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.patch('{}{}/'.format(self.baseurl, kit.id), data=data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kit_pk_delete(self):
        # Get Object
        kit = Kit.objects.first()
        # Build Test Specific Objects
        part = Part.objects.create(name='Test_View_Part1', description='This is a short-lived part', sku='shortlived', price=5.00, cost=0.00)
        storage = Storage.objects.create(name='Test_View_Storage1', description='This is a short-lived container')
        partstorage = PartStorage.objects.create(part=part, storage=storage, count=1)
        KitPartStorage.objects.create(kit=kit, partstorage=partstorage, count=1)
        # Run Generic Tests
        TestGenerics().pk_delete(self, self.baseurl, kit)
        # Run Specific Tests
        self.assertIsNone(Kit.objects.filter(name=kit.name).first())
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.delete('{}{}/'.format(self.baseurl, kit.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kit_sub(self):
        self.user.user_permissions.add(Permission.objects.get(name='Can decrement count kit'))
        # Get Object
        kit = Kit.objects.first()
        # Build Test Specific Objects
        part = Part.objects.create(name='Test_View_Part2', description='This is a short-lived part', sku='shortlived2', price=5.00, cost=0.00)
        storage = Storage.objects.create(name='Test_View_Storage2', description='This is a short-lived container')
        partstorage = PartStorage.objects.create(part=part, storage=storage, count=1)
        kitpartstorage = KitPartStorage.objects.create(kit=kit, partstorage=partstorage, count=1)
        # Run Specific Tests
        response = self.client.get('{}subkit/{}/{}/'.format(api_baseurl, kit.id, 1))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        partstorage.refresh_from_db()
        self.assertEqual(partstorage.count, 0)
        response = self.client.get('{}subkit/{}/{}/'.format(api_baseurl, kit.id, 1))
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}subkit/{}/1/'.format(api_baseurl, kit.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kit_sub2(self):
        self.user.user_permissions.add(Permission.objects.get(name='Can decrement count kit'))
        # Get Object
        kit = Kit.objects.first()
        # Build Test Specific Objects
        part = Part.objects.create(name='Test_View_Part2', description='This is a short-lived part', sku='shortlived2', price=5.00, cost=0.00)
        storage = Storage.objects.create(name='Test_View_Storage2', description='This is a short-lived container')
        partstorage = PartStorage.objects.create(part=part, storage=storage, count=1)
        kitpartstorage = KitPartStorage.objects.create(kit=kit, partstorage=partstorage, count=1)
        # Run Specific Tests
        response = self.client.get('{}{}/subtract/1/'.format(self.baseurl, kit.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        partstorage.refresh_from_db()
        self.assertEqual(partstorage.count, 0)
        response = self.client.get('{}{}/subtract/1/'.format(self.baseurl, kit.id))
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/subtract/1/'.format(self.baseurl, kit.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kit_add(self):
        self.user.user_permissions.add(Permission.objects.get(name='Can increment count kit'))
        # Get Object
        kit = Kit.objects.first()
        # Build Test Specific Objects
        part = Part.objects.create(name='Test_View_Part2', description='This is a short-lived part', sku='shortlived2', price=5.00, cost=0.00)
        storage = Storage.objects.create(name='Test_View_Storage2', description='This is a short-lived container')
        partstorage = PartStorage.objects.create(part=part, storage=storage, count=1)
        kitpartstorage = KitPartStorage.objects.create(kit=kit, partstorage=partstorage, count=1)
        # Run Specific Tests
        response = self.client.get('{}{}/add/1/'.format(self.baseurl, kit.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        partstorage.refresh_from_db()
        self.assertEqual(partstorage.count, 2)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/add/1/'.format(self.baseurl, kit.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    
    def test_apiview_kit_pk_partstorage(self):
        self.user.user_permissions.add(Permission.objects.get(name='Can view kit part storage'))
        # Get Object
        kit = Kit.objects.first()
        # Build Test Specific Objects
        part = Part.objects.create(name='Test_View_Part_1',description='This is a short-lived part',sku='Part1',price=2,cost=1)
        storage = Storage.objects.create(name='Test_View_Storage_1',description='This is a short-lived container')
        ps = PartStorage.objects.create(part=part,storage=storage,count=5)
        KitPartStorage.objects.create(kit=kit,partstorage=ps,count=1)
        part = Part.objects.create(name='Test_View_Part_2',description='This is a short-lived part',sku='Part2',price=2,cost=1)
        ps = PartStorage.objects.create(part=part,storage=storage,count=5)
        KitPartStorage.objects.create(kit=kit,partstorage=ps,count=1)
        # Run Specific Tests
        response = self.client.get('{}{}/partstorage/'.format(self.baseurl,kit.id))
        self.assertContains(response, ps.part.name)
        self.assertNotContains(response, kit.name)
        response = self.client.get('{}{}/partstorage/?search=Test_View_Part&limit=1&sort=part__description'.format(self.baseurl,kit.id))
        self.assertNotContains(response, ps.part.name)
        with self.assertRaises(FieldError):
            response = self.client.get('{}{}/partstorage/?name=Test_View_Storage&offset=1&sort=banana'.format(self.baseurl,kit.id))
        response = self.client.get('{}{}/partstorage/?part__description={}&sort=-part__name'.format(self.baseurl,kit.id,'This is a short'))
        self.assertContains(response, ps.part.name)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/partstorage/'.format(self.baseurl, kit.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            user.user_permissions.add(Permission.objects.get(name='Can view kit'))
            response = self.client.get('{}{}/partstorage/'.format(self.baseurl, kit.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apivew_kit_pk_count(self):
        # Get Object
        kit = Kit.objects.first()
        # Build Test Specific Objects
        part = Part.objects.create(name='Test_View_Part_1',description='This is a short-lived part',sku='Part1',price=2,cost=1)
        storage = Storage.objects.create(name='Test_View_Storage_1', description='This is a short-lived container')
        ps = PartStorage.objects.create(part=part,storage=storage,count=5)
        KitPartStorage.objects.create(kit=kit,partstorage=ps,count=1)
        part = Part.objects.create(name='Test_View_Part_2',description='This is a short-lived part',sku='Part2',price=2,cost=1)
        storage = Storage.objects.create(name='Test_View_Storage_2', description='This is a short-lived container')
        ps = PartStorage.objects.create(part=part,storage=storage,count=5)
        kps = KitPartStorage.objects.create(kit=kit,partstorage=ps,count=1)
        # Run Specific Tests
        response = self.client.get('{}{}/count/'.format(self.baseurl,kit.id))
        self.assertEqual(response.content, b'{"count":5}')
        ps.subtract(3)
        response = self.client.get('{}{}/count/'.format(self.baseurl,kit.id))
        self.assertEqual(response.content, b'{"count":2}')
        ps.delete()
        kps.delete()
        response = self.client.get('{}{}/count/'.format(self.baseurl,kit.id))
        self.assertEqual(response.content, b'{"count":5}')
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/count/'.format(self.baseurl, kit.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


# Test Inventory API KitPartStorage Views
class InventoryApiKitPartStorageViewTests(TestCase):
    def setUp(self):
        for i in range(101,201):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is an api view TestCase test part', sku=str(i).zfill(6), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is an api view TestCase test storage')
            Kit.objects.create(name='Test_View_Kit_{}'.format(i), description='This is an api view TestCase test kit', sku=str(i).zfill(6), price=10.00)
        for i in range(0,151):
            j = randint(101,200)
            k = randint(101,200)
            l = randint(20,50)
            PartStorage.objects.create(part=Part.objects.get(name='Test_View_Part_{}'.format(j)), storage=Storage.objects.get(name='Test_View_Storage_{}'.format(k)), count=l)
        for i in range(0,101):
            j = randint(101,199)
            k = randint(0,100)
            l = randint(1,11)
            KitPartStorage.objects.create(kit=Kit.objects.get(name='Test_View_Kit_{}'.format(j)), partstorage=PartStorage.objects.all()[k], count=l)
        self.baseurl = '{}kitpartstorage/'.format(api_baseurl)
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view kit part storage','Can add kit part storage','Can change kit part storage','Can delete kit part storage']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_kitpartstorage_get(self):
        # Get Object
        kps = KitPartStorage.objects.first()
        # Run Specific Tests
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"kit":{}'.format(kps.kit.id))
        self.assertContains(response, '"partstorage":{}'.format(kps.partstorage.id))
        response = self.client.get(self.baseurl + '?search=' + kps.kit.name[0:4])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"kit":{}'.format(kps.kit.id))
        self.assertContains(response, '"partstorage":{}'.format(kps.partstorage.id))
        response = self.client.get('{}?partstorage__storage__name={}'.format(self.baseurl, kps.partstorage.storage.name))
        self.assertContains(response, kps.partstorage.id)
        response = self.client.get('{}?partstorage__part__name={}'.format(self.baseurl, kps.partstorage.part.name))
        self.assertContains(response, kps.partstorage.id)
        response = self.client.get('{}?partstorage__storage__description={}'.format(self.baseurl, kps.partstorage.storage.description[0:8]))
        self.assertContains(response, kps.partstorage.id)
        response = self.client.get('{}?partstorage__part__description={}'.format(self.baseurl, kps.partstorage.part.description[0:8]))
        self.assertContains(response, kps.partstorage.id)
        response = self.client.get('{}?partstorage__part__sku={}'.format(self.baseurl, kps.partstorage.part.sku))
        self.assertContains(response, kps.partstorage.id)
        response = self.client.get('{}?limit=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, kps.partstorage.id)
        response = self.client.get('{}?limit=20&offset=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        response = self.client.get('{}?limit=20&sort=storage__name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, kps.partstorage.id)
        response = self.client.get('{}?limit=20&sort=-storage__name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        response = self.client.get('{}?offset=1000'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        response = self.client.get('{}?limit=20&sort=-part__name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        response = self.client.get('{}?sort=-banana'.format(self.baseurl))
        self.assertEqual(100, len(json.loads(response.content)))
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kitpartstorage_post(self):
        # Get Objects and Make Data
        kit = Kit.objects.last()
        ps = PartStorage.objects.last()
        data = {'kit': str(kit.id), 'partstorage': str(ps.id), 'count': 3}
        # Run Specific Tests
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNotNone(KitPartStorage.objects.filter(kit=kit, partstorage=ps, count=3).first())
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.post(self.baseurl, {'kit':0,'partstorage':0,'count':0})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.post(self.baseurl, data=data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kitpartstorage_pk_get(self):
        # Get Object
        kps = KitPartStorage.objects.first()
        # Run Specific Tests
        response = self.client.get('{}{}/'.format(self.baseurl, kps.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"kit":{}'.format(kps.kit.id))
        self.assertContains(response, '"partstorage":{}'.format(kps.partstorage.id))
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/'.format(self.baseurl, kps.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kitpartstorage_pk_put(self):
        # Get Object
        kps = KitPartStorage.objects.first()
        kps.count = 0
        # Run Specific Tests
        response = self.client.put('{}{}/'.format(self.baseurl, kps.id), KitPartStorageSerializer(kps).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': str(kps.id), 'count': 0}
        response = self.client.put('{}{}/'.format(self.baseurl, kps.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        kps.refresh_from_db()
        self.assertEqual(kps.count, 0)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.put('{}{}/'.format(self.baseurl, kps.id), data=KitPartStorageSerializer(kps).data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kitpartstorage_pk_patch(self):
        # Get Object and Make Data
        kps = KitPartStorage.objects.first()
        data = {'id': kps.id, 'count': 0}
        # Run Specific Tests
        response = self.client.patch('{}{}/'.format(self.baseurl, kps.id), data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': kps.id, 'kit': 0}
        response = self.client.patch('{}{}/'.format(self.baseurl, kps.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.patch('{}{}/'.format(self.baseurl, kps.id), data=data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kitpartstorage_pk_delete(self):
        # Get Object
        kps = KitPartStorage.objects.first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, self.baseurl, kps)
        # Run Specific Tests
        self.assertIsNone(KitPartStorage.objects.filter(id=kps.id).first())
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.delete('{}{}/'.format(self.baseurl, kps.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class TestGenerics():
    def get_serialized_data(self, obj):
    # Determine the model and return the data serialized
        if obj.__class__.__name__ == 'Part':
            return PartSerializer(obj).data
        if obj.__class__.__name__ == 'Storage':
            return StorageSerializer(obj).data
        if obj.__class__.__name__ == 'AlternateSKU':
            return AlternateSKUSerializer(obj).data
        if obj.__class__.__name__ == 'PartAlternateSKU':
            return PartAlternateSKUSerializer(obj).data
        if obj.__class__.__name__ == 'PartStorage':
            return PartStorageSerializer(obj).data
        if obj.__class__.__name__ == 'Assembly':
            return AssemblySerializer(obj).data
        if obj.__class__.__name__ == 'Kit':
            return KitSerializer(obj).data
        if obj.__class__.__name__ == 'KitPartStorage':
            return KitPartStorageSerializer(obj).data
        else:
            return json.dumps(obj)

    def get_completed_url(self, url, obj_or_data):
        serialized = self.get_serialized_data(obj_or_data)
        if isinstance(serialized, str):
            serialized = json.loads(serialized)
        return '{}{}/'.format(url, serialized.get('id'))

    def get(self, case, url, obj):
        # GET Valid Request
        response = case.client.get(url)
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        case.assertContains(response, obj)
        if '?' in url:
            response = case.client.get('{}&limit=1'.format(url))
            case.assertEqual(response.status_code, status.HTTP_200_OK)
            case.assertContains(response, obj)
            response = case.client.get('{}&limit=1&offset=2'.format(url))
            case.assertEqual(response.status_code, status.HTTP_200_OK)
            case.assertNotContains(response, obj)
        else: 
            response = case.client.get('{}?limit=1'.format(url))
            case.assertEqual(response.status_code, status.HTTP_200_OK)
            case.assertContains(response, obj)
            response = case.client.get('{}?limit=1&offset=2'.format(url))
            case.assertEqual(response.status_code, status.HTTP_200_OK)
            case.assertNotContains(response, obj)
        # Not Implemented Yet
        # Test User without Permissions
        #good_user = case.user
        #bad_user = User.objects.get_or_create(username='bad_user')[0]
        #case.client.force_login(bad_user)
        #response = case.client.get(valid)
        #case.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        #case.client.force_login(good_user)

    def post(self, case, url, data, dup=True):
        # POST Valid Data
        response = case.client.post(url, data)
        case.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # POST Duplicate Data
        if dup:
            response = case.client.post(url, data)
            case.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # POST Invalid Data
        bad_data = {'name': 'bad'}
        response = case.client.post(url, bad_data)
        case.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # Test User without Permissions
        good_user = case.user
        bad_user = User.objects.get_or_create(username='bad_user')[0]
        case.client.force_login(bad_user)
        response = case.client.post(url, data)
        case.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        case.client.force_login(good_user)

    def pk_get(self, case, url, obj):
        # GET Valid Request
        valid = self.get_completed_url(url, obj)
        response = case.client.get(valid)
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        case.assertContains(response, obj)
        # GET Invalid Request
        response = case.client.get(url + '0/')
        case.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # Not Implemented Yet
        # Test User without Permissions
        #good_user = case.user
        #bad_user = User.objects.get_or_create(username='bad_user')[0]
        #case.client.force_login(bad_user)
        #response = case.client.get(valid)
        #case.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        #case.client.force_login(good_user)

    def pk_put(self, case, url, obj):
        serialized = self.get_serialized_data(obj)
        # PUT Valid Request
        valid = self.get_completed_url(url, obj)
        response = case.client.put(valid, serialized, content_type='application/json')
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        # PUT Invalid Request
        bad_data = {'id': obj.id, 'name': obj.name}
        response = case.client.put(valid, bad_data, content_type='application/json')
        case.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # Test User without Permissions
        good_user = case.user
        bad_user = User.objects.get_or_create(username='bad_user')[0]
        case.client.force_login(bad_user)
        response = case.client.put(valid, serialized, content_type='application/json')
        case.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        case.client.force_login(good_user)

    def pk_patch(self, case, url, data):
        # PATCH Valid Data
        valid = self.get_completed_url(url, data)
        response = case.client.patch(valid, data, content_type='application/json')
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        # PATCH Invalid Data
        bad_data = {'name': 'bad'.zfill(1000)}
        response = case.client.patch(valid, bad_data, content_type='application/json')
        case.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # Test User without Permissions
        good_user = case.user
        bad_user = User.objects.get_or_create(username='bad_user')[0]
        case.client.force_login(bad_user)
        response = case.client.patch(valid, data, content_type='application/json')
        case.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        case.client.force_login(good_user)

    def pk_delete(self, case, url, obj):
        # DELETE Valid Request
        valid = self.get_completed_url(url, obj)
        response = case.client.delete(valid)
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        # Test User without Permissions
        good_user = case.user
        bad_user = User.objects.get_or_create(username='bad_user')[0]
        case.client.force_login(bad_user)
        response = case.client.delete(valid)
        case.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        case.client.force_login(good_user)
