import django
django.setup()
from django.contrib.auth.models import AnonymousUser, User, Permission
from django.test import TestCase, override_settings
from random import randint
from rest_framework import status
from inventory.models import Part, Storage, AlternateSKU, PartAlternateSKU, PartStorage, Assembly, Kit, KitPartStorage

baseurl = '/inventory/admin/'

# Test module root view
class InventoryRootViewTests(TestCase):
    def test_view_root(self):
        response = self.client.get(baseurl)
        self.assertRedirects(response, '/accounts/login/?next={}'.format(baseurl))
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.get(baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/admin/home.html')

# Test PartThreshold views
class InventoryPartAdminViewTests(TestCase):
    def setUp(self):
        for i in range(2001,2010):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a TestCase test part', sku=str(i), price=10.00, cost=5.00)
        self.baseurl = baseurl + 'part/'

    def test_view_part_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_part')
        TestGenerics().get(self, self.baseurl, permission, 'inventory/admin/view.html')
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.get('{}?q=Test&page=1000'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_part_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_part')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'inventory/admin/create.html')
        data = {'name': 'Test_View_Part_New', 'description': 'New Test View Part', 'sku': 'new', 'price': 9.99, 'cost': 4.99}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'name': 'banana'}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_part_change(self):
        # Test the change view
        permission = Permission.objects.get(codename='change_part')
        p = Part.objects.last()
        TestGenerics().get(self, '{}change/{}/'.format(self.baseurl, p.id), permission, 'inventory/admin/update.html')
        data = {'name': 'Test_View_PartThreshold_Modified', 'description': 'Modified description', 'sku': 'new', 'price': 5, 'cost': 4}
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, p.id), data, permission)
        data = {'name': 'Test_View_PartThreshold_Modified', 'description': 'Does this work too?'}
        self.client.post('{}change/{}/'.format(self.baseurl, p.id), data)

    def test_view_part_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_part')
        p = Part.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, p.id), permission)
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}delete/{}/'.format(self.baseurl, p.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class IventoryPartStorageAdminViewTests(TestCase):
    def setUp(self):
        for i in range(2011,2020):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a TestCase test part', sku=str(i), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a view TestCase test storage')
        for i in range(100,105):
            j = randint(2011,2019)
            k = randint(2011,2019)
            PartStorage.objects.create(part=Part.objects.get(name='Test_View_Part_{}'.format(j)), storage=Storage.objects.get(name='Test_View_Storage_{}'.format(k)), count=12)
        self.baseurl = baseurl + 'partstorage/'

    def test_view_partstorage_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_partstorage')
        TestGenerics().get(self, self.baseurl, permission, 'inventory/admin/view.html')

    def test_view_partstorage_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_partstorage')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'inventory/admin/create.html')
        self.client.get('{}create/?part={}'.format(self.baseurl, Part.objects.last().id))
        self.client.get('{}create/?storage={}'.format(self.baseurl, Storage.objects.last().id))
        ps_list = PartStorage.objects.all()
        ids = []
        for ps in ps_list:
            ids.append(ps.part.id)
        p = Part.objects.exclude(id__in=ids)[0]
        data = {'part': p.id, 'storage': Storage.objects.last().id, 'count': '9'}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'count': 10}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_partstorage_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_partstorage')
        ps = PartStorage.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, ps.id), permission)

class InventoryPartAlternateSKUAdminViewTests(TestCase):
    def setUp(self):
        for i in range(2021,2030):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a TestCase test part', sku=str(i), price=10.00, cost=5.00)
            AlternateSKU.objects.create(manufacturer='Zephyrus Computing', sku=str(i))
        for i in range(105,110):
            j = randint(2021,2029)
            k = randint(2021,2029)
            PartAlternateSKU.objects.create(part=Part.objects.get(name='Test_View_Part_{}'.format(j)), alt_sku=AlternateSKU.objects.get(sku=str(k)))
        self.baseurl = baseurl + 'partalternatesku/'

    def test_view_partalternatesku_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_partalternatesku')
        TestGenerics().get(self, self.baseurl, permission, 'inventory/admin/view.html')

    def test_view_partalternatesku_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_partalternatesku')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'inventory/admin/create.html')
        self.client.get('{}create/?part={}'.format(self.baseurl, Part.objects.last().id))
        self.client.get('{}create/?alternatesku={}'.format(self.baseurl, AlternateSKU.objects.last().id))
        pas_list = PartAlternateSKU.objects.all()
        ids = []
        for pas in pas_list:
            ids.append(pas.part.id)
        p = Part.objects.exclude(id__in=ids)[0]
        data = {'part': p.id, 'alternatesku': AlternateSKU.objects.last().id}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'part': p.id}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_partalternatesku_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_partalternatesku')
        pas = PartAlternateSKU.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, pas.id), permission)

class InventoryAssemblyAdminViewTests(TestCase):
    def setUp(self):
        for i in range(2031,2040):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a TestCase test part', sku=str(i), price=10.00, cost=5.00)
        for i in range(2031,2035):
            Assembly.objects.create(parent=Part.objects.get(name='Test_View_Part_{}'.format(i)), part=Part.objects.get(name='Test_View_Part_{}'.format(i+5)), count=5)
        self.baseurl = baseurl + 'assembly/'

    def test_view_assembly_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_assembly')
        TestGenerics().get(self, self.baseurl, permission, 'inventory/admin/view.html')

    def test_view_assembly_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_assembly')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'inventory/admin/create.html')
        self.client.get('{}create/?part={}'.format(self.baseurl, Part.objects.last().id))
        self.client.get('{}create/?parent={}'.format(self.baseurl, Part.objects.last().id))
        data = {'part': Part.objects.get(name='Test_View_Part_2035').id, 'parent': Part.objects.last().id, 'count': 4}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'part': Part.objects.last().id}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_assembly_change(self):
        # Test the change view
        permission = Permission.objects.get(codename='change_assembly')
        a = Assembly.objects.last()
        TestGenerics().get(self, '{}change/{}/'.format(self.baseurl, a.id), permission, 'inventory/admin/update.html')
        data = {'part': a.part.id, 'parent': a.parent.id, 'count': 8}
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, a.id), data, permission)
        data = {'name': 'banana'}
        self.client.post('{}change/{}/'.format(self.baseurl, a.id), data)

    def test_view_assembly_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_assembly')
        a = Assembly.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, a.id), permission)

class InventoryStorageAdminViewTests(TestCase):
    def setUp(self):
        for i in range(2041,2050):
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a TestCase test storage')
        self.baseurl = baseurl + 'storage/'

    def test_view_storage_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_storage')
        TestGenerics().get(self, self.baseurl, permission, 'inventory/admin/view.html')

    def test_view_storage_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_storage')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'inventory/admin/create.html')
        data = {'name': 'Test_View_Storage_New', 'description': 'New Storage'}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'name': 'banana'}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_storage_change(self):
        # Test the change view
        permission = Permission.objects.get(codename='change_storage')
        s = Storage.objects.last()
        TestGenerics().get(self, '{}change/{}/'.format(self.baseurl, s.id), permission, 'inventory/admin/update.html')
        data = {'name': s.name, 'description': 'A new description'}
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, s.id), data, permission)
        data = {'count': 8}
        self.client.post('{}change/{}/'.format(self.baseurl, s.id), data)

    def test_view_storage_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_storage')
        s = Storage.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, s.id), permission)

class InventoryAlternateSKUAdminViewTests(TestCase):
    def setUp(self):
        for i in range(2051,2060):
            AlternateSKU.objects.create(manufacturer='Zephyrus Computing',sku=str(i))
        self.baseurl = baseurl + 'alternatesku/'

    def test_view_alternatesku_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_alternatesku')
        TestGenerics().get(self, self.baseurl, permission, 'inventory/admin/view.html')

    def test_view_alternatesku_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_alternatesku')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'inventory/admin/create.html')
        data = {'manufacturer': 'Zephyrus Computing', 'sku': 'New'}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'name': 'banana'}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_alternatesku_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_alternatesku')
        s = AlternateSKU.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, s.id), permission)

class InventoryKitViewTests(TestCase):
    def setUp(self):
        for i in range(2061,2070):
            Kit.objects.create(name='Test_View_Kit_{}'.format(i), description='This is a TestCase test kit', sku=str(i), price=10.00)
        self.baseurl = '{}kit/'.format(baseurl)

    def test_view_kit_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_kit')
        TestGenerics().get(self, self.baseurl, permission, 'inventory/admin/view.html')
        
    def test_view_kit_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_kit')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'inventory/admin/create.html')
        data = {'name': 'Test_View_Kit_New', 'description': 'New Test View Kit', 'sku': 'new', 'price': 9.99}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'name': 'banana'}
        self.client.post('{}create/'.format(self.baseurl), data)
        
    def test_view_kit_change(self):
        # Test the change view
        permission = Permission.objects.get(codename='change_kit')
        k = Kit.objects.last()
        TestGenerics().get(self, '{}change/{}/'.format(self.baseurl, k.id), permission, 'inventory/admin/update.html')
        data = {'name': 'Test_View_Kit_Modified', 'description': 'Modified description', 'sku': 'new', 'price': 5}
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, k.id), data, permission)
        data = {'name': 'Test_View_Kit_Modified', 'description': 'Does this work too?'}
        self.client.post('{}change/{}/'.format(self.baseurl, k.id), data)
        
    def test_view_kit_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_kit')
        k = Kit.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, k.id), permission)

class InventoryKitPartStorageViewTests(TestCase):
    def setUp(self):
        for i in range(2071,2080):
            Kit.objects.create(name='Test_View_Kit_{}'.format(i), description='This is a TestCase test kit', sku=str(i), price=10.00)
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a TestCase test part', sku=str(i), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a TestCase test storage')
            PartStorage.objects.create(part=Part.objects.last(),storage=Storage.objects.last(),count=5)
            KitPartStorage.objects.create(kit=Kit.objects.last(),partstorage=PartStorage.objects.last(),count=2)
        self.baseurl = '{}kitpartstorage/'.format(baseurl)
        
    def test_view_kitpartstorage_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_kitpartstorage')
        TestGenerics().get(self, self.baseurl, permission, 'inventory/admin/view.html')

    def test_view_kitpartstorage_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_kitpartstorage')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'inventory/admin/create.html')
        self.client.get('{}create/?kit={}'.format(self.baseurl, Kit.objects.last().id))
        self.client.get('{}create/?partstorage={}'.format(self.baseurl, PartStorage.objects.last().id))
        data = {'kit': Kit.objects.first().id, 'partstorage': PartStorage.objects.last().id, 'count': 4}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'name': 'banana'}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_kitpartstorage_change(self):
        # Test the change view
        permission = Permission.objects.get(codename='change_kitpartstorage')
        kps = KitPartStorage.objects.last()
        TestGenerics().get(self, '{}change/{}/'.format(self.baseurl, kps.id), permission, 'inventory/admin/update.html')
        data = {'kit': kps.kit.id, 'partstorage': kps.partstorage.id, 'count': 8}
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, kps.id), data, permission)
        data = {'name': 'banana'}
        self.client.post('{}change/{}/'.format(self.baseurl, kps.id), data)
        
    def test_view_kitpartstorage_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_kitpartstorage')
        kps = KitPartStorage.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, kps.id), permission)

class TestGenerics():
    def get(self, case, url, perm, temp):
        response = case.client.get(url)
        case.assertRedirects(response, '/accounts/login/?next={}'.format(url))
        user = User.objects.get_or_create(username='testuser')[0]
        user.user_permissions.add(perm)
        case.client.force_login(user)
        response = case.client.get(url)
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        case.assertTemplateUsed(response, temp)

    def post(self, case, url, data, perm):
        user = User.objects.get_or_create(username='testuser')[0]
        user.user_permissions.add(perm)
        case.client.force_login(user)
        response = case.client.post(url, data)
        case.assertNotEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        case.assertNotEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        case.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        case.assertNotEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        case.assertNotEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)

    def delete(self, case, url, perm):
        user = User.objects.get_or_create(username='testuser')[0]
        user.user_permissions.add(perm)
        case.client.force_login(user)
        response = case.client.get(url)
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        case.assertTemplateUsed(response, 'inventory/ajax_result.html')
        case.assertContains(response, 'green')
