import django
django.setup()
from django.test import TestCase
from django.urls import reverse
from random import randint
from inventory.models import Part, Storage, AlternateSKU, PartAlternateSKU, PartStorage, Assembly, Kit, KitPartStorage, get_sentinel_part, get_sentinel_sku, get_sentinel_storage, get_sentinel_kit, get_sentinel_partstorage, IsInt

# Unit tests for Part
class PartUnitTests(TestCase):
    def setUp(self):
        for i in range(1,101):
            Part.objects.create(name='test_part_' + str(i), description='This is a TestCase test part', sku=str(i).zfill(4), price=9.99, cost=3.14)

    def test_part_str(self):
        i = randint(1,100)
        p = Part.objects.get(name='test_part_' + str(i))
        self.assertEqual(p.__str__(), 'test_part_' + str(i))

    def test_part_abosolute_url(self):
        i = randint(1,100)
        p = Part.objects.get(name='test_part_' + str(i))
        self.assertEqual(p.get_absolute_url(), reverse('inventory:part_detail', args=[p.id]))

    def test_part_alt_sku(self):
        i = randint(1,100)
        p = Part.objects.get(name='test_part_' + str(i))
        self.assertEqual(p.has_alt_sku(), False)

    def test_part_storage(self):
        i = randint(1,100)
        p = Part.objects.get(name='test_part_' + str(i))
        self.assertEqual(p.has_storage(), False)

    def test_part_total(self):
        i = randint(1,100)
        p = Part.objects.get(name='test_part_' + str(i))
        self.assertEqual(p.get_total(), 0)

# Unit tests for Storage
class StorageUnitTests(TestCase):
    def setUp(self):
        for i in range(1,13):
            if i == 1:
                Storage.objects.create(name='test_storage_{}'.format(i), description='This is a TestCast test storage')
            else:
                Storage.objects.create(name='test_storage_{}'.format(i), description='This is a TestCast test storage', parent=Storage.objects.first())

    def test_storage_str(self):
        i = randint(1,12)
        s = Storage.objects.get(name='test_storage_' + str(i))
        self.assertEqual(s.__str__(), 'test_storage_' + str(i))

    def test_storage_abosolute_url(self):
        i = randint(1,12)
        s = Storage.objects.get(name='test_storage_' + str(i))
        self.assertEqual(s.get_absolute_url(), reverse('inventory:storage_detail', args=[s.id]))

    def test_storage_parts(self):
        i = randint(1,12)
        s = Storage.objects.get(name='test_storage_' + str(i))
        self.assertEqual(s.has_parts(), False)

    def test_has_children(self):
        s = Storage.objects.first()
        self.assertTrue(s.has_children())

# Unit tests for AlternateSKU
class AlternateSkuUnitTests(TestCase):
    def setUp(self):
        for i in range(1010,1061):
            AlternateSKU.objects.create(sku=str(i).zfill(5), manufacturer='zephyruscomputing')

    def test_alternatesku_str(self):
        i = randint(1010,1060)
        a = AlternateSKU.objects.get(sku=str(i).zfill(5))
        self.assertEqual(a.__str__(), 'zephyruscomputing-' + str(i).zfill(5))

# Unit tests for PartAlternateSKU
class PartAlternateSkuUnitTests(TestCase):
    def setUp(self):
        for i in range(201,301):
            Part.objects.create(name='test_part_' + str(i), description='This is a TestCase test part', sku=str(i).zfill(4), price=9.99, cost=3.14)
        for i in range(1061,1071):
            AlternateSKU.objects.create(sku=str(i).zfill(5), manufacturer='zephyruscomputing')
        for i in range(1,11):
            j = randint(201,300)
            k = randint(1061,1070)
            PartAlternateSKU.objects.create(part=Part.objects.get(name='test_part_' + str(j)), alt_sku=AlternateSKU.objects.get(sku=str(k).zfill(5)))

    def test_partalternatesku_str(self):
        i = randint(0,9)
        pa = PartAlternateSKU.objects.all()[i]
        p = pa.part
        a = pa.alt_sku
        self.assertEqual(pa.__str__(), p.name + ' AKA ' + a.manufacturer + '-' + a.sku)

# Unit tests for PartStorage
class PartStorageUnitTests(TestCase):
    def setUp(self):
        for i in range(301,401):
            Part.objects.create(name='test_part_' + str(i), description='This is a TestCase test part', sku=str(i).zfill(4), price=9.99, cost=3.14)
        for i in range(13,21):
            Storage.objects.create(name='test_storage_' + str(i), description='This is a TestCase test storage')
        for i in range(1,51):
            j = randint(301,400)
            k = randint(13,20)
            PartStorage.objects.create(part=Part.objects.get(name='test_part_' + str(j)), storage=Storage.objects.get(name='test_storage_' + str(k)), count=k)

    def test_partstorage_str(self):
        i = randint(0,49)
        ps = PartStorage.objects.all()[i]
        p = ps.part
        s = ps.storage
        self.assertEqual(ps.__str__(), s.name + '-' + p.name)

    def test_partstorage_add(self):
        i = randint(0,49)
        ps = PartStorage.objects.all()[i]
        k = ps.count
        ps.add(12)
        self.assertEqual(ps.count, k + 12)

    def test_partstorage_subtract(self):
        i = randint(0,49)
        ps = PartStorage.objects.all()[i]
        k = ps.count - 1
        ps.subtract(k)
        self.assertEqual(ps.count, 1)
        try:
            ps.subtract(5)
            self.Fail()
        except ValueError:
            self.assertEqual(ps.count, 1)


# Unit tests for Assembly
class AssemblyUnitTests(TestCase):
    def setUp(self):
        for i in range(601,701):
            Part.objects.create(name='test_part_' + str(i), description='This is a TestCase test part', sku=str(i).zfill(4), price=9.99, cost=3.14)
        for i in range(701,801):
            Part.objects.create(name='test_part_' + str(i), description='This is a TestCase test part', sku=str(i).zfill(4), price=9.99, cost=3.14)
        p = Part.objects.first()
        for i in range(1,11):
            j = randint(602,700)
            Assembly.objects.create(part=Part.objects.get(name='test_part_' + str(j)), parent=p, count=1)

    def test_assembly_str(self):
        i = randint(0,9)
        a = Assembly.objects.all()[i]
        p1 = a.part
        p2 = a.parent
        self.assertEqual(a.__str__(), p2.name + '-' + p1.name)

    def test_assembly_absolute_url(self):
        i = randint(0,9)
        a = Assembly.objects.all()[i]
        self.assertEqual(a.get_absolute_url(), reverse('inventory:assembly_detail', args=[a.parent.id]))

class KitUnitTests(TestCase):
    def setUp(self):
        for i in range(801,901):
            Kit.objects.create(name='test_kit_{}'.format(i), description='This is a TestCase test kit', sku=str(i).zfill(4), price=9.99)

    def test_kit_str(self):
        i = randint(801,900)
        k = Kit.objects.get(name='test_kit_{}'.format(i))
        self.assertEqual(k.__str__(), 'test_kit_{}'.format(i))

    def test_kit_absolute_url(self):
        i = randint(801,900)
        k = Kit.objects.get(name='test_kit_{}'.format(i))
        self.assertEqual(k.get_absolute_url(), reverse('inventory:kit_detail', args=[k.id]))

    def test_kit_components(self):
        i = randint(801,900)
        k = Kit.objects.get(name='test_kit_{}'.format(i))
        self.assertFalse(k.has_components())

    def test_kit_available_count(self):
        i = randint(801,900)
        k = Kit.objects.get(name='test_kit_{}'.format(i))
        self.assertEqual(k.get_available_count(), 0)

    def test_kit_cost(self):
        i = randint(801,900)
        k = Kit.objects.get(name='test_kit_{}'.format(i))
        self.assertEqual(str(k.get_cost()), '0.00')

# Unit tests for KitPartStorage
class KitPartStorageUnitTests(TestCase):
    def setUp(self):
        for i in range(901,1001):
            Kit.objects.create(name='test_kit_{}'.format(i), description='This is a TestCase test kit', sku=str(i).zfill(4), price=9.99)
            Part.objects.create(name='test_part_{}'.format(i), description='This is a TestCase test part', sku=str(i).zfill(4), price=9.99, cost=3.14)
            Storage.objects.create(name='test_storage_{}'.format(i), description='This is a TestCase test storage')
        for i in range(0,16):
            j = randint(901,1000)
            k = randint(901,1000)
            l = randint(901,1000)
            PartStorage.objects.create(part=Part.objects.get(name='test_part_{}'.format(j)), storage=Storage.objects.get(name='test_storage_{}'.format(k)), count=5)
            KitPartStorage.objects.create(kit=Kit.objects.get(name='test_kit_{}'.format(l)), partstorage=PartStorage.objects.last(), count=2)

    def test_kitpartstorage_str(self):
        i = randint(0,15)
        kps = KitPartStorage.objects.all()[i]
        k = kps.kit
        ps = kps.partstorage
        self.assertEqual(kps.__str__(), '{}-{}'.format(k,ps))

# Unit tests for model methods
class ModelMethodsUnitTest(TestCase):
    def test_sentinel_part(self):
        get_sentinel_part()
        part = get_sentinel_part()
        self.assertEqual(part.name, 'deleted')
        self.assertEqual(part.description, 'deleted')
        self.assertEqual(part.sku, 'deleted')
        self.assertEqual(part, Part.objects.get(name='deleted'))

    def test_sentinel_sku(self):
        get_sentinel_sku()
        sku = get_sentinel_sku()
        self.assertEqual(sku.sku, 'deleted')
        self.assertEqual(sku.manufacturer, 'deleted')
        self.assertEqual(sku, AlternateSKU.objects.get(sku='deleted'))

    def test_sentinel_storage(self):
        get_sentinel_storage()
        storage = get_sentinel_storage()
        self.assertEqual(storage.name, 'deleted')
        self.assertEqual(storage.description, 'deleted')
        self.assertEqual(storage, Storage.objects.get(name='deleted'))

    def test_sentinel_partstorage(self):
        get_sentinel_partstorage()
        partstorage = get_sentinel_partstorage()
        self.assertEqual(partstorage.part.name, 'deleted')
        self.assertEqual(partstorage.part.description, 'deleted')
        self.assertEqual(partstorage.part.sku, 'deleted')
        self.assertEqual(partstorage.storage.name, 'deleted')
        self.assertEqual(partstorage.storage.description, 'deleted')
        self.assertEqual(partstorage.count, 0)
        self.assertEqual(partstorage, PartStorage.objects.get(part=Part.objects.get(name='deleted'), storage=Storage.objects.get(name='deleted')))

    def test_sentinel_kit(self):
        get_sentinel_kit()
        kit = get_sentinel_kit()
        self.assertEqual(kit.name, 'deleted')
        self.assertEqual(kit.description, 'deleted')
        self.assertEqual(kit.sku, 'deleted')
        self.assertEqual(kit, Kit.objects.get(name='deleted'))

    def test_IsInt(self):
        self.assertTrue(IsInt(0))
        self.assertTrue(IsInt(-3))
        self.assertTrue(IsInt(15))
        self.assertFalse(IsInt('banana'))
        self.assertTrue(IsInt('23'))
        self.assertFalse(IsInt('*'))

# Integration Test for Storage
class StorageIntegrationTests(TestCase):
    def setUp(self):
        for i in range(401,501):
            Part.objects.create(name='test_part_' + str(i), description='This is a TestCase test part', sku=str(i).zfill(4), price=9.99, cost=3.14)
        for i in range(21,26):
            Storage.objects.create(name='test_storage_' + str(i), description='This is a TestCase test storage')
        Storage.objects.create(name='test_storage_0', description='This is a TestCase test storage')
        for i in range(0,100):
            j = randint(401,500)
            k = randint(21,25)
            l = randint(0,101)
            PartStorage.objects.create(part=Part.objects.get(name='test_part_' + str(j)), storage=Storage.objects.get(name='test_storage_' + str(k)), count=l)

    def test_get_parts(self):
        for storage in Storage.objects.all():
            if not storage.has_parts():
                continue
            parts = storage.get_parts()
            self.assertTrue(len(parts) > 0)
            self.assertNotEqual(parts, [])

    def test_get_parts_storage(self):
        for storage in Storage.objects.all():
            if not storage.has_parts():
                continue
            ps = storage.get_parts_storage()
            self.assertTrue(len(ps) > 0)
            self.assertNotEqual(ps, [])

# Integration Tests for AlternateSKU
class AlternateSKUIntegrationTests(TestCase):
    def setUp(self):
        for i in range(501,601):
            Part.objects.create(name='test_part_' + str(i), description='This is a TestCase test part', sku=str(i).zfill(4), price=9.99, cost=3.14)
        for i in range(0,11):
            AlternateSKU.objects.create(manufacturer='ZephyrusComputing', sku='1' + str(i).zfill(3))
        for i in range(0,11):
            j = randint(501,600)
            k = randint(0,10)
            PartAlternateSKU.objects.create(part=Part.objects.get(name='test_part_' + str(j)), alt_sku=AlternateSKU.objects.get(sku='1' + str(k).zfill(3)))
        AlternateSKU.objects.create(manufacturer='ZephyrusComputing', sku='0000')

    def test_get_parts(self):
        for alt in AlternateSKU.objects.all():
            if not alt.has_parts():
                continue
            pa = alt.get_parts()
            self.assertTrue(len(pa) > 0)
            self.assertNotEqual(pa, [])

# Integration Tests for Part
class PartIntegrationTests(TestCase):
    def setup(self):
        for i in range(601,701):
            Part.objects.create(name='test_part_{}'.format(i), description='This is a TestCase test part', sku=str(i).zfill(4), price=9.99, cost=3.14)
            Storage.objects.create(name='test_storage_{}'.format(i), description='This is a TestCase test storage')
            Kit.objects.create(name='test_kit_{}'.format(i), description='This is a TestCase test kit', sku=str(i).zfill(4), price=9.99)
        for i in range(0,11):
            j = randint(601,700)
            k = randint(601,700)
            p = Part.objects.get(name='test_part_{}'.format(j))
            s = Storage.objects.get(name='test_storage_{}'.format(k))
            ps = PartStorage.objects.create(part=p, storage=s, count=5)
            l = randint(1,7)
            for m in range(0,l):
                n = randint(601,700)
                Assembly.objects.create(parent=p, part=Part.objects.get(name='test_part_{}'.format(n)), count=1)
                KitPartStorage.objects.create(kit=Kit.objects.get(name='test_kit_{}'.format(n)), partstorage=ps, count=2)

    def test_get_parent_assemblies(self):
        for part in Part.objects.all():
            if not part.has_parent_assembiles():
                continue
            parents = part.get_parent_assemblies()
            self.assertTrue(len(parents) > 0)
            self.assertNotEqual(parents, [])

    def test_get_parent_kits(self):
        for part in Part.objects.all():
            if not part.has_parent_kits():
                continue
            parents = part.get_parent_kits()
            self.assertTrue(len(parents) > 0)
            self.assertNotEqual(parents, [])

    def test_get_components(self):
        for part in Part.objects.all():
            if not part.has_components():
                continue
            components = part.get_components()
            self.assertTrue(len(components) > 0)
            self.assertNotEqual(components, [])
