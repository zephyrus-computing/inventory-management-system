import django
django.setup()
from django.conf import settings
from django.test import TestCase, override_settings
from os import path, remove
from shutil import copy2
import environ
import re

class ImsUrlsTests(TestCase):
    def test_base_urls(self):
        from ims.urls import urlpatterns
        length = len(urlpatterns)
        self.assertGreaterEqual(length, 13)
        expected = ['mfa/','accounts/','accounts/admin/','accounts/password_change/','accounts/password_change/done/',
                    'accounts/password_reset/','accounts/password_reset/done/','accounts/reset/<uidb64>/<token>/',
                    'accounts/reset/done/','admin/login/','admin/','inventory/admin/','inventory/',
                    'logs/','api/v1/inventory/','api/v1/log/','api/v1/accounts/','api-auth/','static/','^$',
                    'tenant/','tenant/admin/','tenant/api','tracker/','tracker/admin/','tracker/api/','scanner/',
                    'scanner/admin/','scanner/api/','asset/','asset/admin/','asset/api/']
        for line in urlpatterns:
            if line.__class__.__name__ == 'URLPattern':
                pattern = line.pattern.describe().split('[')[0].replace("'","").strip()
                self.assertIn(pattern, expected)
            if line.__class__.__name__ == 'URLResolver':
                pattern = line.pattern.describe().replace("'","").strip()
                self.assertIn(pattern, expected)

    def test_addon_logic(self):
        if path.exists('{}'.format(path.join(settings.BASE_DIR,'ims/addons/scanner/'))):
            tracker = [{'name': 'Scanner', 'version': '1.0.1', 'build': '230411', 'enabled': True, 'url': 'scanner/'}]
            with override_settings(INSTALLED_ADDONS = tracker):
                urlpatterns = _addon_url_logic('scanner')
                self.assertEqual(urlpatterns[0]['pattern'].replace("'","").strip(), 'scanner/')
        if path.exists('{}'.format(path.join(settings.BASE_DIR,'ims/addons/tenant/'))):
            tenant = [{'name': 'Tenant', 'version': '1.0', 'build': '230403', 'enabled': True, 'admin_url': 'tenant/admin/', 'api_url': 'api/v1/tenant/', 'other_urls': [{'path': 'tenant/', 'include': 'ims.addons.tenant.urls'}]}]
            with override_settings(INSTALLED_ADDONS = tenant):
                urlpatterns = _addon_url_logic('tenant')
                self.assertEqual(urlpatterns[2]['pattern'].replace("'","").strip(), 'tenant/')
                self.assertEqual(urlpatterns[1]['pattern'].replace("'","").strip(), 'tenant/admin/')
                self.assertEqual(urlpatterns[0]['pattern'].replace("'","").strip(), 'api/v1/tenant/')

class ImsSettingsTests(TestCase):
    def test_installed_apps_append(self):
        for app in ['ims.addons.tracker',]:
            settings.INSTALLED_APPS.append(app)
        self.assertTrue('ims.addons.tracker' in str(settings.INSTALLED_APPS))
    
    def test_third_party_libraries_append(self):
        for lib in ['vendor=zephyruscomputing;package=ims.js;version=3.3',]:
            settings.THIRD_PARTY_LIBRARIES.append({k:v for k,v in map(lambda l: l.split('='), lib.split(';'))})
        self.assertTrue('zephyruscomputing' in str(settings.THIRD_PARTY_LIBRARIES))

    def test_installed_addons_append(self):
        for addon in ['name=Tracker;version=1.1-dev;build=230714;enabled=True;url=tracker/;admin_url=tracker/admin/;api_url=api/v1/tracker/',]:
            settings.INSTALLED_ADDONS.append({k:v for k,v in map(lambda a: a.split('='), addon.split(';'))})
        self.assertTrue('Tracker' in str(settings.INSTALLED_ADDONS))


def _addon_url_logic(addon):
    urlpatterns = []
    for line in settings.INSTALLED_ADDONS:
        if line['name'].lower() == addon:
            if line['enabled']:
                if 'api_url' in line:
                    urlpatterns.append({'pattern':line['api_url']})
                if 'admin_url' in line:
                    urlpatterns.append({'pattern':line['admin_url']})
                if 'url' in line:
                    urlpatterns.append({'pattern':line['url']})
                if 'other_urls' in line:
                    for url in line['other_urls']:
                        urlpatterns.append({'pattern':url['path']})
    return urlpatterns