import django
django.setup()
from django.test import TestCase
from random import randint
from inventory.models import Part, Storage, AlternateSKU, PartAlternateSKU, PartStorage, Assembly, Kit, KitPartStorage
from inventory.admin.forms import PartForm, StorageForm, AlternateSKUForm, PartAlternateSKUForm, PartStorageForm, AssemblyForm, KitForm, KitPartStorageForm
import inventory.helpers as helpers

# Unit test for lookup methods
class LookupMethodsUnitTests(TestCase):
    def setUp(self):
        for i in range(1,10):
            AlternateSKU.objects.create(sku='test_sku_{}'.format(str(i).zfill(3)), manufacturer='Zephyrus Computing')
            Part.objects.create(name='test_part_{}'.format(str(i).zfill(3)), description='TestCase test part', sku=str(i).zfill(3), price=1.0, cost=0.0)
            PartAlternateSKU.objects.create(part=Part.objects.last(), alt_sku=AlternateSKU.objects.last())
            Storage.objects.create(name='test_storage_{}'.format(str(i).zfill(3)), description='TestCase test storage')
            PartStorage.objects.create(part=Part.objects.last(), storage=Storage.objects.last(), count=4)
            Kit.objects.create(name='test_kit_{}'.format(str(i).zfill(3)), description='TestCase test kit', sku=str(i).zfill(3), price=1.0)
            KitPartStorage.objects.create(kit=Kit.objects.last(), partstorage=PartStorage.objects.last(), count=1)
        for i in range(1,4):
            j = randint(1,len(Part.objects.all()) -1)
            Assembly.objects.create(parent=Part.objects.first(),part=Part.objects.all()[j], count=2)

    def test_get_admin_permission(self):
        actions = ['view','add','change','delete']
        models = ['AlternateSKU','Assembly','Kit','KitPartStorage','Part','PartAlternateSKU','PartStorage','Storage']
        for model in models:
            for action in actions:
                result = helpers.get_admin_permission('inventory',model, action)
                self.assertNotEqual(result, 'inventory.{}_{}'.format(action, model))
                self.assertEqual(result, 'inventory.{}_{}'.format(action, model.lower()))

    def test_get_object(self):
        expected = AlternateSKU.objects.last()
        result = helpers.get_object('AlternateSKU', expected.id)
        self.assertEqual(expected, result)
        expected = Assembly.objects.last()
        result = helpers.get_object('Assembly', expected.id)
        self.assertEqual(expected, result)
        expected = Part.objects.last()
        result = helpers.get_object('Part', expected.id)
        self.assertEqual(expected, result)
        expected = Storage.objects.last()
        result = helpers.get_object('Storage', expected.id)
        self.assertEqual(expected, result)
        expected = PartStorage.objects.last()
        result = helpers.get_object('PartStorage', expected.id)
        self.assertEqual(expected, result)
        expected = PartAlternateSKU.objects.last()
        result = helpers.get_object('PartAlternateSKU', expected.id)
        self.assertEqual(expected, result)
        expected = Kit.objects.last()
        result = helpers.get_object('Kit', expected.id)
        self.assertEqual(expected, result)
        expected = KitPartStorage.objects.last()
        result = helpers.get_object('KitPartStorage', expected.id)
        self.assertEqual(expected, result)
        result = helpers.get_object('Cat', expected.id)
        self.assertEqual('', result)
        with self.assertRaises(AlternateSKU.DoesNotExist):
            helpers.get_object('AlternateSKU', 0)
        with self.assertRaises(Assembly.DoesNotExist):
            helpers.get_object('Assembly', 0)
        with self.assertRaises(Part.DoesNotExist):
            helpers.get_object('Part', 0)
        with self.assertRaises(Storage.DoesNotExist):
            helpers.get_object('Storage', 0)
        with self.assertRaises(PartStorage.DoesNotExist):
            helpers.get_object('PartStorage', 0)
        with self.assertRaises(PartAlternateSKU.DoesNotExist):
            helpers.get_object('PartAlternateSKU', 0)
        with self.assertRaises(Kit.DoesNotExist):
            helpers.get_object('Kit', 0)
        with self.assertRaises(KitPartStorage.DoesNotExist):
            helpers.get_object('KitPartStorage', 0)

    def test_get_object_list(self):
        expected = AlternateSKU.objects.last()
        result = helpers.get_object_list('AlternateSKU')
        self.assertIn(expected, result)
        expected = Assembly.objects.last()
        result = helpers.get_object_list('Assembly')
        self.assertIn(expected, result)
        expected = Part.objects.last()
        result = helpers.get_object_list('Part')
        self.assertIn(expected, result)
        expected = Storage.objects.last()
        result = helpers.get_object_list('Storage')
        self.assertIn(expected, result)
        expected = PartStorage.objects.last()
        result = helpers.get_object_list('PartStorage')
        self.assertIn(expected, result)
        expected = PartAlternateSKU.objects.last()
        result = helpers.get_object_list('PartAlternateSKU')
        self.assertIn(expected, result)
        expected = Kit.objects.last()
        result = helpers.get_object_list('Kit')
        self.assertIn(expected, result)
        expected = KitPartStorage.objects.last()
        result = helpers.get_object_list('KitPartStorage')
        self.assertIn(expected, result)
        result = helpers.get_object_list('Cat')
        self.assertEqual('', result)

    def test_get_search_fields(self):
        for model in ['AlternateSKU','Assembly','Kit','KitPartStorage','Part','PartAlternateSKU','PartStorage','Storage','Group','User','Cat']:
            result = helpers.get_search_fields(model)
            if 'AlternateSKU' in model:
                self.assertTrue('sku' or 'alt_sku__sku' in result)
                self.assertTrue('manufacturer' or 'alt_sku__manufacturer' in result)
            if 'Part' in model:
                self.assertTrue('sku' or 'part__sku' in result)
                self.assertTrue('name' or 'part__name' in result)
                self.assertTrue('description' or 'part__description' in result)
            if 'Storage' in model:
                self.assertTrue('name' or 'storage__name' or 'partstorage__storage__name' in result)
                self.assertTrue('description' or 'storage__description' or 'partstorage__storage__description' in result)
            if 'Kit' in model:
                self.assertTrue('sku' or 'kit__sku' in result)
                self.assertTrue('name' or 'kit__name' in result)
                self.assertTrue('description' or 'kit__description' in result)
            if model == 'Assembly':
                self.assertTrue('parent__name' in result)
                self.assertTrue('parent__sku' in result)
                self.assertTrue('parent__description' in result)
            if model == 'KitPartStorage':
                self.assertTrue('partstorage__part__name' in result)
                self.assertTrue('partstorage__part__sku' in result)
                self.assertTrue('partstorage__part__description' in result)
            if model == 'Cat':
                self.assertEqual(result, [])

    def test_get_admin_form_p(self):
        model = AlternateSKU.objects.first()
        result = helpers.get_admin_form_p('AlternateSKU',instance=model)
        self.assertIsInstance(result, AlternateSKUForm)
        self.assertEqual(result.instance, model)
        result = helpers.get_admin_form_p('Assembly')
        self.assertIsInstance(result, AssemblyForm)
        result = helpers.get_admin_form_p('Part')
        self.assertIsInstance(result, PartForm)
        result = helpers.get_admin_form_p('Storage')
        self.assertIsInstance(result, StorageForm)
        data = {'part':Part.objects.first().id}
        result = helpers.get_admin_form_p('PartStorage',data=data)
        self.assertIsInstance(result, PartStorageForm)
        self.assertEqual(result.data, data)
        result = helpers.get_admin_form_p('PartAlternateSKU')
        self.assertIsInstance(result, PartAlternateSKUForm)
        result = helpers.get_admin_form_p('Kit')
        self.assertIsInstance(result, KitForm)
        result = helpers.get_admin_form_p('KitPartStorage')
        self.assertIsInstance(result, KitPartStorageForm)
        with self.assertRaises(Exception):
            result = helpers.get_admin_form_p('Cat')

    def test_get_admin_form_g(self):
        model = AlternateSKU.objects.first()
        result = helpers.get_admin_form_g('AlternateSKU',instance=model)
        self.assertIsInstance(result, AlternateSKUForm)
        self.assertEqual(result.instance, model)
        result = helpers.get_admin_form_g('Assembly')
        self.assertIsInstance(result, AssemblyForm)
        result = helpers.get_admin_form_g('Part')
        self.assertIsInstance(result, PartForm)
        result = helpers.get_admin_form_g('Storage')
        self.assertIsInstance(result, StorageForm)
        data = {'part':Part.objects.first().id}
        result = helpers.get_admin_form_g('PartStorage',GET=data)
        self.assertIsInstance(result, PartStorageForm)
        self.assertEqual(result.initial, data)
        result = helpers.get_admin_form_g('PartAlternateSKU')
        self.assertIsInstance(result, PartAlternateSKUForm)
        result = helpers.get_admin_form_g('Kit')
        self.assertIsInstance(result, KitForm)
        result = helpers.get_admin_form_g('KitPartStorage')
        self.assertIsInstance(result, KitPartStorageForm)
        with self.assertRaises(Exception):
            result = helpers.get_admin_form_g('Cat')

    def test_is_int(self):
        for i in range(0,10):
            j = randint(0,9999)
            self.assertTrue(helpers.is_int(j))
        for i in 'these','are','not','ints',None:
            self.assertFalse(helpers.is_int(i))
        for i in 1.0,3.14,'-1':
            self.assertTrue(helpers.is_int(i))
        for model in Part, PartForm:
            self.assertFalse(helpers.is_int(model))

    def test_parse_querytype(self):
        result = helpers.parse_querytype(1)
        self.assertEqual(result, '^')
        result = helpers.parse_querytype('1')
        self.assertEqual(result, '^')
        result = helpers.parse_querytype(2)
        self.assertEqual(result, '=')
        result = helpers.parse_querytype('2')
        self.assertEqual(result, '=')
        result = helpers.parse_querytype(3)
        self.assertEqual(result, '')
        result = helpers.parse_querytype('3')
        self.assertEqual(result, '')
        result = helpers.parse_querytype('banana')
        self.assertEqual(result, '')
        result = helpers.parse_querytype([])
        self.assertEqual(result, '')
        result = helpers.parse_querytype(None)
        self.assertEqual(result, '')

    def test_lookup_dictionary(self):
        dic = [('Banana', 1),('Orange', 2),('Strawberry', 3)]
        self.assertEqual(helpers.lookup_dictionary(dic,'Banana'), 1)
        self.assertEqual(helpers.lookup_dictionary(dic,'Orange'), 2)
        self.assertEqual(helpers.lookup_dictionary(dic,'Strawberry'), 3)
        self.assertEqual(helpers.lookup_dictionary(dic,'Loganberry'), 'unknown')

    def test_rlookup_dictionary(self):
        dic = [('Banana', 1),('Orange', 2),('Strawberry', 3)]
        self.assertEqual(helpers.rlookup_dictionary(dic,1), 'Banana')
        self.assertEqual(helpers.rlookup_dictionary(dic,2), 'Orange')
        self.assertEqual(helpers.rlookup_dictionary(dic,3), 'Strawberry')
        self.assertEqual(helpers.rlookup_dictionary(dic,4), '')