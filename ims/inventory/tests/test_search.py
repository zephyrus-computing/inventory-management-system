import django
django.setup()
from django.test import TestCase
from django.core.exceptions import FieldError
from inventory.search import Search
from inventory.models import Part, Storage, AlternateSKU

# Unit tests for Search
class SearchUnitTests(TestCase):
    def setUp(self):
        for i in range(0,5):
            Part.objects.create(name='SearchPart' + str(i), description='This is a TestCase test part', sku=str(i).zfill(3), price=10.00, cost=5.00)
        for i in range(0,2):
            Storage.objects.create(name='SearchStorage' + str(i), description='This is a TestCase test storage')
        AlternateSKU.objects.create(sku='00123', manufacturer='test_manufacturer')

    def test_search_caret(self):
        sf = Search().filter(['^name', '^sku'], '003')
        self.assertEqual(str(sf), "(OR: ('name__istartswith', '003'), ('sku__istartswith', '003'))")
        found = Part.objects.filter(sf)
        self.assertEqual(found.first(), Part.objects.get(name='SearchPart3'))
        sf = Search().filter(['^sku', '^manufacturer'], 'test_')
        self.assertEqual(str(sf), "(OR: ('sku__istartswith', 'test_'), ('manufacturer__istartswith', 'test_'))")
        found = AlternateSKU.objects.filter(sf)
        self.assertEqual(found.first(), AlternateSKU.objects.get(sku='00123'))

    def test_search_at(self):
        sf = Search().filter(['@name', '@description', '@sku'], '3')
        self.assertEqual(str(sf), "(OR: ('name__search', '3'), ('description__search', '3'), ('sku__search', '3'))")
        try:
            found = Part.objects.filter(sf)
            self.assertFail()
        except FieldError:
            pass

    def test_search_equal(self):
        sf = Search().filter(['=sku', '=manufacturer'], '00123')
        self.assertEqual(str(sf), "(OR: ('sku__iexact', '00123'), ('manufacturer__iexact', '00123'))")
        found = AlternateSKU.objects.filter(sf)
        self.assertEqual(found.first(), AlternateSKU.objects.get(sku='00123'))
        sf = Search().filter(['=name', '=description'], 'SearchStorage1')
        self.assertEqual(str(sf), "(OR: ('name__iexact', 'SearchStorage1'), ('description__iexact', 'SearchStorage1'))")
        found = Storage.objects.filter(sf)
        self.assertEqual(found.first(), Storage.objects.get(name='SearchStorage1'))

    def test_search_contains(self):
        sf = Search().filter(['sku', 'manufacturer'], '_manufacturer')
        self.assertEqual(str(sf), "(OR: ('sku__icontains', '_manufacturer'), ('manufacturer__icontains', '_manufacturer'))")
        found = AlternateSKU.objects.filter(sf)
        self.assertEqual(found.first(), AlternateSKU.objects.get(manufacturer='test_manufacturer'))


