import django
django.setup()
from django.contrib.auth.models import User, Permission
from django.test import TestCase, override_settings
from random import randint,choice
from rest_framework import status
from rest_framework.authtoken.models import Token
from inventory.models import Part, Storage, AlternateSKU, PartAlternateSKU, PartStorage, Assembly, Kit, KitPartStorage
from log.models import Log

baseurl = '/inventory/'

@override_settings(ALLOW_ANONYMOUS=True)
class ImsViewTests(TestCase):
    def test_unit_root_view(self):
        response = self.client.get('/')
        self.assertRedirects(response, baseurl, status_code=301)

@override_settings(ALLOW_ANONYMOUS=True)
class InventoryPartViewTests(TestCase):
    def setUp(self):
        for i in range(0,101):
            Part.objects.create(name='Test_View_Part_' + str(i), description='This is a view TestCase test part', sku=str(i*10).zfill(5), price=10.00, cost=5.00)
        self.baseurl = '{}part/'.format(baseurl)

    def test_view_parts(self):
        # Test the default view
        response = self.client.get('/inventory/part/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #self.assertTemplateUsed(response, 'inventory/part/list.html')
        # Test the pagination of the view
        response = self.client.get('/inventory/part/?page=-1')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=-1')
        response = self.client.get('/inventory/part/?page=100')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=100')
        response = self.client.get('/inventory/part/?q=test')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'q=test')
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get('/inventory/part/')
            self.assertRedirects(response, '/accounts/login/?next=/inventory/part/')
            user = User.objects.get_or_create(username='testuser')[0]
            user.user_permissions.add(Permission.objects.get(name='Can view part'))
            self.client.force_login(user)
            response = self.client.get('/inventory/part/')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(True)

    def test_view_part_details(self):
        # Test a valid part
        i = randint(0,100)
        p = Part.objects.all()[i]
        response = self.client.get('{}{}/'.format(self.baseurl, p.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #self.assertTemplateUsed(response, 'inventory/part/detail.html')
        # Test an invalid part
        response = self.client.get('{}idonotexist/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get('{}{}/'.format(self.baseurl, p.id))
            self.assertRedirects(response, '/accounts/login/?next={}{}/'.format(self.baseurl, p.id))
            user = User.objects.get_or_create(username='testuser')[0]
            user.user_permissions.add(Permission.objects.get(name='Can view part'))
            self.client.force_login(user)
            response = self.client.get('{}{}/'.format(self.baseurl, p.id))
            self.assertEqual(response.status_code, status.HTTP_200_OK)

@override_settings(ALLOW_ANONYMOUS=True)
class InventoryStorageViewTests(TestCase):
    def setUp(self):
        for i in range(0,22):
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a view TestCase test storage')

    def test_view_storages(self):
        # Test the default view
        response = self.client.get('/inventory/storage/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #self.assertTemplateUsed(response, 'inventory/storage/list.html')
        # Test the pagination of the view
        response = self.client.get('/inventory/storage/?page=-1')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=-1')
        response = self.client.get('/inventory/storage/?q=test')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'q=test')
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get('/inventory/storage/')
            self.assertRedirects(response, '/accounts/login/?next=/inventory/storage/')
            user = User.objects.get_or_create(username='testuser')[0]
            user.user_permissions.add(Permission.objects.get(name='Can view storage'))
            self.client.force_login(user)
            response = self.client.get('/inventory/storage/')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(True)

    def test_view_storage_detail(self):
        # Test a valid storage
        i = randint(0,21)
        s = Storage.objects.all()[i]
        response = self.client.get('/inventory/storage/{}/'.format(s.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #self.assertTemplateUsed(response, 'inventory/storage/detail.html')
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get('/inventory/storage/{}/'.format(s.id))
            self.assertRedirects(response, '/accounts/login/?next=/inventory/storage/{}/'.format(s.id))
            user = User.objects.get_or_create(username='testuser')[0]
            user.user_permissions.add(Permission.objects.get(name='Can view storage'))
            self.client.force_login(user)
            response = self.client.get('/inventory/storage/{}/'.format(s.id))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(True)
        # Test an invalid storage
        response = self.client.get('/inventory/storage/100000/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_view_storage_hierarchy_list(self):
        # Add hierarchy
        s = Storage.objects.first()
        for i in range(0,3):
            j = randint(1,20)
            s2 = Storage.objects.get(name='Test_View_Storage_{}'.format(j))
            s2.parent = s
            s2.save()
            k = randint(1,5)
            for l in range(0,k):
                Storage.objects.create(name='Test_Hierarchy_Storage_{}_{}'.format(j,k), description='short-lived test storage', parent=s2)
                
        # Tests
        response = self.client.get('/inventory/container/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_storage_hierarchy_detail(self):
        # Add hierarchy
        s = Storage.objects.first()
        for i in range(0,3):
            j = randint(1,20)
            s2 = Storage.objects.get(name='Test_View_Storage_{}'.format(j))
            s2.parent = s
            s2.save()
            k = randint(1,5)
            for l in range(0,k):
                Storage.objects.create(name='Test_Hierarchy_Storage_{}_{}'.format(j,k), description='short-lived test storage', parent=s2)
                Storage.objects.create(name='Test_Hierarchy_Storage_{}_{}_child'.format(j,k), description='short-lived child test storage', parent=Storage.objects.last())
        for i in range(1101,1200):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='short-lived test part', sku=str(i).zfill(6), price=1.0, cost=0.5)
        for child in s.get_children():
            i = randint(1,5)
            j = randint(1101,1199)
            for k in range(0,i):
                PartStorage.objects.create(part=Part.objects.get(name='Test_View_Part_{}'.format(j)), storage=child, count=5)

        # Tests
        response = self.client.get('/inventory/container/{}/'.format(s.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

@override_settings(ALLOW_ANONYMOUS=True)
class InventoryAlternateSKUViewTests(TestCase):
    def setUp(self):
        for i in range(16,21):
            AlternateSKU.objects.create(manufacturer='ZephyrusComputing', sku=str(i).zfill(4))
        for i in range(701,801):
            Part.objects.create(name='Test_Part_' + str(i), description='This is a view TestCase test part', sku='1' + str(i), price=10.00, cost=5.00)
        for i in range(0,5):
            j = randint(16,20)
            k = randint(701,800)
            PartAlternateSKU.objects.create(part=Part.objects.get(name='Test_Part_' + str(k)), alt_sku=AlternateSKU.objects.get(sku=str(j).zfill(4)))

    def test_view_alternates(self):
        # Test the default view
        response = self.client.get('/inventory/altsku/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #self.assertTemplateUsed(response, 'inventory/partaltsku/list.html')
        # Test the pagination of the view
        response = self.client.get('/inventory/altsku/?page=-5')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=-5')
        response = self.client.get('/inventory/altsku/?q=test')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'q=test')
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get('/inventory/altsku/')
            self.assertRedirects(response, '/accounts/login/?next=/inventory/altsku/')
            user = User.objects.get_or_create(username='testuser')[0]
            user.user_permissions.add(Permission.objects.get(name='Can view alternate sku'))
            self.client.force_login(user)
            response = self.client.get('/inventory/altsku/')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(True)

@override_settings(ALLOW_ANONYMOUS=True)
class InventoryAssemblyViewTests(TestCase):
    def setUp(self):
        for i in range(901,1001):
            Part.objects.create(name='Test_View_Part_' + str(i), description='This is a view TestCase test part', sku=str(i*10).zfill(5), price=10.00, cost=5.00)
        for i in range(0,11):
            j = randint(901,1000)
            p = Part.objects.get(name='Test_View_Part_' + str(j))
            k = randint(1,7)
            for l in range(0,k):
                m = randint(901,1000)
                Assembly.objects.create(parent=p, part=Part.objects.get(name='Test_View_Part_' + str(m)), count=1)
        pass

    def test_view_assemblies(self):
        # Test the default view
        response = self.client.get('/inventory/assembly/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #self.assertTemplateUsed(response, 'inventory/assembly/list.html')
        # Test the pagination of the view
        response = self.client.get('/inventory/assembly/?page=-5')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=-5')
        response = self.client.get('/inventory/assembly/?q=test')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'q=test')
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get('/inventory/assembly/')
            self.assertRedirects(response, '/accounts/login/?next=/inventory/assembly/')
            user = User.objects.get_or_create(username='testuser')[0]
            user.user_permissions.add(Permission.objects.get(name='Can view assembly'))
            self.client.force_login(user)
            response = self.client.get('/inventory/assembly/')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(True)

    def test_view_assembly_detail(self):
        # Test a valid alternate
        i = randint(0,6)
        a = Assembly.objects.all()[i]
        response = self.client.get('/inventory/assembly/{}/'.format(a.parent.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #self.assertTemplateUsed(response, 'inventory/assembly/detail.html')
        # Test a valid part without assembly
        while True:
            j = randint(901,1000)
            part = Part.objects.get(name='Test_View_Part_' + str(j))
            if len(Assembly.objects.filter(part=part)) == 0:
                response = self.client.get('/inventory/assembly/{}/'.format(part.id))
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                #self.assertTemplateUsed(response, 'inventory/assembly/detail.html')
                break;
        # Test an invalid part
        response = self.client.get('/inventory/assembly/idonotexist/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get('/inventory/assembly/{}/'.format(part.id))
            self.assertRedirects(response, '/accounts/login/?next=/inventory/assembly/{}/'.format(part.id))
            user = User.objects.get_or_create(username='testuser')[0]
            user.user_permissions.add(Permission.objects.get(name='Can view assembly'))
            self.client.force_login(user)
            response = self.client.get('/inventory/assembly/{}/'.format(part.id))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(True)

@override_settings(ALLOW_ANONYMOUS=True)
class InventoryKitViewTests(TestCase):
    def setUp(self):
        for i in range(1001,1100):
            Kit.objects.create(name='Test_View_Kit_{}'.format(i), description='This is a view TestCase test kit', sku=str(i*10).zfill(5), price=10.00)
        self.baseurl = '{}kit/'.format(baseurl)
            
    def test_view_kits(self):
        # Test the default view
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #self.assertTemplateUsed(response, 'inventory/kit/list.html')
        # Test the pagination of the view
        response = self.client.get('{}?page=-1'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=-1')
        response = self.client.get('{}?page=100'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=100')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'q=test')
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get('{}'.format(self.baseurl))
            self.assertRedirects(response, '/accounts/login/?next={}'.format(self.baseurl))
            user = User.objects.get_or_create(username='testuser')[0]
            user.user_permissions.add(Permission.objects.get(name='Can view kit'))
            self.client.force_login(user)
            response = self.client.get('{}'.format(self.baseurl))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            
    def test_view_kit_details(self):
        # Test a valid kit
        i = randint(1001,1099)
        k = Kit.objects.get(name='Test_View_Kit_{}'.format(i))
        url = '{}{}/'.format(self.baseurl, k.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #self.assertTemplateUsed(response, 'inventory/kit/detail.html')
        # Test an invalid part
        response = self.client.get('{}idonotexist/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get(url)
            self.assertRedirects(response, '/accounts/login/?next={}'.format(url))
            user = User.objects.get_or_create(username='testuser')[0]
            user.user_permissions.add(Permission.objects.get(name='Can view kit'))
            self.client.force_login(user)
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

@override_settings(ALLOW_ANONYMOUS=True)
class AJaxViewTests(TestCase):
    def setUp(self):
        for i in range(0,11):
            Part.objects.create(name='Test_View_Part_Ajax{}'.format(i), description='This is a view TestCase test part', sku='ajax{}'.format(i), price=9.99, cost=4.49)
            Storage.objects.create(name='Test_View_Storage_Ajax{}'.format(i), description='This is a view TestCase test storage')
        for i in range(0,2):
            Kit.objects.create(name='Test_View_Kit_Ajax{}'.format(i), description='This is a view TestCase test kit', sku='ajax0', price=12.50)
        for i in range(0,21):
            j = randint(0,10)
            k = randint(0,10)
            PartStorage.objects.create(part=Part.objects.get(name='Test_View_Part_Ajax{}'.format(j)), storage=Storage.objects.get(name='Test_View_Storage_Ajax{}'.format(k)), count=5)
        for i in range(0,randint(5,10)):
            j = randint(0,20)
            KitPartStorage.objects.create(kit=Kit.objects.first(), partstorage=PartStorage.objects.all()[j], count=2)
            KitPartStorage.objects.create(kit=Kit.objects.last(), partstorage=PartStorage.objects.all()[j], count=2)

    def test_ajax_kit_subtract(self):
        self.baseurl = '{}kit/'.format(baseurl)
        kit = Kit.objects.first()
        self.assertEqual(kit.get_available_count(), 2)
        url = '{}{}/sub/1/'.format(self.baseurl, kit.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertContains(response, 'red')
        self.assertContains(response, 'Access is denied')
        user = User.objects.get_or_create(username='testuser')[0]
        user.user_permissions.add(Permission.objects.get(name='Can decrement count kit'))
        self.client.force_login(user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'green')
        self.client.get(url)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertContains(response, 'red')
        self.assertContains(response, 'Insufficient parts')

    def test_ajax_kit_add(self):
        self.baseurl = '{}kit/'.format(baseurl)
        kit = Kit.objects.first()
        self.assertEqual(kit.get_available_count(), 2)
        url = '{}{}/add/1/'.format(self.baseurl, kit.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertContains(response, 'red')
        self.assertContains(response, 'Access is denied')
        user = User.objects.get_or_create(username='testuser')[0]
        user.user_permissions.add(Permission.objects.get(name='Can increment count kit'))
        self.client.force_login(user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'green')

    def test_ajax_kit_update(self):
        self.baseurl = '{}kit/'.format(baseurl)
        kit = Kit.objects.last()
        self.assertEqual(kit.get_available_count(), 2)
        url = '{}{}/update/kitdetails/'.format(self.baseurl, kit.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result2.html')
        self.assertContains(response, kit.description)
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            user = User.objects.get_or_create(username='testuser')[0]
            user.user_permissions.add(Permission.objects.get(name='Can view kit'))
            self.client.force_login(user)
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTemplateUsed(response, 'inventory/ajax_result2.html')
        url = '{}{}/update/kitcomponents/'.format(self.baseurl, kit.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result2.html')

    def test_ajax_partstorage_add(self):
        self.baseurl = '{}partstorage/'.format(baseurl)
        ps = PartStorage.objects.first()
        ps.count = 2
        ps.save()
        url = '{}{}/add/1/'.format(self.baseurl, ps.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertContains(response, 'red')
        self.assertContains(response, 'Access is denied')
        user = User.objects.get_or_create(username='testuser')[0]
        user.user_permissions.add(Permission.objects.get(name='Can increment count part storage'))
        self.client.force_login(user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertContains(response, 'green')
        ps.refresh_from_db()
        self.assertEqual(ps.count, 3)

    def test_ajax_partstorage_sub(self):
        self.baseurl = '{}partstorage/'.format(baseurl)
        ps = PartStorage.objects.last()
        ps.count = 2
        ps.save()
        url = '{}{}/sub/1/'.format(self.baseurl, ps.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertContains(response, 'red')
        self.assertContains(response, 'Access is denied')
        user = User.objects.get_or_create(username='testuser')[0]
        user.user_permissions.add(Permission.objects.get(name='Can decrement count part storage'))
        self.client.force_login(user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertContains(response, 'green')
        ps.refresh_from_db()
        self.assertEqual(ps.count, 1)
        self.client.get(url)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertContains(response, 'red')
        self.assertContains(response, 'Insufficient parts')

    def test_ajax_partstorage_update(self):
        self.baseurl = '{}partstorage/'.format(baseurl)
        i = randint(0,20)
        ps = PartStorage.objects.all()[i]
        url = '{}{}/update/pspart/'.format(self.baseurl, ps.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result2.html')
        self.assertContains(response, ps.part.name)
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            user = User.objects.get_or_create(username='testuser')[0]
            user.user_permissions.add(Permission.objects.get(name='Can view part storage'))
            self.client.force_login(user)
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTemplateUsed(response, 'inventory/ajax_result2.html')
            self.assertContains(response, ps.part.name)
        url = '{}{}/update/psstorage/'.format(self.baseurl, ps.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result2.html')
        self.assertContains(response, ps.storage.name)

@override_settings(ALLOW_ANONYMOUS=True)
class StaticPageViewTests(TestCase):
    def test_view_about(self):
        url = '/inventory/about/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.client.force_login(User.objects.get_or_create(username='teststaff',is_staff=1)[0])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.logout()
        self.client.force_login(User.objects.get_or_create(username='newuser',is_staff=0)[0])
        response = self.client.get(url)
        self.assertRedirects(response, '/inventory/')

    def test_view_search(self):
        response = self.client.get('/inventory/search/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get('/inventory/search/')
            self.assertRedirects(response, '/accounts/login/?next=/inventory/search/')
            self.client.force_login(User.objects.get_or_create(username='testuser')[0])
            response = self.client.get('/inventory/search/')
            self.assertEqual(response.status_code, status.HTTP_200_OK)

@override_settings(ALLOW_ANONYMOUS=True)
class SearchViewTests(TestCase):
    def setUp(self):
        Part.objects.create(name='Test_View_Assembly_Search1', description='This is a view TestCase test part', sku='searchass1', price=10.00, cost=5.00)
        for i in range(1,3):
            Part.objects.create(name='Test_View_Part_Search' + str(i), description='This is a view TestCase test part', sku='searchpart' + str(i), price=5.00, cost=2.50)
        Assembly.objects.create(parent=Part.objects.get(name='Test_View_Assembly_Search1'), part=Part.objects.get(name='Test_View_Part_Search1'), count=1)
        Storage.objects.create(name='Test_View_Storage_Search1', description='This is a view TestCase test storage')
        PartStorage.objects.create(storage=Storage.objects.get(name='Test_View_Storage_Search1'), part=Part.objects.get(name='Test_View_Assembly_Search1'), count=1)
        PartStorage.objects.create(storage=Storage.objects.get(name='Test_View_Storage_Search1'), part=Part.objects.get(name='Test_View_Part_Search2'), count=1)
        AlternateSKU.objects.create(sku='ZephyrusSearch1', manufacturer='Zephyrus Computing')
        PartAlternateSKU.objects.create(part=Part.objects.get(name='Test_View_Assembly_Search1'), alt_sku=AlternateSKU.objects.get(sku='ZephyrusSearch1'))
        Kit.objects.create(name='Test_View_Kit_Search1', description='This is a view TestCase test kit', sku='searchkit', price=10.00)
        KitPartStorage.objects.create(kit=Kit.objects.get(name='Test_View_Kit_Search1'), partstorage=PartStorage.objects.first(), count=1)

    def test_view_search_part(self):
        response = self.client.get('/inventory/search/?m=part&q=2&t=3')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Part_Search2')
        self.assertNotContains(response, 'Test_View_Assembly_Search1')
        response = self.client.get('/inventory/search/?m=part&q=1&t=3')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Part_Search')
        self.assertContains(response, 'Test_View_Assembly_Search1')
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get('/inventory/search/')
            self.assertRedirects(response, '/accounts/login/?next=/inventory/search/')
            user = User.objects.get_or_create(username='testuser')[0]
            self.client.force_login(user)
            response = self.client.get('/inventory/search/?m=part&q=2&t=3')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertNotContains(response, 'Test_View_Part_Search2')
            user.user_permissions.add(Permission.objects.get(name='Can view part'))
            response = self.client.get('/inventory/search/?m=part&q=2&t=3')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertContains(response, 'Test_View_Part_Search2')

    def test_view_search_assembly(self):
        response = self.client.get('/inventory/search/?m=assembly&q=1&t=3')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Assembly_Search1')
        self.assertNotContains(response, 'Test_View_Part_Search1')
        response = self.client.get('/inventory/search/?m=assembly&q=2&t=3')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, 'Test_View_Assembly_Search1')
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get('/inventory/search/')
            self.assertRedirects(response, '/accounts/login/?next=/inventory/search/')
            user = User.objects.get_or_create(username='testuser')[0]
            self.client.force_login(user)
            response = self.client.get('/inventory/search/?m=assembly&q=1&t=3')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertNotContains(response, 'Test_View_Assembly_Search1')
            user.user_permissions.add(Permission.objects.get(name='Can view assembly'))
            response = self.client.get('/inventory/search/?m=assembly&q=1&t=3')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertContains(response, 'Test_View_Assembly_Search1')

    def test_view_search_alternatesku(self):
        response = self.client.get('/inventory/search/?m=alternatesku&q=1&t=3')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Zephyrus Computing-ZephyrusSearch1')
        response = self.client.get('/inventory/search/?m=alternatesku&q=test&t=3')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, 'Zephyrus Computing-ZephyrusSearch1')
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get('/inventory/search/')
            self.assertRedirects(response, '/accounts/login/?next=/inventory/search/')
            user = User.objects.get_or_create(username='testuser')[0]
            self.client.force_login(user)
            response = self.client.get('/inventory/search/?m=alternatesku&q=1&t=3')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertNotContains(response, 'Zephyrus Computing-ZephyrusSearch1')
            user.user_permissions.add(Permission.objects.get(name='Can view alternate sku'))
            response = self.client.get('/inventory/search/?m=alternatesku&q=1&t=3')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertContains(response, 'Zephyrus Computing-ZephyrusSearch1')

    def test_view_search_storage(self):
        response = self.client.get('/inventory/search/?m=storage&q=1&t=3')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Storage_Search1')
        response = self.client.get('/inventory/search/?m=storage&q=2&t=3')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, 'Test_View_Storage_Search1')
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get('/inventory/search/')
            self.assertRedirects(response, '/accounts/login/?next=/inventory/search/')
            user = User.objects.get_or_create(username='testuser')[0]
            self.client.force_login(user)
            response = self.client.get('/inventory/search/?m=storage&q=1&t=3')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertNotContains(response, 'Test_View_Storage_Search1')
            user.user_permissions.add(Permission.objects.get(name='Can view storage'))
            response = self.client.get('/inventory/search/?m=storage&q=1&t=3')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertContains(response, 'Test_View_Storage_Search1')

    def test_view_search_kit(self):
        response = self.client.get('/inventory/search/?m=kit&q=1&t=3')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Kit_Search1')
        response = self.client.get('/inventory/search/?m=kit&q=2&t=3')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, 'Test_View_Kit_Search1')
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get('/inventory/search/')
            self.assertRedirects(response, '/accounts/login/?next=/inventory/search/')
            user = User.objects.get_or_create(username='testuser')[0]
            self.client.force_login(user)
            response = self.client.get('/inventory/search/?m=kit&q=1&t=3')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertNotContains(response, 'Test_View_Kit_Search1')
            user.user_permissions.add(Permission.objects.get(name='Can view kit'))
            response = self.client.get('/inventory/search/?m=kit&q=1&t=3')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertContains(response, 'Test_View_Kit_Search1')

    def test_view_search_querytype(self):
        response = self.client.get('/inventory/search/?m=storage&q=1&t=3')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Storage_Search1')
        response = self.client.get('/inventory/search/?m=part&q=Test_View_Part_Search2&t=2')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Part_Search')
        self.assertNotContains(response, 'Test_View_Part_Search1')
        response = self.client.get('/inventory/search/?m=part&q=Test_View_Part_Search&t=2')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, 'Test_View_Part_Search1')
        response = self.client.get('/inventory/search/?m=alternatesku&q=Zephyrus&t=1')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Zephyrus Computing-ZephyrusSearch1')
        response = self.client.get('/inventory/search/?m=alternatesku&q=Test&t=1')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, 'Zephyrus Computing-ZephyrusSearch1')
        response = self.client.get('/inventory/search/?m=kit&q=Test&t=2')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, 'Test_View_Kit_Search1')

    def test_view_search_multiple(self):
        response = self.client.get('/inventory/search/?m=storage&m=part&q=1&t=3')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Storage_Search1')
        self.assertContains(response, 'Test_View_Part_Search1')
        response = self.client.get('/inventory/search/?m=storage&m=alternatesku&q=Test&t=1')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Storage_Search1')
        self.assertNotContains(response, 'Zephyrus Computing-ZephyrusSearch1')

