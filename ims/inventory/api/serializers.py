from rest_framework import serializers
from inventory.models import Part, Storage, AlternateSKU, PartAlternateSKU, PartStorage, Assembly, Kit, KitPartStorage

class PartSerializer(serializers.ModelSerializer):
    class Meta:
        model = Part
        fields = ('id','name','description','sku','price','cost')

    def create(self, validated_data):
        return Part.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.description = validated_data.get('description', instance.description)
        instance.sku = validated_data.get('sku', instance.sku)
        instance.price = validated_data.get('price', instance.price)
        instance.cost = validated_data.get('cost', instance.cost)
        instance.save()
        return instance

class StorageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Storage
        fields = ('id','name','description','parent')

    def create(self, validated_data):
        return Storage.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.description = validated_data.get('description', instance.description)
        instance.parent = validated_data.get('parent', instance.parent)
        instance.save()
        return instance

class AlternateSKUSerializer(serializers.ModelSerializer):
    class Meta:
        model = AlternateSKU
        fields = ('id','manufacturer','sku')

    def create(self, validated_data):
        return AlternateSKU.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.manufacturer = validated_data.get('manufacturer', instance.manufacturer)
        instance.sku = validated_data.get('sku', instance.sku)
        instance.save()
        return instance

class PartAlternateSKUReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartAlternateSKU
        fields = ('id','part','alt_sku')
        depth = 1

class PartAlternateSKUSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartAlternateSKU
        fields = ('id','part','alt_sku')

    def create(self, validated_data):
        return PartAlternateSKU.objects.create(**validated_data)

class PartStorageReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartStorage
        fields = ('id','part','storage','count')
        depth = 1

class PartStorageFullSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartStorage
        fields = ('id','count','created','lastmodified','part','storage')

class PartStorageFullReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartStorage
        fields = ('id','count','created','lastmodified','part','storage')
        depth = 1

class PartStoragePartCountSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartStorage
        fields = ('id','count','part')
        depth = 1

class PartStorageStorageCountSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartStorage
        fields = ('id','count','storage')
        depth = 1

class PartStorageSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartStorage
        fields = ('id','part','storage','count')

    def create(self, validated_data):
        return PartStorage.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.count = validated_data.get('count', instance.count)
        instance.save()
        return instance

class AssemblyReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Assembly
        fields = ('id','parent','part','count')
        depth = 1

class AssemblyPartCountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Assembly
        fields = ('id','count','part')
        depth = 1

class AssemblyParentCountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Assembly
        fields = ('id','count','parent')
        depth = 1
        
class AssemblySerializer(serializers.ModelSerializer):
    class Meta:
        model = Assembly
        fields = ('id','parent','part','count')

    def create(self, validated_data):
        return Assembly.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.count = validated_data.get('count', instance.count)
        instance.save()
        return instance

class KitReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kit
        fields = ('id','name','description','sku','price')
        depth = 1

class KitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kit
        fields = ('id','name','description','sku','price')

    def create(self, validated_data):
        return Kit.objects.create(**validated_data)
    
    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.description = validated_data.get('description', instance.description)
        instance.sku = validated_data.get('sku', instance.sku)
        instance.price = validated_data.get('price', instance.price)
        instance.save()
        return instance

class KitPartStorageReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = KitPartStorage
        fields = ('id','kit','partstorage','count')
        depth = 1

class KitPartStoragePartStorageSerializer(serializers.ModelSerializer):
    class Meta:
        model = KitPartStorage
        fields = ('id','count','partstorage')
        depth = 2

class KitPartStorageKitSerializer(serializers.ModelSerializer):
    class Meta:
        model = KitPartStorage
        fields = ('id','count','kit')
        depth = 2

class KitPartStorageSerializer(serializers.ModelSerializer):
    class Meta:
        model = KitPartStorage
        fields = ('id','kit','partstorage','count')

    def create(self, validated_data):
        return KitPartStorage.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.count = validated_data.get('count', instance.count)
        instance.save()
        return instance
