from django.conf import settings
from django.core.exceptions import FieldError
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.views import APIView
import logging

from inventory.api.serializers import PartSerializer, StorageSerializer, AlternateSKUSerializer, PartAlternateSKUSerializer, PartAlternateSKUReadSerializer, PartStorageSerializer, PartStorageReadSerializer, AssemblySerializer, AssemblyReadSerializer, KitSerializer, KitReadSerializer, KitPartStorageSerializer, KitPartStorageReadSerializer, PartStoragePartCountSerializer, PartStorageStorageCountSerializer, AssemblyPartCountSerializer, AssemblyParentCountSerializer, KitPartStoragePartStorageSerializer, KitPartStorageKitSerializer
from inventory.models import Part, Storage, AlternateSKU, PartStorage, PartAlternateSKU, Assembly, Kit, KitPartStorage, get_sentinel_part, get_sentinel_sku, get_sentinel_storage, get_sentinel_partstorage, get_sentinel_kit
from inventory.search import Search
from log.logger import getLogger
import inventory.helpers as helpers

logger = logging.getLogger(__name__)
logger2 = getLogger('inventory-api')

# Part API Views
class PartList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = Part
    serializer_class = PartSerializer
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.add_{}'.format(self.model_class.__name__.lower()))

class PartDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = Part
    serializer_class = PartSerializer
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.view_{}'.format(self.model_class.__name__.lower()))
    def put(self, request, id):
        return _pk_put(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.change_{}'.format(self.model_class.__name__.lower()))
    def patch(self, request, id):
        return _pk_patch(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.change_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'inventory.delete_{}'.format(self.model_class.__name__.lower()))
    
class PartDetailAssembly(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm('inventory.view_part'):
            logger.debug('User does not have GET permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if not request.user.has_perm('inventory.view_assembly'):
            logger.debug('User does not have GET permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed Part Assembly List'
        logger.debug('Getting requested Part')
        part = get_object_or_404(Part, id=id)
        logger.debug('Generating list of Part')
        assemblies = part.get_parent_assemblies()
        query = request.GET.get('search')
        if query != None:
            logmsg += ' query={},'.format(query)
            logger.debug('Filtering list of Part with query')
            sf = Search().filter(['parent__name', 'parent__sku', 'parent__description'], query)
            assemblies = assemblies.filter(sf)
        for field in ['name','sku','description']:
            value = request.GET.get(field)
            if value != None:
                logmsg += ' {}={},'.format(field, value)
                logger.debug('Filtering list of Part with query on field "{}"'.format(field))
                sf = Search().filter(['parent__{}'.format(field)], value)
                assemblies = assemblies.filter(sf)
        sort = request.GET.get('sort')
        if sort != None:
            try:
                if sort[0] == '-':
                    sort = sort[1:]
                    order = '-parent__{}'.format(sort)
                else:
                    order = 'parent__{}'.format(sort)
                assemblies = assemblies.order_by(order)
                logmsg += ' sort={},'.format(sort)
            except FieldError:
                logger.warning('Unable to sort Part based on field "{}"'.format(sort))
                logger2.warning(request.user, 'Unable to sort Part based on field "{}"'.format(sort))
        limit = request.GET.get('limit')
        if not helpers.is_int(limit): limit = settings.DEFAULT_API_RESULTS
        else: limit = int(limit)
        offset = request.GET.get('offset')
        if not helpers.is_int(offset): offset = 0
        else: offset = int(offset)
        limit += offset
        logmsg += ' limit={}, offset={}'.format(limit, offset)
        assemblies = assemblies[offset:limit]
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), logmsg)
        return Response(AssemblyParentCountSerializer(assemblies, many=True).data, status.HTTP_200_OK)

class PartDetailComponent(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm('inventory.view_part'):
            logger.debug('User does not have GET permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if not request.user.has_perm('inventory.view_assembly'):
            logger.debug('User does not have GET permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed Part Component List'
        logger.debug('Getting requested Part')
        part = get_object_or_404(Part, id=id)
        logger.debug('Generating list of Part')
        assemblies = part.get_components()
        query = request.GET.get('search')
        if query != None:
            logmsg += ' query={},'.format(query)
            logger.debug('Filtering list of Part with query')
            sf = Search().filter(['part__name', 'part__sku', 'part__description'], query)
            assemblies = assemblies.filter(sf)
        for field in ['name','sku','description']:
            value = request.GET.get(field)
            if value != None:
                logmsg += ' {}={},'.format(field, value)
                logger.debug('Filtering list of Part with query on field "{}"'.format(field))
                sf = Search().filter(['part__{}'.format(field)], value)
                assemblies = assemblies.filter(sf)
        sort = request.GET.get('sort')
        if sort != None:
            try:
                if sort[0] == '-':
                    order = '-part__{}'.format(sort[1:])
                else:
                    order = 'part__{}'.format(sort)
                assemblies = assemblies.order_by(order)
                logmsg += ' sort={},'.format(sort)
            except FieldError:
                logger.warning('Unable to sort Part based on field "{}"'.format(sort))
                logger2.warning(request.user, 'Unable to sort Part based on field "{}"'.format(sort))
        limit = request.GET.get('limit')
        if not helpers.is_int(limit): limit = settings.DEFAULT_API_RESULTS
        else: limit = int(limit)
        offset = request.GET.get('offset')
        if not helpers.is_int(offset): offset = 0
        else: offset = int(offset)
        limit += offset
        logmsg += ' limit={}, offset={}'.format(limit, offset)
        assemblies = assemblies[offset:limit]
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), logmsg)
        return Response(AssemblyPartCountSerializer(assemblies, many=True).data, status.HTTP_200_OK)

class PartDetailStorage(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm('inventory.view_part'):
            logger.debug('User does not have GET permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if not request.user.has_perm('inventory.view_storage'):
            logger.debug('User does not have GET permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed Part Storage List'
        logger.debug('Getting requested Part')
        part = get_object_or_404(Part, id=id)
        logger.debug('Generating list of Storage')
        partstorages = part.get_storage()
        query = request.GET.get('search')
        if query != None:
            logmsg += ' query={},'.format(query)
            logger.debug('Filtering list of Storage with query')
            sf = Search().filter(['storage__name', 'storage__description'], query)
            partstorages = partstorages.filter(sf)
        for field in ['name','description']:
            value = request.GET.get(field)
            if value != None:
                logmsg += ' {}={},'.format(field, value)
                logger.debug('Filtering list of Storage with query on field "{}"'.format(field))
                sf = Search().filter(['storage__{}'.format(field)], value)
                partstorages = partstorages.filter(sf)
        sort = request.GET.get('sort')
        if sort != None:
            try:
                if sort[0] == '-':
                    order = '-storage__{}'.format(sort[1:])
                else:
                    order = 'storage__{}'.format(sort)
                partstorages = partstorages.order_by(order)
                logmsg += ' sort={},'.format(sort)
            except FieldError:
                logger.warning('Unable to sort Storage based on field "{}"'.format(sort))
                logger2.warning(request.user, 'Unable to sort Storage based on field "{}"'.format(sort))
        limit = request.GET.get('limit')
        if not helpers.is_int(limit): limit = settings.DEFAULT_API_RESULTS
        else: limit = int(limit)
        offset = request.GET.get('offset')
        if not helpers.is_int(offset): offset = 0
        else: offset = int(offset)
        limit += offset
        logmsg += ' limit={}, offset={}'.format(limit, offset)
        partstorages = partstorages[offset:limit]
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), logmsg)
        return Response(PartStorageStorageCountSerializer(partstorages, many=True).data, status.HTTP_200_OK)

class PartDetailKit(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm('inventory.view_part'):
            logger.debug('User does not have GET permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if not request.user.has_perm('inventory.view_kit'):
            logger.debug('User does not have GET permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed Part Storage List'
        logger.debug('Getting requested Part')
        part = get_object_or_404(Part, id=id)
        logger.debug('Generating list of Kit')
        kits = part.get_parent_kits()
        query = request.GET.get('search')
        if query != None:
            logmsg += ' query={},'.format(query)
            logger.debug('Filtering list of Kit with query')
            sf = Search().filter(['kit__name', 'kit__sku', 'kit__description'], query)
            kits = kits.filter(sf)
        for field in ['name','sku','description']:
            value = request.GET.get(field)
            if value != None:
                logmsg += ' {}={},'.format(field, value)
                logger.debug('Filtering list of Kit with query on field "{}"'.format(field))
                sf = Search().filter(['kit__{}'.format(field)], value)
                kits = kits.filter(sf)
        sort = request.GET.get('sort')
        if sort != None:
            try:
                if sort[0] == '-':
                    order = '-kit__{}'.format(sort[1:])
                else:
                    order = 'kit__{}'.format(sort)
                kits = kits.order_by(order)
                logmsg += ' sort={},'.format(sort)
            except FieldError:
                logger.warning('Unable to sort Kit based on field "{}"'.format(sort))
                logger2.warning(request.user, 'Unable to sort Kit based on field "{}"'.format(sort))
        limit = request.GET.get('limit')
        if not helpers.is_int(limit): limit = settings.DEFAULT_API_RESULTS
        else: limit = int(limit)
        offset = request.GET.get('offset')
        if not helpers.is_int(offset): offset = 0
        else: offset = int(offset)
        limit += offset
        logmsg += ' limit={}, offset={}'.format(limit, offset)
        kits = kits[offset:limit]
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), logmsg)
        return Response(KitPartStorageKitSerializer(kits, many=True).data, status.HTTP_200_OK)

class PartDetailKitSub(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, partid, kitid, inc):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, kitid))
        if not request.user.has_perm('inventory.view_part'):
            logger.debug('User does not have GET permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if not request.user.has_perm('inventory.decrement_kit'):
            logger.debug('User does not have permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logger.debug('Checking and converting "inc" to int')
        try:
            inc = int(inc)
        except Exception as err:
            logger2.error(request.user, err)
            return Response(
                {
                    'message': 'Unable to process the value "{}"'.format(inc),
                },
                status.HTTP_400_BAD_REQUEST
            )
        logger.debug('Getting requested Kit object for subtracting')
        kit = get_object_or_404(Kit, id=kitid)
        available = kit.get_available_count()
        if inc > available:
            logger.info('HTTP_409_CONFLICT response for {} APIView'.format(cname))
            logger.debug('Responding (409) to request for {} APIView'.format(cname))
            logger2.warning((request.user or 0), 'Insufficient parts to subtract {} from Kit id {}'.format(inc, kitid))
            return Response(
                {
                    'message': 'Kit cannot be decremented by {}, there are not enough parts available.'.format(inc),
                    'kit': KitSerializer(kit).data
                },
                status.HTTP_409_CONFLICT
            )
        try:
            logger.debug('Subtracting {} from Kit {}'.format(inc, kit.name))
            kit.subtract(inc)
            logger.debug('Responding (200) to request for {} APIView'.format(cname))
            logger2.info((request.user or 0), 'Subtracted {} from Kit id {}; current count: {}'.format(inc, kitid, kit.get_available_count()))
            return Response(
                {
                    'message': 'Part counts have been decremented for {} number of Kit {}'.format(inc, kit.name),
                    'kit': KitSerializer(kit).data,
                },
                status.HTTP_200_OK
            )
        except ValueError:
            logger.info('Cannot subtract {} from Kit {}. Insufficient amount available'.format(inc, kit.name))
            logger.info('HTTP_409_CONFLICT response for {} APIView'.format(cname))
            logger.debug('Responding (409) to request for {} APIView'.format(cname))
            logger2.warning((request.user or 0), 'Insufficient parts to subtract {} from Kit id {}'.format(inc, kitid))
            return Response(
                {
                    'message': 'Kit cannot be decremented by {}, there is not enough parts available.'.format(inc),
                    'kit': KitSerializer(kit).data
                },
                status.HTTP_409_CONFLICT
            )

class PartDetailKitAdd(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, partid, kitid, inc):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, kitid))
        if not request.user.has_perm('inventory.view_part'):
            logger.debug('User does not have GET permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if not request.user.has_perm('inventory.increment_kit'):
            logger.debug('User does not have permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logger.debug('Checking and converting "inc" to int')
        try:
            inc = int(inc)
        except Exception as err:
            logger2.error(request.user, err)
            return Response(
                {
                    'message': 'Unable to process the value "{}"'.format(inc),
                },
                status.HTTP_400_BAD_REQUEST
            )
        logger.debug('Getting requested Kit object for adding')
        kit = get_object_or_404(Kit, id=kitid)
        logger.debug('Adding {} from Kit {}'.format(inc, kit.name))
        kit.add(inc)
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), 'Added {} to Kit id {}; current count: {}'.format(inc, kitid, kit.get_available_count()))
        return Response(
            {
                'message': 'Part counts have been incremented for {} number of Kit {}'.format(inc, kit.name),
                'kit': KitSerializer(kit).data,
            },
            status.HTTP_200_OK
        )

class PartDetailCount(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm('inventory.view_part'):
            logger.debug('User does not have GET permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logger.debug('Getting requested Part')
        part = get_object_or_404(Part, id=id)
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), 'Accessed {} id {}'.format(cname.replace('DetailCount',''), id))
        data = {}
        data['count'] = part.get_total()
        return Response(data, status.HTTP_200_OK)

# Storage API Views
class StorageList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = Storage
    serializer_class = StorageSerializer
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.add_{}'.format(self.model_class.__name__.lower()))

class StorageDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = Storage
    serializer_class = StorageSerializer
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.view_{}'.format(self.model_class.__name__.lower()))
    def put(self, request, id):
        return _pk_put(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.change_{}'.format(self.model_class.__name__.lower()))
    def patch(self, request, id):
        return _pk_patch(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.change_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'inventory.delete_{}'.format(self.model_class.__name__.lower()))

class StorageDetailParts(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API'.format(cname))
        if not request.user.has_perm('inventory.view_storage'):
            logger.debug('User does not have GET permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if not request.user.has_perm('inventory.view_part'):
            logger.debug('User does not have GET permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed Storage Detail Parts'
        logger.debug('Getting requested Storage')
        storage = get_object_or_404(Storage, id=id)
        partstorages = storage.get_parts_storage()
        query = request.GET.get('search')
        if query != None:
            logmsg += ' query={},'.format(query)
            logger.debug('Filtering list of Part with query')
            sf = Search().filter(['part__name','part__sku','part__description'], query)
            partstorages = partstorages.filter(sf)
        for field in ['name','sku','description']:
            value = request.GET.get(field)
            if value != None:
                logmsg += ' {}={},'.format(field, value)
                logger.debug('Filtering list of Part with query on field "{}"'.format(field))
                sf = Search().filter(['part__{}'.format(field)], value)
                partstorages = partstorages.filter(sf)
        sort = request.GET.get('sort')
        if sort != None:
            try:
                if sort[0] == '-':
                    order = '-part__{}'.format(sort[1:])
                else:
                    order = 'part__{}'.format(sort)
                partstorages = partstorages.order_by(order)
                logmsg += ' sort={},'.format(sort)
            except FieldError:
                logger.warning('Unable to sort Part based on field "{}"'.format(sort))
                logger2.warning(request.user, 'Unable to sort Part based on field "{}"'.format(sort))
        limit = request.GET.get('limit')
        if not helpers.is_int(limit): limit = settings.DEFAULT_API_RESULTS
        else: limit = int(limit)
        offset = request.GET.get('offset')
        if not helpers.is_int(offset): offset = 0
        else: offset = int(offset)
        limit += offset
        logmsg += ' limit={}, offset={}'.format(limit, offset)
        partstorages = partstorages[offset:limit]
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), logmsg)
        return Response(PartStoragePartCountSerializer(partstorages, many=True).data, status.HTTP_200_OK)

# AlternateSKU API Views
class AlternateSKUList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = AlternateSKU
    serializer_class = AlternateSKUSerializer
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.add_{}'.format(self.model_class.__name__.lower()))

class AlternateSKUDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = AlternateSKU
    serializer_class = AlternateSKUSerializer
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.view_{}'.format(self.model_class.__name__.lower()))
    def put(self, request, id):
        return _pk_put(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.change_{}'.format(self.model_class.__name__.lower()))
    def patch(self, request, id):
        return _pk_patch(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.change_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'inventory.delete_{}'.format(self.model_class.__name__.lower()))

# PartAlternateSKU API Views
class PartAlternateSKUList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = PartAlternateSKU
    serializer_class = PartAlternateSKUSerializer
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, PartAlternateSKUReadSerializer, 'inventory.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post2(request, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.add_{}'.format(self.model_class.__name__.lower()))

class PartAlternateSKUDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = PartAlternateSKU
    serializer_class = PartAlternateSKUSerializer
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, PartAlternateSKUReadSerializer, 'inventory.view_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'inventory.delete_{}'.format(self.model_class.__name__.lower()))

# PartStorage API Views
class PartStorageList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = PartStorage
    serializer_class = PartStorageSerializer
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, PartStorageReadSerializer, 'inventory.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post2(request, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.add_{}'.format(self.model_class.__name__.lower()))

class PartStorageDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = PartStorage
    serializer_class = PartStorageSerializer
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, PartStorageReadSerializer, 'inventory.view_{}'.format(self.model_class.__name__.lower()))
    def put(self, request, id):
        return _pk_put(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.change_{}'.format(self.model_class.__name__.lower()))
    def patch(self, request, id):
        return _pk_patch(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.change_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'inventory.delete_{}'.format(self.model_class.__name__.lower()))

class PartStorageAdd(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    def get(self, request, id, count):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm('inventory.increment_partstorage'):
            logger.debug('User does not have permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        try:
            count = int(count)
        except Exception as err:
            logger2.error(request.user, err)
            return Response(
                {
                    'message': 'Unable to process the value "{}"'.format(count),
                },
                status.HTTP_400_BAD_REQUEST
            )
        logger.debug('Getting requested PartStorage')
        ps = get_object_or_404(PartStorage, id=id)
        logger.debug('Adding {} to PartStorage id {}'.format(count, id))
        ps.add(count)
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), 'Added {} to PartStorage id {}; current count: {}'.format(count, id, ps.count))
        return Response(
            {
                'message': 'Part count has been incremented by {} for PartStorage id {}.'.format(count, id),
                'partstorage': PartStorageReadSerializer(ps).data,
            }, 
            status.HTTP_200_OK
        )
    
class PartStorageSub(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    def get(self, request, id, count):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm('inventory.decrement_partstorage'):
            logger.debug('User does not have permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        try:
            count = int(count)
        except Exception as err:
            logger2.error(request.user, err)
            return Response(
                {
                    'message': 'Unable to process the value "{}"'.format(count),
                },
                status.HTTP_400_BAD_REQUEST
            )
        logger.debug('Getting requested PartStorage')
        ps = get_object_or_404(PartStorage, id=id)
        if count > ps.count:
            logger.info('HTTP_409_CONFLICT response for {} APIView'.format(cname))
            logger.debug('Responding (409) to request for {} APIView'.format(cname))
            logger2.warning((request.user or 0), 'Insufficient parts to subtract {} from PartStorage id {}'.format(count, id))
            return Response(
                {
                    'message': 'PartStorage cannot be decremented by {}, there are not enough parts available.'.format(count),
                    'partstorage': PartStorageReadSerializer(ps).data
                },
                status.HTTP_409_CONFLICT
            )
        try:
            logger.debug('Subtracting {} to PartStorage id {}'.format(count, id))
            ps.subtract(count)
            logger.debug('Responding (200) to request for {} APIView'.format(cname))
            logger2.info((request.user or 0), 'Subtracted {} to PartStorage id {}; current count: {}'.format(count, id, ps.count))
            return Response(
                {
                    'message': 'Part count has been decremented by {} for PartStorage id {}.'.format(count, id),
                    'partstorage': PartStorageReadSerializer(ps).data,
                }, 
                status.HTTP_200_OK
            )
        except ValueError:
            logger.info('Cannot subtract {} from PartStorage id {}. Insufficient amount available.'.format(count, id))
            logger.info('HTTP_409_CONFLICT response for {} APIView'.format(cname))
            logger.debug('Responding (409) to request for {} APIView'.format(cname))
            logger2.warning((request.user or 0), 'Insufficient parts to subtract {} from PartStorage id {}'.format(count, id))
            return Response(
                {
                    'message': 'Part count cannot be decremented by {}, there is not enough of PartStorage id {}.'.format(count, id),
                    'partstorage': PartStorageReadSerializer(ps).data,
                }, 
                status.HTTP_409_CONFLICT
            )

# Assembly API Views
class AssemblyList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = Assembly
    serializer_class = AssemblySerializer
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, AssemblyReadSerializer, 'inventory.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post2(request, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.add_{}'.format(self.model_class.__name__.lower()))

class AssemblyDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = Assembly
    serializer_class = AssemblySerializer
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, AssemblyReadSerializer, 'inventory.view_{}'.format(self.model_class.__name__.lower()))
    def put(self, request, id):
        return _pk_put(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.change_{}'.format(self.model_class.__name__.lower()))
    def patch(self, request, id):
        return _pk_patch(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.change_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'inventory.delete_{}'.format(self.model_class.__name__.lower()))
    
class AssemblyDetailParts(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API'.format(cname))
        if not request.user.has_perm('inventory.view_assembly'):
            logger.debug('User does not have GET permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if not request.user.has_perm('inventory.view_part'):
            logger.debug('User does not have GET permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed Assembly Detail Parts'
        logger.debug('Getting requested Assembly')
        assembly = get_object_or_404(Assembly, id=id)
        assemblies = assembly.get_related_parts()
        query = request.GET.get('search')
        if query != None:
            logmsg += ' query={},'.format(query)
            logger.debug('Filtering list of Part with query')
            sf = Search().filter(['part__name','part__sku','part__description'], query)
            assemblies = assemblies.filter(sf)
        for field in ['name','sku','description']:
            value = request.GET.get(field)
            if value != None:
                logmsg += ' {}={},'.format(field, value)
                logger.debug('Filtering list of Part with query on field "{}"'.format(field))
                sf = Search().filter(['part__{}'.format(field)], value)
                assemblies = assemblies.filter(sf)
        sort = request.GET.get('sort')
        if sort != None:
            try:
                if sort[0] == '-':
                    order = '-part__{}'.format(sort[1:])
                else:
                    order = 'part__{}'.format(sort)
                assemblies = assemblies.order_by(order)
                logmsg += ' sort={},'.format(sort)
            except FieldError:
                logger.warning('Unable to sort Part based on field "{}"'.format(sort))
                logger2.warning(request.user, 'Unable to sort Part based on field "{}"'.format(sort))
        limit = request.GET.get('limit')
        if not helpers.is_int(limit): limit = settings.DEFAULT_API_RESULTS
        else: limit = int(limit)
        offset = request.GET.get('offset')
        if not helpers.is_int(offset): offset = 0
        else: offset = int(offset)
        limit += offset
        logmsg += ' limit={}, offset={}'.format(limit, offset)
        assemblies = assemblies[offset:limit]
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), logmsg)
        return Response(AssemblyPartCountSerializer(assemblies, many=True).data, status.HTTP_200_OK)

# Kit API Views
class KitList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = Kit
    serializer_class = KitSerializer
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.add_{}'.format(self.model_class.__name__.lower()))

class KitDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = Kit
    serializer_class = KitSerializer
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.view_{}'.format(self.model_class.__name__.lower()))
    def put(self, request, id):
        return _pk_put(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.change_{}'.format(self.model_class.__name__.lower()))
    def patch(self, request, id):
        return _pk_patch(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.change_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'inventory.delete_{}'.format(self.model_class.__name__.lower()))
    
class KitDetailPartStorage(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm('inventory.view_kit'):
            logger.debug('User does not have GET permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if not request.user.has_perm('inventory.view_kitpartstorage'):
            logger.debug('User does not have GET permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed Kit Detail PartStorage'
        logger.debug('Getting requested Kit')
        kit = get_object_or_404(Kit, id=id)
        kitpartstorages = kit.get_components()
        query = request.GET.get('search')
        if query != None:
            logmsg += ' query={},'.format(query)
            logger.debug('Filtering list of PartStorage with query')
            sf = Search().filter(['partstorage__part__name', 'partstorage__part__sku', 'partstorage__part__description', 'partstorage__storage__name', 'partstorage__storage__description'], query)
            kitpartstorages = kitpartstorages.filter(sf)
        for field in ['part__name', 'part__sku', 'part__description', 'storage__name', 'storage__description']:
            value = request.GET.get(field)
            if value != None:
                logmsg += ' {}={},'.format(field, value)
                logger.debug('Filtering list of PartStorage with query on field "{}"'.format(field))
                sf = Search().filter(['partstorage__{}'.format(field)], value)
                kitpartstorages = kitpartstorages.filter(sf)
        sort = request.GET.get('sort')
        if sort != None:
            try:
                if sort[0] == '-':
                    order = '-partstorage__{}'.format(sort[1:])
                else:
                    order = 'partstorage__{}'.format(sort)
                kitpartstorages = kitpartstorages.order_by(order)
                logmsg += ' sort={},'.format(sort)
            except FieldError:
                logger.warning('Unable to sort PartStorage based on field "{}"'.format(sort))
                logger2.warning(request.user, 'Unable to sort PartStorage based on field "{}"'.format(sort))
        limit = request.GET.get('limit')
        if not helpers.is_int(limit): limit = settings.DEFAULT_API_RESULTS
        else: limit = int(limit)
        offset = request.GET.get('offset')
        if not helpers.is_int(offset): offset = 0
        else: offset = int(offset)
        limit += offset
        logmsg += ' limit={}, offset={}'.format(limit, offset)
        kitpartstorages = kitpartstorages[offset:limit]
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), logmsg)
        return Response(KitPartStoragePartStorageSerializer(kitpartstorages, many=True).data, status.HTTP_200_OK)
    
class KitDetailCount(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm('inventory.view_kit'):
            logger.debug('User does not have GET permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logger.debug('Getting requested Kit')
        kit = get_object_or_404(Kit, id=id)
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), 'Accessed {} id {}'.format(cname.replace('DetailCount',''), id))
        data = {}
        data['count'] = kit.get_available_count()
        return Response(data, status.HTTP_200_OK)

# KitPartStorage API Views
class KitPartStorageList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = KitPartStorage
    serializer_class = KitPartStorageSerializer
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post2(request, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.add_{}'.format(self.model_class.__name__.lower()))

class KitPartStorageDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = KitPartStorage
    serializer_class = KitPartStorageSerializer
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.view_{}'.format(self.model_class.__name__.lower()))
    def put(self, request, id):
        return _pk_put(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.change_{}'.format(self.model_class.__name__.lower()))
    def patch(self, request, id):
        return _pk_patch(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'inventory.change_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'inventory.delete_{}'.format(self.model_class.__name__.lower()))

# Action API Views
class PartAdd(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, stoid, parid, inc):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for Part id {} and Storage id {}'.format(cname, parid, stoid))
        if not request.user.has_perm('inventory.increment_partstorage'):
            logger.debug('User does not have permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logger.debug('Getting requested PartStorage object for adding')
        part = get_object_or_404(Part, id=parid)
        storage = get_object_or_404(Storage, id=stoid)
        container = PartStorage.objects.filter(part=part, storage=storage).first()
        if container == None:
            logger.info('HTTP_404_NOT_FOUND response for {} APIView'.format(cname))
            logger.debug('Responding (404) to request for {} APIView'.format(cname))
            logger2.warning((request.user or 0), 'PartStorage for Part id {} and Storage id {} Not Found'.format(parid,stoid))
            return Response({}, status.HTTP_404_NOT_FOUND)
        logger.debug('Adding {} to Part id {} in Storage id {}'.format(inc, parid, stoid))
        container.add(inc)
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), 'Added {} to PartStorage id {}; current count: {}'.format(inc, container.id, container.count))
        return Response(
            {
                'message': 'Part count has been incremented by {} for Part id {} in Storage id {}.'.format(inc, parid, stoid),
                'part': PartSerializer(part).data,
                'storage': StorageSerializer(storage).data,
                'current_count': str(container.count)
            }, 
            status.HTTP_200_OK
        )

class PartSub(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, stoid, parid, inc):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for Part id {} and Storage id {}'.format(cname, parid, stoid))
        if not request.user.has_perm('inventory.decrement_partstorage'):
            logger.debug('User does not have permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logger.debug('Getting requested PartStorage object for subtracting')
        part = get_object_or_404(Part, id=parid)
        storage = get_object_or_404(Storage, id=stoid)
        container = PartStorage.objects.filter(part=part, storage=storage).first()
        if container == None:
            logger.info('HTTP_404_NOT_FOUND response for {} APIView'.format(cname))
            logger.debug('Responding (404) to request for {} APIView'.format(cname))
            logger2.warning((request.user or 0), 'PartStorage for Part id {} and Storage id {} Not Found'.format(parid,stoid))
            return Response({}, status.HTTP_404_NOT_FOUND)
        try:
            logger.debug('Subtracting {} from Part id {} in Storage id {}'.format(inc, parid, stoid))
            container.subtract(inc)
            logger.debug('Responding (200) to request for {} APIView'.format(cname))
            logger2.info((request.user or 0), 'Subtracted {} to PartStorage id {}; current count: {}'.format(inc, container.id, container.count))
            return Response(
                {
                    'message': 'Part count has been decremented by {} for Part id {} in Storage id {}.'.format(inc, parid, stoid),
                    'part': PartSerializer(part).data,
                    'storage': StorageSerializer(storage).data,
                    'current_count': str(container.count)
                }, 
                status.HTTP_200_OK
            )
        except ValueError:
            logger.info('Cannot subtract {} from Part id {} in Storage id {}. Insufficient amount available.'.format(inc, parid, stoid))
            logger.info('HTTP_409_CONFLICT response for {} APIView'.format(cname))
            logger.debug('Responding (409) to request for {} APIView'.format(cname))
            logger2.warning((request.user or 0), 'Insufficient parts to subtract {} from PartStorage id {}'.format(inc, container.id))
            return Response(
                {
                    'message': 'Part count cannot be decremented by {}, there is not enough of Part id {} in Storage id {}.'.format(inc, parid, stoid),
                    'part': PartSerializer(part).data,
                    'storage': StorageSerializer(storage).data,
                    'current_count': str(container.count)
                }, 
                status.HTTP_409_CONFLICT
            )

class KitSub(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, kitid, inc):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, kitid))
        if not request.user.has_perm('inventory.decrement_kit'):
            logger.debug('User does not have permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logger.debug('Checking and converting "inc" to int')
        try:
            inc = int(inc)
        except Exception as err:
            logger2.error(request.user, err)
            return Response(
                {
                    'message': 'Unable to process the value "{}"'.format(inc),
                },
                status.HTTP_400_BAD_REQUEST
            )
        logger.debug('Getting requested Kit object for subtracting')
        kit = get_object_or_404(Kit, id=kitid)
        available = kit.get_available_count()
        if inc > available:
            logger.info('HTTP_409_CONFLICT response for {} APIView'.format(cname))
            logger.debug('Responding (409) to request for {} APIView'.format(cname))
            logger2.warning((request.user or 0), 'Insufficient parts to subtract {} from Kit id {}'.format(inc, kitid))
            return Response(
                {
                    'message': 'Kit cannot be decremented by {}, there are not enough parts available.'.format(inc),
                    'kit': KitSerializer(kit).data
                },
                status.HTTP_409_CONFLICT
            )
        try:
            logger.debug('Subtracting {} from Kit {}'.format(inc, kit.name))
            kit.subtract(inc)
            logger.debug('Responding (200) to request for {} APIView'.format(cname))
            logger2.info((request.user or 0), 'Subtracted {} from Kit id {}; current count: {}'.format(inc, kitid, kit.get_available_count()))
            return Response(
                {
                    'message': 'Part counts have been decremented for {} number of Kit {}'.format(inc, kit.name),
                    'kit': KitSerializer(kit).data,
                },
                status.HTTP_200_OK
            )
        except ValueError:
            logger.info('Cannot subtract {} from Kit {}. Insufficient amount available'.format(inc, kit.name))
            logger.info('HTTP_409_CONFLICT response for {} APIView'.format(cname))
            logger.debug('Responding (409) to request for {} APIView'.format(cname))
            logger2.warning((request.user or 0), 'Insufficient parts to subtract {} from Kit id {}'.format(inc, kitid))
            return Response(
                {
                    'message': 'Kit cannot be decremented by {}, there is not enough parts available.'.format(inc),
                    'kit': KitSerializer(kit).data
                },
                status.HTTP_409_CONFLICT
            )

class KitAdd(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, kitid, inc):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, kitid))
        if not request.user.has_perm('inventory.increment_kit'):
            logger.debug('User does not have permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logger.debug('Checking and converting "inc" to int')
        try:
            inc = int(inc)
        except Exception as err:
            logger2.error(request.user, err)
            return Response(
                {
                    'message': 'Unable to process the value "{}"'.format(inc),
                },
                status.HTTP_400_BAD_REQUEST
            )
        logger.debug('Getting requested Kit object for adding')
        kit = get_object_or_404(Kit, id=kitid)
        logger.debug('Adding {} from Kit {}'.format(inc, kit.name))
        kit.add(inc)
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), 'Added {} to Kit id {}; current count: {}'.format(inc, kitid, kit.get_available_count()))
        return Response(
            {
                'message': 'Part counts have been incremented for {} number of Kit {}'.format(inc, kit.name),
                'kit': KitSerializer(kit).data,
            },
            status.HTTP_200_OK
        )

def _get(request, cname, model, serializer, permission):
    fname = 'GET'
    logger.info('{} Request made to {} APIView'.format(fname, cname))
    logger2.debug((request.user or 0), '{} Request to {} API'.format(fname, cname))
    if not request.user.has_perm(permission) and not settings.ALLOW_ANONYMOUS:
        logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
        logger2.warning((request.user or 0), 'Access Denied for {} request to {} API'.format(fname, cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logmsg = 'Accessed {} API'.format(cname)
    logger.debug('Generating list of {}'.format(model.__name__))
    objs = model.objects.all()
    if not settings.SHOW_DELETED_OBJECTS:
        if model.__name__ == 'AlternateSKU':
            objs = model.objects.exclude(id=get_sentinel_sku().id)
        if model.__name__ == 'Kit':
            objs = model.objects.exclude(id=get_sentinel_kit().id)
        if model.__name__ == 'Part':
            objs = model.objects.exclude(id=get_sentinel_part().id)
        if model.__name__ == 'PartStorage':
            objs = model.objects.exclude(id=get_sentinel_partstorage().id)
        if model.__name__ == 'Storage':
            objs = model.objects.exclude(id=get_sentinel_storage().id)
    logmsg, objs = helpers.filter_api_request(request, logmsg, model.__name__, objs)
    logger.debug('Responding (200) to request for {} APIView'.format(cname))
    logger2.info((request.user or 0), logmsg)
    return Response(serializer(objs, many=True).data, status.HTTP_200_OK)
def _post(request, cname, model, serializer, permission):
    fname = 'POST'
    logger.info('{} Request made to {} APIView'.format(fname, cname))
    logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
    if not request.user.has_perm(permission):
        logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
        logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logger.debug('Serializing data in request')
    serializer = serializer(data=request.data)
    if serializer.is_valid():
        logger.debug('Serializer is valid')
        logger.info('Creating new {} "{}"'.format(model.__name__, serializer.validated_data.get('name')))
        inst = serializer.save()
        logger.debug('Responding (201) to request for {} APIView'.format(cname))
        logger2.info(request.user, 'Created {} id {}'.format(model.__name__, inst.id))
        return Response(serializer.data, status.HTTP_201_CREATED)
    logger.info('HTTP_400_BAD_REQUEST response for {} APIView'.format(cname))
    logger.debug('Responding (400) to request for {} APIView'.format(cname))
    logger2.warning(request.user, 'Bad {} Request to {} API'.format(fname, cname))
    return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)
def _post2(request, cname, model, serializer, permission):
    fname = 'POST'
    logger.info('{} Request made to {} APIView'.format(fname, cname))
    logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
    if not request.user.has_perm(permission):
        logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
        logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logger.debug('Serializing data in request')
    serializer = serializer(data=request.data)
    if serializer.is_valid():
        logger.debug('Serializer is valid')
        obj = None
        name = "None"
        if model == PartAlternateSKU:
            obj = model.objects.filter(part=serializer.validated_data.get('part'),alt_sku=serializer.validated_data.get('alt_sku')).first()
            name = "{}-{}".format(serializer.validated_data.get('part'), serializer.validated_data.get('alt_sku'))
        if model == PartStorage:
            obj = PartStorage.objects.filter(storage=serializer.validated_data.get('storage'),part=serializer.validated_data.get('part')).first()
            name = "{}-{}".format(serializer.validated_data.get('storage'), serializer.validated_data.get('part'))
        if model == Assembly:
            obj = Assembly.objects.filter(parent=serializer.validated_data.get('parent'),part=serializer.validated_data.get('part')).first()
            name = "{}-{}".format(serializer.validated_data.get('parent'), serializer.validated_data.get('part'))
        if model == KitPartStorage:
            obj = KitPartStorage.objects.filter(kit=serializer.validated_data.get('kit'), partstorage=serializer.validated_data.get('partstorage')).first()
            name = "{}-{}".format(serializer.validated_data.get('kit'), serializer.validated_data.get('partstorage'))
        if name == "None":
            logger.info('HTTP_400_BAD_REQUEST response for {} APIView'.format(cname))
            logger.debug('Unrecognized object model. Responding (400) to request for {} APIView'.format(cname))
            logger2.warning(request.user, 'Bad {} Request to {} API'.format(fname, cname))
            return Response({}, status.HTTP_400_BAD_REQUEST)
        if obj is None:
            logger.info('Creating new {} "{}"'.format(model.__name__, name))
            inst = serializer.save()
            logger.debug('Responding (201) to request for {} APIView'.format(cname))
            logger2.info(request.user, 'Created {} id {}'.format(model.__name__,inst.id))
            return Response(serializer.data, status.HTTP_201_CREATED)
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, '{} Already Exists with id {}'.format(model.__name__, obj.id))
        return Response(serializer.data, status.HTTP_200_OK)
    logger.info('HTTP_400_BAD_REQUEST response for {} APIView'.format(cname))
    logger.debug('Responding (400) to request for {} APIView'.format(cname))
    logger2.warning(request.user, 'Bad {} Request to {} API'.format(fname, cname))
    return Response({}, status.HTTP_400_BAD_REQUEST)
def _pk_get(request, id, cname, model, serializer, permission):
    fname = 'GET'
    logger.info('{} Request made to {} APIView'.format(fname, cname))
    logger2.debug((request.user or 0), '{} Request to {} API for id {}'.format(fname, cname, id))
    if not request.user.has_perm(permission):
        logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
        logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logger.debug('Getting requested {}'.format(model.__name__))
    obj = get_object_or_404(model, id=id)
    logger.debug('Responding (200) to request for {} APIView'.format(cname))
    logger2.info((request.user or 0), 'Accessed {} API for id {}'.format(cname, id))
    return Response(serializer(obj).data, status.HTTP_200_OK)
def _pk_put(request, id, cname, model, serializer, permission):
    return _pk_update('PUT', request, id, cname, model, serializer, permission, False)
def _pk_patch(request, id, cname, model, serializer, permission):
    return _pk_update('PATCH', request, id, cname, model, serializer, permission, True)
def _pk_update(fname, request, id, cname, model, serializer, permission, partial):
    logger.info('{} Request made to {} APIView'.format(fname, cname))
    logger2.debug(request.user, '{} Request to {} API for id {}'.format(fname, cname, id))
    if not request.user.has_perm(permission):
        logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
        logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logger.debug('Getting requested {}'.format(model.__name__))
    obj = get_object_or_404(model, id=id)
    logger.debug('Serializing data in request')
    serializer = serializer(obj, data=request.data, partial=partial)
    if serializer.is_valid():
        logger.debug('Serializer is valid')
        serializer.save()
        logger.info('{} request successfully updated {} id {}'.format(fname, model.__name__, id))
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, 'Updated {} for id {}'.format(cname, id))
        return Response(serializer.data, status.HTTP_200_OK)
    logger.info('HTTP_400_BAD_REQUEST response for {} APIView'.format(cname))
    logger.debug('Responding (400) to request for {} APIView'.format(cname))
    logger2.warning(request.user, 'Bad {} Request to {} API'.format(fname, cname))
    return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)
def _pk_delete(request, id, cname, model, permission):
    fname = 'DELETE'
    logger.info('{} Request made to {} APIView'.format(fname, cname))
    logger2.debug(request.user, '{} Request to {} API for id {}'.format(fname, cname, id))
    if not request.user.has_perm(permission):
        logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
        logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logger.debug('Getting requested {}'.format(model.__name__))
    obj = get_object_or_404(model, id=id)
    message = {
        'message': '{} has sucessfully been deleted'.format(model.__name__),
    }
    if model.__name__ == 'Part':
        message = {
            'message': '{} has successfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'storage': StorageSerializer((s.storage for s in obj.get_storage()), many=True).data,
            'alternate_sku': AlternateSKUSerializer((a.alt_sku for a in obj.get_alt_sku()), many=True).data,
            'assembly(parent)': PartSerializer((p.parent for p in obj.get_parent_assemblies()), many=True).data,
            'assembly(child)': PartSerializer((c.part for c in obj.get_components()), many=True).data,
            'kit': KitSerializer((k.kit for k in obj.get_parent_kits()), many=True).data,
        }
    if model.__name__ == 'Storage':
        message = {
            'message': '{} has successfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'part': PartSerializer(obj.get_parts(), many=True).data,
            'parent': StorageSerializer(obj.parent).data,
            'child': StorageSerializer(obj.get_children(), many=True).data,
        }
    if model.__name__ == 'AlternateSKU':
        message = {
            'message': '{} has successfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'part': PartSerializer(obj.get_parts(), many=True).data,
        }
    if model.__name__ == 'PartAlternateSKU':
        message = {
            'message': '{} has successfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'part': PartSerializer(obj.part).data,
            'alternate_sku': AlternateSKUSerializer(obj.alt_sku).data,
        }
    if model.__name__ == 'PartStorage':
        kitpartstorages = KitPartStorage.objects.filter(partstorage=obj)
        kitserial = []
        for kitpartstorage in kitpartstorages:
            kitserial.append(KitSerializer(kitpartstorage.kit).data)
        message = {
            'message': '{} has successfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'part': PartSerializer(obj.part).data,
            'storage': StorageSerializer(obj.storage).data,
            'kit': KitSerializer(kitserial, many=True).data,
        }
    if model.__name__ == 'Assembly':
        message = {
            'message': '{} has successfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'parent': PartSerializer(obj.parent).data,
            'part': PartSerializer(obj.part).data,
        }
    if model.__name__ == 'Kit':
        message = {
            'message': '{} has successfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'partstorage': PartStorageSerializer((kps.partstorage for kps in obj.get_components()), many=True).data,
        }
    if model.__name__ == 'KitPartStorage':
        message = {
            'message': '{} has successfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'kit': KitSerializer(obj.kit).data,
            'partstorage': PartStorageSerializer(obj.partstorage).data,
        }
    logger.info('DELETE request for {} id {}'.format(model.__name__, id))
    obj.delete()
    logger.debug('Responding (200) to request for {} APIView'.format(cname))
    logger2.info(request.user, 'Deleted {} id {}'.format(model.__name__, id))
    return Response(message, status.HTTP_200_OK)