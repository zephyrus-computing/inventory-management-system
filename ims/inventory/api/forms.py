from django import forms
from inventory.models import Part, Storage, AlternateSKU, PartAlternateSKU, PartStorage, Assembly, Kit, KitPartStorage

class PartForm(forms.ModelForm):
    class Meta:
        model = Part
        fields = ('name', 'description', 'sku', 'price', 'cost')

class StorageForm(forms.ModelForm):
    class Meta:
        model = Storage
        fields = ('name', 'description')

class AlternateSKUForm(forms.ModelForm):
    class Meta:
        model = AlternateSKU
        fields = ('manufacturer', 'sku')

class PartAlternateSKUForm(forms.ModelForm):
    class Meta:
        model = PartAlternateSKU
        fields = ('part', 'alt_sku')

class PartStorageForm(forms.ModelForm):
    class Meta:
        model = PartStorage
        fields = ('part', 'storage', 'count')

class AssemblyForm(forms.ModelForm):
    class Meta:
        model = Assembly
        fields = ('parent', 'part', 'count')

class KitForm(forms.ModelForm):
    class Meta:
        model = Kit
        fields = ('name', 'description', 'sku', 'price')

class KitPartStorage(forms.ModelForm):
    class Meta:
        model = KitPartStorage
        fields = ('kit', 'partstorage', 'count')
