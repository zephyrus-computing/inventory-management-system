from decimal import Decimal
from django.conf import settings
from django.db import models
from django.urls import reverse
from math import floor
import logging

logger = logging.getLogger(__name__)

# Model Methods
def get_sentinel_part():
    return Part.objects.get_or_create(name='deleted',description='deleted',sku='deleted',price=0.00,cost=0.00)[0]

def get_sentinel_sku():
    return AlternateSKU.objects.get_or_create(sku='deleted',manufacturer='deleted')[0]

def get_sentinel_storage():
    return Storage.objects.get_or_create(name='deleted', description='deleted')[0]

def get_sentinel_partstorage():
    return PartStorage.objects.get_or_create(part=get_sentinel_part(), storage=get_sentinel_storage(), count=0)[0]

def get_sentinel_kit():
    return Kit.objects.get_or_create(name='deleted',description='deleted',sku='deleted',price=0.0)[0]

# Model Classes
class Part(models.Model):
    name = models.CharField(max_length=256)
    description = models.CharField(max_length=512)
    sku = models.CharField(max_length=50)
    price = models.DecimalField(decimal_places=2, max_digits=11)
    cost = models.DecimalField(decimal_places=2, max_digits=11)
    created = models.DateTimeField(auto_now_add=True,editable=False)
    lastmodified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['name']

    def save(self, *args, **kwargs):
        if self.id != None:
            logger.info('Saving changes made to Part id {}'.format(self.id))
        else:
            logger.info('Creating new Part "{}"'.format(self.name))
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        logger.info('Deleting Part id {}'.format(self.id))
        super().delete(*args, **kwargs)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        logger.debug('Generating absolute URL for Part id {}'.format(self.id))
        return reverse('inventory:part_detail', args=[self.id])

    def get_alt_sku(self):
        logger.debug('Getting object_list of PartAlternateSKU for Part id {}'.format(self.id))
        object_list = PartAlternateSKU.objects.filter(part=self)
        if not settings.SHOW_DELETED_OBJECTS:
            object_list = object_list.exclude(alt_sku__sku='deleted')
        return object_list

    def get_total(self):
        logger.debug('Getting total count of Part id {}'.format(self.id))
        total = 0
        object_list = self.get_storage()
        for obj in object_list:
            total += obj.count
        return total

    def has_alt_sku(self):
        logger.debug('Checking if AlternateSKU exists for Part id {}'.format(self.id))
        alts = self.get_alt_sku()
        return len(alts) > 0

    def get_storage(self):
        logger.debug('Getting object_list of PartStorage for Part id {}'.format(self.id))
        object_list = PartStorage.objects.filter(part=self)
        if not settings.SHOW_DELETED_OBJECTS:
            object_list = object_list.exclude(storage__name='deleted')
        return object_list

    def has_storage(self):
        logger.debug('Checking if Storage exists for Part id {}'.format(self.id))
        containers = self.get_storage()
        return len(containers) > 0

    def has_parent_assemblies(self):
        logger.debug('Checking if Assembly (Parent) exists for Part id {}'.format(self.id))
        parents = self.get_parent_assemblies()
        return len(parents) > 0

    def get_parent_assemblies(self):
        logger.debug('Getting object_list of Assembly (Parent) for Part id {}'.format(self.id))
        object_list = Assembly.objects.filter(part=self)
        if not settings.SHOW_DELETED_OBJECTS:
            object_list = object_list.exclude(parent__name='deleted')
        return object_list

    def has_parent_kits(self):
        logger.debug('Checking if Kit (Parent) exists for Part id {}'.format(self.id))
        parents = self.get_parent_kits()
        return len(parents) > 0

    def get_parent_kits(self):
        logger.debug('Getting object_list of Kit (Parent) for Part id {}'.format(self.id))
        ps = self.get_storage()
        object_list = KitPartStorage.objects.filter(partstorage__in=ps)
        if not settings.SHOW_DELETED_OBJECTS:
            object_list = object_list.exclude(kit__name='deleted')
        return object_list

    def has_components(self):
        logger.debug('Checking if Assembly (Children) exists for Part id {}'.format(self.id))
        components = self.get_components()
        return len(components) > 0

    def get_components(self):
        logger.debug('Getting object_list of Assembly (Children) for Part id {}'.format(self.id))
        object_list = Assembly.objects.filter(parent=self)
        if not settings.SHOW_DELETED_OBJECTS:
            object_list = object_list.exclude(part__name='deleted')
        return object_list

class Storage(models.Model):
    name = models.CharField(max_length=256)
    description = models.CharField(max_length=512)
    parent = models.ForeignKey('self', on_delete=models.SET_NULL, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True,editable=False)
    lastmodified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['name']
        
    def save(self, *args, **kwargs):
        if self.id != None:
            logger.info('Saving changes made to Storage id {}'.format(self.id))
            if self.parent == self:
                self.parent = None
        else:
            logger.info('Creating new Storage "{}"'.format(self.name))
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        logger.info('Deleting Storage id {}'.format(self.id))
        super().delete(*args, **kwargs)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        logger.debug('Generating absolute URL for Storage id {}'.format(self.id))
        return reverse('inventory:storage_detail', args=[self.id])

    def get_container_url(self):
        logger.debug('Generating container URL for Storage id {}'.format(self.id))
        return reverse('inventory:storage_hierarchy_detail', args=[self.id])

    def get_parts(self):
        logger.debug('Generating part_list of Part for Storage id {}'.format(self.id))
        object_list = PartStorage.objects.filter(storage=self)
        if not settings.SHOW_DELETED_OBJECTS:
            object_list = object_list.exclude(part__name='deleted')
        part_list = []
        for obj in object_list:
            if obj.part not in part_list:
                part_list.append(obj.part)
        return part_list

    def get_parts_storage(self):
        logger.debug('Generating object_list of PartStorage for Storage id {}'.format(self.id))
        object_list = PartStorage.objects.filter(storage=self)
        if not settings.SHOW_DELETED_OBJECTS:
            object_list = object_list.exclude(part__name='deleted')
        return object_list

    def has_parts(self):
        logger.debug('Checking if Part exists for Storage id {}'.format(self.id))
        object_list = self.get_parts()
        return len(object_list) > 0

    def get_children(self):
        logger.debug('Generating child_list of Storage for Storage id {}'.format(self.id))
        child_list = Storage.objects.filter(parent=self)
        return child_list

    def has_children(self):
        logger.debug('Checking if children exist for Storage id {}'.format(self.id))
        object_list = self.get_children()
        return len(object_list) > 0

class AlternateSKU(models.Model):
    manufacturer = models.CharField(max_length=50)
    sku = models.CharField(max_length=50)
    created = models.DateTimeField(auto_now_add=True,editable=False)
    lastmodified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['manufacturer','sku']
        
    def save(self, *args, **kwargs):
        if self.id != None:
            logger.info('Saving changes made to AlternateSKU id {}'.format(self.id))
        else:
            logger.info('Creating new AlternateSKU "{}"'.format(self))
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        logger.info('Deleting AlternateSKU id {}'.format(self.id))
        super().delete(*args, **kwargs)

    def __str__(self):
        return self.manufacturer + '-' + self.sku

    def get_parts(self):
        logger.debug('Generating part_list of Part for AlternateSKU id {}'.format(self.id))
        object_list = PartAlternateSKU.objects.filter(alt_sku=self)
        if not settings.SHOW_DELETED_OBJECTS:
            object_list = object_list.exclude(part__name='deleted')
        part_list = []
        for obj in object_list:
            if obj.part not in part_list:
                part_list.append(obj.part)
        return part_list

    def has_parts(self):
        logger.debug('Checking if Part exists for AlternateSKU id {}'.format(self.id))
        object_list = self.get_parts()
        return len(object_list) > 0

class PartAlternateSKU(models.Model):
    part = models.ForeignKey('Part', on_delete=models.SET(get_sentinel_part), related_name='+')
    alt_sku = models.ForeignKey('AlternateSKU', on_delete=models.SET(get_sentinel_sku), related_name='+')
    created = models.DateTimeField(auto_now_add=True,editable=False)
    lastmodified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['part','alt_sku']
        
    def save(self, *args, **kwargs):
        if self.id != None:
            logger.info('Saving changes made to PartAlternateSKU id {}'.format(self.id))
        else:
            logger.info('Creating new PartAlternateSKU "{}"'.format(self))
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        logger.info('Deleting PartAlternateSKU id {}'.format(self.id))
        super().delete(*args, **kwargs)

    def __str__(self):
        return '{} AKA {}'.format(self.part, self.alt_sku)

class PartStorage(models.Model):
    part = models.ForeignKey('Part', on_delete=models.SET(get_sentinel_part), related_name='+')
    storage = models.ForeignKey('Storage', on_delete=models.SET(get_sentinel_storage), related_name='+')
    count = models.PositiveIntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True,editable=False)
    lastmodified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['storage','part','count']
        permissions = [
            ('increment_partstorage','Can increment count part storage'),
            ('decrement_partstorage','Can decrement count part storage'),
        ]
        
    def save(self, *args, **kwargs):
        if self.id != None:
            logger.info('Saving changes made to PartStorage id {}'.format(self.id))
            super().save(update_fields=['count'], *args, **kwargs)
        else:
            logger.info('Creating new PartStorage "{}"'.format(self))
            super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        logger.info('Deleting PartStorage id {}'.format(self.id))
        super().delete(*args, **kwargs)

    def __str__(self):
        return '{}-{}'.format(self.storage, self.part)

    def add(self, increment):
        if IsInt(increment):
            logger.debug('Adding {} to Part {} in Storage {}'.format(increment, self.part.name, self.storage.name))
            self.count = self.count + int(increment)
            self.save()

    def subtract(self, increment):
        if IsInt(increment):
            if self.count >= int(increment):
                logger.debug('Subtracting {} from Part {} in Storage {}'.format(increment, self.part.name, self.storage.name))
                self.count -= int(increment)
                self.save()
            else:
                logger.info('Cannot subtract {} from Part {} in Storage {}. Insufficient amount available'.format(increment, self.part.name, self.storage.name))
                raise ValueError('Cannot subtract more than current amount of inventory')


class Assembly(models.Model):
    parent = models.ForeignKey('Part', on_delete=models.SET(get_sentinel_part), related_name='+')
    part = models.ForeignKey('Part', on_delete=models.SET(get_sentinel_part), related_name='+')
    count = models.PositiveIntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True,editable=False)
    lastmodified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['parent','part']
        
    def save(self, *args, **kwargs):
        if self.id != None:
            logger.info('Saving changes made to Assembly id {}'.format(self.id))
        else:
            logger.info('Creating new Assembly "{}"'.format(self))
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        logger.info('Deleting Assembly id {}'.format(self.id))
        super().delete(*args, **kwargs)

    def __str__(self):
        return '{}-{}'.format(self.parent, self.part)

    def get_absolute_url(self):
        logger.debug('Generating absolute URL for Assembly id {}'.format(self.id))
        return reverse('inventory:assembly_detail', args=[self.parent.id])
    
    def get_related_parts(self):
        logger.debug('Generating object_list of Assemblies for Assembly id {}'.format(self.id))
        object_list = Assembly.objects.filter(parent=self.parent)
        if not settings.SHOW_DELETED_OBJECTS:
            object_list = object_list.exclude(part__name='deleted')
        return object_list

class Kit(models.Model):
    name = models.CharField(max_length=256)
    description = models.CharField(max_length=512)
    sku = models.CharField(max_length=50)
    price = models.DecimalField(decimal_places=2, max_digits=11)
    created = models.DateTimeField(auto_now_add=True,editable=False)
    lastmodified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['name']
        permissions = [
            ('increment_kit','Can increment count kit'),
            ('decrement_kit','Can decrement count kit'),
        ]

    def save(self, *args, **kwargs):
        if self.id != None:
            logger.info('Saving changes made to Kit id {}'.format(self.id))
        else:
            logger.info('Creating new Kit "{}"'.format(self))
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        logger.info('Deleting Kit id {}'.format(self.id))
        super().delete(*args, **kwargs)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        logger.debug('Generating absolute URL for Kit id {}'.format(self.id))
        return reverse('inventory:kit_detail', args=[self.id])

    def get_components(self):
        logger.debug('Generating partstorage_list of PartStorage for Kit id {}'.format(self.id))
        partstorage_list = KitPartStorage.objects.filter(kit=self)
        if not settings.SHOW_DELETED_OBJECTS:
            partstorage_list = partstorage_list.exclude(kit__name='deleted')
        return partstorage_list

    def has_components(self):
        logger.debug('Checking if PartStorage exists for Kit id {}'.format(self.id))
        object_list = self.get_components()
        return len(object_list) > 0

    def get_cost(self):
        logger.debug('Checking the total cost for Kit id {}'.format(self.id))
        total = Decimal('0.00')
        kps = KitPartStorage.objects.filter(kit=self)
        for obj in kps:
            total += (obj.count * obj.partstorage.part.cost)
        return total

    def get_available_count(self):
        logger.debug('Checking the available Part counts for Kit id {}'.format(self.id))
        available = -1
        kps = KitPartStorage.objects.filter(kit=self)
        for obj in kps:
            count = floor(obj.partstorage.count / obj.count)
            if count < available or available == -1:
                available = count
        if available == -1:
            available = 0
        return available

    def add(self, increment):
        logger.debug('Adding count {} to PartStorage for Kit id {}'.format(increment, self.id))
        if IsInt(increment):
            kps = KitPartStorage.objects.filter(kit=self)
            for obj in kps:
                number = int(increment) * obj.count
                obj.partstorage.add(number)

    def subtract(self, increment):
        logger.debug('Subtracting count {} from PartStorage for Kit id {}'.format(increment, self.id))
        if IsInt(increment):
            kps = KitPartStorage.objects.filter(kit=self)
            for obj in kps:
                number = int(increment) * obj.count
                if number > obj.partstorage.count:
                    logger.info('Cannot subtract {} number of Kits with id {}. Insufficient amount available in PartStorage id {}'.format(increment, self.id, obj.partstorage.id))
                    raise ValueError('Cannot subtract more than current amount of inventory')
            for obj in kps:
                number = int(increment) * obj.count
                obj.partstorage.subtract(number)

class KitPartStorage(models.Model):
    kit = models.ForeignKey('Kit', on_delete=models.SET(get_sentinel_kit), related_name='+')
    partstorage = models.ForeignKey('PartStorage', on_delete=models.SET(get_sentinel_partstorage), related_name='+')
    count = models.PositiveIntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True,editable=False)
    lastmodified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['kit','partstorage']

    def __str__(self):
        return '{}-{}'.format(self.kit.name, self.partstorage)

    def save(self, *args, **kwargs):
        if self.id != None:
            logger.info('Saving changes made to KitPartStorage id {}'.format(self.id))
        else:
            logger.info('Creating new KitPartStorage "{}"'.format(self))
        super().save(*args, **kwargs)
        
    def delete(self, *args, **kwargs):
        logger.info('Deleting KitPartStorage id {}'.format(self.id))
        super().delete(*args, **kwargs)

def IsInt(i):
    try:
        int(i)
        return True
    except ValueError:
        return False
