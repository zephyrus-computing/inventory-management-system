from django.contrib.auth.decorators import login_required, permission_required
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404, redirect
from itertools import chain
from pip._internal.operations.freeze import freeze
import logging

from inventory.search import Search
from inventory.models import Part, Storage, AlternateSKU, PartStorage, PartAlternateSKU, Assembly, Kit, KitPartStorage
from inventory.forms import ModelSearchForm
from log.logger import getLogger
import inventory.helpers as helpers

# Configure logger
logger = logging.getLogger(__name__)
logger2 = getLogger('inventory')

# Retrieve all of the Parts and display a table
def part_list(request):
    logger.debug('Request made to part_list view')
    if settings.ALLOW_ANONYMOUS or request.user.has_perm(helpers.get_admin_permission('inventory','Part','view')):
        # Get all Parts
        logger.debug('Generating object_list of Part')
        logger2.debug((request.user or 0), 'Request to view Part List')
        logmsg = 'Accessed Part List'
        object_list = Part.objects.all()
        if not settings.SHOW_DELETED_OBJECTS:
            object_list = object_list.exclude(name='deleted')

        # Create the Search filter and apply to the existing results
        query = request.GET.get('q')
        if query != None:
            logmsg = '{} with query \'{}\''.format(logmsg, query)
            logger.debug('Filtering object_list of Part with query')
            querytype = request.GET.get('t')
            qt = _parse_querytype(querytype)
            sf = Search().filter(['name', 'sku', 'description'], qt + query)
            object_list = object_list.filter(sf)

        # Create the Paginator and get the existing page
        logger.debug('Paginate object_list of Part')
        paginator = Paginator(object_list, settings.PART_PAGE_SIZE)
        page = request.GET.get('page')

        # Try to display the page or catch and process the errors
        try:
            parts = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver the first page
            logger.debug('PageNotAnInteger, show page 1')
            parts = paginator.page(1)
        except EmptyPage:
            # If page is out of range, deliver the last page
            logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
            parts = paginator.page(paginator.num_pages)
        # Return the list of parts
        logger.info('Rendering request for part_list view')
        logger2.info(request.user, logmsg)
        return render(request, 'inventory/part/list.html', {'query': query, 'page': page, 'parts': parts})
    else:
        logger.debug('User was not authenticated and ALLOW_ANONYMOUS is set to False')
        logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
        return redirect('/accounts/login/?next={}'.format(request.path))

# Retrieve information about the specified Part
def part_detail(request, id):
    logger.debug('Request made to part_detail view')
    if settings.ALLOW_ANONYMOUS or request.user.has_perm(helpers.get_admin_permission('inventory','Part','view')):
        # Get the Part object to display in detail
        logger.debug('Getting requested Part')
        logger2.debug((request.user or 0), 'Request to view Part Details for id {}'.format(id))
        p = get_object_or_404(Part, id=id)
        logger.debug('Getting previous and next Part objects')
        pp = Part.objects.filter(id__lt=p.id).order_by('-id').first()
        if pp == None:
            pp = Part.objects.order_by('-id').first()
        pn = Part.objects.filter(id__gt=p.id).order_by('id').first()
        if pn == None:
            pn = Part.objects.order_by('id').first()
        logger.info('Rendering request for part_detail view')
        logger2.info(request.user, 'Accessed Part id {}'.format(id))
        return render(request, 'inventory/part/detail.html', {'part': p, 'previous': pp.get_absolute_url(), 'next': pn.get_absolute_url()})
    else:
        logger.debug('User was not authenticated and ALLOW_ANONYMOUS is set to False')
        logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
        return redirect('/accounts/login/?next={}'.format(request.path))

# Retrieve all of the Storage containers and display them in a table
def storage_list(request):
    logger.debug('Request made to storage_list view')
    if settings.ALLOW_ANONYMOUS or request.user.has_perm(helpers.get_admin_permission('inventory','Storage','view')):
        # Get all Storage
        logger.debug('Generating object_list of Storage')
        logger2.debug((request.user or 0), 'Request to view Storage List')
        logmsg = 'Accessed Storage List'
        object_list = Storage.objects.all()
        if not settings.SHOW_DELETED_OBJECTS:
            object_list = object_list.exclude(name='deleted')

        # Create the Search filter and apply to the existing results
        query = request.GET.get('q')
        if query != None:
            logmsg = '{} with query \'{}\''.format(logmsg, query)
            logger.debug('Filtering object_list of Storage with query')
            querytype = request.GET.get('t')
            qt = _parse_querytype(querytype)
            sf = Search().filter(['name', 'description'], qt + query)
            object_list = object_list.filter(sf)

        # Create the Paginator and get the current page
        logger.debug('Paginate object_list of Storage')
        paginator = Paginator(object_list, settings.STORAGE_PAGE_SIZE)
        page = request.GET.get('page')
        # Try to display the page or catch and process errors
        try:
            containers = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver the first page
            logger.debug('PageNotAnInteger, show page 1')
            containers = paginator.page(1)
        except EmptyPage:
            # If page is out of range, deliver the last page
            logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
            containers = paginator.page(paginator.num_pages)
        # Return the list of containers
        logger.info('Rendering request for storage_list view')
        logger2.info((request.user or 0), logmsg)
        return render(request, 'inventory/storage/list.html', {'query': query, 'page': page, 'containers': containers})
    else:
        logger.debug('User was not authenticated and ALLOW_ANONYMOUS is set to False')
        logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
        return redirect('/accounts/login/?next={}'.format(request.path))

# Retrieve information about the specified container
def storage_detail(request, id):
    logger.debug('Request made to storage_detail view')
    if settings.ALLOW_ANONYMOUS or request.user.has_perm(helpers.get_admin_permission('inventory','Storage','view')):
        # Get the Storage object to display in detail
        logger.debug('Getting requested Storage')
        logger2.debug((request.user or 0), 'Request to view Storage Details for id {}'.format(id))
        s = get_object_or_404(Storage, id=id)
        logger.debug('Getting previous and next Storage objects')
        sp = Storage.objects.filter(id__lt=s.id).order_by('-id').first()
        if sp == None:
            sp = Storage.objects.order_by('-id').first()
        sn = Storage.objects.filter(id__gt=s.id).order_by('id').first()
        if sn == None:
            sn = Storage.objects.order_by('id').first()
        # Return the Storage Details
        logger.info('Rendering request for storage_detail view')
        logger2.info((request.user or 0), 'Accessed Storage id {}'.format(id))
        return render(request, 'inventory/storage/detail.html', {'container': s, 'next': sn.get_absolute_url(), 'previous': sp.get_absolute_url()})
    else:
        logger.debug('User was not authenticated and ALLOW_ANONYMOUS is set to False')
        logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
        return redirect('/accounts/login/?next={}'.format(request.path))

# Retrieve all of the Storage containers and display them in a tree
def storage_hierarchy_list(request):
    logger.debug('Request made to storage_hierarchy_list view')
    if settings.ALLOW_ANONYMOUS or request.user.has_perm(helpers.get_admin_permission('inventory','Storage','view')):
        # Get all Storage
        logger.debug('Generating object_list of Storage')
        logger2.debug((request.user or 0), 'Request to view Storage Hierarchy List')
        object_list = Storage.objects.filter(parent=None)
        if not settings.SHOW_DELETED_OBJECTS:
            object_list = object_list.exclude(name='deleted')
            
        # Return the list of containers
        logger.info('Rendering request for storage_hierarchy view')
        logger2.info((request.user or 0), 'Accessed Storage Hierarchy List')
        return render(request, 'inventory/storage/hierarchy_list.html', {'containers': object_list})
    else:
        logger.debug('User was not authenticated and ALLOW_ANONYMOUS is set to False')
        logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
        return redirect('/accounts/login/?next={}'.format(request.path))

# Retrieve information about the specified Storage container in tree summary
def storage_hierarchy_detail(request, id):
    logger.debug('Request made to storage_hierarchy_detail view')
    if settings.ALLOW_ANONYMOUS or request.user.has_perm(helpers.get_admin_permission('inventory','Storage','view')):
        # Get the Storage object to display in detail
        logger.debug('Getting requested Storage')
        logger2.debug((request.user or 0), 'Request to view Storage Hierarchy Details for id {}'.format(id))
        s = get_object_or_404(Storage, id=id)
        sp = Storage.objects.filter(id__lt=s.id).order_by('-id').first()
        if sp == None:
            sp = Storage.objects.order_by('-id').first()
        sn = Storage.objects.filter(id__gt=s.id).order_by('id').first()
        if sn == None:
            sn = Storage.objects.order_by('id').first()
        pss = {}
        children_ids = _child_storage_loop(s, [s.id])
        if children_ids is not None:
            for ps in PartStorage.objects.filter(storage_id__in=children_ids):
                if ps.part.id not in pss.keys():
                    pss[ps.part.id] = ps.count
                else:
                    pss[ps.part.id] = ps.count + pss[ps.part.id]
        # Get list of Parts
        parts = Part.objects.filter(id__in=pss.keys())
        # Return the Storage details
        logger.info('Rendering request for storage_hierarchy_detail view')
        logger2.info((request.user or 0), 'Accessed Storage Hierarchy for id {}'.format(id))
        return render(request, 'inventory/storage/hierarchy_detail.html', {'container': s, 'partstoragesummary': pss, 'parts': parts, 'next': sn.get_container_url(), 'previous': sp.get_container_url()})
    else:
        logger.debug('User was not authenticated and ALLOW_ANONYMOUS is set to False')
        logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
        return redirect('/accounts/login/?next={}'.format(request.path))

# Retrieve all of the Alternate SKUs and display them in a table
def altsku_list(request):
    logger.debug('Request made to altsku_list view')
    if settings.ALLOW_ANONYMOUS or request.user.has_perm(helpers.get_admin_permission('inventory','AlternateSKU','view')):
        # Get all AlternateSKUs
        logger.debug('Generating object_list of AlternateSKU')
        logger2.debug((request.user or 0), 'Request to view AlternateSKU List')
        logmsg = 'Accessed AlternateSku List'
        object_list = AlternateSKU.objects.all()
        if not settings.SHOW_DELETED_OBJECTS:
            object_list = object_list.exclude(sku='deleted')

        # Create the Search filter and apply to the existing results
        query = request.GET.get('q')
        if query != None:
            logmsg = '{} with query \'{}\''.format(logmsg, query)
            logger.debug('Filtering object_list of AlternateSKU with query')
            querytype = request.GET.get('t')
            qt = _parse_querytype(querytype)
            sf = Search().filter(['manufacturer', 'sku'], qt + query)
            object_list = object_list.filter(sf)

        # Create the Paginator and get the current page
        logger.debug('Paginate object_list of AlternateSKU')
        paginator = Paginator(object_list, settings.ALTSKU_PAGE_SIZE)
        page = request.GET.get('page')
        # Try to display the page or catch and process errors
        try:
            alternates = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver the first page
            logger.debug('PageNotAnInteger, show page 1')
            alternates = paginator.page(1)
        except EmptyPage:
            # If page is out of range, deliver the last page
            logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
            alternates = paginator.page(paginator.num_pages)
        # Return the list of alternates
        logger.info('Rendering request for altsku_list view')
        logger2.info((request.user or 0), logmsg)
        return render(request, 'inventory/altsku/list.html', {'query': query, 'page': page, 'alternates': alternates})
    else:
        logger.debug('User was not authenticated and ALLOW_ANONYMOUS is set to False')
        logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
        return redirect('/accounts/login/?next={}'.format(request.path))

# Retrieve all of the Assemblies and display them in a table
def assembly_list(request):
    logger.debug('Request made to assembly_list view')
    if settings.ALLOW_ANONYMOUS or request.user.has_perm(helpers.get_admin_permission('inventory','Assembly','view')):
        # Get all Assemblies
        logger.debug('Generating object_list of Assembly')
        logger2.debug((request.user or 0), 'Request to view Assembly List')
        logmsg = 'Accessed Assembly List'
        objects = list(set(Assembly.objects.values_list('parent', flat=True)))
        object_list = Part.objects.filter(pk__in=objects)
        if not settings.SHOW_DELETED_OBJECTS:
            object_list = object_list.exclude(name='deleted')

        # Create the Search filter and apply to the existing results
        query = request.GET.get('q')
        if query != None:
            logmsg = '{} with query \'{}\''.format(logmsg, query)
            logger.debug('Filtering object_list of Assembly with query')
            querytype = request.GET.get('t')
            qt = _parse_querytype(querytype)
            sf = Search().filter(['name', 'sku', 'description'], qt + query)
            object_list = object_list.filter(sf)

        # Create the Paginator and get the current page
        logger.debug('Paginate object_list of Assembly')
        paginator = Paginator(object_list, settings.ASSEMBLY_PAGE_SIZE)
        page = request.GET.get('page')
        # Try to display the page or catch and process errors
        try:
            assemblies = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver the first page
            logger.debug('PageNotAnInteger, show page 1')
            assemblies = paginator.page(1)
        except EmptyPage:
            # If page is out of range, deliver the last page
            logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
            assemblies = paginator.page(paginator.num_pages)
        # Return the list of alternates
        logger.info('Rendering request for assembly_list view')
        logger2.info((request.user or 0), logmsg)
        return render(request, 'inventory/assembly/list.html', {'query': query, 'page': page, 'assemblies': assemblies})
    else:
        logger.debug('User was not authenticated and ALLOW_ANONYMOUS is set to False')
        logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
        return redirect('/accounts/login/?next={}'.format(request.path))

# Retrieve information about the specified Assembly
def assembly_detail(request, id):
    logger.debug('Request made to assembly_detail view')
    if settings.ALLOW_ANONYMOUS or request.user.has_perm(helpers.get_admin_permission('inventory','Assembly','view')):
        # Get the Assembly object to display in detail
        logger.debug('Getting requested Assembly')
        logger2.debug((request.user or 0), 'Request to view Assembly id {}'.format(id))
        a = get_object_or_404(Part, id=id)
        logger.debug('Getting previous and next Assembly objects')
        ap = Assembly.objects.filter(parent__id__lt=a.id).order_by('-parent__id').first()
        if ap == None:
            ap = Assembly.objects.order_by('-parent__id').first()
        an = Assembly.objects.filter(parent__id__gt=a.id).order_by('parent__id').first()
        if an == None:
            an = Assembly.objects.order_by('parent__id').first()
        logger.info('Rendering request for assembly_detail view')
        logger2.info((request.user or 0), 'Accessed Assembly id {}'.format(id))
        return render(request, 'inventory/assembly/detail.html', {'assembly': a, 'previous': ap.get_absolute_url(), 'next': an.get_absolute_url()})
    else:
        logger.debug('User was not authenticated and ALLOW_ANONYMOUS is set to False')
        logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
        return redirect('/accounts/login/?next={}'.format(request.path))

# Retrieve all of the Assemblies and display them in a table
def kit_list(request):
    cname = 'kit_list'
    logger.debug('Request made to {} view'.format(cname))
    if settings.ALLOW_ANONYMOUS or request.user.has_perm(helpers.get_admin_permission('inventory','Kit','view')):
        # Get all Kits
        logger.debug('Generating object_list of Kit')
        logger2.debug((request.user or 0), 'Request to view Kit list')
        logmsg = 'Accessed Kit List'
        object_list = Kit.objects.all()
        if not settings.SHOW_DELETED_OBJECTS:
            object_list = object_list.exclude(name='deleted')

        # Create the Search filter and apply to the existing results
        query = request.GET.get('q')
        if query != None:
            logmsg = '{} with query \'{}\''.format(logmsg, query)
            logger.debug('Filtering object_list of Assembly with query')
            querytype = request.GET.get('t')
            qt = _parse_querytype(querytype)
            sf = Search().filter(['name', 'sku', 'description'], qt + query)
            object_list = object_list.filter(sf)
            
        # Create the Paginator and get the current page
        logger.debug('Paginate object_list of Kit')
        paginator = Paginator(object_list, settings.KIT_PAGE_SIZE)
        page = request.GET.get('page')
        # Try to display the page or catch and process errors
        try:
            kits = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver the first page
            logger.debug('PageNotAnInteger, show page 1')
            kits = paginator.page(1)
        except EmptyPage:
            # If page is out of range, deliver the last page
            logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
            kits = paginator.page(paginator.num_pages)
        # Return the list of alternates
        logger.info('Rendering request for {} view'.format(cname))
        logger2.info((request.user or 0), logmsg)
        return render(request, 'inventory/kit/list.html', {'query': query, 'page': page, 'kits': kits})
    else:
        logger.debug('User was not authenticated and ALLOW_ANONYMOUS is set to False')
        logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
        return redirect('/accounts/login/?next={}'.format(request.path))

def kit_detail(request, id):
    cname = 'kit_detail'
    logger.debug('Request made to {} view'.format(cname))
    if settings.ALLOW_ANONYMOUS or request.user.has_perm(helpers.get_admin_permission('inventory','Kit','view')):
        # Get the Kit object to display in detail
        logger.debug('Getting requested Kit')
        logger2.debug((request.user or 0), 'Request to view Kit id {}'.format(id))
        k = get_object_or_404(Kit, id=id)
        logger.debug('Getting previous and next Kit objects')
        kp = Kit.objects.filter(id__lt=k.id).order_by('-id').first()
        if kp == None:
            kp = Kit.objects.order_by('-id').first()
        kn = Kit.objects.filter(id__gt=k.id).order_by('id').first()
        if kn == None:
            kn = Kit.objects.order_by('id').first()
        # Return the Storage Details
        logger.info('Rendering request for {} view'.format(cname))
        logger2.info((request.user or 0), 'Accessed Kit id {}'.format(id))
        return render(request, 'inventory/kit/detail.html', {'kit': k, 'next': kn.get_absolute_url(), 'previous': kp.get_absolute_url()})
    else:
        logger.debug('User was not authenticated and ALLOW_ANONYMOUS is set to False')
        logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
        return redirect('/accounts/login/?next={}'.format(request.path))

def ajax_kit_subtract(request, id, inc):
    cname = 'ajax_kit_subtract'
    logger.debug('Request made to {} view'.format(cname))
    if request.user.has_perm(helpers.get_admin_permission('inventory','Kit','decrement')):
        logger.debug('Getting requested Kit')
        logger2.debug((request.user or 0), 'Request to subtract {} from Kit id {}'.format(inc, id))
        if not request.user.has_perm('inventory.decrement_kit'):
            logger.debug('User does not have permissions to {}'.format(cname))
            logger2.warning(request.user, 'Access Denied for Request to {}'.format(cname))
            return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Access is denied'})
        kit = get_object_or_404(Kit, id=id)
        try:
            logger.debug('Subtracting {} for Kit id {}'.format(inc, kit.id))
            kit.subtract(inc)
            logger.info('Rendering request for {} view'.format(cname))
            logger2.info((request.user or 0), 'Subtracted {} from Kit id {}; current count: {}'.format(inc, id, kit.get_available_count()))
            return render(request, 'inventory/ajax_result.html', {'color': 'green', 'result': 'Update successful'})
        except ValueError:
            logger.info('Unable to subtract {} for Kit id {}'.format(inc, kit.id))
            logger2.warning((request.user or 0), 'Insufficient parts to subtract {} from Kit id {}'.format(inc, id))
            return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Insufficient parts'})
        except Exception as err:
            logger.error(err)
            logger2.error((request.user or 0), err)
            return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Unknown error'})
    logger.debug('Access was denied to {} view'.format(cname))
    logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
    return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Access is denied'})


def ajax_kit_add(request, id, inc):
    cname = 'ajax_kit_add'
    logger.debug('Request made to {} view'.format(cname))
    if request.user.has_perm(helpers.get_admin_permission('inventory','Kit','increment')):
        logger.debug('Getting requested Kit')
        logger2.debug((request.user or 0), 'Request to add {} to Kit id {}'.format(inc, id))
        if not request.user.has_perm('inventory.increment_kit'):
            logger.debug('User does not have permissions to {}'.format(cname))
            logger2.warning(request.user, 'Access Denied for Request to {}'.format(cname))
            return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Access is denied'})
        kit = get_object_or_404(Kit, id=id)
        try:
            logger.debug('Adding {} for Kit id {}'.format(inc, kit.id))
            kit.add(inc)
            logger.info('Rendering request for {} view'.format(cname))
            logger2.info((request.user or 0), 'Subtracted {} from Kit id {}; current count: {}'.format(inc, id, kit.get_available_count()))
            return render(request, 'inventory/ajax_result.html', {'color': 'green', 'result': 'Update successful'})
        except Exception as err:
            logger.error(err)
            logger2.error((request.user or 0), err)
            return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Unknown error'})
    logger.debug('Access was denied to {} view'.format(cname))
    logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
    return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Access is denied'})

def ajax_kit_update(request, id, section):
    cname = 'ajax_kit_update'
    logger.debug('Request made to {} view'.format(cname))
    if settings.ALLOW_ANONYMOUS or request.user.has_perm(helpers.get_admin_permission('inventory','Kit','view')):
        logger2.debug((request.user or 0), 'Request to retrieve {} updates for Kit id {}'.format(section, id))
        kit = get_object_or_404(Kit, id=id)
        logger2.info((request.user or 0), 'Accessed {} updates for Kit id {}'.format(section, id))
        return render(request, 'inventory/ajax_result2.html', {'type': section, 'kit': kit})
    logger.debug('Access was denied to {} view'.format(cname))
    logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
    raise PermissionDenied

def ajax_partstorage_add(request, id, inc):
    cname = 'ajax_partstorage_add'
    logger.debug('Request made to {} view'.format(cname))
    if request.user.has_perm(helpers.get_admin_permission('inventory','PartStorage','increment')):
        logger2.debug((request.user or 0), 'Request to add {} to PartStorage id {}'.format(inc, id))
        if not request.user.has_perm('inventory.increment_partstorage'):
            logger.debug('User does not have permissions to {}'.format(cname))
            logger2.warning(request.user, 'Access Denied for Request to {}'.format(cname))
            return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Access is denied'})
        ps = get_object_or_404(PartStorage, id=id)
        try:
            logger.debug('Adding {} for PartStorage id {}'.format(inc, ps.id))
            ps.add(inc)
            logger.info('Rendering request for {} view'.format(cname))
            logger2.info((request.user or 0), 'Added {} to PartStorage id {}; current count: {}'.format(inc, id, ps.count))
            return render(request, 'inventory/ajax_result.html', {'color': 'green', 'result': 'Update successful'})
        except Exception as err:
            logger.info('Unable to add {} for PartStorage id {}'.format(inc, ps.id))
            logger2.error(request.user, err)
            logger2.warning((request.user or 0), 'Unable to add {} to PartStorage id {}'.format(inc, id))
            return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count.'})
    logger.debug('Access was denied to {} view'.format(cname))
    logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
    return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Access is denied'})

def ajax_partstorage_sub(request, id, inc):
    cname = 'ajax_partstorage_sub'
    logger.debug('Request made to {} view'.format(cname))
    if request.user.has_perm(helpers.get_admin_permission('inventory','PartStorage','decrement')):
        logger2.debug((request.user or 0), 'Request to subtract {} to PartStorage id {}'.format(inc, id))
        if not request.user.has_perm('inventory.decrement_partstorage'):
            logger.debug('User does not have permissions to {}'.format(cname))
            logger2.warning(request.user, 'Access Denied for Request to {}'.format(cname))
            return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Access is denied'})
        ps = get_object_or_404(PartStorage, id=id)
        try:
            logger.debug('Subtracting {} for PartStorage id {}'.format(inc, ps.id))
            ps.subtract(inc)
            logger.info('Rendering request for {} view'.format(cname))
            logger2.info((request.user or 0), 'Subtracted {} from PartStorage id {}; current count: {}'.format(inc, id, ps.count))
            return render(request, 'inventory/ajax_result.html', {'color': 'green', 'result': 'Update successful'})
        except ValueError:
            logger.info('Unable to subtract {} for Kit id {}'.format(inc, ps.id))
            logger2.warning((request.user or 0), 'Insufficient parts to subtract {} from PartStorage id {}'.format(inc, id))
            return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Insufficient parts'})
        except Exception as err:
            logger.error(err)
            logger2.error((request.user or 0), err)
            return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Unknown error'})
    logger.debug('Access was denied to {} view'.format(cname))
    logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
    return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Access is denied'})

def ajax_partstorage_update(request, id, section):
    cname = 'ajax_partstorage_update'
    logger.debug('Request made to {} view'.format(cname))
    if settings.ALLOW_ANONYMOUS or request.user.has_perm(helpers.get_admin_permission('inventory','PartStorage','view')):
        logger2.debug((request.user or 0), 'Request to retrieve {} updates for PartStorage id {}'.format(section, id))
        ps = get_object_or_404(PartStorage, id=id)
        logger2.info((request.user or 0), 'Accessed {} updates for PartStorage id {}'.format(section, id))
        return render(request, 'inventory/ajax_result2.html', {'type': section, 'ps': ps})
    logger.debug('Access was denied to {} view'.format(cname))
    logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
    raise PermissionDenied

def search(request):
    logger.debug('Request made to search view')
    if settings.ALLOW_ANONYMOUS or request.user.is_authenticated:
        logger2.debug((request.user or 0), 'Request to Search page')
        search_form = ModelSearchForm(data=request.GET)
        if search_form.is_valid():
            logger.debug('Generating search parameters')
            querytype = _parse_querytype(request.GET.get('t'))
            objecttypes = request.GET.getlist('m')
            terms = request.GET.get('q')
            raw = ''
            if 'part' in objecttypes and (settings.ALLOW_ANONYMOUS or request.user.has_perm(helpers.get_admin_permission('inventory','Part','view'))):
                logger.debug('Searching for Part')
                sf = Search().filter([querytype + 'name', querytype + 'sku', querytype + 'description'], terms)
                raw = chain(raw, Part.objects.filter(sf))
            if 'assembly' in objecttypes and (settings.ALLOW_ANONYMOUS or request.user.has_perm(helpers.get_admin_permission('inventory','Assembly','view'))):
                logger.debug('Searching for Assembly')
                sf = Search().filter([querytype + 'name', querytype + 'sku', querytype + 'description'], terms)
                assemblies = Assembly.objects.all()
                ids = []
                for i in range(assemblies.count()):
                    j = assemblies[i].parent_id
                    if j not in ids:
                        ids.append(j)
                raw = chain(raw, Part.objects.filter(id__in=ids).filter(sf))
            if 'kit' in objecttypes and (settings.ALLOW_ANONYMOUS or request.user.has_perm(helpers.get_admin_permission('inventory','Kit','view'))):
                logger.debug('Searching for Kit')
                sf = Search().filter([querytype + 'name', querytype + 'sku', querytype + 'description'], terms)
                raw = chain(raw, Kit.objects.filter(sf))
            if 'alternatesku' in objecttypes and (settings.ALLOW_ANONYMOUS or request.user.has_perm(helpers.get_admin_permission('inventory','AlternateSKU','view'))):
                logger.debug('Searching for AlternateSKU')
                sf = Search().filter([querytype + 'sku', querytype + 'manufacturer'], terms)
                raw = chain(raw, AlternateSKU.objects.filter(sf))
            if 'storage' in objecttypes and (settings.ALLOW_ANONYMOUS or request.user.has_perm(helpers.get_admin_permission('inventory','Storage','view'))):
                logger.debug('Searching for Storage')
                sf = Search().filter([querytype + 'name', querytype + 'description'], terms)
                raw = chain(raw, Storage.objects.filter(sf))
            logger.debug('Removing duplicates and sorting results')
            unique = set(raw)
            results = sorted(list(unique), key=lambda r: str(r))
            logger.info('Rendering request for search view')
            logger2.info((request.user or 0), 'Accessed Search results of objects {} for query {}'.format(','.join(objecttypes), terms))
            return render(request, 'inventory/search.html', {'search_form': search_form, 'results': results})
        else:
            logger.info('Generating new SearchForm and Rendering request for search view')
            logger2.info((request.user or 0), 'Accessed Search page')
            search_form = ModelSearchForm()
            return render(request, 'inventory/search.html', {'search_form': search_form})
    else:
        logger.debug('User was not authenticated and ALLOW_ANONYMOUS is set to False')
        logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
        return redirect('/accounts/login/?next={}'.format(request.path))


# Retrieve information to display About page
@login_required()
def about_ims(request):
    logger.debug('Request made to about_ims view')
    if request.user.is_staff:
        logger2.debug((request.user or 0), 'Request to view About page')
        buildnumber = '{} build {}'.format(settings.IMS_VERSION, settings.IMS_BUILD)
        pip_list = []
        logger.debug('Generating pip_list')
        for i in settings.INSTALLED_ADDONS:
            pip_list.append('IMS Addon|{}=={}-{}'.format(i['name'], i['version'], i['build']))
        for i in freeze(local_only=True):
            pip_list.append(i)
        for i in settings.THIRD_PARTY_LIBRARIES:
            pip_list.append('{}|{}=={}'.format(i['vendor'], i['package'], i['version']))
        logger.info('Rendering request for about_ims view')
        logger2.info((request.user or 0), 'Accessed About page')
        return render(request, 'inventory/about.html', {'buildnumber': buildnumber, 'pip_list': pip_list})
    logger.debug('User is not authorized to view the about page. Redirecting to home.')
    logger2.warning(0, 'Unauthorized request to {}'.format(request.path))
    return redirect('/inventory/')

def ajax_theme(request):
    cname = 'ajax_theme'
    logger.debug('Request made to {} view'.format(cname))
    if request.user.is_staff:
        logger2.debug(request.user, 'Request to toggle the theme of the UI')
        logger2.info(request.user, 'Toggled the UI Theme')
        if settings.THEME_CHOICE.lower() == 'dark':
            settings.THEME_CHOICE = 'Default'
        else:
            settings.THEME_CHOICE = 'Dark'
        return render(request, 'inventory/ajax_result.html', {'result': 'Theme toggled to {}'.format(settings.THEME_CHOICE), 'color': 'green'})
    logger.debug('Access was denied to {} view'.format(cname))
    logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
    return render(request, 'inventory/ajax_result.html', {'result': 'Access Denied', 'color': 'red'})

def _parse_querytype(value):
    if value == 1 or value == '1':
        return "^"
    if value == 2 or value == '2':
        return "="
    return ""

def _child_storage_loop(storage, child_ids = []):
    # infinite loop protection counter
    counter = 0
    for child in storage.get_children():
        if child.id not in child_ids:
            child_ids.append(child.id)
            counter += 1
    if counter == 0:
        # possible loop, just return
        return child_ids
    for child in storage.get_children():
        subchild_ids = _child_storage_loop(child, child_ids)
        if subchild_ids != child_ids:
            for subchild in subchild_ids:
                if subchild not in child_ids:
                    child_ids.append(subchild)
    return child_ids
