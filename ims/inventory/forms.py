from django import forms

SEARCH_CHOICES =(
    ("3","Contains"),
    ("2","Exact"),
    ("1","Starts With"),
)

MODEL_CHOICES =(
    ("assembly","Assembly"),
    ("kit","Kit"),
    ("part","Part"),
    ("storage","Storage"),
    ("alternatesku","AlternateSKU"),
)

class ModelSearchForm(forms.Form):
    q = forms.CharField(max_length=100)
    m = forms.MultipleChoiceField(choices = MODEL_CHOICES, widget = forms.CheckboxSelectMultiple)
    t = forms.ChoiceField(choices = SEARCH_CHOICES, widget = forms.RadioSelect)

