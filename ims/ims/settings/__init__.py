import os
import environ

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
envroot = environ.Path(BASE_DIR)
env = environ.Env()
environ.Env.read_env(os.path.join(BASE_DIR, '.env'))
###

# SECURITY WARNING: keep the secret key used in production secret!
# This key is automatically generated on installation
SECRET_KEY = env.str('DJANGO_SECRET_KEY', default='f7ae8ab2-a319-4268-ad0f-6f8c7586ff6a')
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env.bool('DEBUG', default=False)
DEVENV = env.bool('DEVENV', default=False)
# SECURITY SETTINGS
SECURE_BROWSER_XSS_FILTER = True
SECURE_CONTENT_TYPE_NOSNIFF = True
if DEBUG:
    SESSION_COOKIE_SECURE = False
    CSRF_COOKIE_SECURE = False
else:
    SESSION_COOKIE_SECURE = True
    CSRF_COOKIE_SECURE = True
X_FRAME_OPTIONS = 'DENY'
LOCAL_HOSTNAME = env.str('LOCAL_HOSTNAME', default='localhost')
LOCAL_IPADDR = env.str('LOCAL_IPADDR', default='127.0.0.1')
CSRF_TRUSTED_ORIGINS = ['http://localhost','https://localhost','https://{}'.format(LOCAL_HOSTNAME),'http://{}'.format(LOCAL_HOSTNAME),'https://{}'.format(LOCAL_IPADDR),'http://{}'.format(LOCAL_IPADDR)]
ALLOWED_HOSTS = env.list('ALLOWED_HOSTS', default=['10.0.0.0/8','172.16.0.0/12','192.168.0.0/16','localhost'])
ALLOWED_HOSTS += [LOCAL_HOSTNAME,LOCAL_IPADDR]
###

# Application definition
IMS_VERSION = '3.3.4'
IMS_BUILD = '250131'
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'accounts',
    'inventory',
    'log',
    'django_filters',
    'rest_framework',
    'rest_framework.authtoken',
    'mfa',
]
if env.list('INSTALLED_APPS', default=[]) != []:
    for app in env.list('INSTALLED_APPS'):
        INSTALLED_APPS.append(app)
THIRD_PARTY_LIBRARIES = [
    {'vendor': 'jquery', 'package': 'jquery.js', 'version': '3.7.1'},
    {'vendor': 'paroga', 'package': 'cbor-js.js', 'version': '0.1.0'},
]
if env.list('THIRD_PARTY_LIBRARIES', default=[]) != []:
    for lib in env.list('THIRD_PARTY_LIBRARIES'):
        THIRD_PARTY_LIBRARIES.append({k:v for k,v in map(lambda l: l.split('='), lib.split(';'))})
###

# Django Base Configuration
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
if env.list('MIDDLEWARE', default=[]) != []:
    for mid in env.list('MIDDLEWARE'):
        MIDDLEWARE.append(mid)
ROOT_URLCONF = 'ims.urls'
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates'),os.path.join(BASE_DIR, 'media')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
WSGI_APPLICATION = 'ims.wsgi.application'
AUTHENTICATION_BACKENDS = (
    ('django.contrib.auth.backends.ModelBackend'),
)
###

# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
###

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
    '/static/',
]
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, "media")
###

# Multi-Factor Authentication
MFA_ENABLED = env.bool('MFA_ENABLED', default=True)
MFA_DOMAIN = env.str('MFA_DOMAIN', default='localhost.local')
MFA_SITE_TITLE = env.str('MFA_SITE_TITLE', default='IMS')
MFA_ENFORCE = env.bool('MFA_ENFORCE', default=True)
###


# --- Rest Configuration ---
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'accounts.authentication.ImsTokenAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ],
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
    ),
}
###

# Database
DATABASES = {
    'default': env.db_url('IMS_DATABASE', default='sqlite:////{}'.format(os.path.join(BASE_DIR, 'db.sqlite3')))
}
###

# Logging Settings
LOG_DIR = env.str('LOG_DIR', default='/var/log/zc/ims')
if DEVENV:
    LOG_DIR = os.path.join(BASE_DIR, 'logs')
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '{asctime} - {levelname} - {name} - {message}',
            'style': '{',
        },
    },
    'handlers': {
        'file': env.dict('LOG_HANDLERS_FILE', {'value':str,'cast':{'maxBytes':int,'backupCount':int}}, default={
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(LOG_DIR, 'ims-default.log'),
            'maxBytes': 1024*1024*5, # 5MB
            'backupCount': 3,
            'formatter': 'standard',
        }),
    },
    'loggers': {
        '': {
            'handlers': ['file'],
            'level': env.str('LOG_LOGGERS_DEFAULT_LEVEL', default='INFO'),
            'propagate': True,
        },
    },
}
INSTALL_LOG_PATH = os.path.join(LOG_DIR, '/install.log')
###

# Email Settings
EMAIL_BACKEND = env.str('EMAIL_BACKEND', default='django.core.mail.backends.filebased.EmailBackend')
EMAIL_FILE_PATH = env.str('EMAIL_FILE_PATH', default=os.path.join(BASE_DIR, 'logs/email/'))
EMAIL_USE_TLS = env.bool('EMAIL_USE_TLS', default=True)
EMAIL_HOST = env.str('EMAIL_HOST', default='')
EMAIL_HOST_USER = env.str('EMAIL_HOST_USER', default='')
EMAIL_HOST_PASSWORD = env.str('EMAIL_HOST_PASSWORD', default='')
EMAIL_PORT = env.int('EMAIL_PORT', default=0)
EMAIL_DEFAULT_SUBJECT = env.str('EMAIL_DEFAULT_SUBJECT', default='IMS Notification')
DEFAULT_FROM_EMAIL = env.str('DEFAULT_FROM_EMAIL', default='support@zephyruscomputing.com')
###

# Addons
INSTALLED_ADDONS = []
if env.list('INSTALLED_ADDONS', default=[]) != []:
    for addon in env.list('INSTALLED_ADDONS'):
        INSTALLED_ADDONS.append({k:v for k,v in map(lambda a: a.split('='), addon.split(';'))})
###

# Internationalization
LANGUAGE_CODE = env.str('LANGUAGE_CODE', default='en-us')
TIME_ZONE = env.str('TIME_ZONE', default='UTC')
USE_I18N = env.bool('USE_I18N', default=True)
USE_L10N = env.bool('USE_L10N', default=True)
USE_TZ = env.bool('USE_TZ', default=True)
###

# IMS Settings
ALLOW_ANONYMOUS = env.bool('ALLOW_ANONYMOUS', default=False)
ALLOW_USER_API_KEYS = env.bool('ALLOW_USER_API_KEYS', default=False)
ALLOW_USER_EDIT_PROFILE = env.bool('ALLOW_USER_EDIT_PROFILE', default=False)
ALLOW_USER_PASSWORD_RESET = env.bool('ALLOW_USER_PASSWORD_RESET', default=False)
ALLOW_USER_SIGN_UP = env.bool('ALLOW_USER_SIGN_UP', default=False)
USER_PASSWORDS_EXPIRE = env.bool('USER_PASSWORDS_EXPIRE', default=True)
MAX_PASSWORD_AGE = env.int('MAX_PASSWORD_AGE', default=60)
BAD_PASSWORD_ACCOUNT_LOCKOUT = env.bool('BAD_PASSWORD_ACCOUNT_LOCKOUT', default=True)
BAD_PASSWORD_ATTEMPTS = env.int('BAD_PASSWORD_ATTEMPTS', default=3)
SHOW_DELETED_OBJECTS = env.bool('SHOW_DELETED_OBJECTS', default=False)
DEFAULT_PAGE_SIZE = env.int('DEFAULT_PAGE_SIZE', default=20)
DEFAULT_API_RESULTS = env.int('DEFAULT_API_RESULTS', default=DEFAULT_PAGE_SIZE * 5)
DEFAULT_API_KEY_EXPIRATION = env.int('DEFAULT_API_KEY_EXPIRATION', default=60)
STORAGE_PAGE_SIZE = env.int('STORAGE_PAGE_SIZE', default=DEFAULT_PAGE_SIZE)
PART_PAGE_SIZE = env.int('PART_PAGE_SIZE', default=DEFAULT_PAGE_SIZE)
ASSEMBLY_PAGE_SIZE = env.int('ASSEMBLY_PAGE_SIZE', default=DEFAULT_PAGE_SIZE)
ALTSKU_PAGE_SIZE = env.int('ALTSKU_PAGE_SIZE', default=DEFAULT_PAGE_SIZE)
KIT_PAGE_SIZE = env.int('KIT_PAGE_SIZE', default=DEFAULT_PAGE_SIZE)
LOG_PAGE_SIZE = env.int('LOG_PAGE_SIZE', default=DEFAULT_PAGE_SIZE)
ADMIN_PAGE_SIZE = env.int('ADMIN_PAGE_SIZE', default=DEFAULT_PAGE_SIZE * 4)
PAGINATION_STYLE2 = env.int('PAGINATION_STYLE2', default=5)
PAGINATION_STYLE3 = env.int('PAGINATION_STYLE3', default=10)
PAGINATION_OVERRIDE = env.int('PAGINATION_OVERRIDE', default=0)
LOGGING_LEVEL = env.str('LOGGING_LEVEL', default='INFO')
THEME_CHOICE = env.str('THEME_CHOICE', default='default')
###

def update_settings(from_dict, to_dict):
    for (key, value) in from_dict.items():
        if key in to_dict.keys() and isinstance(to_dict[key], dict) and isinstance(value, dict):
            update_settings(value, to_dict[key])
        else:
            to_dict[key] = value

if env.list('INSTALLED_ADDONS', default=[]) != []:
    current = __name__
    for addon in env.list('INSTALLED_ADDONS'):
        dictionary = {k:v for k,v in map(lambda a: a.split('='), addon.split(';'))}
        name = '{}_settings'.format(dictionary['name'].lower())
        module  = getattr(__import__(current, globals(), locals(), [name]), name)
        current_settings = {}
        for setting in dir(module):
            if setting == setting.upper():
                current_settings[setting] = getattr(module, setting)
        update_settings(current_settings, locals())

if USER_PASSWORDS_EXPIRE:
    MIDDLEWARE.append('accounts.middleware.PasswordExpirationMiddleware')