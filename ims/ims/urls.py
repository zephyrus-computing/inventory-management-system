"""
Definition of urls for ims.
"""

from django.contrib import admin
from django.contrib.auth.views import PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView, PasswordResetCompleteView, PasswordChangeView, PasswordChangeDoneView
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path, re_path
from django.conf import settings
from django.conf.urls.static import static
from mfa.views import LoginView
from . import views
import re

urlpatterns = [
    path('mfa/', include('mfa.urls', namespace='mfa')),
    path('accounts/admin/', include('accounts.admin.urls')),
    path('accounts/password_change/', PasswordChangeView.as_view(), name='password_change'),
    path('accounts/password_change/done/', PasswordChangeDoneView.as_view(), name='password_change_done'),
    path('accounts/password_reset/', PasswordResetView.as_view(), name='password_reset'),
    path('accounts/password_reset/done/', PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('accounts/reset/<uidb64>/<token>/', PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('accounts/reset/done/', PasswordResetCompleteView.as_view(), name='password_reset_complete'),
    path('accounts/', include('accounts.urls')),
    path('admin/login/', LoginView.as_view()),
    path('admin/', admin.site.urls),
    path('inventory/admin/', include('inventory.admin.urls')),
    path('inventory/', include('inventory.urls')),
    path('logs/', include('log.urls')),
    path('api/v1/accounts/', include('accounts.api.urls')),
    path('api/v1/inventory/', include('inventory.api.urls')),
    path('api/v1/log/', include('log.api.urls')),
    path('api-auth/', include('rest_framework.urls')),
    re_path(r'^$', views.root_view, name='redirect'),
]

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

regex = re.compile(r'ims.addons.*')
addons = [i for i in settings.INSTALLED_APPS if regex.match(i)]

for module in addons:
    name = module.replace('ims.addons.','')
    for line in settings.INSTALLED_ADDONS:
        if line['name'].lower() == name:
            if line['enabled']:
                if 'api_url' in line:
                    urlpatterns.append(path(line['api_url'], include('{}.api.urls'.format(module))))
                if 'admin_url' in line:
                    urlpatterns.append(path(line['admin_url'], include('{}.admin.urls'.format(module))))
                if 'url' in line:
                    urlpatterns.append(path(line['url'], include('{}.urls'.format(module))))
                if 'other_urls' in line:
                    array = line['other_urls'].split('|')
                    if len(array)%2 == 0:
                        i=0
                        while i < len(array):
                            urlpatterns.append(path(array[i].split(':')[1], include(array[i+1].split(':')[1])))
                            i+=2
