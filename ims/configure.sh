#! /bin/bash
folder=$(dirname $0); # Should be /etc/zc/ims/
# Find the .env file
if [[ -f "$folder/.env" ]]; then
    envfile="$folder/.env";
else
    echo "ERROR: IMS .env File Not Found!";
    exit 1;
fi
if [[ $(grep -e "LOG_DIR" $envfile) ]]; then
    logfile=$(echo $(grep -e "LOG_DIR" $envfile | cut -d'=' -f2 | sed -e "s/\/$//" -e "s/'//g")"/install.log");
else
    logfile="/var/log/zc/ims/install.log";
fi
if [[ ! -d $(dirname $logfile) ]]; then
    [[ -d "$folder/logs/" ]] && logfile="$folder/logs/install.log" || logfile="$folder/install.log";
fi
if [ ! $1 ]; then
    main="Please make a selection: ";
    PS3=$main;
    options=("database" "webserver" "service" "superuser" "support" "mfa" "addons" "email" "quit");
    select opt in "${options[@]}";
    do
        case $opt in
            "database")
                PS3="Please select a supported database: ";
                suboptions=("postgresql" "mariadb" "sqlite3" "back");
                select sub in "${suboptions[@]}";
                do
                    case $sub in
                        "postgresql")
                            ${folder}/scripts/configure_postgresql.sh;
                            PS3=$main;
                            break;
                            ;;
                        "mariadb")
                            ${folder}/scripts/configure_mariadb.sh;
                            PS3=$main;
                            break;
                            ;;
                        "sqlite3")
                            ${folder}/scripts/configure_sqlite3.sh;
                            PS3=$main;
                            break;
                            ;;
                        "back")
                            PS3=$main;
                            break;
                            ;;
                    esac
                done
                ;;
            "webserver")
                PS3="Please select a supported web server: ";
                suboptions=("nginx" "apache2" "back");
                select sub in "${suboptions[@]}";
                do
                    case $sub in
                        "nginx")
                            ${folder}/scripts/configure_nginx.sh;
                            PS3=$main;
                            break;
                            ;;
                        "apache2")
                            ${folder}/scripts/configure_apache2.sh;
                            PS3=$main;
                            break;
                            ;;
                        "back")
                            PS3=$main;
                            break;
                            ;;
                    esac
                done
                ;;
            "service")
                PS3="Please select an action: ";
                suboptions=("start" "stop" "restart" "status" "back");
                select sub in "${suboptions[@]}";
                do
                    case $sub in
                        "start")
                            systemctl start ims.socket;
                            systemctl start ims.service;
                            ;;
                        "stop")
                            systemctl stop ims.service;
                            systemctl stop ims.socket;
                            ;;
                        "restart")
                            systemctl stop ims.service;
                            systemctl stop ims.socket;
                            systemctl start ims.socket;
                            systemctl start ims.service;
                            ;;
                        "status")
                            systemctl status ims.socket;
                            systemctl status ims.service;
                            ;;
                        "back")
                            PS3=$main;
                            break;
                            ;;
                    esac
                done
                ;;
            "addons")
                PS3="Please select a module: ";
                suboptions=();
                if [ $(ls ${folder}/ims/addons/ | wc -l) -gt 1 ]; then
                    dirs=$(ls -d ${folder}/ims/addons/*/);
                    for item in $dirs;
                    do
                        name=$(echo $item | cut -d'/' -f7);
                        if [ $name != '__pycache__' ]; then
                            suboptions+=("$name");
                        fi
                    done
                fi
                suboptions+=("back");
                select sub in "${suboptions[@]}";
                do
                    case $sub in
                        "back")
                            PS3=$main;
                            break;
                            ;;
                        *)
                            if [[ " ${suboptions[*]} " =~ " ${sub} " ]]; then
                                if [ ! -f "${folder}/scripts/configure_$sub.sh" ]; then
                                    echo "Missing required configure script for $sub.";
                                else
                                    echo "$(date)" >> $logfile;
                                    echo "Excuting configure $sub command..." >> $logfile;
                                    ${folder}/scripts/configure_$sub.sh;
                                fi
                            fi
                            ;;
                    esac
                done
                ;;
            "superuser")
                echo "$(date)" >> $logfile;
                echo "Excuting superuser command..." >> $logfile;
                su -c "source ${folder}/venv/bin/activate; python ${folder}/manage.py createsuperuser; deactivate;" imsuser;
                ;;
            "support")
                PS3="Please select an option: ";
                suboptions=("package" "relationship" "back");
                select sub in "${suboptions[@]}";
                do
                    case $sub in
                        "package")
                            echo "$(date)" >> $logfile;
                            echo "Excuting support package command..." >> $logfile;
                            su -c "${folder}/scripts/create_support_bundle.sh;" imsuser;
                            ;;
                        "relationship")
                            echo "$(date)" >> $logfile;
                            echo "Excuting support relationship command..." >> $logfile;
                            su -c "${folder}/scripts/get_stale_relationships.sh;" imsuser;
                            ;;
                        "back")
                            PS3=$main;
                            break;
                            ;;
                    esac
                done
                ;;
            "mfa")
                echo "$(date)" >> $logfile;
                echo "Excuting configure mfa command..." >> $logfile;
                su -c "${folder}/scripts/configure_mfa.sh;" imsuser;
                ;;
            "email")
                echo "$(date)" >> $logfile;
                echo "Excuting configure email command..." >> $logfile;
                su -c "${folder}/scripts/configure_email.sh;" imsuser;
                ;;
            "quit")
                exit;
                ;;
        esac
    done
else
    list=(database webserver service addons mfa superuser email support);
    if [[ ! " ${list[*]} " =~ " ${1} " ]]; then
        echo "Invalid option.";
        echo "$0 [${list[*]}]";
        exit;
    fi
    if [ $1 == "database" ]; then
        list=(postgresql mariadb sqlite3);
        if [ ! $2 ]; then
            echo "Missing required argument.";
            echo "$0 $1 [${list[*]}]";
        else
            if [[ " ${list[*]} " =~ " ${2} " ]]; then
                if [ $2 == "postgresql" ]; then
                    shift 2;
                    ${folder}/scripts/configure_postgresql.sh $@;
                    exit;
                fi
                if [ $2 == "mariadb" ]; then
                    shift 2;
                    ${folder}/scripts/configure_mariadb.sh $@;
                    exit;
                fi
                if [ $2 == "sqlite3" ]; then
                    ${folder}/scripts/configure_sqlite3.sh;
                fi
            else
                echo "Invalid argument.";
                echo "$0 $1 [${list[*]}]";
            fi
        fi
        exit;
    fi
    if [ $1 == "webserver" ]; then
        list=(apache2 nginx);
        if [ ! $2 ]; then
            echo "Missing required argument.";
            echo "$0 $1 [${list[*]}]";
        else
            if [[ " ${list[*]} " =~ " ${2} " ]]; then
                if [ $2 == "apache2" ]; then
                    shift 2;
                    ${folder}/scripts/configure_apache2.sh $@;
                    exit;
                fi
                if [ $2 == "nginx" ]; then
                    shift 2;
                    ${folder}/scripts/configure_nginx.sh $@;
                    exit;
                fi
            else
                echo "Invalid argument.";
                echo "$0 $1 [${list[*]}]";
            fi
        fi
        exit;
    fi
    if [ $1 == "service" ]; then
        list=(start stop restart status);
        if [ ! $2 ]; then
            echo "Missing required argument.";
            echo "$0 $1 [${list[*]}]";
        else
            if [[ " ${list[*]} " =~ " ${2} " ]]; then
                if [ $2 == "start" ]; then
                    systemctl start ims.socket;
                    systemctl start ims.service;
                fi
                if [ $2 == "stop" ]; then
                    systemctl stop ims.service;
                    systemctl stop ims.socket;
                fi
                if [ $2 == "restart" ]; then
                    systemctl stop ims.service;
                    systemctl stop ims.socket;
                    systemctl start ims.socket;
                    systemctl start ims.service;
                fi
                if [ $2 == "status" ]; then
                    systemctl status ims.socket;
                    systemctl status ims.service;
                fi
            else
                echo "Invalid argument.";
                echo "$0 $1 [${list[*]}]";
            fi
        fi	   
        exit;
    fi
    if [ $1 == "addons" ]; then
        addons=();
        if [ $(ls ${folder}/ims/addons/ | wc -l) -gt 1 ]; then
            dirs=$(ls -d ${folder}/ims/addons/*/);
            for item in $dirs;
            do
                name=$(echo $item | cut -d'/' -f7);
                if [ $name != '__pycache__' ]; then
                    addons+=("$name");
                fi
            done
        fi
        if [ ! $2 ]; then
            echo "Missing required argument.";
            echo "$0 $1 [${addons[*]}]";
        fi
        if [ ! -f "${folder}/scripts/configure_$2.sh" ]; then
            echo "Missing required configure script for $2.";
            exit;
        fi
        echo "$(date)" >> $logfile;
        echo "Executing $2 command..." >> $logfile;
        local addon=$2;
        shift 2;
        ${folder}/scripts/configure_$addon.sh $@;
        exit;
    fi
    if [ $1 == "superuser" ]; then
        echo "$(date)" >> $logfile;
        echo "Excuting superuser command..." >> $logfile;
        if [ ! $3 ]; then
            su -c "source ${folder}/venv/bin/activate; python ${folder}/manage.py createsuperuser; deactivate;" imsuser;
        else
            su -c "source ${folder}/venv/bin/activate; python ${folder}/manage.py createsuperuser --username $2 --email $3; deactivate;" imsuser;
        fi
        exit;
    fi
    if [ $1 == "support" ]; then
        list=(package relationship);
        if [ ! $2 ]; then
            echo "Missing required argument.";
            echo "$0 $1 [${list[*]}]";
        else
            if [[ " ${list[*]} " =~ " ${2} " ]]; then
                if [ $2 == "package" ]; then
                    echo "$(date)" >> $logfile;
                    echo "Excuting support package command..." >> $logfile;
                    su -c "${folder}/scripts/create_support_bundle.sh;" imsuser;
                fi
                if [ $2 == "relationship" ]; then
                    echo "$(date)" >> $logfile;
                    echo "Excuting support relationship command..." >> $logfile;
                    su -c "${folder}/scripts/get_stale_relationships.sh;" imsuser;
                fi
            else
                echo "Invalid argument.";
                echo "$0 $1 [${list[*]}]";
            fi
        fi
        exit;
    fi
    if [ $1 == "mfa" ]; then
        echo "$(date)" >> $logfile;
        echo "Excuting configure mfa command..." >> $logfile;
        shift 1;
        su -c "${folder}/scripts/configure_mfa.sh $@;" imsuser;
        exit;
    fi
    if [ $1 == "email" ]; then
        echo "$(date)" >> $logfile;
        echo "Executing configure email command..." >> $logfile;
        shift 1;
        su -c "${folder}/scripts/configure_email.sh $@;" imsuser;
        exit;
    fi
fi
